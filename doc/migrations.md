# Database migrations changelog

## CIMS
### 0007
* Run `./manage.py createinitialrevisions` after migration to create initial
  revisions for new models, *OR*
* Run a script along the lines of:
```python
import reversion
from cims.models import CourseInstance
with reversion.create_revision():
    django.contrib.admin.autodiscover()
    reversion.set_comment('Created to capture changes from database migration '
                          'cims_0007_auto_20170811_1108.')
    for ci in CourseInstance.objects.all():
        ci.save()
```
