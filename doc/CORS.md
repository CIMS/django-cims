# Notes about CORS configuration

## CORS settings
#### Access-Control-Allow-Origin:
**Current setting:** *
**django-cors-headers setting:** CORS\_ORIGIN\_ALLOW\_ALL = True 
API is currently public and isn't returning different data based on credentials
for models that should be being accessed cross-domain, so '\*' is appropriate.
May need to change in the future.

#### Access-Control-Allow-Credentials
**Current setting:** false
**django-cors-headers setting:** CORS\_ALLOW\_CREDENTIALS = False
Might need to set this due to interaction of logged in browser session with a
page from another domain running a script that gets data from CIMS.

## django-cors-headers settings

#### CORS\_URLS\_REGEX
**Current setting:** r'^/api/.\*$'
Cross site requests should only be addressed to the API.
