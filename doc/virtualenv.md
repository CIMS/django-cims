# Notes associated with correctly setting up the virtualenv for CIMS

### pip and setuptools>=34.0
Setuptools from 34.0 onward changed how it handles it's own required packages.
These changes require a modern pip to already be installed to handle them
gracefully, therefore the process is:
1. Upgrade pip, then
2. Upgrade setuptools

### django-auth-ldap and pyldap!=2.4.35
pyldap 2.4.35 has a regression bug that causes LDAP authentication to fail.
django-auth-ldap uses pyldap when running under python3 so check what is the
current version of pyldap and if necessary install a version other than 2.4.35
before django-auth-ldap
