# CECS CIMS API

## Standard model

## Non-standard models
### cims.models.Annotation
* These objects cannot be modified via the API, only viewed

### cims.auth.models.Role
* The Role model is only able to be viewed by users with add/change permissions

### cims.auth.models.User
* Anonymous users cannot view or perform any actions on this model
* There is currently no facility for users with a local password via the API
* Users lacking permission to change User objects can only view themselves
* Users with user edit permissions can view/change all users
* Users authenticating via LDAP can be added via a request to
  /user/set_ldap_user/ with a `username` argument. This will check the user
  exists in LDAP and if so, add them with an unusable password.
