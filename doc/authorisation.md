# Authentication and Authorisation in CECS CIMS

## Overview
Authentication in CIMS is handled by a combination of a limited number of local
accounts, and LDAP using UDS as the directory.

Authorisation is based on a hierarchical roles based system, with granularity
down to the field level.

## Authentication
For a limited number of local accounts e.g. default superuser, authentication
is performed against the database using the standard Django auth package.

For the majority of users authentication will be performed against UDS LDAP,
using *django-auth-ldap*.

By default LDAP users are created with an unusable password, which means that
authentication against the local database will fail. This is desirable as it
means authentication will depend on the state of the user's UDS account. This
also means that LDAP users must be prevented from changing their local
password, and instead be redirected to identity.anu.edu.au or the ServiceDesk.

### Migrating users between LDAP and Local auth
Django's default authentication system checks each authentication backend in
turn, and if none succeed then authentication fails. We take adavantage of this
when migrating from LDAP to local authentication and vice versa.

#### Local to LDAP
All that needs to happen here is that the user's password is set unusable.
There is a *set_unusable_password* User list action in the User Admin, and an
api endpoint */api/user/set_ldap_user/* that will accomplish this. Both require
permission to change user passwords.

#### LDAP to Local
In this case all that needs to happen is a usable password needs to be set for
the user in the local user database. Because LDAP users are normally prevented from
changing their password admin intervention is required. Options include:
* Having the user set their password while present with an admin
* Set a complex password for the user and then generate a reset email for them
* Generate a time limited token for them that allows them to change their
  password, ignoring the LDAP user restriction.

## Authorisation
Generally speaking, CIMS uses roles based access control: users are assigned to
roles either explicitly or through some relationship in the data model (e.g.
they are a class convener), and roles are given permission to access parts of
the system and perform operations.

This system is then extended to reflect inherited access based on the tree
structure of the core data model, and a basic status field based workflow.

### Users
Since roles are the primary mechanism for granting permissions it is intended
that permissions are granted to roles, and users are given roles rather than
permissions being allocated directly to users. Permissions should only be
granted directly to a user when an exception is required.

### Roles
Roles are the primary mechanism by which permissions are granted in the
system. Roles have optional inheritance. Roles can have arbitrarily many
children and parents, so role inheritance forms a graph. Membership in a role
is granted to users who are members of a child/included role. This has the
effect that permissions are inherited by any child or included role. The
distance of a user from a role is the number of graph edges between the role
and the closest role the user has direct membership of.

#### Operations
Relationships between roles are managed using standard ManyToMany field
operations.

```python
# Set child role
role1.child_roles.add(role2)

# Set parent role
role1.parent_roles.add(role2)
```

### Granularity
CIMS allows permissions to be granted at the class/model level (e.g. all
courses), or the object/instance level (e.g. a particular course). Within each
of these access can be further restricted to particular fields.

Since object permissions are enabled, making a permission check without
passing an object as the additional parameter is equivalent to asking *"Does
this user have this permission on at least one object."* It tells you nothing
about whether a user has the permission on a specific object, therefore when
checking whether an operation on a specific object is allowed the permission
check(s) **MUST** pass the object in question.

The field level permission implementation is provided by
_django-perfieldperms_. See the documentation for this package for details of
how to create per-field permissions for a particular model.

Per-field permissions are an extension to the standard Django Permission model
so are allocated the same way, using ManyToMany relationship operations e.g.
`.add() .set() .remove() .clear()`.

```python
user.user_permissions.add(perm1, perm2)
user.user_permissions.clear()

role.permissions.set([perm1, perm2, perm3])
role.permissions.remove(perm2)
```

### Access Resolution
Access resolution is implemented using the default Django authorisation
backends system. See
<https://docs.djangoproject.com/en/1.11/topics/auth/customizing/> for details
of how this system can be customised.

```python
# Check user has permission
user.has_perm(perm_string)

# Check user has permission for a specific object
user.has_perm(perm_string, obj=some_object)
```

Three backends are used in concert to control access.

#### CIMSBackend
CIMSBackend implements the checking of the combination of users and roles with
model/instance/field permissions to determine if a user has permission for a
requested action.

The combination of role inheritance and permissions grantable
at a combination of model, object and field level leads to fair degree of
complexity when resolving whether a user has sufficient acces to perform an
action. CIMS tries to take a least astonishment approach:
*   The more specific permission will override the more general (with one
    exception).
*   The closer permission (in terms of distance from the user in the role
    graph) will override the more distant.

In a bit more detail:
*   Object specific permissions are checked first, and if nothing relevant is
    found permissions pertaining to the whole model will apply.
*   Within the model and object permission checks, if any field specific
    permissions are found access will be restricted to those fields, otherwise
    access to all fields is granted.
*   For each of the object and model permission checks the algorithm is:
    1.  The user's roles are sorted in order of increasing distance.
    2.  Within a set of roles at the same distance from the user, permissions
        are additive. Field permissions will be ignored if a role in the set
        has access to all fields.
    3.  Permissions of the same type are additive between adjacent roles.
    4.  A field or model permission from a closer role will - respectively -
        override a model or field permission .
    5.  Any permissions granted explicitly to the user will override those
        granted to their set of roles.

#### ConditionalPermissionsMapBackend
The core data model of the CIMS application i.e. courses, course versions,
classes, is pretty much a tree. The combination of the data model with the
College organisational structure, and how courses are managed within the
College means there are a number of situations where whether a user is allowed
to perform an action on an object depends on whether they have some
relationship with another object. More concretely, a class convener should be
able to perform actions on objects which are related to her class(es), e.g. an
assessment task. Given e.g. a class object we should be able to work out it's
related objects, if this user is the convener of that class, and so what
(additional) permissions this user should be granted.

ConditionalPermissionsMapBackend enables these kinds of checks while attempting
to avoid storing authorisation information directly in the data model. It
avoids needing to set and update permissions on individual objects as they are
created and changed. It's basic idea is as follows:
*   A set of maps is maintained in the database which describe the
    relationships we want to check. This is the _ConditionalPermissionsMap_
    model
*   Each map is:
    *   The model we want to run the test against
    *   The test we want to run 
    *   A set of permissions granted for objects related to the test object if
        the test passes

The rough alogrithm is:
*   For each map:
    1.  Check if the combination of user and test object passes the test
    2.  Retrieve the list of permissions granted by this map.
    3.  Compare these permissions to the object and permission being requested

__Creating ConditionalPermissionsMaps__
This can be done through the Django Admin.

#### ObjectStatusPermissionsMapBackend
CECS CIMS currently implements a basic workflow model based on the value of a
status field. When the status field on an object is set to certain values a set
of permissions is denied to some users when they attempt to acces some set of
objects which are related to the one with the status field. E.g. when a
version of a course is submitted for approval its status is set to
"Submitted", this means the convener can no longer edit the details of the
version or some of its related objects like learning outcomes, but could
continue creating course offerings related to this version.

Note that ObjectStatusPermissionsMapBackend does not grant any permissions, it
serves only to deny permissions that might be granted later on. Permissions are
properly granted using other backends.

Similarly to the _ConditionalPermissionsMapBackend_ this is done using a set of
maps stored in the database. Each _ObjectStatusPermissionsMap_ comprises:
*   The Django _ContentType_ that has the status field we're interested in
*   The name of the status field
*   The set of status field values which will or won't trigger this check
*   The set of permissions which will be denied
*   The set of _ContentTypes_ with a relationship to the one with the status
    field which are also affected by this check
*   The set of users who can bypass this check
*   The set of roles which can bypass this check

The algorithm for ObjectStatusPermissionsMapBackend is:
* For each map:
    1.  Skip if the user is in the set of allowed users or roles
    2.  Check if the object we want to access is of the right ContentType or is
        in the set of related ContentTypes. If in the related set retrieve the
        related object with the status field
    3. Check the value of the status field on the object
    4. Check the denied permissions against the one being requested

__Creating ObjectStatusPermissionsMaps__
```python
map.ObjectStatusPermissionsMap.create(
    check_content_type=some_content_type,
    check_field_name='status',
    )
map.allowed_states.add(status_1, status_2)
map.denied_permissions.add(perm1, perm2, perm3)
map.related_content_types.add(related_ctype1, related_ctype2)
map.permitted_roles.set([role1, role2, role3])
map.permitted_users.set([user1, user2])
```
