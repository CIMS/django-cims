beautifulsoup4>=4.6.0
bleach
django-auth-ldap>=1.6.1
django-crispy-forms
django-filter
django-mptt>=0.9.1
django-perfieldperms
django-reversion>=3.0.0
djangorestframework>=3.8.0
drf-yasg
html5lib
psycopg2
requests
