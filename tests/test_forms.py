"""
Tests for CIMS forms.
"""
import unittest
from datetime import date
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.forms import modelform_factory
from django.test import TestCase

from cims import forms
from cims import models
from .helpers import get_perm
from .models import Pizza, PizzaBase, Topping

User = get_user_model()


class CourseInstanceImportFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')

    def test_no_user_fields_disabled(self):
        """
        If the user lacks permissions for an import checkbox field it is left disabled and set to
        False.
        """
        form = forms.CourseInstanceImportForm()
        for field_name in ['import_courserelatedrole', 'import_schedule', 'import_task']:
            with self.subTest(field_name=field_name):
                self.assertTrue(form.fields[field_name].disabled)
                self.assertFalse(form.fields[field_name].initial)

    def test_user_permission_field_enabled(self):
        """The import checkbox field is enabled if the user has sufficient permissions."""
        self.user.user_permissions.add(
                get_perm('cims', 'task', 'add_task'),
                get_perm('cims', 'task', 'delete_task'),
                )
        form = forms.CourseInstanceImportForm(user=self.user)
        field_name = 'import_task'
        self.assertFalse(form.fields[field_name].disabled)
        self.assertTrue(form.fields[field_name].initial)
        for field_name in ['import_courserelatedrole', 'import_schedule']:
            with self.subTest(field_name=field_name):
                self.assertTrue(form.fields[field_name].disabled)
                self.assertFalse(form.fields[field_name].initial)


class CourseInstanceFormTest(unittest.TestCase):

    def test_start_date_widget(self):
        """`start_date` field widget has input_type of `date`."""
        form = forms.CourseInstanceForm()
        self.assertEqual(form.base_fields['start_date'].widget.input_type, 'date')

    def test_end_date(self):
        """`end_date` field widget has input_type of `date`."""
        form = forms.CourseInstanceForm()
        self.assertEqual(form.base_fields['end_date'].widget.input_type, 'date')


class CourseVersionFormTest(unittest.TestCase):

    def test_approval_date_widget(self):
        """`approval_date` field widget has input_type of `date`."""
        form = forms.CourseVersionForm()
        self.assertEqual(form.base_fields['approval_date'].widget.input_type, 'date')


class TaskFormTest(unittest.TestCase):

    def test_due_date_widget(self):
        """`due_date` field widget has input_type of `date`."""
        form = forms.TaskForm()
        self.assertEqual(form.base_fields['due_date'].widget.input_type, 'date')

    def test_due_time(self):
        """`due_time` field widget has input_type of `time`."""
        form = forms.TaskForm()
        self.assertEqual(form.base_fields['due_time'].widget.input_type, 'time')


class TestValidatedModelForm(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.cheese = Topping.objects.validated_create(name='Cheese')
        cls.mushrooms = Topping.objects.validated_create(name='Mushrooms')
        cls.capsicums = Topping.objects.validated_create(name='Capsicums')
        cls.salami = Topping.objects.validated_create(name='salami')
        cls.traditional = PizzaBase.objects.validated_create(name='Traditional')
        cls.dc = PizzaBase.objects.validated_create(name='Deep Crust')

    def test_form_validation(self):
        PizzaForm = modelform_factory(Pizza, form=forms.ValidatedModelForm, fields='__all__')
        form = PizzaForm(
                {
                    'name': 'Proper Pizza',
                    'price': '10.95',
                    'toppings': [self.salami, self.capsicums, self.mushrooms, self.cheese],
                    'bases': [self.traditional, self.dc],
                    }
                )
        self.assertRaisesMessage(
                AssertionError,
                "ValidatedModelForm doesn't support delayed commit saves.",
                **{'commit': False}
                )
        self.assertFalse(Pizza.objects.all().exists())
        # Compliant pizza passes
        instance = form.save()
        self.assertTrue(Pizza.objects.filter(id=instance.id).exists())
        self.assertEqual('Proper Pizza', instance.name)
        self.assertEqual(Decimal('10.95'), instance.price)
        self.assertSetEqual(
                {self.salami, self.capsicums, self.mushrooms, self.cheese},
                set(instance.toppings.all()),
                )
        self.assertSetEqual(
                {self.traditional, self.dc},
                set(instance.bases.all()),
                )
        # Extended validation makes entry in form.errors, doesn't create instance
        form = PizzaForm(
                {
                    'name': 'Tasteless Pizza',
                    'price': '8.95',
                    'toppings': [self.mushrooms, self.cheese],
                    'bases': [self.traditional, self.dc],
                    }
                )
        instance = form.save()
        self.assertIn(
                ('Pizzas with fewer than three toppings are tasteless, those with greater than 7 '
                 'are overcomplicated mush.'),
                form.errors['__all__'],
                )
        self.assertFalse(Pizza.objects.filter(id=instance.id).exists())


class SetWeightedLOToSOFormQuerysetsTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Objective relationships
        cls.std_body = models.StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        cls.std_fwork1 = models.StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 1',
                )
        cls.node1 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 1',
                short_name='N1',
                )
        models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 2',
                short_name='N2',
                )
        # Weighting relationship
        cls.scheme1 = models.WeightingScheme.objects.validated_create(name='Valid Scheme')
        cls.scheme2 = models.WeightingScheme.objects.validated_create(name='Invalid Scheme')
        cls.weight1 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='High'
                )
        cls.weight2 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='Low'
                )
        models.Weight.objects.validated_create(
                weightingscheme=cls.scheme2,
                value='Middle'
                )
        # Course objects
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                standards_frameworks=[cls.std_fwork1],
                weightingschemes=[cls.scheme1]
                )
        cls.cv1.objectives.add(cls.node1)
        cv2 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version 2',
                status=status,
                standards_frameworks=[cls.std_fwork1],
                )
        cv2.objectives.add(cls.node1)
        cls.lo1 = models.LearningOutcome.objects.validated_create(
                courseversion=cls.cv1,
                outcome='Something good.',
                )
        models.LearningOutcome.objects.validated_create(
                courseversion=cv2,
                outcome='Is gonna happen.',
                )

    def test_unsaved_courseversion(self):
        """
        If the CourseVersion passed to the factory function hasn't been saved the form field
        querysets are set to none().
        """
        Form = modelform_factory(models.WeightedLOToSO, fields='__all__')
        forms.set_form_querysets(Form, Form.base_fields.keys(), **{'courseversion': None})
        form = Form()
        self.assertFalse(form.fields['learningoutcome'].queryset)
        self.assertFalse(form.fields['standardobjective'].queryset)
        self.assertFalse(form.fields['weights'].queryset)

    def test_form_field_filtering(self):
        """Form field querysets are filtered according to the CourseVersion passed in."""
        Form = modelform_factory(models.WeightedLOToSO, fields='__all__')
        forms.set_form_querysets(Form, Form.base_fields.keys(), **{'courseversion': self.cv1})
        form = Form()
        self.assertListEqual([self.lo1], list(form.fields['learningoutcome'].queryset))
        self.assertListEqual([self.node1], list(form.fields['standardobjective'].queryset))
        self.assertListEqual(
                [self.weight1, self.weight2],
                list(form.fields['weights'].queryset.order_by('value'))
                )


class SetWeightedTaskToLOFormQuerysetsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Weighting relationship
        cls.scheme1 = models.WeightingScheme.objects.validated_create(name='Valid Scheme')
        cls.scheme2 = models.WeightingScheme.objects.validated_create(name='Invalid Scheme')
        cls.weight1 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='High'
                )
        cls.weight2 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='Low'
                )
        models.Weight.objects.validated_create(
                weightingscheme=cls.scheme2,
                value='Middle'
                )
        # Course objects
        school = models.OrganisationalUnit.objects.validated_create(
                name='Foo School',
                short_name='FS'
                )
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                weightingschemes=[cls.scheme1]
                )
        cv2 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version 2',
                status=status,
                )
        cls.lo1 = models.LearningOutcome.objects.validated_create(
                courseversion=cls.cv1,
                outcome='Something good.',
                )
        models.LearningOutcome.objects.validated_create(
                courseversion=cv2,
                outcome='Is gonna happen.',
                )
        cls.ci = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test instance',
                start_date=date.today(),
                end_date=date.today(),
                taught_by=[school],
                )
        ci2 = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Another test instance',
                start_date=date.today(),
                end_date=date.today(),
                taught_by=[school],
                )
        cls.task = models.Task.objects.validated_create(
                courseinstance=cls.ci,
                title='The correct task.'
                )
        models.Task.objects.validated_create(
                courseinstance=ci2,
                title='The incorrect task.'
                )

    def test_unsaved_courseversion(self):
        """
        If the CourseInstance passed to the function hasn't been saved the form field
        querysets are set to none().
        """
        Form = modelform_factory(models.WeightedTaskToLO, fields='__all__')
        forms.set_form_querysets(Form, Form.base_fields.keys(), **{'courseinstance': None})
        form = Form()
        self.assertFalse(form.fields['learningoutcome'].queryset)
        self.assertFalse(form.fields['task'].queryset)
        self.assertFalse(form.fields['weights'].queryset)

    def test_form_field_filtering(self):
        """Form field querysets are filtered according to the CourseInstance passed in."""
        Form = modelform_factory(models.WeightedTaskToLO, fields='__all__')
        forms.set_form_querysets(Form, Form.base_fields.keys(), **{'courseinstance': self.ci})
        form = Form()
        self.assertListEqual([self.lo1], list(form.fields['learningoutcome'].queryset))
        self.assertListEqual([self.task], list(form.fields['task'].queryset))
        self.assertListEqual(
                [self.weight1, self.weight2],
                list(form.fields['weights'].queryset.order_by('value'))
                )
