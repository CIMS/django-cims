"""
Tests for checking data migrations behave as expected.
"""
from datetime import date

from django.apps import apps as django_apps
from django.db import connection
from django.db.migrations.executor import MigrationExecutor
from django.test import TestCase


class TestMigrations(TestCase):
    """
    Lifted from https://www.caktusgroup.com/blog/2016/02/02/writing-unit-tests-django-migrations/
    """
    migrate_from = None
    migrate_to = None

    @property
    def app(self):
        return django_apps.get_containing_app_config(type(self).__module__).name

    def setUp(self):
        assert self.migrate_from and self.migrate_to, (
                "TestCase '{}' must define migrate_from and migrate_to properties".format(
                    type(self).__name__
                    )
                )
        self.migrate_from = [(self.app, self.migrate_from)]
        self.migrate_to = [(self.app, self.migrate_to)]
        executor = MigrationExecutor(connection)

        # Check these migrations exist in the migration graph and haven't been squashed out.
        try:
            executor.loader.get_migration(*self.migrate_from[0])
            executor.loader.get_migration(*self.migrate_to[0])
        except KeyError:
            self.skipTest('Skipping tests for squashed migration.')

        old_apps = executor.loader.project_state(self.migrate_from).apps

        # Reverse to the original migration
        executor.migrate(self.migrate_from)

        self.setUpBeforeMigration(old_apps)

        # Run the migration to test
        executor = MigrationExecutor(connection)
        executor.loader.build_graph()  # reload.
        executor.migrate(self.migrate_to)

        self.apps = executor.loader.project_state(self.migrate_to).apps

    def setUpBeforeMigration(self, apps):
        pass


class Migration0022ForwardsTestCase(TestMigrations):
    app = 'cims'
    migrate_from = '0021_auto_20171207_1138'
    migrate_to = '0022_auto_20180314_1217'

    def setUpBeforeMigration(self, apps):
        ReviewStatus = apps.get_model('cims', 'ReviewStatus')
        Course = apps.get_model('cims', 'Course')
        CourseVersion = apps.get_model('cims', 'CourseVersion')
        CourseInstance = apps.get_model('cims', 'CourseInstance')
        status = ReviewStatus.objects.create(name='Stately')
        course = Course.objects.create(code='TEST1001')
        cv1 = CourseVersion.objects.create(
                course=course,
                version_descriptor='Test CV 1',
                status=status,
                workload='A bit of work.',
                prescribed_texts='Moby Dick'
                )
        CourseInstance.objects.create(
                courseversion=cv1,
                instance_descriptor='Test instance 1',
                start_date=date.today(),
                end_date=date.today(),
                )

    def test_tags_migrated(self):
        CourseInstance = self.apps.get_model('cims', 'CourseInstance')
        ci1 = CourseInstance.objects.get(instance_descriptor='Test instance 1')

        self.assertEqual(ci1.workload, 'A bit of work.')
        self.assertEqual(ci1.prescribed_texts, 'Moby Dick')


class Migration0022BackwardsTestCase(TestMigrations):
    app = 'cims'
    migrate_from = '0022_auto_20180314_1217'
    migrate_to = '0021_auto_20171207_1138'

    def setUpBeforeMigration(self, apps):
        ReviewStatus = apps.get_model('cims', 'ReviewStatus')
        Course = apps.get_model('cims', 'Course')
        CourseVersion = apps.get_model('cims', 'CourseVersion')
        CourseInstance = apps.get_model('cims', 'CourseInstance')
        status = ReviewStatus.objects.create(name='Stately')
        course = Course.objects.create(code='TEST1001')
        cv1 = CourseVersion.objects.create(
                course=course,
                version_descriptor='Test CV 1',
                status=status,
                )
        cv2 = CourseVersion.objects.create(
                course=course,
                version_descriptor='Test CV 2',
                status=status,
                )
        CourseInstance.objects.create(
                courseversion=cv1,
                instance_descriptor='Test instance 1',
                start_date=date.today(),
                end_date=date.today(),
                workload='A bit of work.',
                prescribed_texts='Moby Dick'
                )
        CourseInstance.objects.create(
                courseversion=cv2,
                instance_descriptor='Test instance 2',
                start_date=date.today(),
                end_date=date.today(),
                workload='A lot of work.',
                prescribed_texts='Hard Times'
                )
        CourseInstance.objects.create(
                courseversion=cv2,
                instance_descriptor='Test instance 3',
                start_date=date.today(),
                end_date=date.today(),
                workload='A lot of work.',
                prescribed_texts='Hard Times'
                )
        CourseInstance.objects.create(
                courseversion=cv2,
                instance_descriptor='Test instance 4',
                start_date=date.today(),
                end_date=date.today(),
                workload='A lot of work, quickly.',
                prescribed_texts='Oliver Twisty'
                )

    def test_tags_migrated(self):
        CourseVersion = self.apps.get_model('cims', 'CourseVersion')
        cv1 = CourseVersion.objects.get(version_descriptor='Test CV 1')
        cv2 = CourseVersion.objects.get(version_descriptor='Test CV 2')

        self.assertEqual(cv1.workload, 'A bit of work.')
        self.assertEqual(cv1.prescribed_texts, 'Moby Dick')
        self.assertEqual(cv2.workload, 'A lot of work, quickly. A lot of work.')
        self.assertEqual(cv2.prescribed_texts, 'Hard Times; Oliver Twisty')


class Migration0028NoWeightsForwardsTestCase(TestMigrations):
    app = 'cims'
    migrate_from = '0027_learningoutcome_outcome_short_form'
    migrate_to = '0028_auto_20180523_1403'

    def setUpBeforeMigration(self, apps):
        ReviewStatus = apps.get_model('cims', 'ReviewStatus')
        Course = apps.get_model('cims', 'Course')
        CourseVersion = apps.get_model('cims', 'CourseVersion')
        CourseInstance = apps.get_model('cims', 'CourseInstance')
        LearningOutcome = apps.get_model('cims', 'LearningOutcome')
        StandardsBody = apps.get_model('cims', 'StandardsBody')
        StandardsFramework = apps.get_model('cims', 'StandardsFramework')
        StandardObjective = apps.get_model('cims', 'StandardObjective')
        Task = apps.get_model('cims', 'Task')
        Weighted_LO_To_SO = apps.get_model('cims', 'Weighted_LO_To_SO')
        Weighted_Task_To_LO = apps.get_model('cims', 'Weighted_Task_To_LO')
        # Objective relationships
        std_body = StandardsBody.objects.create(name='Engineers Australia', short_name='EA')
        std_fwork1 = StandardsFramework.objects.create(standardsbody=std_body, name='Stage 1')
        node1 = StandardObjective.objects.create(
                standardsframework=std_fwork1,
                name='Node 1',
                lft=0, rght=0, level=0, tree_id=0
                )
        # Course objects
        status = ReviewStatus.objects.create(name='Stately')
        course = Course.objects.create(code='TEST1001')
        cv1 = CourseVersion.objects.create(
                course=course,
                version_descriptor='Test CV 1',
                status=status,
                )
        lo1 = LearningOutcome.objects.create(courseversion=cv1, outcome='Awesome stuff.')
        lo2 = LearningOutcome.objects.create(courseversion=cv1, outcome='Mildly amusing stuff.')
        ci = CourseInstance.objects.create(
                courseversion=cv1,
                instance_descriptor='Test instance 1',
                start_date=date.today(),
                end_date=date.today(),
                )
        task = Task.objects.create(courseinstance=ci, title='Onerous task')
        Weighted_LO_To_SO.objects.create(learningoutcome=lo1, standardobjective=node1)
        Weighted_LO_To_SO.objects.create(learningoutcome=lo2, standardobjective=node1)
        Weighted_Task_To_LO.objects.create(task=task, learningoutcome=lo1)
        Weighted_Task_To_LO.objects.create(task=task, learningoutcome=lo2)

    def test_weighted_through_models_migrated(self):
        """Confirm new Weighted... objects were created for each of the old ones."""
        LearningOutcome = self.apps.get_model('cims', 'LearningOutcome')
        StandardObjective = self.apps.get_model('cims', 'StandardObjective')
        WeightedLOToSO = self.apps.get_model('cims', 'WeightedLOToSO')
        lo1 = LearningOutcome.objects.get(outcome='Awesome stuff.')
        lo2 = LearningOutcome.objects.get(outcome='Mildly amusing stuff.')
        node1 = StandardObjective.objects.get(name='Node 1')
        self.assertEqual(2, WeightedLOToSO.objects.count())
        self.assertEqual(
                1,
                WeightedLOToSO.objects.filter(
                        learningoutcome=lo1,
                        standardobjective=node1,
                        weight__isnull=True
                        ).count()
                )
        self.assertEqual(
                1,
                WeightedLOToSO.objects.filter(
                        learningoutcome=lo2,
                        standardobjective=node1,
                        weight__isnull=True
                        ).count()
                )
        Task = self.apps.get_model('cims', 'Task')
        WeightedTaskToLO = self.apps.get_model('cims', 'WeightedTaskToLO')
        task = Task.objects.get(title='Onerous task')
        self.assertEqual(2, WeightedTaskToLO.objects.count())
        self.assertEqual(
                1,
                WeightedTaskToLO.objects.filter(
                        task=task,
                        learningoutcome=lo1,
                        weight__isnull=True
                        ).count()
                )
        self.assertEqual(
                1,
                WeightedTaskToLO.objects.filter(
                        task=task,
                        learningoutcome=lo2,
                        weight__isnull=True
                        ).count()
                )


class Migration0028ForwardsTestCase(TestMigrations):
    app = 'cims'
    migrate_from = '0027_learningoutcome_outcome_short_form'
    migrate_to = '0028_auto_20180523_1403'

    def setUpBeforeMigration(self, apps):
        ReviewStatus = apps.get_model('cims', 'ReviewStatus')
        Course = apps.get_model('cims', 'Course')
        CourseVersion = apps.get_model('cims', 'CourseVersion')
        CourseInstance = apps.get_model('cims', 'CourseInstance')
        LearningOutcome = apps.get_model('cims', 'LearningOutcome')
        StandardsBody = apps.get_model('cims', 'StandardsBody')
        StandardsFramework = apps.get_model('cims', 'StandardsFramework')
        StandardObjective = apps.get_model('cims', 'StandardObjective')
        Task = apps.get_model('cims', 'Task')
        Weighted_LO_To_SO = apps.get_model('cims', 'Weighted_LO_To_SO')
        Weighted_Task_To_LO = apps.get_model('cims', 'Weighted_Task_To_LO')
        WeightingScheme = apps.get_model('cims', 'WeightingScheme')
        Weight = apps.get_model('cims', 'Weight')
        # Objective relationships
        std_body = StandardsBody.objects.create(name='Engineers Australia', short_name='EA')
        std_fwork1 = StandardsFramework.objects.create(standardsbody=std_body, name='Stage 1')
        node1 = StandardObjective.objects.create(
                standardsframework=std_fwork1,
                name='Node 1',
                lft=0, rght=0, level=0, tree_id=0
                )
        # Course objects
        status = ReviewStatus.objects.create(name='Stately')
        course = Course.objects.create(code='TEST1001')
        cv1 = CourseVersion.objects.create(
                course=course,
                version_descriptor='Test CV 1',
                status=status,
                )
        lo1 = LearningOutcome.objects.create(courseversion=cv1, outcome='Awesome stuff.')
        lo2 = LearningOutcome.objects.create(courseversion=cv1, outcome='Mildly amusing stuff.')
        lo3 = LearningOutcome.objects.create(courseversion=cv1, outcome='Great stuff.')
        LearningOutcome.objects.create(courseversion=cv1, outcome='Terrible stuff.')
        ci = CourseInstance.objects.create(
                courseversion=cv1,
                instance_descriptor='Test instance 1',
                start_date=date.today(),
                end_date=date.today(),
                )
        task1 = Task.objects.create(courseinstance=ci, title='Onerous task')
        task2 = Task.objects.create(courseinstance=ci, title='Fun task')
        task3 = Task.objects.create(courseinstance=ci, title='Difficult task')
        Task.objects.create(courseinstance=ci, title='Unfun task')
        wscheme1 = WeightingScheme.objects.create(name='Test Weighting Scheme 1')
        wscheme2 = WeightingScheme.objects.create(name='Test Weighting Scheme 2')
        weight1 = Weight.objects.create(weightingscheme=wscheme1, value='Low')
        weight2 = Weight.objects.create(weightingscheme=wscheme2, value='Middle')
        Weighted_LO_To_SO.objects.create(learningoutcome=lo1, standardobjective=node1)
        Weighted_LO_To_SO.objects.create(
                learningoutcome=lo2,
                standardobjective=node1,
                weight=weight1
                )
        Weighted_LO_To_SO.objects.create(
                learningoutcome=lo2,
                standardobjective=node1,
                weight=weight2
                )
        Weighted_LO_To_SO.objects.create(
                learningoutcome=lo3,
                standardobjective=node1,
                weight=weight2
                )
        Weighted_Task_To_LO.objects.create(task=task1, learningoutcome=lo1)
        Weighted_Task_To_LO.objects.create(
                task=task2,
                learningoutcome=lo2,
                weight=weight1,
                )
        Weighted_Task_To_LO.objects.create(
                task=task2,
                learningoutcome=lo2,
                weight=weight2,
                )
        Weighted_Task_To_LO.objects.create(
                task=task3,
                learningoutcome=lo3,
                weight=weight1,
                )

    def test_weighted_through_models_migrated(self):
        """
        Confirm new Weighted... objects were created for each of the old ones, consolidating
        links with multiple weights into a single instance.
        """
        LearningOutcome = self.apps.get_model('cims', 'LearningOutcome')
        StandardObjective = self.apps.get_model('cims', 'StandardObjective')
        Weight = self.apps.get_model('cims', 'Weight')
        WeightedLOToSO = self.apps.get_model('cims', 'WeightedLOToSO')
        lo1 = LearningOutcome.objects.get(outcome='Awesome stuff.')
        lo2 = LearningOutcome.objects.get(outcome='Mildly amusing stuff.')
        lo3 = LearningOutcome.objects.get(outcome='Great stuff.')
        node1 = StandardObjective.objects.get(name='Node 1')
        weight1 = Weight.objects.get(value='Low')
        weight2 = Weight.objects.get(value='Middle')
        self.assertEqual(3, WeightedLOToSO.objects.count())
        self.assertEqual(
                1,
                WeightedLOToSO.objects.filter(
                        learningoutcome=lo1,
                        standardobjective=node1,
                        weight__isnull=True
                        ).count()
                )
        weightedtasktolo = WeightedLOToSO.objects.get(learningoutcome=lo2, standardobjective=node1)
        self. assertQuerysetEqual(
                weightedtasktolo.weight.order_by('value'),
                [repr(weight1), repr(weight2)]
                )
        weightedtasktolo = WeightedLOToSO.objects.get(learningoutcome=lo3, standardobjective=node1)
        self. assertQuerysetEqual(weightedtasktolo.weight.order_by('value'), [repr(weight1)])

        Task = self.apps.get_model('cims', 'Task')
        WeightedTaskToLO = self.apps.get_model('cims', 'WeightedTaskToLO')
        task1 = Task.objects.get(title='Onerous task')
        task2 = Task.objects.get(title='Fun task')
        task3 = Task.objects.get(title='Difficult task')
        self.assertEqual(3, WeightedTaskToLO.objects.count())
        self.assertEqual(
                1,
                WeightedTaskToLO.objects.filter(
                        task=task1,
                        learningoutcome=lo1,
                        weight__isnull=True
                        ).count()
                )
        weightedtasktolo = WeightedTaskToLO.objects.get(task=task2, learningoutcome=lo2)
        self. assertQuerysetEqual(
                weightedtasktolo.weight.order_by('value'),
                [repr(weight1), repr(weight2)]
                )
        weightedtasktolo = WeightedTaskToLO.objects.get(task=task3, learningoutcome=lo3)
        self. assertQuerysetEqual(weightedtasktolo.weight.order_by('value'), [repr(weight1)])


class Migration0028BackwardsTestCase(TestMigrations):
    app = 'cims'
    migrate_from= '0028_auto_20180523_1403'
    migrate_to = '0027_learningoutcome_outcome_short_form'

    def setUpBeforeMigration(self, apps):
        ReviewStatus = apps.get_model('cims', 'ReviewStatus')
        Course = apps.get_model('cims', 'Course')
        CourseVersion = apps.get_model('cims', 'CourseVersion')
        CourseInstance = apps.get_model('cims', 'CourseInstance')
        LearningOutcome = apps.get_model('cims', 'LearningOutcome')
        StandardsBody = apps.get_model('cims', 'StandardsBody')
        StandardsFramework = apps.get_model('cims', 'StandardsFramework')
        StandardObjective = apps.get_model('cims', 'StandardObjective')
        Task = apps.get_model('cims', 'Task')
        WeightedLOToSO = apps.get_model('cims', 'WeightedLOToSO')
        WeightedTaskToLO = apps.get_model('cims', 'WeightedTaskToLO')
        WeightingScheme = apps.get_model('cims', 'WeightingScheme')
        Weight = apps.get_model('cims', 'Weight')
        # Objective relationships
        std_body = StandardsBody.objects.create(name='Engineers Australia', short_name='EA')
        std_fwork1 = StandardsFramework.objects.create(standardsbody=std_body, name='Stage 1')
        node1 = StandardObjective.objects.create(
                standardsframework=std_fwork1,
                name='Node 1',
                lft=0, rght=0, level=0, tree_id=0
                )
        # Course objects
        status = ReviewStatus.objects.create(name='Stately')
        course = Course.objects.create(code='TEST1001')
        cv1 = CourseVersion.objects.create(
                course=course,
                version_descriptor='Test CV 1',
                status=status,
                )
        lo1 = LearningOutcome.objects.create(courseversion=cv1, outcome='Awesome stuff.')
        lo2 = LearningOutcome.objects.create(courseversion=cv1, outcome='Mildly amusing stuff.')
        ci = CourseInstance.objects.create(
                courseversion=cv1,
                instance_descriptor='Test instance 1',
                start_date=date.today(),
                end_date=date.today(),
                )
        task1 = Task.objects.create(courseinstance=ci, title='Onerous task')
        task2 = Task.objects.create(courseinstance=ci, title='Fun task')
        wscheme1 = WeightingScheme.objects.create(name='Test Weighting Scheme 1')
        wscheme2 = WeightingScheme.objects.create(name='Test Weighting Scheme 2')
        weight1 = Weight.objects.create(weightingscheme=wscheme1, value='Low')
        weight2 = Weight.objects.create(weightingscheme=wscheme2, value='Middle')
        WeightedLOToSO.objects.create(learningoutcome=lo1, standardobjective=node1)
        weightedlotoso = WeightedLOToSO.objects.create(
                learningoutcome=lo2,
                standardobjective=node1,
                )
        weightedlotoso.weight.add(weight1, weight2)
        WeightedTaskToLO.objects.create(task=task1, learningoutcome=lo1)
        weightedtasktolo = WeightedTaskToLO.objects.create(
                task=task2,
                learningoutcome=lo2,
                )
        weightedtasktolo.weight.add(weight1, weight2)

    def test_weighted_through_models_migrated(self):
        """
        Confirm new Weighted... objects were created for each of the old ones, consolidating
        links with multiple weights into a single instance.
        """
        LearningOutcome = self.apps.get_model('cims', 'LearningOutcome')
        StandardObjective = self.apps.get_model('cims', 'StandardObjective')
        Weight = self.apps.get_model('cims', 'Weight')
        Weighted_LO_To_SO = self.apps.get_model('cims', 'Weighted_LO_To_SO')
        lo1 = LearningOutcome.objects.get(outcome='Awesome stuff.')
        lo2 = LearningOutcome.objects.get(outcome='Mildly amusing stuff.')
        node1 = StandardObjective.objects.get(name='Node 1')
        weight1 = Weight.objects.get(value='Low')
        weight2 = Weight.objects.get(value='Middle')
        self.assertEqual(3, Weighted_LO_To_SO.objects.count())
        self.assertEqual(
                1,
                Weighted_LO_To_SO.objects.filter(
                        learningoutcome=lo1,
                        standardobjective=node1,
                        weight__isnull=True
                        ).count()
                )
        self.assertEqual(
                1,
                Weighted_LO_To_SO.objects.filter(
                        learningoutcome=lo2,
                        standardobjective=node1,
                        weight=weight1
                        ).count()
                )
        self.assertEqual(
                1,
                Weighted_LO_To_SO.objects.filter(
                        learningoutcome=lo2,
                        standardobjective=node1,
                        weight=weight2
                        ).count()
                )

        Task = self.apps.get_model('cims', 'Task')
        Weighted_Task_To_LO = self.apps.get_model('cims', 'Weighted_Task_To_LO')
        task1 = Task.objects.get(title='Onerous task')
        task2 = Task.objects.get(title='Fun task')
        self.assertEqual(3, Weighted_Task_To_LO.objects.count())
        self.assertEqual(
                1,
                Weighted_Task_To_LO.objects.filter(
                        task=task1,
                        learningoutcome=lo1,
                        weight__isnull=True
                        ).count()
                )
        self.assertEqual(
                1,
                Weighted_Task_To_LO.objects.filter(
                        task=task2,
                        learningoutcome=lo2,
                        weight=weight1
                        ).count()
                )
        self.assertEqual(
                1,
                Weighted_Task_To_LO.objects.filter(
                        task=task2,
                        learningoutcome=lo2,
                        weight=weight2
                        ).count()
                )


class Migration0036ForwardsTestCase(TestMigrations):
    app = 'cims'
    migrate_from = '0035_auto_20180801_1321'
    migrate_to = '0036_auto_20180801_1433'

    def setUpBeforeMigration(self, apps):
        ReviewStatus = apps.get_model('cims', 'ReviewStatus')
        Course = apps.get_model('cims', 'Course')
        CourseVersion = apps.get_model('cims', 'CourseVersion')
        CourseInstance = apps.get_model('cims', 'CourseInstance')
        status = ReviewStatus.objects.create(name='Stately')
        course = Course.objects.create(code='TEST1001')
        cv1 = CourseVersion.objects.create(
                course=course,
                version_descriptor='Test CV 1',
                status=status,
                )
        CourseInstance.objects.create(
                courseversion=cv1,
                instance_descriptor='Test instance 1',
                start_date=date.today(),
                end_date=date.today(),
                )
        CourseInstance.objects.create(
                courseversion=cv1,
                instance_descriptor='Test instance 2 - Imported from Test instance 1',
                start_date=date.today(),
                end_date=date.today(),
                )

    def test_descriptor_modified(self):
        CourseInstance = self.apps.get_model('cims', 'CourseInstance')
        self.assertTrue(
                isinstance(
                        CourseInstance.objects.get(instance_descriptor='Test instance 2'),
                        CourseInstance
                        )
                )
