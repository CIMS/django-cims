from django.apps import AppConfig


class TestsApiConfig(AppConfig):
    name = 'tests.api'
    label = 'tests_api'
