"""
Rest framework serializers for testing the CIMS API.
"""
from rest_framework import serializers

from cims.api.fields import BleachField
from cims.api.serializers import (CIMSBaseModelSerializer, FilteredFieldsModelSerializer,
                                  ValidatedModelSerializer)
from tests import models
from .models import LogTester, VersionTester


class BasicModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BasicModel
        fields = '__all__'


class LogTesterSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = LogTester
        fields = ('id', 'name', 'short_name', 'description', 'yaf')


class VersionTesterSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = VersionTester
        fields = ('id', 'name', 'short_name', 'description', 'yaf')


class PizzaSerializer(ValidatedModelSerializer):
    class Meta:
        model = models.Pizza
        fields = ('id', 'name', 'price', 'toppings', 'bases')


class NonValidatingModelSerializer(ValidatedModelSerializer):
    class Meta:
        model = models.BasicModel
        fields = ('id', 'name')


# Bleachfield test serializer
class PeroxideBlondeSerializer(ValidatedModelSerializer):
    dirty_text = BleachField()

    class Meta:
        model = models.PeroxideBlonde
        fields = ('id', 'dirty_text')


# ForeignKey Models
class AuthorSerializer(ValidatedModelSerializer):
    class Meta:
        model = models.Author
        fields = ('name', 'book_set')


class BookSerializer(ValidatedModelSerializer):
    class Meta:
        model = models.Book
        fields = ('author', 'title', 'pages')


# Field filtering serializers
class FilteredPizzaSerializer(FilteredFieldsModelSerializer):
    class Meta:
        model = models.Pizza
        fields = ('id', 'created', 'last_modified', 'name', 'price', 'toppings', 'bases')


class ToppinglessFilteredPizzaSerializer(FilteredFieldsModelSerializer):
    class Meta:
        model = models.Pizza
        fields = ('id', 'created', 'last_modified', 'name', 'price', 'bases')
