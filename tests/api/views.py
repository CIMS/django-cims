"""
Views for testing the CIMS API.
"""
from rest_framework.viewsets import GenericViewSet

from cims.api.mixins import (LoggingCreateModelMixin, LoggingUpdateModelMixin, PFPModelMixin,
                             VersionedCreateModelMixin, VersionedUpdateModelMixin)
from tests.models import Author, Book
from .models import LogTester, VersionTester
from .serializers import (AuthorSerializer, BookSerializer, LogTesterSerializer,
                          VersionTesterSerializer)


class AuthorViewSet(VersionedCreateModelMixin, VersionedUpdateModelMixin, GenericViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class BookViewSet(VersionedCreateModelMixin, VersionedUpdateModelMixin, GenericViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class LogTesterViewSet(LoggingCreateModelMixin, LoggingUpdateModelMixin, GenericViewSet):
    queryset = LogTester.objects.all()
    serializer_class = LogTesterSerializer


class PFPTesterViewSet(PFPModelMixin, GenericViewSet):
    queryset = LogTester.objects.all()
    serializer_class = LogTesterSerializer


class VersionTesterViewSet(VersionedCreateModelMixin, VersionedUpdateModelMixin, GenericViewSet):
    queryset = VersionTester.objects.all()
    serializer_class = VersionTesterSerializer
