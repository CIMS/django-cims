"""
Tests for API serializers.
"""
from django.test import TestCase
from bs4 import BeautifulSoup

from .serializers import PeroxideBlondeSerializer


class TestBleachField(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_bleach_appliation(self):
        """Confirm wanted tags are preserved, unwanted are escaped."""
        data = {
                'dirty_text': ('<div>'
                               '<blockquote>This is allowed</blockquote>'
                               '<a href="https://google.com.au/">As is this</a>'
                               '<script type="text/javascript">'
                               'window.alert("But not this.")</script>'
                               '</div>'),
                }
        blondie = PeroxideBlondeSerializer(data=data)
        self.assertTrue(blondie.is_valid())
        obj = blondie.save()
        soup = BeautifulSoup(obj.dirty_text, 'html.parser')
        self.assertIsNone(soup.find('div'))
        self.assertIsNone(soup.find('script'))
        self.assertTrue(soup.find('a', href='https://google.com.au/'))
        self.assertTrue(soup.find('blockquote'))
