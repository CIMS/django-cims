"""
Smoke tests of basic functionality of data model viewsets. Test for Create/List/Retrieve/Update
functionality. Check anon users can't create or update.
"""
from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from perfieldperms.utils import get_usable_model_fields
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from cims.models import (AcademicCareer, Course, CourseClassification, CourseInstance,
                         CourseRelatedRole, CourseRelatedRoleTitle, CourseVersion, DeliveryMode,
                         LearningOutcome, OrganisationalUnit, ReviewStatus, Schedule,
                         StandardObjective, StandardsBody, StandardsFramework, Task,
                         TeachingSession, Weight, WeightedLOToSO, WeightedTaskToLO, WeightingScheme)

User = get_user_model()


# Model based viewsets
@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        )
class TestStandardsModels(TestCase):
    """Tests of standards related models."""
    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(
                username='superuser',
                password='sekret',
                email='superuser@example.com',
                )

    def setUp(self):
        self.client = APIClient()

    def test_superuser(self):
        """Check superuser ."""
        self.client.force_authenticate(user=self.superuser)
        # StandardsBody
        # Create
        response = self.client.post(
                reverse('standardsbody-list'),
                {'name': 'Test Standards Body', 'short_name': 'TSBody'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(StandardsBody.objects.count(), 1)
        std_body = StandardsBody.objects.get(short_name='TSBody')
        # List
        response = self.client.get(reverse('standardsbody-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('standardsbody-detail', args=[std_body.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(StandardsBody):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('standardsbody-detail', args=[std_body.pk]),
                {'name': 'Test Standards Body', 'short_name': 'TSB'},
                )
        std_body.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(std_body.short_name, 'TSB')
        # Delete
        response = self.client.delete(reverse('standardsbody-detail', args=[std_body.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # StandardsFramework
        # Create
        response = self.client.post(
                reverse('standardsframework-list'),
                {
                    'standardsbody': std_body.pk,
                    'name': 'Test Standards Framework',
                    'short_name': 'TSF'
                    },
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(StandardsFramework.objects.count(), 1)
        std_framework = StandardsFramework.objects.get(short_name='TSF')
        # List
        response = self.client.get(reverse('standardsframework-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('standardsframework-detail', args=[std_framework.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(StandardsFramework):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('standardsframework-detail', args=[std_framework.pk]),
                {
                    'standardsbody': std_body.pk,
                    'name': 'Test Standards Framework',
                    'short_name': 'TSF',
                    'description': 'Test Framework',
                    },
                )
        std_framework.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(std_framework.description, 'Test Framework')
        # Delete
        response = self.client.delete(reverse('standardsframework-detail', args=[std_framework.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        # StandardObjective
        # Create
        response = self.client.post(
                reverse('standardobjective-list'),
                {
                    'standardsframework': std_framework.pk,
                    'name': 'Test Standard Objective',
                    'short_name': 'TSO'
                    },
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(StandardObjective.objects.count(), 1)
        std_objective = StandardObjective.objects.get(short_name='TSO')
        # List
        response = self.client.get(reverse('standardobjective-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('standardobjective-detail', args=[std_objective.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(StandardObjective):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('standardobjective-detail', args=[std_objective.pk]),
                {
                    'standardsframework': std_framework.pk,
                    'name': 'Test Standard Objective',
                    'short_name': 'TSO',
                    'description': 'Test Objective',
                    },
                )
        std_objective.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(std_objective.description, 'Test Objective')
        # Delete
        response = self.client.delete(reverse('standardobjective-detail', args=[std_objective.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_anon_user(self):
        """Anon user can view but not create/update."""
        # Setup data
        std_body = StandardsBody.objects.validated_create(
                name='Test Standards Body',
                short_name='TSB',
                )
        std_framework = StandardsFramework.objects.validated_create(
                standardsbody=std_body,
                name='Test Standards Framework',
                short_name='TSF'
                )
        std_objective = StandardObjective.objects.validated_create(
                standardsframework=std_framework,
                name='Test Standard Objective',
                short_name='TSO'
                )
        # Test
        self.client.force_authenticate(user=None)
        # StandardsBody
        response = self.client.post(reverse('standardsbody-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.get(reverse('standardsbody-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(reverse('standardsbody-detail', args=[std_body.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.put(reverse('standardsbody-detail', args=[std_body.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.delete(reverse('standardsbody-detail', args=[std_body.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # StandardsFramework
        response = self.client.post(reverse('standardsframework-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.get(reverse('standardsframework-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(reverse('standardsframework-detail', args=[std_framework.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.put(reverse('standardsframework-detail', args=[std_framework.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.delete(reverse('standardsframework-detail', args=[std_framework.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # StandardObjective
        response = self.client.post(reverse('standardobjective-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.get(reverse('standardobjective-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(reverse('standardobjective-detail', args=[std_objective.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.put(reverse('standardobjective-detail', args=[std_objective.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.delete(reverse('standardobjective-detail', args=[std_objective.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        )
class TestWeightingModels(TestCase):
    """Create/List/Retrieve/Update smoketest of weighting related models."""
    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(
                username='superuser',
                password='sekret',
                email='superuser@example.com',
                )
        std_body = StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        std_fwork = StandardsFramework.objects.validated_create(
                standardsbody=std_body,
                name='Stage 1',
                )
        cls.std_objective = StandardObjective.objects.validated_create(
                standardsframework=std_fwork,
                name='Node 1',
                short_name='N1',
                )
        review_status = ReviewStatus.objects.validated_create(name='TestStatus')
        course = Course.objects.validated_create(code='Test Course')
        cls.cv = CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=review_status,
                standards_frameworks=[std_fwork],
                )
        cls.cv.objectives.add(cls.std_objective)
        cls.lo = LearningOutcome.objects.validated_create(
                courseversion=cls.cv,
                outcome='Good Stuff.',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        cls.ci = CourseInstance.objects.validated_create(
                courseversion=cls.cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        cls.task = Task.objects.validated_create(courseinstance=cls.ci, title='Exam')

    def setUp(self):
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)

    def test_weightingscheme(self):
        # Create
        response = self.client.post(
                reverse('weightingscheme-list'),
                {'name': 'Test Weighting Scheme'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(WeightingScheme.objects.count(), 1)
        wscheme = WeightingScheme.objects.get(name='Test Weighting Scheme')
        # List
        response = self.client.get(reverse('weightingscheme-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('weightingscheme-detail', args=[wscheme.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(WeightingScheme):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('weightingscheme-detail', args=[wscheme.pk]),
                {'name': 'Test Weighting Scheme', 'description': 'TWS'},
                )
        wscheme.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(wscheme.description, 'TWS')
        # Delete
        response = self.client.delete(reverse('weightingscheme-detail', args=[wscheme.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_weightingscheme_anon(self):
        self.client.force_authenticate(user=None)
        wscheme = WeightingScheme.objects.validated_create(name='Test Weighting Scheme')
        # Create
        response = self.client.post(reverse('weightingscheme-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('weightingscheme-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('weightingscheme-detail', args=[wscheme.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('weightingscheme-detail', args=[wscheme.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('weightingscheme-detail', args=[wscheme.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_weight(self):
        wscheme = WeightingScheme.objects.validated_create(name='Test Weighting Scheme')
        # Create
        response = self.client.post(
                reverse('weight-list'),
                {
                    'weightingscheme': wscheme.pk,
                    'value': '10',
                    },
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Weight.objects.count(), 1)
        weight = Weight.objects.get(value='10')
        # List
        response = self.client.get(reverse('weight-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('weight-detail', args=[weight.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(Weight):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('weight-detail', args=[weight.pk]),
                {
                    'weightingscheme': wscheme.pk,
                    'value': '20',
                    },
                )
        weight.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(weight.value, '20')
        # Delete
        response = self.client.delete(reverse('weight-detail', args=[weight.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_weight_anon(self):
        self.client.force_authenticate(user=None)
        wscheme = WeightingScheme.objects.validated_create(name='Test Weighting Scheme')
        weight = Weight.objects.validated_create(weightingscheme=wscheme, value='10')
        # Create
        response = self.client.post(reverse('weight-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('weight-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('weight-detail', args=[weight.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('weight-detail', args=[weight.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('weight-detail', args=[weight.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_weighted_relationship(self):
        wscheme = WeightingScheme.objects.validated_create(name='Test Weighting Scheme')
        weight = Weight.objects.validated_create(weightingscheme=wscheme, value='10')
        self.cv.weightingschemes.add(wscheme)
        # Create
        response = self.client.post(
                reverse('weightedlotoso-list'),
                {
                    'learningoutcome': self.lo.pk,
                    'standardobjective': self.std_objective.pk,
                    },
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(WeightedLOToSO.objects.count(), 1)
        weightedlotoso = WeightedLOToSO.objects.get(learningoutcome=self.lo)
        # List
        response = self.client.get(reverse('weightedlotoso-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('weightedlotoso-detail', args=[weightedlotoso.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(WeightedLOToSO):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('weightedlotoso-detail', args=[weightedlotoso.pk]),
                {
                    'learningoutcome': self.lo.pk,
                    'standardobjective': self.std_objective.pk,
                    'weights': [weight.pk],
                    },
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(weight.value in weightedlotoso.weights.values_list('value', flat=True))
        # Delete
        response = self.client.delete(reverse('weightedlotoso-detail', args=[weightedlotoso.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_weighted_relationship_anon(self):
        self.client.force_authenticate(user=None)
        wscheme = WeightingScheme.objects.validated_create(name='Test Weighting Scheme')
        self.cv.weightingschemes.add(wscheme)
        weight = Weight.objects.validated_create(weightingscheme=wscheme, value='10')
        weightedlotoso = WeightedLOToSO.objects.validated_create(
                learningoutcome=self.lo,
                standardobjective=self.std_objective,
                weights=[weight],
                )
        # Create
        response = self.client.post(reverse('weightedlotoso-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('weightedlotoso-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('weightedlotoso-detail', args=[weightedlotoso.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('weightedlotoso-detail', args=[weightedlotoso.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('weightedlotoso-detail', args=[weightedlotoso.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_weighted_task_lo_relationship(self):
        wscheme = WeightingScheme.objects.validated_create(name='Test Weighting Scheme')
        weight = Weight.objects.validated_create(weightingscheme=wscheme, value='10')
        self.cv.weightingschemes.add(wscheme)
        # Create
        response = self.client.post(
                reverse('weightedtasktolo-list'),
                {
                    'task': self.task.pk,
                    'learningoutcome': self.lo.pk,
                    },
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(WeightedTaskToLO.objects.count(), 1)
        weightedtasktolo = WeightedTaskToLO.objects.get(learningoutcome=self.lo)
        # List
        response = self.client.get(reverse('weightedtasktolo-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('weightedtasktolo-detail', args=[weightedtasktolo.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(WeightedTaskToLO):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('weightedtasktolo-detail', args=[weightedtasktolo.pk]),
                {
                    'task': self.task.pk,
                    'learningoutcome': self.lo.pk,
                    'weights': [weight.pk],
                    },
                )
        weightedtasktolo.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(weight.value in weightedtasktolo.weights.values_list('value', flat=True))
        # Delete
        response = self.client.delete(
                reverse('weightedtasktolo-detail', args=[weightedtasktolo.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_weighted_task_relationship_anon(self):
        self.client.force_authenticate(user=None)
        wscheme = WeightingScheme.objects.validated_create(name='Test Weighting Scheme')
        self.cv.weightingschemes.add(wscheme)
        weight = Weight.objects.validated_create(weightingscheme=wscheme, value='10')
        weighted_tasklo = WeightedTaskToLO.objects.validated_create(
                task=self.task,
                learningoutcome=self.lo,
                weights=[weight],
                )
        # Create
        response = self.client.post(reverse('weightedtasktolo-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('weightedtasktolo-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('weightedtasktolo-detail', args=[weighted_tasklo.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('weightedtasktolo-detail', args=[weighted_tasklo.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(
                reverse('weightedtasktolo-detail', args=[weighted_tasklo.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        )
class TestPeripheralModels(TestCase):
    """Create/List/Retrieve/Update smoketest of minor models."""
    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(
                username='superuser',
                password='sekret',
                email='superuser@example.com',
                )

    def setUp(self):
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)

    def test_academiccareer(self):
        # Create
        response = self.client.post(reverse('academiccareer-list'), {'career': 'Undergraduate'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AcademicCareer.objects.count(), 1)
        academiccareer = AcademicCareer.objects.get(career='Undergraduate')
        # List
        response = self.client.get(reverse('academiccareer-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('academiccareer-detail', args=[academiccareer.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(AcademicCareer):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('academiccareer-detail', args=[academiccareer.pk]),
                {'career': 'Postgraduate'}
                )
        academiccareer.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(academiccareer.career, 'Postgraduate')
        # Delete
        response = self.client.delete(reverse('academiccareer-detail', args=[academiccareer.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_academiccareer_anon(self):
        self.client.force_authenticate(user=None)
        academiccareer = AcademicCareer.objects.validated_create(career='Undergraduate')
        # Create
        response = self.client.post(reverse('academiccareer-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('academiccareer-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('academiccareer-detail', args=[academiccareer.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('academiccareer-detail', args=[academiccareer.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('academiccareer-detail', args=[academiccareer.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

#   def test_annotation(self):
#       annotation = Annotation.objects.validated_create(
#               content_type=ContentType.objects.get_for_model(User),
#               object_id=self.superuser.pk,
#               note='SuperUserMan',
#               )
#       # Create - Not enabled
#       response = self.client.post(reverse('annotation-list'))
#       self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
#       # List
#       response = self.client.get(reverse('annotation-list'))
#       self.assertEqual(response.status_code, status.HTTP_200_OK)
#       self.assertEqual(len(response.data), 1)
#       # Retrieve - confirm all useful fields are displayed
#       response = self.client.get(
#               reverse('annotation-detail', args=[annotation.pk]),
#               )
#       self.assertEqual(response.status_code, status.HTTP_200_OK)
#       for fname in get_usable_model_fields(Annotation):
#           with self.subTest(fname=fname):
#               self.assertIn(fname, response.data)
#       # Update
#       response = self.client.put(reverse('annotation-detail', args=[annotation.pk]))
#       self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
#       # Delete
#       response = self.client.delete(reverse('annotation-detail', args=[annotation.pk]))
#       self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

#   def test_annotation_anon(self):
#       self.client.force_authenticate(user=None)
#       annotation = Annotation.objects.validated_create(
#               content_type=ContentType.objects.get_for_model(User),
#               object_id=self.superuser.pk,
#               note='SuperUserMan',
#               )
#       # Create
#       response = self.client.post(reverse('annotation-list'))
#       self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
#       # List
#       response = self.client.get(reverse('annotation-list'))
#       self.assertEqual(response.status_code, status.HTTP_200_OK)
#       # Retrieve - confirm all useful fields are displayed
#       response = self.client.get(reverse('annotation-detail', args=[annotation.pk]))
#       self.assertEqual(response.status_code, status.HTTP_200_OK)
#       # Update
#       response = self.client.put(reverse('annotation-detail', args=[annotation.pk]))
#       self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
#       # Delete
#       response = self.client.delete(reverse('annotation-detail', args=[annotation.pk]))
#       self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_organisationalunit(self):
        # Create
        response = self.client.post(
                reverse('organisationalunit-list'),
                {'name': 'Test Org Unit', 'short_name': 'TOU'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(OrganisationalUnit.objects.count(), 1)
        ou = OrganisationalUnit.objects.get(short_name='TOU')
        # List
        response = self.client.get(reverse('organisationalunit-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('organisationalunit-detail', args=[ou.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(OrganisationalUnit):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('organisationalunit-detail', args=[ou.pk]),
                {'name': 'Test Org Unit', 'short_name': 'TDU'},
                )
        ou.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(ou.short_name, 'TDU')
        # Delete
        response = self.client.delete(reverse('organisationalunit-detail', args=[ou.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_organisationalunit_anon(self):
        self.client.force_authenticate(user=None)
        ou = OrganisationalUnit.objects.validated_create(name='Test Org Unit', short_name='TOU')
        # Create
        response = self.client.post(reverse('organisationalunit-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('organisationalunit-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('organisationalunit-detail', args=[ou.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('organisationalunit-detail', args=[ou.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('organisationalunit-detail', args=[ou.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_teachingsession(self):
        # Create
        response = self.client.post(
                reverse('teachingsession-list'),
                {
                    'name': 'Test Session',
                    'start_date': '2017-01-01',
                    'end_date': '2017-01-02',
                    },
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TeachingSession.objects.count(), 1)
        ts = TeachingSession.objects.get(name='Test Session')
        # List
        response = self.client.get(reverse('teachingsession-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('teachingsession-detail', args=[ts.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(TeachingSession):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('teachingsession-detail', args=[ts.pk]),
                {
                    'name': 'Test Session 1',
                    'start_date': '2017-01-01',
                    'end_date': '2017-01-02',
                    },
                )
        ts.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(ts.name, 'Test Session 1')
        # Delete
        response = self.client.delete(reverse('teachingsession-detail', args=[ts.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_teachingsession_anon(self):
        self.client.force_authenticate(user=None)
        ts = TeachingSession.objects.validated_create(
                name='Test Session',
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        # Create
        response = self.client.post(reverse('teachingsession-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('teachingsession-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('teachingsession-detail', args=[ts.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('teachingsession-detail', args=[ts.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('teachingsession-detail', args=[ts.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_course(self):
        # Create
        response = self.client.post(reverse('course-list'), {'code': 'TEST123456'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Course.objects.count(), 1)
        course = Course.objects.get(code='TEST123456')
        # List
        response = self.client.get(reverse('course-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('course-detail', args=[course.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(Course):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('course-detail', args=[course.pk]),
                {'code': 'TEST654321'}
                )
        course.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(course.code, 'TEST654321')
        # Delete
        response = self.client.delete(reverse('course-detail', args=[course.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_course_anon(self):
        self.client.force_authenticate(user=None)
        course = Course.objects.validated_create(code='TEST654321')
        # Create
        response = self.client.post(reverse('course-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('course-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('course-detail', args=[course.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('course-detail', args=[course.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('course-detail', args=[course.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_courseclassification(self):
        # Create
        response = self.client.post(
                reverse('courseclassification-list'),
                {'classification': 'Advanced'}
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CourseClassification.objects.count(), 1)
        courseclassification = CourseClassification.objects.get(classification='Advanced')
        # List
        response = self.client.get(reverse('courseclassification-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('courseclassification-detail', args=[courseclassification.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(CourseClassification):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('courseclassification-detail', args=[courseclassification.pk]),
                {'classification': 'Transitional'}
                )
        courseclassification.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(courseclassification.classification, 'Transitional')
        # Delete
        response = self.client.delete(
                reverse('courseclassification-detail', args=[courseclassification.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_courseclassification_anon(self):
        self.client.force_authenticate(user=None)
        courseclassification = CourseClassification.objects.validated_create(
                classification='Advanced'
                )
        # Create
        response = self.client.post(reverse('courseclassification-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('courseclassification-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('courseclassification-detail', args=[courseclassification.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(
                reverse('courseclassification-detail', args=[courseclassification.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(
                reverse('courseclassification-detail', args=[courseclassification.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_reviewstatus(self):
        # There is a default status so count/list lengths will be 2 not 1.
        # Create
        total_states = ReviewStatus.objects.count() + 1
        response = self.client.post(reverse('reviewstatus-list'), {'name': 'Approved'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ReviewStatus.objects.count(), total_states)
        reviewstatus = ReviewStatus.objects.get(name='Approved')
        # List
        response = self.client.get(reverse('reviewstatus-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), total_states)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('reviewstatus-detail', args=[reviewstatus.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(ReviewStatus):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('reviewstatus-detail', args=[reviewstatus.pk]),
                {'name': 'Approved'}
                )
        reviewstatus.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(reviewstatus.name, 'Approved')
        # Delete
        response = self.client.delete(reverse('reviewstatus-detail', args=[reviewstatus.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_reviewstatus_anon(self):
        self.client.force_authenticate(user=None)
        reviewstatus = ReviewStatus.objects.validated_create(name='Approved')
        # Create
        response = self.client.post(reverse('reviewstatus-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('reviewstatus-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('reviewstatus-detail', args=[reviewstatus.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('reviewstatus-detail', args=[reviewstatus.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('reviewstatus-detail', args=[reviewstatus.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_courserelatedroletitle(self):
        # Create
        response = self.client.post(reverse('courserelatedroletitle-list'), {'title': 'Tutor'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CourseRelatedRoleTitle.objects.count(), 1)
        courserelatedroletitle = CourseRelatedRoleTitle.objects.get(title='Tutor')
        # List
        response = self.client.get(reverse('courserelatedroletitle-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('courserelatedroletitle-detail', args=[courserelatedroletitle.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(CourseRelatedRoleTitle):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('courserelatedroletitle-detail', args=[courserelatedroletitle.pk]),
                {'title': 'Lecturer'}
                )
        courserelatedroletitle.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(courserelatedroletitle.title, 'Lecturer')
        # Delete
        response = self.client.delete(
                reverse('courserelatedroletitle-detail', args=[courserelatedroletitle.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_courserelatedroletitle_anon(self):
        self.client.force_authenticate(user=None)
        courserelatedroletitle = CourseRelatedRoleTitle.objects.validated_create(title='Tutor')
        # Create
        response = self.client.post(reverse('courserelatedroletitle-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('courserelatedroletitle-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('courserelatedroletitle-detail', args=[courserelatedroletitle.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(
                reverse('courserelatedroletitle-detail', args=[courserelatedroletitle.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(
                reverse('courserelatedroletitle-detail', args=[courserelatedroletitle.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_deliverymode(self):
        # Create
        response = self.client.post(reverse('deliverymode-list'), {'mode': 'Online'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(DeliveryMode.objects.count(), 1)
        deliverymode = DeliveryMode.objects.get(mode='Online')
        # List
        response = self.client.get(reverse('deliverymode-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('deliverymode-detail', args=[deliverymode.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(DeliveryMode):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('deliverymode-detail', args=[deliverymode.pk]),
                {'mode': 'In Person'}
                )
        deliverymode.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(deliverymode.mode, 'In Person')
        # Delete
        response = self.client.delete(reverse('deliverymode-detail', args=[deliverymode.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_deliverymode_anon(self):
        self.client.force_authenticate(user=None)
        deliverymode = DeliveryMode.objects.validated_create(mode='Online')
        # Create
        response = self.client.post(reverse('deliverymode-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('deliverymode-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('deliverymode-detail', args=[deliverymode.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(
                reverse('deliverymode-detail', args=[deliverymode.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('deliverymode-detail', args=[deliverymode.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        )
class TestCoreModels(TestCase):
    """Create/List/Retrieve/Update smoketest of major models."""
    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(
                username='superuser',
                password='sekret',
                email='superuser@example.com',
                )
        cls.course = Course.objects.validated_create(code='TEST123456')
        cls.status = ReviewStatus.objects.validated_create(name='Approved')

    def setUp(self):
        self.client = APIClient()
        self.client.force_authenticate(user=self.superuser)

    def test_courseversion(self):
        # Create
        response = self.client.post(
                reverse('courseversion-list'),
                {'course': self.course.pk, 'status': self.status.pk,
                 'version_descriptor': 'Test Version'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CourseVersion.objects.count(), 1)
        courseversion = CourseVersion.objects.get(version_descriptor='Test Version')
        # List
        response = self.client.get(reverse('courseversion-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('courseversion-detail', args=[courseversion.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(CourseVersion):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('courseversion-detail', args=[courseversion.pk]),
                {'course': self.course.pk, 'status': self.status.pk,
                 'version_descriptor': 'Test Version', 'title': 'Testing'},
                )
        courseversion.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(courseversion.title, 'Testing')
        # Delete
        response = self.client.delete(reverse('courseversion-detail', args=[courseversion.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_courseversion_anon(self):
        self.client.force_authenticate(user=None)
        courseversion = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        # Create
        response = self.client.post(reverse('courseversion-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('courseversion-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('courseversion-detail', args=[courseversion.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('courseversion-detail', args=[courseversion.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('courseversion-detail', args=[courseversion.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_learningoutcome(self):
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        # Create
        response = self.client.post(
                reverse('learningoutcome-list'),
                {'courseversion': cv.pk, 'outcome': 'Stuff'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(LearningOutcome.objects.count(), 1)
        learningoutcome = LearningOutcome.objects.get(outcome='Stuff')
        # List
        response = self.client.get(reverse('learningoutcome-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('learningoutcome-detail', args=[learningoutcome.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(LearningOutcome):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('learningoutcome-detail', args=[learningoutcome.pk]),
                {'courseversion': cv.pk, 'outcome': 'Good Stuff'},
                )
        learningoutcome.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(learningoutcome.outcome, 'Good Stuff')
        # Delete
        response = self.client.delete(reverse('learningoutcome-detail', args=[learningoutcome.pk]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_learningoutcome_anon(self):
        self.client.force_authenticate(user=None)
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        learningoutcome = LearningOutcome.objects.validated_create(
            courseversion=cv,
            outcome='Stuff'
                )
        # Create
        response = self.client.post(reverse('learningoutcome-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('learningoutcome-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('learningoutcome-detail', args=[learningoutcome.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('learningoutcome-detail', args=[learningoutcome.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('learningoutcome-detail', args=[learningoutcome.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_courseinstance(self):
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        # Create
        response = self.client.post(
                reverse('courseinstance-list'),
                {'courseversion': cv.pk, 'instance_descriptor': 'Test Instance',
                 'taught_by': [ou.pk], 'start_date': '2017-01-01', 'end_date': '2017-01-02'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CourseInstance.objects.count(), 1)
        courseinstance = CourseInstance.objects.get(instance_descriptor='Test Instance')
        # List
        response = self.client.get(reverse('courseinstance-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('courseinstance-detail', args=[courseinstance.pk]),
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(CourseInstance):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('courseinstance-detail', args=[courseinstance.pk]),
                {'courseversion': cv.pk, 'instance_descriptor': 'Test Instance 1',
                 'taught_by': [ou.pk], 'start_date': '2017-01-01', 'end_date': '2017-01-02'},
                )
        courseinstance.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(courseinstance.instance_descriptor, 'Test Instance 1')
        # Delete
        response = self.client.delete(reverse('courseinstance-detail', args=[courseinstance.pk]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_courseinstance_anon(self):
        self.client.force_authenticate(user=None)
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        courseinstance = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        # Create
        response = self.client.post(reverse('courseinstance-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('courseinstance-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(
                reverse('courseinstance-detail', args=[courseinstance.pk])
            )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(
                reverse('courseinstance-detail', args=[courseinstance.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('courseinstance-detail', args=[courseinstance.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task(self):
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        # Create
        response = self.client.post(
                reverse('task-list'),
                {'courseinstance': ci.pk, 'title': 'Exam'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Task.objects.count(), 1)
        task = Task.objects.get(title='Exam')
        # List
        response = self.client.get(reverse('task-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('task-detail', args=[task.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(Task):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('task-detail', args=[task.pk]),
                {'courseinstance': ci.pk, 'title': 'Lab'},
                )
        task.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(task.title, 'Lab')
        # Delete
        response = self.client.delete(reverse('task-detail', args=[task.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_task_anon(self):
        self.client.force_authenticate(user=None)
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        task = Task.objects.validated_create(courseinstance=ci, title='Exam')
        # Create
        response = self.client.post(reverse('task-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('task-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('task-detail', args=[task.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('task-detail', args=[task.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('task-detail', args=[task.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_schedule(self):
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        # Create
        response = self.client.post(
                reverse('schedule-list'),
                {'courseinstance': ci.pk, 'session_name': 'Week 1'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Schedule.objects.count(), 1)
        schedule = Schedule.objects.get(session_name='Week 1')
        # List
        response = self.client.get(reverse('schedule-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('schedule-detail', args=[schedule.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(Schedule):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('schedule-detail', args=[schedule.pk]),
                {'courseinstance': ci.pk, 'session_name': 'Week 3'},
                )
        schedule.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(schedule.session_name, 'Week 3')
        # Delete
        response = self.client.delete(reverse('schedule-detail', args=[schedule.pk]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_schedule_anon(self):
        self.client.force_authenticate(user=None)
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        schedule = Schedule.objects.validated_create(
                courseinstance=ci,
                session_name='Week 1'
                )
        # Create
        response = self.client.post(reverse('schedule-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('schedule-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('schedule-detail', args=[schedule.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('schedule-detail', args=[schedule.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(reverse('schedule-detail', args=[schedule.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_courserelatedrole(self):
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        user = User.objects.create_user(username='user', password='sekret')
        lecturer = CourseRelatedRoleTitle.objects.validated_create(title='Lecturer')
        tutor = CourseRelatedRoleTitle.objects.validated_create(title='Tutor')
        # Create
        response = self.client.post(
                reverse('courserelatedrole-list'),
                {'courseinstance': ci.pk, 'user': user.pk, 'role_title': lecturer.pk},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CourseRelatedRole.objects.count(), 1)
        courserelatedrole = CourseRelatedRole.objects.get(role_title=lecturer)
        # List
        response = self.client.get(reverse('courserelatedrole-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('courserelatedrole-detail', args=[courserelatedrole.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fname in get_usable_model_fields(CourseRelatedRole):
            with self.subTest(fname=fname):
                self.assertIn(fname, response.data)
        # Update
        response = self.client.put(
                reverse('courserelatedrole-detail', args=[courserelatedrole.pk]),
                {'courseinstance': ci.pk, 'user': user.pk, 'role_title': tutor.pk},
                )
        courserelatedrole.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(courserelatedrole.role_title.title, tutor.title)
        # Delete
        response = self.client.delete(
                reverse('courserelatedrole-detail', args=[courserelatedrole.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_courserelatedrole_anon(self):
        self.client.force_authenticate(user=None)
        cv = CourseVersion.objects.validated_create(
                course=self.course,
                status=self.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        user = User.objects.create_user(username='user', password='sekret')
        lecturer = CourseRelatedRoleTitle.objects.validated_create(title='Lecturer')
        courserelatedrole = CourseRelatedRole.objects.validated_create(
                courseinstance=ci,
                user=user,
                role_title=lecturer,
                )
        # Create
        response = self.client.post(reverse('courserelatedrole-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # List
        response = self.client.get(reverse('courserelatedrole-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Retrieve - confirm all useful fields are displayed
        response = self.client.get(reverse('courserelatedrole-detail', args=[courserelatedrole.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Update
        response = self.client.put(reverse('courserelatedrole-detail', args=[courserelatedrole.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Delete
        response = self.client.delete(
                reverse('courserelatedrole-detail', args=[courserelatedrole.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
