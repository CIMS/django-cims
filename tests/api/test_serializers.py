"""
Tests for API serializers.
"""
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.db import connection, transaction
from django.db.utils import IntegrityError
from django.test import TestCase
from rest_framework.exceptions import ValidationError

from tests.models import Pizza, PizzaBase, Topping
from .serializers import (FilteredPizzaSerializer, PizzaSerializer,
                          ToppinglessFilteredPizzaSerializer)

User = get_user_model()


class TestValidatedModelSerializer(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.cheese = Topping.objects.validated_create(name='Cheese')
        cls.mushrooms = Topping.objects.validated_create(name='Mushrooms')
        cls.capsicums = Topping.objects.validated_create(name='Capsicums')
        cls.salami = Topping.objects.validated_create(name='salami')
        cls.traditional = PizzaBase.objects.validated_create(name='Traditional')
        cls.dc = PizzaBase.objects.validated_create(name='Deep Crust')

    def test_serializer_create(self):
        """
        Test the serializer raises exceptions when validation fails, and the instance isn't saved
        to the database.
        """
        # Compliant pizza passes
        data = {
                'name': 'Proper Pizza',
                'price': '10.95',
                'toppings': [self.salami.id, self.capsicums.id, self.mushrooms.id, self.cheese.id],
                'bases': [self.traditional.id, self.dc.id],
                }
        serializer = PizzaSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertTrue(Pizza.objects.filter(id=instance.id).exists())
        self.assertEqual('Proper Pizza', instance.name)
        self.assertEqual(Decimal('10.95'), instance.price)
        self.assertSetEqual(
                {self.salami, self.capsicums, self.mushrooms, self.cheese},
                set(instance.toppings.all()),
                )
        self.assertSetEqual({self.traditional, self.dc}, set(instance.bases.all()))
        # Validation in model.clean() fails
        data = {
                'name': 'Overpriced pizza',
                'price': '31.00',
                }
        serializer = PizzaSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertRaisesMessage(
                ValidationError,
                "{'__all__': [ErrorDetail(string='You think people will pay $31.00 for a pizza?', "
                "code='invalid')]}",
                serializer.save,
                )
        self.assertFalse(Pizza.objects.filter(name='Overpriced pizza').exists())
        # Validation in model.validate_other() fails
        data = {
                'name': 'Tasteless Pizza',
                'price': '8.95',
                'toppings': [self.mushrooms.id, self.cheese.id],
                'bases': [self.traditional.id, self.dc.id],
                }
        serializer = PizzaSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertRaisesMessage(
                ValidationError,
                ("{'non_field_errors': [ErrorDetail(string='Pizzas with fewer than three toppings "
                 "are tasteless, those with greater than 7 are overcomplicated mush.', "
                 "code='invalid')]}"),
                serializer.save,
                )
        self.assertFalse(Pizza.objects.filter(name='Tasteless Pizza').exists())

    def test_serializer_update(self):
        """
        Test the serializer raises exceptions when validation fails, and the instance isn't saved
        to the database.
        """
        # Compliant pizza passes
        data = {
                'name': 'Proper Pizza',
                'price': '10.95',
                'toppings': [self.salami.id, self.capsicums.id, self.mushrooms.id, self.cheese.id],
                'bases': [self.traditional.id, self.dc.id],
                }
        serializer = PizzaSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        # Validation in model.clean() fails
        data = {
                'name': 'Overpriced pizza',
                'price': '31.00',
                }
        serializer = PizzaSerializer(instance=instance, data=data)
        self.assertTrue(serializer.is_valid())
        self.assertRaisesMessage(
                ValidationError,
                "{'__all__': [ErrorDetail(string='You think people will pay $31.00 for a pizza?', "
                "code='invalid')]}",
                serializer.save,
                )
        self.assertFalse(Pizza.objects.filter(name='Overpriced pizza').exists())
        # Validation in model.validate_other() fails
        instance.refresh_from_db()
        data = {
                'name': 'Tasteless Pizza',
                'price': '8.95',
                'toppings': [self.mushrooms.id, self.cheese.id],
                'bases': [self.traditional.id, self.dc.id],
                }
        serializer = PizzaSerializer(instance=instance, data=data)
        self.assertTrue(serializer.is_valid())
        self.assertRaisesMessage(
                ValidationError,
                ("{'non_field_errors': [ErrorDetail(string='Pizzas with fewer than three toppings "
                 "are tasteless, those with greater than 7 are overcomplicated mush.', "
                 "code='invalid')]}"),
                serializer.save,
                )
        self.assertFalse(Pizza.objects.filter(name='Tasteless Pizza').exists())


class TestFilteredFieldsModelSerializer(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.cheese = Topping.objects.validated_create(name='Cheese')
        cls.mushrooms = Topping.objects.validated_create(name='Mushrooms')
        cls.capsicums = Topping.objects.validated_create(name='Capsicums')
        cls.salami = Topping.objects.validated_create(name='salami')
        cls.traditional = PizzaBase.objects.validated_create(name='Traditional')
        cls.dc = PizzaBase.objects.validated_create(name='Deep Crust')

    def test_fields_filtering(self):
        """Fields are set read_only or removed from serializer based on args."""
        data = {
                'name': 'Proper Pizza',
                'price': '10.95',
                }
        # Nothing changes if it isn't told to
        serializer = FilteredPizzaSerializer(data=data)
        self.assertTrue(serializer.fields['id'].read_only)
        self.assertTrue(serializer.fields['created'].read_only)
        self.assertTrue(serializer.fields['last_modified'].read_only)
        self.assertFalse(serializer.fields['name'].read_only)
        self.assertFalse(serializer.fields['price'].read_only)
        self.assertFalse(serializer.fields['toppings'].read_only)
        self.assertFalse(serializer.fields['bases'].read_only)
        # Set some fields read_only
        serializer = FilteredPizzaSerializer(data=data, disabled_fields=['price', 'bases'])
        self.assertTrue(serializer.fields['id'].read_only)
        self.assertTrue(serializer.fields['created'].read_only)
        self.assertTrue(serializer.fields['last_modified'].read_only)
        self.assertFalse(serializer.fields['name'].read_only)
        self.assertTrue(serializer.fields['price'].read_only)
        self.assertFalse(serializer.fields['toppings'].read_only)
        self.assertTrue(serializer.fields['bases'].read_only)
        # Fields are removed
        serializer = FilteredPizzaSerializer(
                data=data,
                disabled_fields=['price', 'bases'],
                disable_fields=False,
                )
        self.assertTrue(serializer.fields['id'].read_only)
        self.assertTrue(serializer.fields['created'].read_only)
        self.assertTrue(serializer.fields['last_modified'].read_only)
        self.assertFalse(serializer.fields['name'].read_only)
        self.assertFalse('price' in serializer.fields)
        self.assertFalse(serializer.fields['toppings'].read_only)
        self.assertFalse('bases' in serializer.fields)
        # Works with a serializer without all fields from model
        ToppinglessFilteredPizzaSerializer(data=data, disabled_fields=['price', 'toppings'])

    def test_filtered_save(self):
        """Fields set read_only via args can't be set/changed."""
        data = {
                'name': 'Proper Pizza',
                'price': '10.95',
                'toppings': [self.salami.pk, self.capsicums.pk, self.mushrooms.pk, self.cheese.pk],
                'bases': [self.traditional.pk, self.dc.pk],
                }
        # Required field set read_only so save fails
        serializer = FilteredPizzaSerializer(
                data=data,
                disabled_fields=['name', 'price', 'toppings'],
                )
        self.assertTrue(serializer.is_valid())
        # Have to do this in a transaction or it breaks transaction wrapping this test
        with transaction.atomic():
            if connection.vendor == 'postgresql':
                    self.assertRaisesRegex(
                            IntegrityError,
                            '^null value in column "price" violates not-null constraint',
                            serializer.save,
                            )
            else:
                self.assertRaisesMessage(
                        IntegrityError,
                        'NOT NULL constraint failed: tests_pizza.price',
                        serializer.save,
                        )
        # Non-required field isn't set, save succeeds
        serializer = FilteredPizzaSerializer(data=data, disabled_fields=['bases', 'toppings'])
        serializer.is_valid()
        pizza = serializer.save()
        self.assertEqual('Proper Pizza', pizza.name)
        self.assertEqual(Decimal('10.95'), pizza.price)
        self.assertQuerysetEqual(pizza.bases.all(), [])
        # Block update to field on existing instance
        data = {
                'name': 'Price Fixed Pizza',
                'price': '13.95',
                }
        serializer = FilteredPizzaSerializer(
                instance=pizza,
                data=data,
                disabled_fields=['price', 'bases', 'toppings'],
                )
        serializer.is_valid()
        pizza = serializer.save()
        self.assertEqual('Price Fixed Pizza', pizza.name)
        self.assertEqual(Decimal('10.95'), pizza.price)
