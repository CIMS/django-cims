from django.contrib.admin.models import LogEntry
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test import TestCase, override_settings
from rest_framework import status
from rest_framework.test import APIClient
from reversion.models import Revision

from cims.api import mixins
from tests.helpers import dict_a_from_b, get_perm
from .models import LogTester, VersionTester

User = get_user_model()


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class TestLoggingMixins(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user1 = User.objects.create_user(username='user1', password='sekret')
        cls.user2 = User.objects.create_user(username='user2', password='sekret')
        perm1 = get_perm('tests_api', 'logtester', 'add_logtester')
        perm2 = get_perm('tests_api', 'logtester', 'change_logtester')
        cls.user1.user_permissions.add(perm1, perm2)

    def setUp(self):
        self.client = APIClient()

    def test_perform_create_logging(self):
        """Ensure create/update actions make a log entry."""
        self.client.force_authenticate(user=self.user1)
        response = self.client.post(
                '/testapi/logtester/',
                {'name': 'Test Logging 1', 'short_name': 'TL1'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(
                response.data,
                {'id': 1, 'name': 'Test Logging 1', 'short_name': 'TL1', 'description': '',
                 'yaf': None},
                )
        logentry = LogEntry.objects.get(user=self.user1)
        self.assertEqual('Added.', logentry.get_change_message())
        logtester = LogTester.objects.first()
        self.assertEqual('Test Logging 1', logtester.name)
        self.assertEqual('TL1', logtester.short_name)
        # If the object isn't saved nothing is logged
        self.client.force_authenticate(user=self.user2)
        response = self.client.post(
                '/testapi/logtester/',
                {'name': 'Test Logging 2', 'short_name': 'TL2'},
                )
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=status.HTTP_403_FORBIDDEN,
                )
        self.assertEqual(1, LogEntry.objects.count())

    def test_perform_update_logging(self):
        """Ensure create/update actions make a log entry, save object."""
        self.client.force_authenticate(user=self.user1)
        response = self.client.post(
                '/testapi/logtester/',
                {'name': 'Test Logging 3', 'short_name': 'TL3'}
                )
        logtester_id = response.data['id']
        response = self.client.put(
                '/testapi/logtester/{}/'.format(logtester_id),
                {'name': 'Test Logging 3', 'short_name': 'TL3;DR', 'description': 'TL for all!',
                    'yaf': 2},
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
                response.data,
                {'id': logtester_id, 'name': 'Test Logging 3', 'short_name': 'TL3;DR',
                    'description': 'TL for all!', 'yaf': 2},
                )
        logentry = LogEntry.objects.filter(user=self.user1).order_by('-action_time').first()
        self.assertEqual('Changed short_name, description and yaf.', logentry.get_change_message())
        # Object was saved.
        logtester = LogTester.objects.get(pk=logtester_id)
        self.assertEqual('TL3;DR', logtester.short_name)
        self.assertEqual('TL for all!', logtester.description)
        self.assertEqual(2, logtester.yaf)
        # If the object isn't saved nothing is logged
        self.client.force_authenticate(user=self.user2)
        response = self.client.put(
                '/testapi/logtester/{}/'.format(logtester_id),
                {'name': 'Test Logging 4', 'short_name': 'TL4'},
                )
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=status.HTTP_403_FORBIDDEN,
                )
        self.assertEqual(2, LogEntry.objects.count())


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('tests_api', 'logtester')],
        PFP_IGNORE_PERMS={},
        )
class TestPFPMixins(TestCase):

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')

    def setUp(self):
        self.client = APIClient()

    def test_perform_create_strip_field(self):
        """
        Test that fields the user lacks access to are stripped from the data being saved. If
        those fields are required fields the action fails and an approprate response is returned.
        """
        user1 = User.objects.create_user(username='user1', password='sekret')
        user2 = User.objects.create_user(username='user2', password='sekret')
        user3 = User.objects.create_user(username='user3', password='sekret')
        self.client.force_authenticate(user=user1)
        # Strip at least two fields.
        pfp1 = get_perm('tests_api', 'logtester', 'add_logtester__name')
        pfp2 = get_perm('tests_api', 'logtester', 'add_logtester__short_name')
        user1.user_permissions.add(pfp1)
        # Lacks permission for a required field
        response = self.client.post(
                '/testapi/pfptester/',
                {'name': 'Test PFP 1', 'short_name': 'TP1', 'description': 'TP for all!'},
                )
        self.assertContains(
                response,
                '{"short_name":["This field cannot be blank."]}',
                status_code=400,
                )
        # Has permission for required fields
        self.client.force_authenticate(user=user2)
        user2.user_permissions.add(pfp1, pfp2)
        response = self.client.post(
                '/testapi/pfptester/',
                {'name': 'Test PFP 2', 'short_name': 'TP2', 'description': 'TP for all!'},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_contents = {'name': 'Test PFP 2', 'short_name': 'TP2', 'description': '',
                             'yaf': None, 'CIMS_NOTE': 'The fields [\'description\', \'yaf\'] '
                                                       'were not set because you lack required '
                                                       'permissions.'}
        self.assertEqual(response_contents, dict_a_from_b(response_contents, response.data))
        pfptester = LogTester.objects.get(pk=response.data['id'])
        self.assertEqual('Test PFP 2', pfptester.name)
        self.assertEqual('TP2', pfptester.short_name)
        self.assertEqual('', pfptester.description)
        self.assertEqual(None, pfptester.yaf)
        # Check return when non-field validation fails
        self.client.force_authenticate(user=user3)
        perm1 = get_perm('tests_api', 'logtester', 'add_logtester')
        user3.user_permissions.add(perm1)
        response = self.client.post(
                '/testapi/pfptester/',
                {'name': 'Test PFP 3', 'short_name': 'TP3', 'yaf': 42},
                )
        self.assertContains(
                response,
                '{"__all__":["Yet Another Field can\'t contain the meaning of life."]}',
                status_code=status.HTTP_400_BAD_REQUEST,
                )
        # Everything is fine...
        response = self.client.post(
                '/testapi/pfptester/',
                {'name': 'Test PFP 4', 'short_name': 'TP4', 'description': 'TP for all!',
                    'yaf': 64},
                )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_contents = {'name': 'Test PFP 4', 'short_name': 'TP4',
                             'description': 'TP for all!', 'yaf': 64}
        self.assertEqual(response_contents, dict_a_from_b(response_contents, response.data))
        pfptester = LogTester.objects.get(pk=response.data['id'])
        self.assertEqual('Test PFP 4', pfptester.name)
        self.assertEqual('TP4', pfptester.short_name)
        self.assertEqual('TP for all!', pfptester.description)
        self.assertEqual(64, pfptester.yaf)

    def test_perform_update_strip_field(self):
        """
        Test that fields the user lacks access to are stripped from the data being saved during
        an update, and that stripping required fields doesn't cause an exception.
        """
        user1 = User.objects.create_user(username='user1', password='sekret')
        self.client.force_authenticate(user=user1)
        # Strip at least two fields.
        perm = get_perm('tests_api', 'logtester', 'add_logtester')
        pfp = get_perm('tests_api', 'logtester', 'change_logtester__description')
        user1.user_permissions.add(perm, pfp)
        response = self.client.post(
                '/testapi/pfptester/',
                {'name': 'Test PFP 5', 'short_name': 'TP5'}
                )
        pfptester_id = response.data['id']
        response = self.client.put(
                '/testapi/pfptester/{}/'.format(pfptester_id),
                {'name': 'Test PFP 6', 'short_name': 'TP6', 'description': 'TP for all!',
                    'yaf': 2},
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_contents = {'name': 'Test PFP 5', 'short_name': 'TP5',
                             'description': 'TP for all!', 'yaf': None,
                             'CIMS_NOTE': 'The fields [\'name\', \'short_name\', \'yaf\'] were '
                                          'not updated because you lack required permissions.'}
        self.assertEqual(response_contents, dict_a_from_b(response_contents, response.data))
        pfptester = LogTester.objects.get(pk=pfptester_id)
        self.assertEqual('Test PFP 5', pfptester.name)
        self.assertEqual('TP5', pfptester.short_name)
        self.assertEqual('TP for all!', pfptester.description)
        self.assertEqual(None, pfptester.yaf)
        # Everything works fine
        user2 = User.objects.create_user(username='user2', password='sekret')
        self.client.force_authenticate(user=user2)
        perm2 = get_perm('tests_api', 'logtester', 'change_logtester')
        user2.user_permissions.add(perm2)
        response = self.client.put(
                '/testapi/pfptester/{}/'.format(pfptester_id),
                {'name': 'Test PFP 7', 'short_name': 'TP7', 'description': 'TP for all!',
                    'yaf': 2},
                )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_contents = {'name': 'Test PFP 7', 'short_name': 'TP7',
                             'description': 'TP for all!', 'yaf': 2}
        self.assertEqual(response_contents, dict_a_from_b(response_contents, response.data))
        pfptester.refresh_from_db()
        self.assertEqual('Test PFP 7', pfptester.name)
        self.assertEqual('TP7', pfptester.short_name)
        self.assertEqual('TP for all!', pfptester.description)
        self.assertEqual(2, pfptester.yaf)


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class TestVersionedMixins(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        self.client = APIClient()

    def test_perform_create_update(self):
        """Ensure create/update actions make a revision entry."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        user2 = User.objects.create_user(username='user2', password='sekret')
        perm1 = get_perm('tests_api', 'versiontester', 'add_versiontester')
        perm2 = get_perm('tests_api', 'versiontester', 'change_versiontester')
        user1.user_permissions.add(perm1, perm2)
        self.client.force_authenticate(user=user1)
        response = self.client.post(
                '/testapi/versiontester/',
                {'name': 'Test Versioning 1', 'short_name': 'TV1'}
                )
        self.assertContains(
                response,
                ('{"id":1,"name":"Test Versioning 1","short_name":"TV1","description":"",'
                 '"yaf":null}'),
                status_code=201,
                )
        versiontester = VersionTester.objects.first()
        revision = Revision.objects.get(user=user1)
        self.assertEqual('[{"added": {}}]', revision.comment)
        self.assertEqual('Test Versioning 1', versiontester.name)
        self.assertEqual('TV1', versiontester.short_name)
        # Version not created if exception raised/permission denied
        self.client.force_authenticate(user=user2)
        response = self.client.post(
                '/testapi/versiontester/',
                {'name': 'Test Versioning 2', 'short_name': 'TV2'}
                )
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        self.assertEqual(0, Revision.objects.filter(user=user2).count())
        # Update
        self.client.force_authenticate(user=user1)
        response = self.client.put(
                '/testapi/versiontester/1/',
                {'name': 'Test Versioning 1', 'short_name': 'TVDB', 'description': 'TV for all!'},
                )
        self.assertContains(
                response,
                ('{"id":1,"name":"Test Versioning 1","short_name":"TVDB","description":"TV for '
                 'all!","yaf":null}'),
                )
        # Get udpated values from DB
        versiontester.refresh_from_db()
        revision = Revision.objects.filter(user=user1).order_by('-date_created').first()
        self.assertEqual(
                '[{"changed": {"fields": ["short_name", "description"]}}]',
                revision.comment
                )
        self.assertEqual('TVDB', versiontester.short_name)
        self.assertEqual('TV for all!', versiontester.description)
        # Version not created if exception raised/permission denied
        self.client.force_authenticate(user=user2)
        response = self.client.put(
                '/testapi/versiontester/1/',
                {'name': 'Test Versioning 2', 'short_name': 'TV2'}
                )
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        self.assertEqual(0, Revision.objects.filter(user=user2).count())


class TestVersionedPFPModelMixin(TestCase):
    def test_mro(self):
        """Make sure our mro is correct so revisions/logs are created, and then permissions
        applied."""
        mro = mixins.VersionedPFPModelMixin.mro()
        self.assertLess(
                mro.index(mixins.VersionedCreateModelMixin),
                mro.index(mixins.PFPCreateModelMixin)
                )
        self.assertLess(
                mro.index(mixins.VersionedUpdateModelMixin),
                mro.index(mixins.PFPUpdateModelMixin)
                )
