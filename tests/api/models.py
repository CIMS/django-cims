"""
Models for testing the CIMS API.
"""
import reversion
from django.core.exceptions import ValidationError
from django.db import models

from cims.models import CIMSBaseModel


# API testing specific models
class LogTester(CIMSBaseModel):
    name = models.CharField(max_length=80)
    short_name = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    yaf = models.IntegerField(blank=True, null=True)

    def clean(self):
        if self.yaf == 42:
            raise ValidationError("Yet Another Field can't contain the meaning of life.")


@reversion.register()
class VersionTester(CIMSBaseModel):
    name = models.CharField(max_length=80)
    short_name = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    yaf = models.IntegerField(blank=True, null=True)
