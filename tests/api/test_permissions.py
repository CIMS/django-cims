"""
Integration tests for Permissions objects. Borrows heavily from Django Rest Framework's
test_permissions.py.
"""
import base64

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from rest_framework import HTTP_HEADER_ENCODING, authentication, generics, status
from rest_framework.test import APIRequestFactory

from cims.api.permissions import DjangoObjectPermissionsOrReadOnly
from tests.models import BasicModel
from .serializers import BasicModelSerializer

factory = APIRequestFactory()
User = get_user_model()


def basic_auth_header(username, password):
    credentials = ('%s:%s' % (username, password))
    base64_credentials = base64.b64encode(
            credentials.encode(HTTP_HEADER_ENCODING)
            ).decode(HTTP_HEADER_ENCODING)
    return 'Basic %s' % base64_credentials


class ObjectPermissionInstanceView(generics.RetrieveUpdateDestroyAPIView):
    queryset = BasicModel.objects.all()
    serializer_class = BasicModelSerializer
    authentication_classes = [authentication.BasicAuthentication]
    permission_classes = [DjangoObjectPermissionsOrReadOnly]


object_permissions_view = ObjectPermissionInstanceView.as_view()


class ObjectPermissionListView(generics.ListAPIView):
    queryset = BasicModel.objects.all()
    serializer_class = BasicModelSerializer
    authentication_classes = [authentication.BasicAuthentication]
    permission_classes = [DjangoObjectPermissionsOrReadOnly]


object_permissions_list_view = ObjectPermissionListView.as_view()


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        )
class ObjectPermissionsOrReadOnlyIntegrationTests(TestCase):
    """
    Integration tests for CIMS object level permissions.
    """
    @classmethod
    def setUpTestData(cls):
        cls.superuser = User.objects.create_superuser(
                username='superuser',
                password='sekret',
                email='superuser@example.com',
                )
        cls.credentials = basic_auth_header(cls.superuser.username, 'sekret')

    def setUp(self):
        BasicModel.objects.create(id=1, text='Test text')

    # Delete
    def test_can_delete_permissions(self):
        request = factory.delete('/1', HTTP_AUTHORIZATION=self.credentials)
        response = object_permissions_view(request, pk='1')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_cannot_delete_permissions(self):
        request = factory.delete('/1')
        response = object_permissions_view(request, pk='1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    # Update
    def test_can_update_permissions(self):
        request = factory.patch(
                '/1', {'text': 'foobar'}, format='json',
                HTTP_AUTHORIZATION=self.credentials
                )
        response = object_permissions_view(request, pk='1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('text'), 'foobar')

    def test_cannot_update_permissions(self):
        request = factory.patch('/1', {'text': 'foobar'}, format='json')
        response = object_permissions_view(request, pk='1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    # Read
    def test_can_read(self):
        request = factory.get('/1', HTTP_AUTHORIZATION=self.credentials)
        response = object_permissions_view(request, pk='1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_anon_can_read(self):
        request = factory.get('/1')
        response = object_permissions_view(request, pk='1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # Read list
    def test_can_read_list_permissions(self):
        request = factory.get('/', HTTP_AUTHORIZATION=self.credentials)
        response = object_permissions_list_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('id'), 1)

    def test_anon_can_read_list(self):
        request = factory.get('/')
        response = object_permissions_list_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].get('id'), 1)
