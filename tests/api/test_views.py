import json
from django.test import TestCase, override_settings
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from cims.auth.models import Role

from tests.helpers import get_perm


User = get_user_model()


# Function based views
@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims_auth', 'user')],
        PFP_IGNORE_PERMS={},
        )
class TestSetLDAPUser(TestCase):

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')

    def setUp(self):
        self.client = APIClient()

    def test_access(self):
        """Non-auth and non-staff denied access."""
        self.client.force_authenticate(user=None)
        # Unauthenticated
        response = self.client.post(reverse('user-set_ldap'))
        self.assertContains(
                response,
                '{"detail":"Authentication credentials were not provided."}',
                status_code=403,
                )
        # Not staff
        user1 = User.objects.create_user(username='user1', password='sekret')
        self.client.force_authenticate(user=user1)
        response = self.client.post(reverse('user-set_ldap'))
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )

    def test_get(self):
        """GET request receives empty response."""
        user1 = User.objects.create_user(username='user1', password='sekret', is_staff=True)
        self.client.force_authenticate(user=user1)
        response = self.client.get(reverse('user-set_ldap'))
        self.assertEqual(200, response.status_code)
        self.assertEqual(b'', response.content)

    def test_arg(self):
        """Absence of required argument raises exception."""
        user1 = User.objects.create_user(username='user1', password='sekret', is_staff=True)
        self.client.force_authenticate(user=user1)
        response = self.client.post(reverse('user-set_ldap'))
        self.assertContains(
                response,
                '{"detail":"A username argument is required."}',
                status_code=400,
                )

    @override_settings()
    def test_add_user(self):
        """Adding an LDAP user. Requires LDAP auth settings to be overriden for full test suite."""
        user1 = User.objects.create_user(username='user1', password='sekret', is_staff=True)
        self.client.force_authenticate(user=user1)
        # Lacks required permission
        response = self.client.post(reverse('user-set_ldap'), {'username': 'nouser'})
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        # No point continuing if LDAP isn't configured.
        if not getattr(settings, 'AUTH_LDAP_SERVER_URI', False):
            return
        # User doesn't exist/LDAPerror
        user2 = User.objects.create_user(username='user2', password='sekret', is_staff=True)
        self.client.force_authenticate(user=user2)
        perm = get_perm('cims_auth', 'user', 'add_user')
        user2.user_permissions.add(perm)
        response = self.client.post(reverse('user-set_ldap'), {'username': 'nouser'})
        self.assertContains(
                response,
                '{"detail":"Not found."}',
                status_code=404,
                )
        self.assertRaises(ObjectDoesNotExist, *[User.objects.get], **{'username': 'nouser'})
        # Successfully add user.
        response = self.client.post(reverse('user-set_ldap'), {'username': ''})
        self.assertContains(
                response,
                '{"id":,"username":"","first_name":"","last_name":"","email":"","is_active":true,'
                '"is_staff":false,"is_superuser":false,"last_login":,"date_joined":""}',
                )
        ldap_user = User.objects.get(username='')
        self.assertFalse(ldap_user.has_usable_password())

    def test_set_user_password_unusable(self):
        """Set an existing user to have an unusable password."""
        user1 = User.objects.create_user(username='user1', password='sekret', is_staff=True)
        self.client.force_authenticate(user=user1)
        # Lacks required permission
        response = self.client.post(reverse('user-set_ldap'), {'username': 'nouser'})
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        # User doesn't exist, lack permission to add user.
        user2 = User.objects.create_user(username='user2', password='sekret', is_staff=True)
        self.client.force_authenticate(user=user2)
        perm = get_perm('cims_auth', 'user', 'change_user')
        user2.user_permissions.add(perm)
        response = self.client.post(reverse('user-set_ldap'), {'username': 'user3'})
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        self.assertRaises(ObjectDoesNotExist, *[User.objects.get], **{'username': 'user3'})
        # Successfully add user.
        response = self.client.post(reverse('user-set_ldap'), {'username': 'user1'})
        user1.refresh_from_db()
        self.assertContains(
                response,
                '{{"id":{},"username":"{}","first_name":"","last_name":"","email":"",'
                '"is_active":true,"is_staff":true,"is_superuser":false,"last_login":null,'
                '"date_joined":"{}"}}'.format(
                    user1.pk,
                    user1.username,
                    user1.date_joined.isoformat(),
                    ),
                )
        self.assertFalse(user1.has_usable_password())


# Model based viewsets
@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims_auth', 'role')],
        PFP_IGNORE_PERMS={},
        )
class TestRoleViewset(TestCase):

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')
        cls.serializer_fields = ('id', 'name', 'users', 'parent_roles', 'child_roles')

    def setUp(self):
        self.client = APIClient()

    def test_anon_user(self):
        """Anonymous user can't see or do anything."""
        role1 = Role.objects.create(name='Role1')
        self.client.force_authenticate(user=None)
        # List roles
        response = self.client.get(reverse('role-list'))
        self.assertContains(
                response,
                '{"detail":"Authentication credentials were not provided."}',
                status_code=403,
                )
        # Specific role
        response = self.client.get(reverse('role-detail', args=[role1.pk]))
        self.assertContains(
                response,
                '{"detail":"Authentication credentials were not provided."}',
                status_code=403,
                )
        # Change role fails
        response = self.client.put(reverse('role-detail', args=[role1.pk]), {'name': 'Foo'})
        self.assertContains(
                response,
                '{"detail":"Authentication credentials were not provided."}',
                status_code=403,
                )
        self.assertRaises(ObjectDoesNotExist, *[Role.objects.get], **{'name': 'Foo'})

    def test_user(self):
        """Users without permission can't see or do anything."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        role1 = Role.objects.create(name='Role1')
        self.client.force_authenticate(user=user1)
        # List roles
        response = self.client.get(reverse('role-list'))
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        # Specific role
        response = self.client.get(reverse('role-detail', args=[role1.pk]))
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        # Change role fails
        response = self.client.put(reverse('role-detail', args=[role1.pk]), {'name': 'Foo'})
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        self.assertRaises(ObjectDoesNotExist, *[Role.objects.get], **{'name': 'Foo'})

    def test_user_with_perm(self):
        """User with permission can see/edit."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        role1 = Role.objects.create(name='Role1')
        perm1 = get_perm('cims_auth', 'role', 'change_role')
        user1.user_permissions.add(perm1)
        self.client.force_authenticate(user=user1)
        # List roles
        response = self.client.get(reverse('role-list'))
        self.assertEqual(200, response.status_code)
        roles = json.loads(str(response.content, 'utf-8'))
        self.assertEqual(Role.objects.count(), len(roles))
        # Specific role
        response = self.client.get(reverse('role-detail', args=[role1.pk]))
        self.assertEqual(200, response.status_code)
        role = json.loads(str(response.content, 'utf-8'))
        for field in self.serializer_fields:
            with self.subTest(field=field):
                self.assertTrue(field in role)
        self.assertEqual('Role1', role['name'])
        # Change role fails
        response = self.client.put(reverse('role-detail', args=[role1.pk]), {'name': 'Foo'})
        self.assertEqual(200, response.status_code)
        role = json.loads(str(response.content, 'utf-8'))
        for field in self.serializer_fields:
            with self.subTest(field=field):
                self.assertTrue(field in role)
        role1.refresh_from_db()
        self.assertEqual('Foo', role1.name)

    def test_user_with_pfp(self):
        """Check view/serializer chain works when using per-field permissions."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        role1 = Role.objects.create(name='Role1')
        perm1 = get_perm('cims_auth', 'role', 'change_role__name')
        user1.user_permissions.add(perm1)
        self.client.force_authenticate(user=user1)
        # Change user succeeds
        response = self.client.put(reverse('role-detail', args=[role1.pk]), {'name': 'Foo'})
        self.assertEqual(200, response.status_code)
        role = json.loads(str(response.content, 'utf-8'))
        role1.refresh_from_db()
        self.assertEqual('Foo', role['name'])

    def test_superuser_with_pfp(self):
        """Check superuser ignores per-field permissions."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        user1.is_superuser = True
        user1.save()
        role1 = Role.objects.create(name='Role1')
        role2 = Role.objects.create(name='Role2')
        perm1 = get_perm('cims_auth', 'role', 'change_role__child_roles')
        user1.user_permissions.add(perm1)
        self.client.force_authenticate(user=user1)
        # Change user succeeds
        response = self.client.put(
                reverse('role-detail', args=[role1.pk]),
                {'name': 'Foo', 'child_roles': [role2.pk]},
                )
        self.assertEqual(200, response.status_code)
        role = json.loads(str(response.content, 'utf-8'))
        role1.refresh_from_db()
        self.assertEqual('Foo', role['name'])
        self.assertQuerysetEqual(role1.child_roles.all(), [repr(role2)])


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims_auth', 'user')],
        PFP_IGNORE_PERMS={},
        )
class TestUserViewset(TestCase):

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')
        cls.serializer_fields = ('id', 'username', 'first_name', 'last_name', 'email',
                                 'is_active', 'is_staff', 'is_superuser', 'last_login',
                                 'date_joined')

    def setUp(self):
        self.client = APIClient()

    def test_anon_user(self):
        """Anonymous user can't see or do anything."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        self.client.force_authenticate(user=None)
        # List users
        response = self.client.get(reverse('user-list'))
        self.assertContains(response, '[]')
        # Specific user
        response = self.client.get(reverse('user-detail', args=[user1.pk]))
        self.assertContains(
                response,
                '{"detail":"Not found."}',
                status_code=404,
                )
        # Change user fails
        response = self.client.put(
                reverse('user-detail', args=[user1.pk]),
                {'username': 'user2', 'first_name': 'Fred'},
                )
        self.assertContains(
                response,
                '{"detail":"Authentication credentials were not provided."}',
                status_code=403,
                )
        user1.refresh_from_db()
        self.assertNotEqual('Fred', user1.first_name)

    def test_standard_user(self):
        """Standard user can see themselves."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        user2 = User.objects.create_user(username='user2', password='sekret')
        self.client.force_authenticate(user=user1)
        # List users
        response = self.client.get(reverse('user-list'))
        self.assertEqual(200, response.status_code)
        users = json.loads(str(response.content, 'utf-8'))
        self.assertEqual(1, len(users))
        user = users[0]
        self.assertEqual('user1', user['username'])
        for field in self.serializer_fields:
            with self.subTest(field=field):
                self.assertTrue(field in user)
        # Can't view another user
        response = self.client.get(reverse('user-detail', args=[user2.pk]))
        self.assertContains(
                response,
                '{"detail":"Not found."}',
                status_code=404,
                )
        # Can view themselves
        response = self.client.get(reverse('user-detail', args=[user1.pk]))
        self.assertEqual(200, response.status_code)
        user = json.loads(str(response.content, 'utf-8'))
        for field in self.serializer_fields:
            with self.subTest(field=field):
                self.assertTrue(field in user)
        self.assertEqual('user1', user['username'])
        # Change user fails
        response = self.client.put(
                reverse('user-detail', args=[user2.pk]),
                {'username': 'user2', 'first_name': 'Fred'},
                )
        self.assertContains(
                response,
                '{"detail":"You do not have permission to perform this action."}',
                status_code=403,
                )
        user2.refresh_from_db()
        self.assertNotEqual('Fred', user2.first_name)

    def test_user_with_perm(self):
        """Standard user with change permission can see/change all."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        user2 = User.objects.create_user(username='user2', password='sekret')
        perm1 = get_perm('cims_auth', 'user', 'change_user')
        user1.user_permissions.add(perm1)
        self.client.force_authenticate(user=user1)
        # List users, can see all
        response = self.client.get(reverse('user-list'))
        self.assertEqual(200, response.status_code)
        users = json.loads(str(response.content, 'utf-8'))
        self.assertEqual(User.objects.count(), len(users))
        # Can view another user
        response = self.client.get(reverse('user-detail', args=[user2.pk]))
        self.assertEqual(200, response.status_code)
        user = json.loads(str(response.content, 'utf-8'))
        for field in self.serializer_fields:
            with self.subTest(field=field):
                self.assertTrue(field in user)
        self.assertEqual('user2', user['username'])
        # Change user succeeds
        response = self.client.put(
                reverse('user-detail', args=[user2.pk]),
                {'username': 'user2', 'first_name': 'Fred'},
                )
        self.assertEqual(200, response.status_code)
        user = json.loads(str(response.content, 'utf-8'))
        for field in self.serializer_fields:
            with self.subTest(field=field):
                self.assertTrue(field in user)
        user2.refresh_from_db()
        self.assertEqual('Fred', user2.first_name)

    def test_user_with_pfp(self):
        """Check view/serializer chain works when using per-field permissions."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        user2 = User.objects.create_user(username='user2', password='sekret')
        pfp1 = get_perm('cims_auth', 'user', 'change_user__is_active')
        user1.user_permissions.add(pfp1)
        self.client.force_authenticate(user=user1)
        # Change user succeeds
        response = self.client.put(
                reverse('user-detail', args=[user2.pk]),
                {'username': 'user2', 'is_active': False, 'is_staff': True},
                )
        self.assertEqual(200, response.status_code)
        user = json.loads(str(response.content, 'utf-8'))
        user2.refresh_from_db()
        self.assertFalse(user['is_active'])
        self.assertFalse(user['is_staff'])

    def test_superuser_with_pfp(self):
        """Check superuser ignores per-field permissions."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        user2 = User.objects.create_user(username='user2', password='sekret')
        user1.is_superuser = True
        user1.save()
        pfp1 = get_perm('cims_auth', 'user', 'change_user__is_active')
        user1.user_permissions.add(pfp1)
        self.client.force_authenticate(user=user1)
        # Change user succeeds
        response = self.client.put(
                reverse('user-detail', args=[user2.pk]),
                {'username': 'user2', 'is_active': False, 'is_staff': True},
                )
        self.assertEqual(200, response.status_code)
        user = json.loads(str(response.content, 'utf-8'))
        user2.refresh_from_db()
        self.assertFalse(user['is_active'])
        self.assertTrue(user['is_staff'])
