import re
from datetime import date, timedelta
from unittest.mock import Mock, patch

from bs4 import BeautifulSoup
from django.contrib.admin.models import LogEntry
from django.contrib.auth import get_user_model
from django.contrib.auth.views import redirect_to_login
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from django.template.defaultfilters import slugify
from django.test import TestCase, override_settings
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from rest_framework import status
from reversion.models import Version

from cims import views
from cims.auth.models import UserObjectPermission
from cims.models import (Course, CourseInstance, CourseRelatedRole, CourseRelatedRoleTitle,
                         CourseVersion, DeliveryMode, LearningOutcome, OrganisationalUnit,
                         ReviewStatus, Schedule, StandardObjective, StandardsBody,
                         StandardsFramework, Task, Weight, WeightedLOToSO, WeightedTaskToLO,
                         WeightingScheme)
from .helpers import get_perm, setup_view

User = get_user_model()


class UserProfileTest(TestCase):

    def test_get_context_data(self):
        user = User.objects.create_user(username='user', password='sekret')
        course = Course.objects.validated_create(code='TEST123456')
        approved = ReviewStatus.objects.validated_create(name='Approved')
        draft = ReviewStatus.objects.validated_create(name='Draft')
        archived = ReviewStatus.objects.validated_create(name='Archived')
        for state in (approved, draft, archived):
            CourseVersion.objects.validated_create(
                    course=course,
                    status=state,
                    version_descriptor='{} Version'.format(state.name),
                    custodians=[user]
                    )
        archived_cv = CourseVersion.objects.get(status=archived)
        approved_cv = CourseVersion.objects.get(status=approved)
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        tutor = CourseRelatedRoleTitle.objects.validated_create(title='Tutor')
        lecturer = CourseRelatedRoleTitle.objects.validated_create(title='Lecturer')
        today = date.today()
        last_year = date(year=today.year-1, month=today.month, day=today.day)
        back_two_years = date(year=today.year-2, month=today.month, day=today.day)
        one_week = timedelta(days=7)
        # Archived classes
        archived_old_ci = CourseInstance.objects.validated_create(
                courseversion=archived_cv,
                instance_descriptor='Archived, old Instance',
                taught_by=[ou.pk],
                start_date=back_two_years,
                end_date=back_two_years + one_week,
                )
        CourseInstance.objects.validated_create(
                courseversion=archived_cv,
                instance_descriptor='Archived Instance',
                taught_by=[ou.pk],
                start_date=last_year,
                end_date=last_year + one_week,
                )
        CourseRelatedRole.objects.validated_create(
                user=user,
                role_title=tutor,
                courseinstance=archived_old_ci
                )
        # Published classes
        published_old_ci = CourseInstance.objects.validated_create(
                courseversion=approved_cv,
                instance_descriptor='Published, old Instance',
                taught_by=[ou.pk],
                start_date=last_year,
                end_date=last_year + one_week,
                conveners=[user]
                )
        published_ci = CourseInstance.objects.validated_create(
                courseversion=approved_cv,
                instance_descriptor='Published Instance',
                taught_by=[ou.pk],
                start_date=today,
                end_date=today + one_week,
                conveners=[user]
                )
        CourseRelatedRole.objects.validated_create(
                user=user,
                role_title=lecturer,
                courseinstance=published_ci
                )
        request = Mock()
        request.user = user
        view = setup_view(views.UserProfile(), request, **{'pk': user.pk})
        view.object = user
        context = view.get_context_data()
        self.assertQuerysetEqual(context['current_courseinstances'], [repr(published_ci)])
        self.assertQuerysetEqual(context['past_courseinstances'], [repr(published_old_ci)])
        self.assertQuerysetEqual(context['current_courseversions'], [repr(approved_cv)])
        self.assertQuerysetEqual(context['archived_courseversions'], [repr(archived_cv)])
        self.assertEqual(len(context['related_roles']), 1)
        self.assertQuerysetEqual(context['related_roles']['Lecturer'], [repr(published_ci)])
        self.assertEqual(len(context['old_roles']), 1)
        self.assertQuerysetEqual(context['old_roles']['Tutor'], [repr(archived_old_ci)])

    def test_redirect_to_login(self):
        """Unauthenticated user is redirected to login when using the personal profile shortcut."""
        self.client.logout()
        response = self.client.get(reverse('cims:user.profile_home'))
        self.assertEqual(response.url, reverse('cims:login'))


class CourseListTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.course1 = Course.objects.validated_create(code='TEST123456')
        Course.objects.validated_create(code='TEST654321')

    def test_get_queryset_no_filter(self):
        """
        No filter returns all courses.
        """
        response = self.client.get(reverse('cims:courses.list'))
        self.assertEqual(
                Course.objects.count(),
                response.context['course_list'].count(),
                )

    def test_get_queryset_empty_filter(self):
        """
        Empty filter returns all courses.
        """
        response = self.client.get(reverse('cims:courses.list'), data={'q': ''})
        self.assertEqual(
                Course.objects.count(),
                response.context['course_list'].count(),
                )

    def test_get_queryset_with_filter(self):
        """
        Doesn't display courses without a filter match, includes courses that lack an approved
        CourseVersion.
        """
        response = self.client.get(reverse('cims:courses.list'), data={'q': '123456'})
        self.assertQuerysetEqual(response.context['course_list'], [repr(self.course1)])

    def test_get_queryset_courseversion_filter(self):
        """
        The filter should only match fields from courseversions that have an approved status,
        filter matches on course codes OR courseversion fields.
        """
        course1 = Course.objects.validated_create(code='TEST111111')
        course2 = Course.objects.validated_create(code='TEST222222')
        course3 = Course.objects.validated_create(code='TEST333333')
        approved = ReviewStatus.objects.validated_create(name='Approved')
        draft = ReviewStatus.objects.validated_create(name='Draft')
        CourseVersion.objects.validated_create(
                course=course2,
                status=approved,
                version_descriptor='Test Version 111111',
                )
        CourseVersion.objects.validated_create(
                course=course3,
                status=draft,
                version_descriptor='Test Version 111111',
                )
        response = self.client.get(reverse('cims:courses.list'), data={'q': '111111'})
        self.assertQuerysetEqual(response.context['course_list'], [repr(course1), repr(course2)])


class ConvenerListTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        course1 = Course.objects.validated_create(code='TEST123456')
        course2 = Course.objects.validated_create(code='TEST654321')
        approved = ReviewStatus.objects.validated_create(name='Approved')
        draft = ReviewStatus.objects.validated_create(name='Draft')
        cls.cv1 = CourseVersion.objects.validated_create(
                course=course1,
                status=approved,
                version_descriptor='Test Version 1',
                )
        # Draft CourseVersion, conveners of CourseInstances attached to this shouldn't show up in
        # results
        cls.cv2 = CourseVersion.objects.validated_create(
                course=course2,
                status=draft,
                version_descriptor='Test Version 2',
                )
        cls.ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        cls.user1 = User.objects.create_user(username='user1', password='sekret',
                                             first_name='user1')
        cls.user2 = User.objects.create_user(username='user2', password='sekret')
        CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test Instance 1',
                taught_by=[cls.ou.pk],
                start_date=date.today(),
                end_date=date.today() + timedelta(days=1),
                conveners=[cls.user1]
                )
        CourseInstance.objects.validated_create(
                courseversion=cls.cv2,
                instance_descriptor='Test Instance',
                taught_by=[cls.ou.pk],
                start_date=date.today(),
                end_date=date.today() + timedelta(days=1),
                conveners=[cls.user2]
                )
        # CourseInstance that started more than a year ago, conveners of this shouldn't show up in
        # results
        CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test Instance',
                taught_by=[cls.ou.pk],
                start_date=date.today() - timedelta(days=400),
                end_date=date.today(),
                conveners=[cls.user2]
                )

    def test_get_queryset(self):
        """
        Only returns users who are convener of a CourseInstance with a start date of this year or
        later, linked to an approved CourseVersion.
        """
        response = self.client.get(reverse('cims:conveners.list'))
        self.assertQuerysetEqual(response.context['user_list'], [repr(self.user1)])

    def test_get_queryset_empty_filter(self):
        """
        Returns all users who are convener of a CourseInstance with a start date of this year or
        later, linked to an approved CourseVersion.
        """
        response = self.client.get(reverse('cims:conveners.list'))
        self.assertQuerysetEqual(response.context['user_list'], [repr(self.user1)])

    def test_get_queryset_with_filter(self):
        """Doesn't display users without a filter match in their name or the class course code."""
        CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Test Instance 4',
                taught_by=[self.ou.pk],
                start_date=date.today(),
                end_date=date.today() + timedelta(days=1),
                conveners=[self.user2]
                )
        response = self.client.get(reverse('cims:conveners.list'))
        self.assertQuerysetEqual(
                response.context['user_list'],
                [repr(self.user1), repr(self.user2)],
                ordered=False
                )
        response = self.client.get(reverse('cims:conveners.list'), data={'q': 'user1'})
        self.assertQuerysetEqual(response.context['user_list'], [repr(self.user1)])


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims', 'courseinstance')],
        )
class CourseInstanceImportTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = Course.objects.validated_create(code='TEST123456')
        course2 = Course.objects.validated_create(code='TEST654321')
        review_status = ReviewStatus.objects.validated_create(name='Approved')
        cls.courseversion = CourseVersion.objects.validated_create(
                course=course,
                status=review_status,
                version_descriptor='Test Version',
                )
        cv2 = CourseVersion.objects.validated_create(
                course=course2,
                status=review_status,
                version_descriptor='Test Version 2',
                )
        cls.ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        cls.ou2 = OrganisationalUnit.objects.validated_create(name='Test OU 2', short_name='TOU2')
        cls.delivery_mode = DeliveryMode.objects.validated_create(mode='Online')
        cls.courseinstance = CourseInstance.objects.validated_create(
                courseversion=cls.courseversion,
                instance_descriptor='Test Instance',
                taught_by=[cls.ou.pk],
                start_date=date.today(),
                end_date=date.today() + timedelta(days=1),
                )
        cls.source = CourseInstance.objects.validated_create(
                courseversion=cv2,
                instance_descriptor='Source Instance',
                taught_by=[cls.ou2.pk],
                start_date=date.today() - timedelta(days=10),
                end_date=date.today() + timedelta(days=10),
                delivery_mode=cls.delivery_mode
                )
        cls.source2 = CourseInstance.objects.validated_create(
                courseversion=cls.courseversion,
                instance_descriptor='Source Instance 2',
                taught_by=[cls.ou2.pk],
                start_date=date.today() - timedelta(days=10),
                end_date=date.today() + timedelta(days=10),
                delivery_mode=cls.delivery_mode
                )
        cls.lecturer = CourseRelatedRoleTitle.objects.validated_create(title='Lecturer')
        cls.tutor = CourseRelatedRoleTitle.objects.validated_create(title='Tutor')
        cls.assistant = CourseRelatedRoleTitle.objects.validated_create(title='Lab assistant')
        call_command('pfp-makeperms')
        call_command('createinitialrevisions', verbosity=0)
        cls.ci_ctype = ContentType.objects.get_for_model(CourseInstance)

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.source.refresh_from_db()

    def test_require_POST(self):
        """GET requests return error."""
        response = self.client.get(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_requires_auth(self):
        """Unauthenticated users receives redirect to login page."""
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(
                response.url,
                redirect_to_login(
                    reverse('cims:courseinstance.import', args=[self.courseinstance.pk])
                    ).url,
                )

    def test_requires_permissions(self):
        """
        Authenticated user without appropiate permission receive a redirect to the login page.
        """
        self.client.force_login(self.user)
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(
                response.url,
                redirect_to_login(
                    reverse('cims:courseinstance.import', args=[self.courseinstance.pk])
                    ).url,
                )

    def test_target_doesnt_exist(self):
        """Return 404 when the target CourseInstance ID doesn't exist."""
        self.user.user_permissions.add(get_perm('cims', 'courseinstance', 'change_courseinstance'))
        self.client.force_login(self.user)
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk + 1000])
                )
        self.assertContains(response, '', status_code=404)

    def test_requires_object_permissions(self):
        """
        Authenticated user without appropiate object specific permission get redirected to the
        login page.
        """
        UserObjectPermission.objects.create(
                user=self.user,
                permission=get_perm('cims', 'courseinstance', 'change_courseinstance'),
                content_type=self.ci_ctype,
                object_id='10',
                )
        self.client.force_login(self.user)
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk])
                )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(
                response.url,
                redirect_to_login(
                    reverse('cims:courseinstance.import', args=[self.courseinstance.pk])
                    ).url,
                )

    @patch('django.contrib.messages.error')
    def test_source_doesnt_exist(self, msgs):
        """
        Redirect to the detail page for the target object and display a message when the source
        CourseInstance ID doesn't exist.
        """
        self.user.user_permissions.add(get_perm('cims', 'courseinstance', 'change_courseinstance'))
        self.client.force_login(self.user)
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.courseinstance.pk + 1000}
                )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        msgs.assert_called_once_with(
                response.wsgi_request,
                'The import form failed validation.'
                )
        self.assertEqual(response.url, self.courseinstance.get_absolute_url())

    @patch('django.contrib.messages.error')
    def test_failed_validation(self, msgs):
        """
        When the update CourseInstance fails validation the changes are wound back, and no log
        entries or revisions are made.
        """
        self.user.user_permissions.add(get_perm('cims', 'courseinstance', 'change_courseinstance'))
        self.client.force_login(self.user)
        self.source.exams = 'Every Week!'
        self.source.start_date = date.today()
        self.source.end_date = date.today() - timedelta(days=1)
        self.source.save()
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        self.assertFalse(target.exams)
        self.assertFalse(Version.objects.get_for_object(target).count() > 1)
        self.assertFalse(
                LogEntry.objects.filter(
                    content_type=ContentType.objects.get_for_model(CourseInstance),
                    object_id=target.pk
                    ).count()
                )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        msgs.assert_called_once_with(
                response.wsgi_request,
                'The import was aborted as the target failed validation after import. The error '
                'was: End date must be greater than or equal to the start date. end_date >= '
                'start_date.',
                )
        self.assertEqual(response.url, self.courseinstance.get_update_url())

    @patch('django.contrib.messages.success')
    def test_success(self, msgs):
        """
        When the import succeeds only fields the user has permissions for are updated, a message is
        appended to the instance_descriptor field.

        Import a variety of different field types e.g. FK, m2m, date to make sure more complex
        field types are copied properly.
        """
        for field in ['exams', 'delivery_mode', 'taught_by', 'end_date', 'conveners']:
            UserObjectPermission.objects.create(
                    user=self.user,
                    content_type=self.ci_ctype,
                    object_id=self.courseinstance.pk,
                    permission=get_perm(
                        'cims',
                        'courseinstance',
                        'change_courseinstance__{}'.format(field)
                        ),
                )
        self.client.force_login(self.user)
        self.source.exams = 'Every Week!'
        self.source.conveners.add(self.user)
        self.source.save()
        self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk, 'import_schedule': False,
                 'import_task': False, 'import_courserelatedrole': False}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        self.assertEqual(target.exams, self.source.exams)
        self.assertEqual(target.delivery_mode, self.source.delivery_mode)
        self.assertEqual(target.end_date, self.source.end_date)
        self.assertNotEqual(target.start_date, self.source.start_date)
        self.assertQuerysetEqual(target.conveners.all(), [repr(self.user)])
        self.assertEqual('Test Instance', target.instance_descriptor)

    @patch('django.contrib.messages.success')
    def test_success_response(self, msgs):
        """
        When the import succeeds a message is logged and the user is redirected to the form page
        for the target object.
        """
        UserObjectPermission.objects.create(
                user=self.user,
                content_type=self.ci_ctype,
                object_id=self.courseinstance.pk,
                permission=get_perm(
                    'cims',
                    'courseinstance',
                    'change_courseinstance__exams'
                    ),
                )
        self.client.force_login(self.user)
        self.source.exams = 'Every Week!'
        self.source.save()
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        msgs.assert_called_once_with(
                response.wsgi_request,
                '{} updated with data imported from {}'.format(str(target), str(self.source)),
                )
        self.assertEqual(response.url, self.courseinstance.get_update_url())

    @patch('django.contrib.messages.success')
    def test_success_create_logs_and_version(self, msgs):
        """
        When the import succeeds a LogEntry is create, a reversion Version is created.
        """
        UserObjectPermission.objects.create(
                user=self.user,
                content_type=self.ci_ctype,
                object_id=self.courseinstance.pk,
                permission=get_perm(
                    'cims',
                    'courseinstance',
                    'change_courseinstance__exams'
                    ),
                )
        self.client.force_login(self.user)
        self.source.exams = 'Every Week!'
        self.source.save()
        self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        self.assertTrue(Version.objects.get_for_object(target).count() > 1)
        self.assertTrue(
                LogEntry.objects.filter(
                    content_type=ContentType.objects.get_for_model(CourseInstance),
                    object_id=target.pk
                    ).count()
                )

    @patch('django.contrib.messages.warning')
    def test_related_model_no_model_permission(self, msgs):
        """
        If the user lacks model permissions to add/remove related models those related models are
        not imported but the rest of the import proceeds.
        """
        self.user.user_permissions.add(get_perm('cims', 'courseinstance', 'change_courseinstance'))
        self.client.force_login(self.user)
        self.source.exams = 'Every Week!'
        self.source.save()
        CourseRelatedRole.objects.validated_create(
                courseinstance=self.source,
                user=self.user,
                role_title=self.lecturer
                )
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk, 'import_courserelatedrole': True}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(target.exams, self.source.exams)
        self.assertFalse(target.relatedroles.exists())
        self.assertFalse(msgs.called)

    @patch('django.contrib.messages.warning')
    def test_related_model_no_object_permission(self, msgs):
        """
        If the user lacks object permissions to remove related models a message is logged but
        the rest of the import proceeds.
        """
        self.user.user_permissions.add(
                get_perm('cims', 'courseinstance', 'change_courseinstance'),
                get_perm('cims', 'courserelatedrole', 'add_courserelatedrole'),
                )
        self.client.force_login(self.user)
        # Instance user will lack permission to
        CourseRelatedRole.objects.validated_create(
                courseinstance=self.courseinstance,
                user=self.user,
                role_title=self.lecturer
                )
        # Instance user has permission to
        role = CourseRelatedRole.objects.validated_create(
                courseinstance=self.courseinstance,
                user=self.user,
                role_title=self.lecturer
                )
        UserObjectPermission.objects.create(
                user=self.user,
                permission=get_perm('cims', 'courserelatedrole', 'delete_courserelatedrole'),
                content_type=ContentType.objects.get_for_model(CourseRelatedRole),
                object_id=role.pk,
                )
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk, 'import_courserelatedrole': True}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertTrue(target.relatedroles.count())
        msgs.assert_called_once_with(
                response.wsgi_request,
                'Roles were not imported as you lack required permissions.'
                )

    @patch('django.contrib.messages.success')
    def test_related_model_import_successful(self, msgs):
        """
        If the user has all the required permissions existing related objects are removed and those
        from the source CourseInstance are copied.
        """
        self.user.user_permissions.add(
                get_perm('cims', 'courseinstance', 'change_courseinstance'),
                get_perm('cims', 'courserelatedrole', 'add_courserelatedrole'),
                get_perm('cims', 'courserelatedrole', 'delete_courserelatedrole'),
                )
        self.client.force_login(self.user)
        role1 = CourseRelatedRole.objects.validated_create(
                courseinstance=self.courseinstance,
                user=self.user,
                role_title=self.lecturer
                )
        for title in [self.tutor, self.assistant]:
            CourseRelatedRole.objects.validated_create(
                    courseinstance=self.source,
                    user=self.user,
                    role_title=title
                    )
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk, 'import_courserelatedrole': True}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRaises(ObjectDoesNotExist, CourseRelatedRole.objects.get, **{'pk': role1.pk})
        self.assertEqual(2, target.relatedroles.count())
        self.assertEqual(2, self.source.relatedroles.count())
        self.assertQuerysetEqual(
                CourseRelatedRoleTitle.objects.filter(
                    courserelatedroles__in=target.relatedroles.all()
                    ),
                [repr(self.tutor), repr(self.assistant)],
                ordered=False
                )
        # Check log entries made
        for role in target.relatedroles.all():
            with self.subTest(role=role):
                logentry = LogEntry.objects.get(
                        content_type=ContentType.objects.get_for_model(role),
                        object_id=role.pk,
                        )
                self.assertEqual('[{"added": {}}]', logentry.change_message)
                self.assertTrue(Version.objects.get_for_object(role).exists())

        msgs.assert_called_once_with(
                response.wsgi_request,
                '{} updated with data imported from {}'.format(str(target), str(self.source)),
                )

    @patch('django.contrib.messages.warning')
    def test_mapping_import_not_possible(self, msgs):
        """
        If the source and target have different CourseVersions then a message is logged that
        mappings cannot be imported.
        """
        self.user.user_permissions.add(
                get_perm('cims', 'courseinstance', 'change_courseinstance'),
                get_perm('cims', 'task', 'add_task'),
                get_perm('cims', 'task', 'delete_task'),
                )
        self.client.force_login(self.user)
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source.pk, 'import_task': True}
                )
        msgs.assert_called_once_with(
                response.wsgi_request,
                'Task -> Learning Outcome mappings were not imported. These mappings can '
                'only be imported when the source and target Classes have a common '
                'parent Course Version.'
                )

    @patch('django.contrib.messages.warning')
    def test_mapping_import_no_permission(self, msgs):
        """
        If the user lacks permission to create the new mappings then a message is logged that
        mappings cannot be imported.
        """
        self.user.user_permissions.add(
                get_perm('cims', 'courseinstance', 'change_courseinstance'),
                get_perm('cims', 'task', 'add_task'),
                get_perm('cims', 'task', 'delete_task'),
                )
        self.client.force_login(self.user)
        response = self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source2.pk, 'import_task': True}
                )
        msgs.assert_called_once_with(
                response.wsgi_request,
                'Task -> Learning Outcome mappings were not imported as you lack '
                'required permissions.'
                )

    @patch('django.contrib.messages.warning')
    def test_mapping_import(self, msgs):
        """
        Mappings are copied along with any associated Weights, original mappings are preserved.
        """
        lo1 = LearningOutcome.objects.validated_create(
                courseversion=self.courseversion,
                outcome='Something good.'
                )
        lo2 = LearningOutcome.objects.validated_create(
                courseversion=self.courseversion,
                outcome='Something learned.'
                )
        wscheme1 = WeightingScheme.objects.validated_create(name='Scheme 1')
        wscheme2 = WeightingScheme.objects.validated_create(name='Scheme 2')
        weight1 = Weight.objects.validated_create(weightingscheme=wscheme1, value='Low')
        weight2 = Weight.objects.validated_create(weightingscheme=wscheme2, value='1')
        self.courseversion.weightingschemes.add(wscheme1, wscheme2)
        task1 = Task.objects.validated_create(courseinstance=self.source2, title='Onerous task')
        wttl1 = WeightedTaskToLO.objects.validated_create(
                task=task1,
                learningoutcome=lo1,
                weights=[weight1, weight2]
                )
        WeightedTaskToLO.objects.validated_create(
                task=task1,
                learningoutcome=lo2,
                )
        self.user.user_permissions.add(
                get_perm('cims', 'courseinstance', 'change_courseinstance'),
                get_perm('cims', 'task', 'add_task'),
                get_perm('cims', 'task', 'delete_task'),
                get_perm('cims', 'weightedtasktolo', 'add_weightedtasktolo'),
                )
        self.client.force_login(self.user)
        self.client.post(
                reverse('cims:courseinstance.import', args=[self.courseinstance.pk]),
                {'source_courseinstance': self.source2.pk, 'import_task': True}
                )
        target = CourseInstance.objects.get(pk=self.courseinstance.pk)
        # No warning messages generated
        self.assertFalse(msgs.called)
        # Original mappings haven't changed
        wttl1.refresh_from_db()
        self.assertEqual(wttl1.task_id, task1.pk)
        self.assertQuerysetEqual(wttl1.weights.all().order_by('pk'), [repr(weight1), repr(weight2)])
        # Mappings were copied
        new_task1 = target.task_set.get(title='Onerous task')
        self.assertEqual(new_task1.weightedtasktolo_set.count(), 2)
        new_wttl1 = new_task1.weightedtasktolo_set.get(learningoutcome=lo1)
        self.assertQuerysetEqual(
                new_wttl1.weights.all().order_by('pk'),
                [repr(weight1), repr(weight2)]
                )
        new_wttl2 = new_task1.weightedtasktolo_set.get(learningoutcome=lo2)
        self.assertFalse(new_wttl2.weights.exists())
        # Version entries and logs created
        logentry = LogEntry.objects.get(
                content_type=ContentType.objects.get_for_model(new_wttl1),
                object_id=new_wttl1.pk,
                )
        self.assertEqual('[{"added": {}}]', logentry.change_message)
        self.assertTrue(Version.objects.get_for_object(new_wttl1).exists())


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class CourseVersionCreateTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.add_courseversion` is denied access."""
        view = setup_view(views.CourseVersionCreate(), self.request)
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.add_courseversion` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'courseversion', 'add_courseversion'))
        view = setup_view(views.CourseVersionCreate(), self.request)
        self.assertTrue(view.has_permission())


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims', 'courseversion'), ('cims', 'learningoutcome'),
                    ('cims', 'courseinstance')],
        )
class CourseVersionUpdateTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = Course.objects.validated_create(code='TEST123456')
        status = ReviewStatus.objects.validated_create(name='Approved')
        cls.courseversion = CourseVersion.objects.validated_create(
                course=course,
                status=status,
                version_descriptor='Test Version',
                )
        call_command('pfp-makeperms')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.change_courseversion` is denied access."""
        view = setup_view(
                views.CourseVersionUpdate(),
                self.request,
                **{'pk': self.courseversion.pk}
                )
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.change_courseversion` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'courseversion', 'change_courseversion'))
        view = setup_view(
                views.CourseVersionUpdate(),
                self.request,
                **{'pk': self.courseversion.pk}
                )
        self.assertTrue(view.has_permission())

    def test_pfp_form_integration(self):
        """
        Ensure fields the user lacks permissions for are disabled in the main form and formsets.
        """
        self.user.user_permissions.add(
            get_perm('cims', 'courseversion', 'change_courseversion__course'),
            get_perm('cims', 'learningoutcome', 'change_learningoutcome__courseversion'),
            get_perm('cims', 'courseinstance', 'change_courseinstance__courseversion'),
            )
        LearningOutcome.objects.validated_create(
            courseversion=self.courseversion,
            outcome='Stuff'
            )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        CourseInstance.objects.validated_create(
            courseversion=self.courseversion,
            instance_descriptor='Test Instance',
            taught_by=[ou.pk],
            start_date=date.today(),
            end_date=date.today() + timedelta(days=1)
            )
        self.client.force_login(self.user)
        response = self.client.get(self.courseversion.get_ui_update_url())
        self.assertFalse(response.context['form'].fields['course'].disabled)
        self.assertTrue(response.context['form'].fields['version_descriptor'].disabled)

        learningoutcome_form = response.context['formsets']['learning outcomes'][1].empty_form
        self.assertFalse(learningoutcome_form.fields['courseversion'].disabled)
        self.assertTrue(learningoutcome_form.fields['outcome'].disabled)

        # courseinstance_form = response.context['formsets']['classes'].empty_form
        # self.assertFalse(courseinstance_form.fields['courseversion'].disabled)
        # self.assertTrue(courseinstance_form.fields['instance_descriptor'].disabled)


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class CourseInstanceCreateTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.add_courseinstance` is denied access."""
        view = setup_view(views.CourseInstanceCreate(), self.request)
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.add_courseinstance` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'courseinstance', 'add_courseinstance'))
        view = setup_view(views.CourseInstanceCreate(), self.request)
        self.assertTrue(view.has_permission())


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims', 'courseinstance'), ('cims', 'schedule'), ('cims', 'task'),
                    ('cims', 'courserelatedrole')],
        )
class CourseInstanceUpdateTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = Course.objects.validated_create(code='TEST123456')
        status = ReviewStatus.objects.validated_create(name='Approved')
        cv = CourseVersion.objects.validated_create(
                course=course,
                status=status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        cls.courseinstance = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        call_command('pfp-makeperms')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.change_courseinstance` is denied access."""
        view = setup_view(
                views.CourseInstanceUpdate(),
                self.request,
                **{'pk': self.courseinstance.pk}
                )
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.change_courseinstance` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'courseinstance', 'change_courseinstance'))
        view = setup_view(
                views.CourseInstanceUpdate(),
                self.request,
                **{'pk': self.courseinstance.pk}
                )
        self.assertTrue(view.has_permission())

    def test_pfp_form_integration(self):
        """
        Ensure fields the user lacks permissions for are disabled in the main form and formsets.
        """
        self.user.user_permissions.add(
            get_perm('cims', 'courseinstance', 'change_courseinstance__courseversion'),
            get_perm('cims', 'schedule', 'change_schedule__courseinstance'),
            get_perm('cims', 'task', 'change_task__courseinstance'),
            get_perm('cims', 'courserelatedrole', 'change_courserelatedrole__courseinstance'),
            )
        lecturer = CourseRelatedRoleTitle.objects.validated_create(title='Lecturer')
        CourseRelatedRole.objects.validated_create(
                user=self.user,
                courseinstance=self.courseinstance,
                role_title=lecturer
                )
        Schedule.objects.validated_create(
                courseinstance=self.courseinstance,
                session_name='Week 2-4'
                )
        Task.objects.validated_create(courseinstance=self.courseinstance, title='Exam')
        self.client.force_login(self.user)
        response = self.client.get(self.courseinstance.get_ui_update_url())

        self.assertFalse(response.context['form'].fields['courseversion'].disabled)
        self.assertTrue(response.context['form'].fields['instance_descriptor'].disabled)

        # roles_form = response.context['formsets']['course related roles'].empty_form
        # self.assertFalse(roles_form.fields['courseinstance'].disabled)
        # self.assertTrue(roles_form.fields['user'].disabled)

        schedule_form = response.context['formsets']['schedules'][1].empty_form
        self.assertFalse(schedule_form.fields['courseinstance'].disabled)
        self.assertTrue(schedule_form.fields['session_name'].disabled)

        task_form = response.context['formsets']['tasks'][1].empty_form
        self.assertFalse(task_form.fields['courseinstance'].disabled)
        self.assertTrue(task_form.fields['title'].disabled)

    def test_get_context_data(self):
        """The CourseImportForm and import view URL are added to the context."""
        self.user.user_permissions.add(get_perm('cims', 'courseinstance', 'change_courseinstance'))
        self.client.force_login(self.user)
        response = self.client.get(self.courseinstance.get_ui_update_url())
        self.assertIn('import_form', response.context)
        self.assertEqual(
                response.context['import_view'],
                reverse_lazy('cims:courseinstance.import', args=[self.courseinstance.pk]),
                )

    def test_import_form_fields_enabled(self):
        """Checkboxes on the import form are enabled/disabled appropriately."""
        self.user.user_permissions.add(
                get_perm('cims', 'courseinstance', 'change_courseinstance'),
                get_perm('cims', 'task', 'add_task'),
                get_perm('cims', 'task', 'delete_task'),
                )
        self.client.force_login(self.user)
        response = self.client.get(self.courseinstance.get_ui_update_url())
        import_form = response.context['import_form']
        self.assertFalse(import_form.fields['import_task'].disabled)
        self.assertTrue(import_form.fields['import_task'].initial)
        self.assertTrue(import_form.fields['import_schedule'].disabled)
        self.assertFalse(import_form.fields['import_schedule'].initial)


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class LearningOutcomeCreateTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.add_learningoutcome` is denied access."""
        view = setup_view(views.LearningOutcomeCreate(), self.request)
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.add_learningoutcome` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'learningoutcome', 'add_learningoutcome'))
        view = setup_view(views.LearningOutcomeCreate(), self.request)
        self.assertTrue(view.has_permission())


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims', 'learningoutcome')],
        )
class LearningOutcomeUpdateTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = Course.objects.validated_create(code='TEST123456')
        status = ReviewStatus.objects.validated_create(name='Approved')
        cv = CourseVersion.objects.validated_create(
                course=course,
                status=status,
                version_descriptor='Test Version',
                )
        cls.learningoutcome = LearningOutcome.objects.validated_create(
                courseversion=cv,
                outcome='Stuff was learned.'
                )
        call_command('pfp-makeperms')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.change_learningoutcome` is denied access."""
        view = setup_view(
                views.LearningOutcomeUpdate(),
                self.request,
                **{'pk': self.learningoutcome.pk}
                )
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.change_learningoutcome` gets access."""
        self.user.user_permissions.add(
                get_perm('cims', 'learningoutcome', 'change_learningoutcome')
                )
        view = setup_view(
                views.LearningOutcomeUpdate(),
                self.request,
                **{'pk': self.learningoutcome.pk}
                )
        self.assertTrue(view.has_permission())

    def test_pfp_form_integration(self):
        """
        Ensure fields the user lacks permissions for are disabled in the main form and formsets.
        """
        self.user.user_permissions.add(
            get_perm('cims', 'learningoutcome', 'change_learningoutcome__courseversion'),
            )
        self.client.force_login(self.user)
        response = self.client.get(self.learningoutcome.get_ui_update_url())
        self.assertFalse(response.context['form'].fields['courseversion'].disabled)
        self.assertTrue(response.context['form'].fields['outcome'].disabled)


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class ScheduleCreateTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.add_schedule` is denied access."""
        view = setup_view(views.ScheduleCreate(), self.request)
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.add_schedule` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'schedule', 'add_schedule'))
        view = setup_view(views.ScheduleCreate(), self.request)
        self.assertTrue(view.has_permission())


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims', 'schedule')],
        )
class ScheduleUpdateTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = Course.objects.validated_create(code='TEST123456')
        status = ReviewStatus.objects.validated_create(name='Approved')
        cv = CourseVersion.objects.validated_create(
                course=course,
                status=status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        cls.schedule = Schedule.objects.validated_create(
                courseinstance=ci,
                session_name='Week 2-4'
                )
        call_command('pfp-makeperms')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.change_schedule` is denied access."""
        view = setup_view(views.ScheduleUpdate(), self.request, **{'pk': self.schedule.pk})
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.change_schedule` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'schedule', 'change_schedule'))
        view = setup_view(views.ScheduleUpdate(), self.request, **{'pk': self.schedule.pk})
        self.assertTrue(view.has_permission())

    def test_pfp_form_integration(self):
        """
        Ensure fields the user lacks permissions for are disabled in the main form and formsets.
        """
        self.user.user_permissions.add(
            get_perm('cims', 'schedule', 'change_schedule__courseinstance'),
            )
        self.client.force_login(self.user)
        response = self.client.get(self.schedule.get_ui_update_url())
        self.assertFalse(response.context['form'].fields['courseinstance'].disabled)
        self.assertTrue(response.context['form'].fields['session_name'].disabled)


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class TaskCreateTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.add_task` is denied access."""
        view = setup_view(views.TaskCreate(), self.request)
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.add_task` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'task', 'add_task'))
        view = setup_view(views.TaskCreate(), self.request)
        self.assertTrue(view.has_permission())


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims', 'task')],
        )
class TaskUpdateTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = Course.objects.validated_create(code='TEST123456')
        status = ReviewStatus.objects.validated_create(name='Approved')
        cv = CourseVersion.objects.validated_create(
                course=course,
                status=status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        ci = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        cls.task = Task.objects.validated_create(courseinstance=ci, title='Exam')
        call_command('pfp-makeperms')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_user_needs_permission(self):
        """User without `cims.change_task` is denied access."""
        view = setup_view(views.TaskUpdate(), self.request, **{'pk': self.task.pk})
        self.assertFalse(view.has_permission())

    def test_user_has_permission(self):
        """User with `cims.change_task` gets access."""
        self.user.user_permissions.add(get_perm('cims', 'task', 'change_task'))
        view = setup_view(views.TaskUpdate(), self.request, **{'pk': self.task.pk})
        self.assertTrue(view.has_permission())

    def test_pfp_form_integration(self):
        """
        Ensure fields the user lacks permissions for are disabled in the main form and formsets.
        """
        self.user.user_permissions.add(
            get_perm('cims', 'task', 'change_task__courseinstance'),
            )
        self.client.force_login(self.user)
        response = self.client.get(self.task.get_ui_update_url())
        self.assertFalse(response.context['form'].fields['courseinstance'].disabled)
        self.assertTrue(response.context['form'].fields['title'].disabled)


class TestMiscViews(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_logout_link_user_unusable_password(self):
        """
        Make sure the logout link displays correctly for valid users without usable passwords (i.e.
        users created via LDAP authentication).
        """
        user = User.objects.create_user(username='user', password='sekret')
        # Un-authenticated user gets a login link
        response = self.client.get('/')
        self.assertContains(
                response,
                '<a class="nav-item nav-link text-light" href="/login/">Log in</a>',
                html=True
                )
        # Authenticated user gets a change password and logout link
        self.client.force_login(user)
        response = self.client.get('/')
        self.assertContains(
                response,
                '<a class="dropdown-item" href="/logout/">Log out</a>',
                html=True
                )
        self.assertContains(
                response,
                '<a class="dropdown-item" href="/password_change/">Change password</a>',
                html=True
                )
        # Authenticated user with unusable password gets logout link only
        self.client.logout()
        user.set_unusable_password()
        user.save()
        self.client.force_login(user)
        response = self.client.get('/')
        self.assertContains(
                response,
                '<a class="dropdown-item" href="/logout/">Log out</a>',
                html=True
                )
        self.assertNotContains(
                response,
                '<a class="dropdown-item" href="/password_change/">Change password</a>',
                html=True
                )


class TestStandardsViews(TestCase):

    @classmethod
    def setUpTestData(cls):
        start_date = timezone.now()
        end_date = start_date + timedelta(days=60)
        cls.college = OrganisationalUnit.objects.validated_create(
                name='College of Engineering and Computer Science',
                short_name='CECS',
                )
        cls.school = OrganisationalUnit.objects.validated_create(
                name='Research School of Computer Science',
                short_name='RSCS',
                parent=cls.college,
                )
        cls.course1 = Course.objects.validated_create(
                code='COMP1234',
                )
        status = ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = CourseVersion.objects.validated_create(
                course=cls.course1,
                version_descriptor='The first course version',
                status=status,
                )
        cls.ci1 = CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Minimal course instance',
                start_date=start_date,
                end_date=end_date,
                taught_by=[cls.school],
                )
        # Standards bodies etc.
        acs = StandardsBody.objects.validated_create(
                name='Australian Computer Society',
                short_name='ACS',
                )
        ea = StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        cbok = StandardsFramework.objects.validated_create(
                standardsbody=acs,
                name='Australian Computer Society ICT Profession Common Body of Knowledge',
                short_name='ACS CBOK',
                )
        stage1 = StandardsFramework.objects.validated_create(
                standardsbody=ea,
                name='Engineers Australia Stage 1 Competencies',
                short_name='EA Stage 1',
                )
        StandardsFramework.objects.validated_create(
                standardsbody=ea,
                name='Engineers Australia Stage 2 Competencies',
                short_name='EA Stage 2',
                )
        ps = StandardObjective.objects.validated_create(
                standardsframework=cbok,
                name='Problem Solving',
                short_name='PS',
                description='Solving problems is something that people appreciate...'
                )
        pk = StandardObjective.objects.validated_create(
                standardsframework=cbok,
                name='Professional Knowledge',
                short_name='PK',
                description='Probably a good idea to know how to do stuff...',
                )
        ks = StandardObjective.objects.validated_create(
                standardsframework=stage1,
                name='Knowledge and Skill base',
                short_name='1',
                )
        eaa = StandardObjective.objects.validated_create(
                standardsframework=stage1,
                name='Engineering Application Ability',
                short_name='2',
                )
        eaa_11 = StandardObjective.objects.validated_create(
                parent=ks,
                name='1.1',
                short_name='1',
                description=('Comprehensive, theory based understanding of the underpinning '
                             'natural and physical sciences and the engineering fundamentals '
                             'applicable to the engineering discipline.'),
                )
        eaa_12 = StandardObjective.objects.validated_create(
                parent=ks,
                name='1.2',
                short_name='2',
                description=('Conceptual understanding of the mathematics, numerical analysis, '
                             'statistics, and computer and information sciences which underpin '
                             'the engineering discipline.'),
                )
        StandardObjective.objects.validated_create(
                parent=ks,
                name='1.3',
                short_name='3',
                description=('In-depth understanding of specialist bodies of knowledge within the '
                             'engineering discipline.'),
                )
        eaa_14 = StandardObjective.objects.validated_create(
                parent=ks,
                name='1.4',
                short_name='4',
                description=('Discernment of knowledge development and research directions within '
                             'the engineering discipline.'),
                )
        StandardObjective.objects.validated_create(
                parent=ks,
                name='1.5',
                short_name='5',
                description=('Knowledge of engineering design practice and contextual factors '
                             'impacting the engineering discipline.'),
                )
        eaa_16 = StandardObjective.objects.validated_create(
                parent=ks,
                name='1.6',
                short_name='6',
                description=('Understanding of the scope, principles, norms, accountabilities and '
                             'bounds of sustainable engineering practice in the specific '
                             'discipline.'),
                )
        eaa_21 = StandardObjective.objects.validated_create(
                parent=eaa,
                name='2.1',
                short_name='1',
                description=('Application of established engineering methods to complex '
                             'engineering problem solving.'),
                )
        StandardObjective.objects.validated_create(
                parent=eaa,
                name='2.2',
                short_name='2',
                description=('Fluent application of engineering techniques, tools and resources.'),
                )
        eaa_23 = StandardObjective.objects.validated_create(
                parent=eaa,
                name='2.3',
                short_name='3',
                description=('Application of systematic engineering synthesis and design '
                             'processes.'),
                )
        eaa_24 = StandardObjective.objects.validated_create(
                parent=eaa,
                name='2.4',
                short_name='4',
                description=('Application of systematic approaches to the conduct and management '
                             'of engineering projects.'),
                )
        # Course version with learning outcomes etc.
        cls.cv2 = CourseVersion.objects.validated_create(
                course=cls.course1,
                version_descriptor='Version with learning outcomes etc.',
                status=status,
                standards_frameworks=[cbok, stage1],
                )
        cls.cv2.objectives.add(ps, pk, eaa_11, eaa_12, eaa_14, eaa_16, eaa_21, eaa_23, eaa_24)
        lo1 = LearningOutcome.objects.validated_create(
                courseversion=cls.cv2,
                outcome='People learned some stuff.',
                )
        lo2 = LearningOutcome.objects.validated_create(
                courseversion=cls.cv2,
                outcome='People did some stuff.',
                )
        for objective in [pk, eaa_11, eaa_12, eaa_14, eaa_16]:
            WeightedLOToSO.objects.validated_create(
                    learningoutcome=lo1,
                    standardobjective=objective,
                    )
        for objective in [eaa_21, eaa_23, eaa_24]:
            WeightedLOToSO.objects.validated_create(
                    learningoutcome=lo2,
                    standardobjective=objective,
                    )
        cls.ci2 = CourseInstance.objects.validated_create(
                courseversion=cls.cv2,
                instance_descriptor='Course Instance with the lot.',
                start_date=start_date,
                end_date=end_date,
                taught_by=[cls.school],
                )
        task1 = Task.objects.validated_create(
                courseinstance=cls.ci2,
                title='Lab',
                description='Do stuff in the lab.',
                weighting=10,
                )
        task2 = Task.objects.validated_create(
                courseinstance=cls.ci2,
                title='Mid-Semester exam',
                is_exam=True,
                is_hurdle=True,
                weighting=20,
                )
        Task.objects.validated_create(
                courseinstance=cls.ci2,
                title='Tutorials',
                )
        WeightedTaskToLO.objects.validated_create(task=task1, learningoutcome=lo2)
        WeightedTaskToLO.objects.validated_create(task=task2, learningoutcome=lo1)

    def test_course_standards_mapping_view(self):
        """Test rendering of the course version - standard objectives mapping view."""
        # No frameworks and no assessment tasks - nothing rendered
        response = self.client.get(reverse('cims:objectives.mapping', args=[self.cv1.pk]))
        self.assertContains(
                response,
                '<p>No Standards Frameworks associated with this Course Version.</p>',
                html=True,
                )
        self.assertNotRegex(
                response.content.decode('utf-8'),
                re.compile(r'<div id="[-_a-z0-9]+-mapping-summary">'),
                )
        self.assertNotRegex(
                response.content.decode('utf-8'),
                re.compile(r'<div id="[-_a-z0-9]+-lo-standards-mapping">'),
                )
        self.assertContains(
                response,
                ('<div id="assessment-lo-mapping"><h3  class="mb-3 mt-4">Learning Outcomes to '
                 'Assessment Tasks Mapping</h3><p>No relevant assessment tasks or learning '
                 'outcomes found.</p></div>'),
                html=True,
                )
        # Framework with no objectives
        stage2 = StandardsFramework.objects.get(name='Engineers Australia Stage 2 Competencies')
        CourseVersion.objects.validated_update(
                self.cv1,
                **{'standards_frameworks': [stage2]}
                )
        response = self.client.get(reverse('cims:objectives.mapping', args=[self.cv1.pk]))
        self.assertContains(
                response,
                '<p>{} has no associated objectives.</p>'.format(stage2),
                html=True,
                )
        # Framework with objectives, none linked to course version, no learning outcomes, a task
        stage1 = StandardsFramework.objects.get(name='Engineers Australia Stage 1 Competencies')
        cbok = StandardsFramework.objects.get(
                name='Australian Computer Society ICT Profession Common Body of Knowledge'
                )
        Task.objects.validated_create(
                courseinstance=self.ci1,
                title='Lab',
                description='Do stuff in the lab.',
                weighting=10,
                )
        CourseVersion.objects.validated_update(
                self.cv1,
                **{'standards_frameworks': [stage1, cbok]}
                )
        response = self.client.get(reverse('cims:objectives.mapping', args=[self.cv1.pk]))
        self.assertContains(
                response,
                '<table id="{}-mapping-summary-table" class="table table-bordered table-sm '
                'table-striped table-auto">'.format(slugify(str(cbok))),
                )
        for objective in StandardObjective.objects.root_nodes().filter(standardsframework=stage1):
            self.assertContains(
                    response,
                    '<table id="{}-mapping-summary-table" class="table table-bordered table-sm '
                    'table-striped table-auto">'.format(slugify(str(objective))),
                    )
        self.assertNotContains(response, '<td>X</td>', html=True)
        self.assertContains(
                response,
                '<div id="{}-lo-standards-mapping"><h5 class="mt-4">Learning Outcomes to {} '
                'Mapping</h5><p>{} has no defined learning outcomes.</p></div>'.format(
                     slugify(str(cbok)), cbok, self.cv1
                     ),
                html=True,
                )
        self.assertContains(
                response,
                '<div id="{}-lo-standards-mapping"><h5 class="mt-4">Learning Outcomes to {} '
                'Mapping</h5><p>{} has no defined learning outcomes.</p></div>'.format(
                     slugify(str(stage1)), stage1, self.cv1
                     ),
                html=True,
                )
        self.assertContains(
                response,
                '<div id="assessment-lo-mapping"><h3 class="mb-3 mt-4">Learning Outcomes to '
                'Assessment Tasks Mapping</h3><p>No relevant assessment tasks or learning '
                'outcomes found.</p></div>',
                html=True,
                )
        # Learning outcomes not linked to objectives or tasks
        LearningOutcome.objects.validated_create(
                courseversion=self.cv1,
                outcome='People milled around aimlessly.',
                )
        response = self.client.get(reverse('cims:objectives.mapping', args=[self.cv1.pk]))
        soup = BeautifulSoup(response.content, 'html.parser')
        lo_soup = soup.find(id='{}-lo-standards-mapping-table'.format(slugify(str(stage1))))
        self.assertTrue(lo_soup)
        # No cells marked as linked
        self.assertInHTML('<td class="mappingcell">X</td>', str(lo_soup), count=0)
        assessment_soup = soup.find(id='assessment-lo-mapping-table')
        self.assertTrue(assessment_soup)
        self.assertInHTML('<td class="mappingcell">X</td>', str(assessment_soup), count=0)
        # Course version with all the trimmings
        response = self.client.get(reverse('cims:objectives.mapping', args=[self.cv2.pk]))
        self.assertContains(
                response,
                """
                <table id="acs-cbok-mapping-summary-table"
                class="table table-bordered table-sm table-striped table-auto">
                <tr>
                <td class="bg-secondary">PS</td>
                <td>Solving problems is something that people
                appreciate...</td><td class="bg-success">X</td>
                </tr>
                <tr>
                <td class="bg-secondary">PK</td>
                <td>Probably a good idea to know how to do stuff...</td>
                <td class="bg-success">X</td>
                </tr>
                </table>
                """,
                html=True,
                )
        self.assertContains(
                response,
                """
                <table id="acs-cbok-lo-standards-mapping-table"
                class="table table-sm table-striped table-auto">
                <thead>
                <tr class="table-bordered">
                <th scope="col" class="col-md-4 col-lg-5 col-xl-6 align-middle">
                Learning outcome</th>
                <th scope="col" class="text-center align-middle">PS</th>
                <th scope="col" class="text-center align-middle">PK</th>
                </tr>
                </thead>
                <tbody class="table-bordered">
                <tr>
                <td>People did some stuff.</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td>People learned some stuff.</td>
                <td></td>
                <td class="bg-success text-center align-middle">X</td>
                </tr>
                </tbody>
                </table>
                """,
                html=True,
                )
        self.assertContains(
                response,
                """
                <table id="ea-stage-1-1-mapping-summary-table"
                class="table table-bordered table-sm table-striped table-auto">
                <tr>
                <td class="bg-secondary">1</td>
                <td>Comprehensive, theory based understanding of the
                underpinning natural and physical sciences and the engineering fundamentals
                applicable to the engineering discipline.</td> <td class="bg-success">X</td>
                </tr>
                <tr>
                <td class="bg-secondary">2</td>
                <td>Conceptual understanding of the mathematics, numerical
                analysis, statistics, and computer and information sciences which underpin the
                engineering discipline.</td> <td class="bg-success">X</td>
                </tr>
                <tr>
                <td class="bg-secondary">3</td>
                <td>In-depth understanding of specialist bodies of knowledge
                within the engineering discipline.</td> <td></td>
                </tr>
                <tr>
                <td class="bg-secondary">4</td>
                <td>Discernment of knowledge development and research
                directions within the engineering discipline.</td>
                <td class="bg-success">X</td>
                </tr>
                <tr>
                <td class="bg-secondary">5</td>
                <td>Knowledge of engineering design practice and contextual
                factors impacting the engineering discipline.</td> <td></td>
                </tr>
                <tr>
                <td class="bg-secondary">6</td>
                <td>Understanding of the scope, principles, norms,
                accountabilities and bounds of sustainable engineering practice in the specific
                discipline.</td> <td class="bg-success">X</td>
                </tr>
                </table>
                """,
                html=True,
                )
        self.assertContains(
                response,
                """
                <table id="ea-stage-1-2-mapping-summary-table"
                class="table table-bordered table-sm table-striped table-auto">
                <tr>
                <td class="bg-secondary">1</td>
                <td>Application of established engineering methods to complex
                engineering problem solving.</td> <td class="bg-success">X</td>
                </tr>
                <tr>
                <td class="bg-secondary">2</td>
                <td>Fluent application of engineering techniques, tools and
                resources.</td> <td></td>
                </tr>
                <tr>
                <td class="bg-secondary">3</td>
                <td>Application of systematic engineering synthesis and design
                processes.</td> <td class="bg-success">X</td>
                </tr>
                <tr>
                <td class="bg-secondary">4</td>
                <td>Application of systematic approaches to the conduct and
                management of engineering projects.</td> <td class="bg-success">X</td>
                </tr>
                </table>
                """,
                html=True,
                )
        self.assertContains(
                response,
                """
                <table id="ea-stage-1-lo-standards-mapping-table"
                class="table table-sm table-striped table-auto">
                <thead>
                <tr>
                <th scope="col" class="col-md-4 col-lg-5 col-xl-6 align-middle border-0"></th>
                <th scope="col" class="text-center align-middle border" colspan="6">
                Knowledge and Skill base</th>
                <th scope="col" class="text-center align-middle border" colspan="4">
                Engineering Application Ability</th>
                </tr>
                <tr class="table-bordered">
                <th scope="col" class="col-md-4 col-lg-5 col-xl-6 align-middle">
                Learning outcome</th>
                <th scope="col" class="text-center align-middle">1</th>
                <th scope="col" class="text-center align-middle">2</th>
                <th scope="col" class="text-center align-middle">3</th>
                <th scope="col" class="text-center align-middle">4</th>
                <th scope="col" class="text-center align-middle">5</th>
                <th scope="col" class="text-center align-middle">6</th>
                <th scope="col" class="text-center align-middle">1</th>
                <th scope="col" class="text-center align-middle">2</th>
                <th scope="col" class="text-center align-middle">3</th>
                <th scope="col" class="text-center align-middle">4</th>
                </tr>
                </thead>
                <tbody class="table-bordered">
                <tr>
                <td>People did some stuff.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="bg-success text-center align-middle">X</td>
                <td></td>
                <td class="bg-success text-center align-middle">X</td>
                <td class="bg-success text-center align-middle">X</td>
                </tr>
                <tr>
                <td>People learned some stuff.</td>
                <td class="bg-success text-center align-middle">X</td>
                <td class="bg-success text-center align-middle">X</td>
                <td></td>
                <td class="bg-success text-center align-middle">X</td>
                <td></td>
                <td class="bg-success text-center align-middle">X</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
                </tbody>
                </table>
                """,
                html=True,
                )
        self.assertContains(
                response,
                """
                <table id="assessment-lo-mapping-table"
                class="table table-striped table-sm table-auto">
                <thead>
                <tr>
                <th scope="col" class="border-0"></th>
                <th scope="col" class="border text-center align-middle" colspan="2">
                Assessment Tasks</th>
                </tr>
                <tr>
                <th scope="col" class="text-center align-middle">Learning outcome</th>
                <th scope="col" class="text-center align-middle">Lab</th>
                <th scope="col" class="text-center align-middle">Mid-Semester exam</th>
                </tr>
                </thead>
                <tbody class="table-bordered">
                <tr>
                <td>People did some stuff.</td>
                <td class="text-center align-middle bg-success">X</td>
                <td></td>
                </tr>
                <tr>
                <td>People learned some stuff.</td> <td></td>
                <td class="text-center align-middle bg-success">X</td>
                </tr>
                </tbody>
                </table>
                """,
                html=True,
                )
