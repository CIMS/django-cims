from django.test import TestCase
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models.query import EmptyQuerySet
from guardian.shortcuts import assign_perm
#from itertools import repeat, product

from . import models, rules
from .templatetags.cims_filters import stripcomp, extract_requisite
import re

def _get_perms_for_model(model):
    """
    Get a list of permission for a given model.

    Permission are formatted to be used in e.g. user.has_perm()

    Positional arguments:
    model -- the model whose permissions we're after
    """
    content_type = ContentType.objects.get_for_model(model)
    perms = Permission.objects.filter(content_type=content_type).values_list(
            'codename', flat=True)
    return ['{}.{}'.format(content_type.app_label, perm) for perm in perms]


###
class AuthorisationTestCase(TestCase):
    """
    Test suite for permissions scheme to make sure they give access to what we
    expect.
    """
    fixtures = ['test_base']

    @classmethod
    def setUpTestData(cls):
        # Add users to groups and assign Guardian permissions
        user = User.objects.get(username='test_cecs_admin')
        group  = Group.objects.get(name='CECS Admins')
        user.groups.add(group)
        user.save()
        cecs = models.College.objects.get(short_name='CECS')
        assign_perm('is_college_admin', group, cecs)

        user = User.objects.get(username='test_rscs_admin')
        group = Group.objects.get(name='RSCS Admins')
        user.groups.add(group)
        user.save()
        rscs = models.School.objects.get(short_name='RSCS')
        assign_perm('is_school_admin', group, rscs)

        user = User.objects.get(username='test_rseng_admin')
        group = Group.objects.get(name='RSEng Admins')
        user.groups.add(group)
        user.save()
        rseng = models.School.objects.get(short_name='RSEng')
        assign_perm('is_school_admin', group, rseng)

        user = User.objects.get(username='test_course_convener')
        group = Group.objects.get(name='Course Conveners')
        user.groups.add(group)
        user.save()
        # Make course convener user convener of a course
        course = models.Course.objects.get(code='COMP1100')
        course.convener = user
        course.save()
        # Create tuple of users for parametrized permissions testing
        cls.perm_test_users = (
                User.objects.get(username='test_cecs_admin'),
                User.objects.get(username='test_rscs_admin'),
                User.objects.get(username='test_course_convener'),
                )
        # Create dict of courses, versions, instances for tests
        cls.course_objs = {
                'comp1100': models.Course.objects.get(code='COMP1100'),
                'econ1102': models.Course.objects.get(code='ECON1102'),
                'engn2228': models.Course.objects.get(code='ENGN2228'),
                }
        cls.course_vers = {
                'comp1100': cls.course_objs['comp1100'].courseversion_set.first(),
                'econ1102': cls.course_objs['econ1102'].courseversion_set.first(),
                'engn2228': cls.course_objs['engn2228'].courseversion_set.first(),
                }
        cls.course_ins = {
                'comp1100': cls.course_vers['comp1100'].courseinstance_set.first(),
                'econ1102': cls.course_vers['econ1102'].courseinstance_set.first(),
                'engn2228': cls.course_vers['engn2228'].courseinstance_set.first(),
                }

    ### College Admins
    def test_college_admin_access(self):
        """
        Test a college admin can access what they should be able to, and can't
        access what they shouldn't.
        """
        user = User.objects.get(username='test_cecs_admin')
        # Has permission for their own college, but not another
        own = models.College.objects.get(short_name='CECS')
        self.assertTrue(user.has_perm('is_college_admin', own))
        other = models.College.objects.get(short_name='CBE')
        self.assertFalse(user.has_perm('is_college_admin', other))

    def test_college_admin_courseversion(self):
        """College Admin can add/edit/delete course versions in their own
        college but not in another."""
        user = User.objects.get(username='test_cecs_admin')
        # Own College
        self.assertTrue(user.has_perm('cims.add_courseversion', self.course_vers['comp1100']))
        self.assertTrue(user.has_perm('cims.change_courseversion', self.course_vers['comp1100']))
        self.assertFalse(user.has_perm('cims.delete_courseversion', self.course_vers['comp1100']))
        # Other College
        self.assertFalse(user.has_perm('cims.add_courseversion', self.course_vers['econ1102']))
        self.assertFalse(user.has_perm('cims.change_courseversion', self.course_vers['econ1102']))
        self.assertFalse(user.has_perm('cims.delete_courseversion', self.course_vers['econ1102']))

    def test_college_admin_courseinstance(self):
        """College Admin can add/edit/delete course instances in their own
        college but not in another."""
        user = User.objects.get(username='test_cecs_admin')
        # Own College
        self.assertTrue(user.has_perm('cims.add_courseinstance', self.course_ins['comp1100']))
        self.assertTrue(user.has_perm('cims.change_courseinstance', self.course_ins['comp1100']))
        self.assertFalse(user.has_perm('cims.delete_courseinstance', self.course_ins['comp1100']))
        # Other College
        self.assertFalse(user.has_perm('cims.add_courseinstance', self.course_ins['econ1102']))
        self.assertFalse(user.has_perm('cims.change_courseinstance', self.course_ins['econ1102']))
        self.assertFalse(user.has_perm('cims.delete_courseinstance', self.course_ins['econ1102']))

    ### School Admins
    def test_school_admin_access(self):
        """
        Test a school admin can access what they should be able to, and can't
        access what they shouldn't.
        """
        user = User.objects.get(username='test_rscs_admin')
        # Has permission for their own school, but not another
        own = models.School.objects.get(short_name='RSCS')
        self.assertTrue(user.has_perm('is_school_admin', own))
        other = models.School.objects.get(short_name='RSEng')
        self.assertFalse(user.has_perm('is_school_admin', other))

    def test_school_admin_courseversion(self):
        """School Admin can add/edit/delete course versions in their own
        school but not in another."""
        user = User.objects.get(username='test_rscs_admin')
        # Own School
        self.assertTrue(user.has_perm('cims.add_courseversion', self.course_vers['comp1100']))
        self.assertTrue(user.has_perm('cims.change_courseversion', self.course_vers['comp1100']))
        self.assertFalse(user.has_perm('cims.delete_courseversion', self.course_vers['comp1100']))
        # Other School
        self.assertFalse(user.has_perm('cims.add_courseversion', self.course_vers['engn2228']))
        self.assertFalse(user.has_perm('cims.change_courseversion', self.course_vers['engn2228']))
        self.assertFalse(user.has_perm('cims.delete_courseversion', self.course_vers['engn2228']))

    def test_school_admin_courseinstance(self):
        """School Admin can add/edit/delete course instances in their own
        school but not in another."""
        user = User.objects.get(username='test_rscs_admin')
        # Own School
        self.assertTrue(user.has_perm('cims.add_courseinstance', self.course_ins['comp1100']))
        self.assertTrue(user.has_perm('cims.change_courseinstance', self.course_ins['comp1100']))
        self.assertFalse(user.has_perm('cims.delete_courseinstance', self.course_ins['comp1100']))
        # Other School
        self.assertFalse(user.has_perm('cims.add_courseinstance', self.course_ins['engn2228']))
        self.assertFalse(user.has_perm('cims.change_courseinstance', self.course_ins['engn2228']))
        self.assertFalse(user.has_perm('cims.delete_courseinstance', self.course_ins['engn2228']))

    ### Course Conveners 
    def test_predicate_is_course_convener(self):
        """
        Test is course convener predicate truth table.
        """
        user = User.objects.get(username='test_course_convener')
        # Are convener of own course but not another
        self.assertTrue(rules.is_course_convener(
                user,
                self.course_objs['comp1100']
                ))
        self.assertFalse(rules.is_course_convener(
                user,
                self.course_objs['engn2228']
                ))
        group = Group.objects.get(name='Course Conveners')
        user.groups.remove(group)
        self.assertFalse(rules.is_course_convener(
                user,
                self.course_objs['comp1100']
                ))
        course = models.Course.objects.get(code='COMP1100')
        course.convener = None
        course.save()
        self.assertFalse(rules.is_course_convener(
                user,
                self.course_objs['comp1100']
                ))

    def test_convener_courseversion(self):
        """convener can add/edit/delete versions in their own courses but not
        another."""
        user = User.objects.get(username='test_course_convener')
        # Own School
        self.assertTrue(user.has_perm('cims.add_courseversion', self.course_vers['comp1100']))
        self.assertTrue(user.has_perm('cims.change_courseversion', self.course_vers['comp1100']))
        self.assertFalse(user.has_perm('cims.delete_courseversion', self.course_vers['comp1100']))
        # Other School
        self.assertFalse(user.has_perm('cims.add_courseversion', self.course_vers['engn2228']))
        self.assertFalse(user.has_perm('cims.change_courseversion', self.course_vers['engn2228']))
        self.assertFalse(user.has_perm('cims.delete_courseversion', self.course_vers['engn2228']))

    def test_convener_courseinstance(self):
        """convener can add/edit/delete instances in their own courses but not
        another."""
        user = User.objects.get(username='test_course_convener')
        # Own School
        self.assertTrue(user.has_perm('cims.add_courseinstance', self.course_ins['comp1100']))
        self.assertTrue(user.has_perm('cims.change_courseinstance', self.course_ins['comp1100']))
        self.assertFalse(user.has_perm('cims.delete_courseinstance', self.course_ins['comp1100']))
        # Other School
        self.assertFalse(user.has_perm('cims.add_courseinstance', self.course_ins['engn2228']))
        self.assertFalse(user.has_perm('cims.change_courseinstance', self.course_ins['engn2228']))
        self.assertFalse(user.has_perm('cims.delete_courseinstance', self.course_ins['engn2228']))

### Model Tests
class ModelTestCase(TestCase):
    """Test module functions."""
    fixtures = ['test_base']


    @classmethod
    def setUpTestData(cls):
        pass

    def test_copy_model(self):
        """Do we copy the model properly?"""
        source = models.IndicativeAssessment.objects.first()
        copy = models.copy_object(source)
        self.assertNotEqual(source.id, copy.id)
        self.assertEqual(source.courseversion, copy.courseversion)
        self.assertEqual(source.short_title, copy.short_title)
        self.assertEqual(source.indicative_percentage,
                copy.indicative_percentage)
        self.assertEqual(source.hurdle, copy.hurdle)
        self.assertSetEqual(set(source.learningoutcomes.all()),
                set(copy.learningoutcomes.all()))

    def test_copy_model_field_override(self):
        """Do we override fields on the model properly?"""
        source = models.IndicativeAssessment.objects.first()
        source_id = source.id
        cv = models.CourseVersion.objects.exclude(
                id=source.courseversion.id).first()
        overrides = [('courseversion', cv)]
        copy = models.copy_object(source, field_overrides=overrides)
        self.assertEqual(source.id, source_id)
        self.assertNotEqual(source.courseversion.id, copy.courseversion.id)

    def test_copy_model_skip_m2m(self):
        """Do we skip the m2m field properly?"""
        source = models.IndicativeAssessment.objects.first()
        copy = models.copy_object(source, skip_m2ms=['learningoutcomes'])
        self.assertQuerysetEqual(copy.learningoutcomes.all(),
                copy.learningoutcomes.none())

    def test_copy_model_copy_related(self):
        """Do we copy related objects as specified?"""
        source = models.CourseInstance.objects.first()
        copy = models.copy_object(source, copy_reverse=['task'])
        self.assertEqual(source.task_set.all().count(),
                copy.task_set.all().count())
        self.assertTrue(set(source.task_set.all()).isdisjoint(
            set(copy.task_set.all())))


class TaxonomyModelTestCase(TestCase):
    """
    Test custom/overriden methods of Taxonomy Model.
    """
    fixtures = ['test_base']

    @classmethod
    def setUpTestData(cls):
        pass

    def test_children_are_leaves(self):
        """Test by running on level 0,2,3 of a depth 4 balanced tree."""
        root = models.Taxonomy.objects.get(name='Engineers Australia')
        self.assertFalse(root.children_are_leaves())
        level1 = root.get_children().first()
        level2 = level1.get_children().first()
        self.assertTrue(level2.children_are_leaves())
        level3 = level2.get_children().first()
        self.assertFalse(level3.children_are_leaves())

    def test_get_leaf_count(self):
        """Compare output with sum of children of mid-level nodes in a depth 3
        balanced tree."""
        root = models.Taxonomy.objects.get(name='Australian Computer Society')
        leaves = 0
        for tax in root.get_children():
            leaves = leaves + tax.get_children().count()
        self.assertEqual(leaves, root.get_leaf_count())


class CourseVersionModelTestCase(TestCase):
    """
    Test custom/overriden methods of Taxonomy Model.
    """
    fixtures = ['test_base']

    @classmethod
    def setUpTestData(cls):
        pass

    def test_get_instance_for_year(self):
        """Test instance starts in given year. Test no instance returned for
        date with no instances."""
        cv = models.CourseVersion.objects.first()
        ci = cv.get_instance_for_year(2016)
        self.assertEqual(ci.start_date.year, 2016)
        ci = cv.get_instance_for_year(3030)
        self.assertIsNone(ci)

    def test_copy_version(self):
        """
        Test we copy a CourseVersion properly.
        -- overriden fields are correct
        -- related models copied outside of copy_object() are disjoint sets
            with source object
        -- learning outcome set has same outcome attributes as source
        -- indicative assessment set has same short title attributes as source
        -- learning outcomes linked from copied indicative assessments belong
            to the course version copy
        """
        cv = models.CourseVersion.objects.first()
        copy = cv.copy_version()
        self.assertTrue(copy.version_descriptor.startswith('Copy of'))
        self.assertIsNone(copy.approval_date)
        self.assertEqual(copy.approval_desc, '')
        self.assertTrue(set(cv.indicativeassessment_set.all()).isdisjoint(
                set(copy.indicativeassessment_set.all())))
        self.assertTrue(set(cv.learningoutcome_set.all()).isdisjoint(
                set(copy.learningoutcome_set.all())))
        cv_los = cv.learningoutcome_set.all().values_list('outcome',
                flat=True)
        copy_los = copy.learningoutcome_set.all().values_list('outcome',
                flat=True)
        self.assertSetEqual(set(cv_los), set(copy_los))
        cv_ias = cv.indicativeassessment_set.all().values_list('short_title',
                flat=True)
        copy_ias = copy.indicativeassessment_set.all().values_list(
                'short_title', flat=True)
        self.assertSetEqual(set(cv_ias), set(copy_ias))
        linked_los = models.LearningOutcome.objects.filter(
                indicativeassessment__courseversion_id=copy.id)
        self.assertTrue(set(linked_los).issubset(set(copy.learningoutcome_set.all())))


class CourseInstanceModelTestCase(TestCase):
    """
    Test custom/overriden methods of CourseInstance Model.
    """
    fixtures = ['test_base']

    @classmethod
    def setUpTestData(cls):
        pass

    def test_get_conveners(self):
        """Test user returned by method is course convener."""
        course = models.Course.objects.get(code='COMP1100')
        ci = course.courseversion_set.first().courseinstance_set.first()
        self.assertEqual(course.convener, ci.get_conveners())

    def test_copy_instance(self):
        """
        Test we copy a course instance properly.
        -- check overriden fields are correct
        -- check copied schedule and task sets are disjoint with the source
            sets
        -- check copied schedule and task sets have same contents as source
            sets
        """
        ci = models.CourseInstance.objects.first()
        copy = ci.copy_instance()
        self.assertTrue(copy.instance_descriptor.startswith('Copy of'))
        self.assertEqual(copy.class_number, '')
        self.assertIsNone(copy.teachingsession)
        self.assertTrue(set(ci.schedule_set.all()).isdisjoint(
                set(copy.schedule_set.all())))
        ci_scheds = ci.schedule_set.all().values_list(
                'session_description', flat=True)
        copy_scheds = copy.schedule_set.all().values_list(
                'session_description', flat=True)
        self.assertSetEqual(set(ci_scheds), set(copy_scheds))
        self.assertTrue(set(ci.task_set.all()).isdisjoint(
                set(copy.task_set.all())))
        ci_tasks = ci.task_set.all().values_list('title', flat=True)
        copy_tasks = copy.task_set.all().values_list('title', flat=True)
        self.assertSetEqual(set(ci_tasks), set(copy_tasks))


class IndicativeAssessmentModelTestCase(TestCase):
    """
    Test custom/overriden methods of IndicativeAssessmentModel.
    """
    fixtures = ['test_base']

    @classmethod
    def setUpTestData(cls):
        pass

class TaskModelTestCase(TestCase):
    """
    Test custom/overriden methods of Task Model.
    """
    fixtures = ['test_base']

    @classmethod
    def setUpTestData(cls):
        pass

class CIMSFiltersTestCase(TestCase):
    """
    TestCIMS template filters.
    """
    @classmethod
    def setUpTestData(cls):
        pass

    def test_stripcomp(self):
        self.assertTrue(stripcomp('foo', 'foo'))
        self.assertFalse(stripcomp('foo', 'bar'))
        self.assertTrue(stripcomp('f o  o ', ' fo   o'))

    def test_extract_requisite(self):
        self.assertIsNone(extract_requisite('Foo.'))
        self.assertEqual('ENGN.', extract_requisite('ENGN.'))
        self.assertEqual('COMP1100.', extract_requisite('Fluff up front:'
                + 'COMP1100.'))
        self.assertEqual('ENGN2228.', extract_requisite('ENGN2228. Stuff'
                + 'after.'))
        self.assertEqual('COMP1100.', extract_requisite('Front: COMP1100.'
                + ' Back.'))


class CIMSTagsTestCase(TestCase):
    """
    Test CIMS template tags.
    """
    @classmethod
    def setUpTestData(cls):
        pass

    def test_taxonomy_map_summary(self):
        pass

    def test_render_listdiff(self):
        pass

