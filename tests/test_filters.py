"""
Tests for CIMS custom filters.
"""
from django.test import TestCase
from unittest import TestCase as UnitTestCase

from cims.templatetags import cims_filters as filters


class TestContains(UnitTestCase):
    def test_contains_true(self):
        self.assertTrue(filters.contains('The cat in the hat', 'cat'))

    def test_contains_false(self):
        self.assertFalse(filters.contains('cat', 'hat'))


class TestPrefix(UnitTestCase):
    def test_prefix(self):
        self.assertEqual(filters.prefix('in the hat', 'The cat '), 'The cat in the hat')


class TestSuffix(UnitTestCase):
    def test_suffix(self):
        self.assertEqual(filters.suffix('The cat ', 'in the hat'), 'The cat in the hat')
