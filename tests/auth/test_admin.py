"""
Tests for cims.auth Admin functions etc..
"""
from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.test import override_settings, TestCase
from cims.auth.admin import admin_check_obj_permissions
from cims.auth.models import ConditionalPermissionsMap, User
from cims.auth.models import UserObjectPermission

from tests import helpers
from tests.auth.models import Cuisine, Pizza
from tests.auth import maps # noqa


@override_settings(
        PFP_MODELS=[('tests_auth', 'pizza')],
        PFP_IGNORE_PERMS={}
        )
class TestAdmin(TestCase):

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')
        cls.map1 = ConditionalPermissionsMap.objects.create(
                description='Master chefs',
                test_model=ContentType.objects.get_for_model(Cuisine),
                test='Is master chef',
                )
        perm = helpers.get_perm('tests_auth', 'pizza', 'change_pizza__name')
        cls.map1.conditional_permissions.add(perm)
        cls.italian = Cuisine.objects.create(name='Italian')

    def test_admin_check_obj_permissions(self):
        # False if no perms anywhere
        user1 = User.objects.create_user(username='user1', password='sekret')
        pizza = Pizza.objects.create(name='Capriciosa', cuisine=self.italian)
        self.assertFalse(admin_check_obj_permissions(user1, 'tests_auth', 'change_pizza'))
        # User has PerFieldPermission on object
        pizza_ctype = ContentType.objects.get_for_model(pizza)
        permission = helpers.get_perm('tests_auth', 'pizza', 'change_pizza__name')
        UserObjectPermission.objects.create(
                user=user1,
                permission=permission,
                content_type=pizza_ctype,
                object_id=pizza.id,
                )
        self.assertTrue(admin_check_obj_permissions(user1, 'tests_auth', 'change_pizza'))
        # User has PerFieldPermission via a ConditionalPermissionsMap
        user2 = User.objects.create_user(username='user2', password='sekret')
        self.italian.master_chef = user2
        self.italian.save()
        self.assertTrue(admin_check_obj_permissions(user2, 'tests_auth', 'change_pizza'))
