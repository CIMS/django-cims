"""
Tests for CIMS auth.utils module.
"""
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

from cims.auth.models import Role, RoleObjectPermission, UserObjectPermission
from cims.auth.utils import (check_user_obj_permissions, get_perm_str)
from tests.helpers import get_perm
from .models import Cuisine

User = get_user_model()


class TestUtils(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_get_perm_str(self):
        """Generates proper string."""
        permission = Permission.objects.get(
                codename='add_role',
                content_type__app_label='cims_auth',
                )
        self.assertEqual('cims_auth.add_role', get_perm_str(permission))

    def test_check_user_obj_permissions(self):
        """True if either queryset is non-empty, False otherwise."""
        # Empty querysets return False
        user1 = User.objects.create_user(username='user1', password='sekret')
        user2 = User.objects.create_user(username='user2', password='sekret')
        self.assertFalse(check_user_obj_permissions(user1))
        # User has object permission
        permission = get_perm('tests_auth', 'cuisine', 'change_cuisine')
        italian = Cuisine.objects.create(name='Italian')
        cuisine_ctype = ContentType.objects.get_for_model(Cuisine)
        UserObjectPermission.objects.create(
                user=user1,
                permission=permission,
                content_type=cuisine_ctype,
                object_id=italian.id,
                )
        filters = {
                'user': {'permission': permission},
                'role': {'permission': permission},
                }
        self.assertTrue(check_user_obj_permissions(user1, filters))
        # User has Role with object permission
        america = Cuisine.objects.create(name='American')
        role = Role.objects.create(name='Bread Role')
        user2.roles.add(role)
        RoleObjectPermission.objects.create(
                role=role,
                permission=permission,
                content_type=cuisine_ctype,
                object_id=america.id,
                )
        self.assertTrue(check_user_obj_permissions(user2, filters))
