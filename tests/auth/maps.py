from cims.auth import maps
from tests.auth.models import Cuisine


@maps.register('Is master chef')
def _is_master_chef(user_obj, test_obj=None, **kwargs):
    if test_obj is None:
        return Cuisine.objects.filter(master_chef=user_obj).exists()
    return test_obj.master_chef_id == user_obj.pk


@maps.register('Is sous chef')
def _is_sous_chef(user_obj, test_obj=None, **kwargs):
    if test_obj is None:
        return Cuisine.objects.filter(sous_chef=user_obj).exists()
    return test_obj.sous_chef_id == user_obj.pk
