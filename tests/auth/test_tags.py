"""
Tests for CIMS auth tags.
"""
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.contrib.contenttypes.models import ContentType
from django.template import Context, Template, TemplateSyntaxError
from django.test import override_settings, TestCase, RequestFactory
from cims.auth.utils import get_perm_str
from cims.auth.models import UserObjectPermission
from tests.helpers import get_perm

from .models import Pizza

User = get_user_model()


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class TestUserHasPermTag(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()

    def setUp(self):
        self.request = self.factory.get('/')
        self.request.user = User.objects.create_user(username='user', password='sekret')

    def test_user_has_perm_insufficient_args(self):
        """TemplateSyntaxError is raised if not enough arguments are passed."""
        self.assertRaisesMessage(
                TemplateSyntaxError,
                'user_has_perm requires: the request, a permission string and an optional object, '
                'plus a variable name to store its result in.',
                *[Template, """{% load cims_auth_tags %}{% user_has_perm request%}"""]
                )

    def test_user_has_perm_two_args(self):
        """
        Result of check of non-object based permission is stored in the supplied context variable
        name.
        """
        perm = get_perm('tests_auth', 'pizza', 'add_pizza')
        perm_str = get_perm_str(perm)
        self.request.user.user_permissions.add(perm)
        template = Template(
                """
                {% load cims_auth_tags %}
                {% user_has_perm request perm_str as can_add_pizza %}
                """
                )
        context = Context({'perm_str': perm_str, 'request': self.request})
        template.render(context)
        self.assertTrue(context['can_add_pizza'])

    def test_user_has_perm_three_args(self):
        """
        Result of check of object based permission is stored in the supplied context variable name.
        """
        the_pizza = Pizza.objects.create(name='capriciosa')
        perm = get_perm('tests_auth', 'pizza', 'add_pizza')
        perm_str = get_perm_str(perm)
        UserObjectPermission.objects.create(
                user=self.request.user,
                permission=perm,
                content_type=ContentType.objects.get_for_model(Pizza),
                object_id=the_pizza.pk,
                )
        template = Template(
                """
                {% load cims_auth_tags %}
                {% user_has_perm request perm_str the_pizza as can_add_pizza %}
                """
                )
        context = Context({'perm_str': perm_str, 'request': self.request, 'the_pizza': the_pizza})
        template.render(context)
        self.assertTrue(context['can_add_pizza'])

    def test_user_has_perm_anon_user(self):
        """Checking the permissions of an anonymous user works as for an authenticated user."""
        self.request.user = AnonymousUser()
        perm = get_perm('tests_auth', 'pizza', 'add_pizza')
        perm_str = get_perm_str(perm)
        template = Template(
                """
                {% load cims_auth_tags %}
                {% user_has_perm request perm_str as can_add_pizza %}
                """
                )
        context = Context({'perm_str': perm_str, 'request': self.request})
        template.render(context)
        self.assertFalse(context['can_add_pizza'])

    def test_user_has_perm_no_context_variable(self):
        """
        TemplateSyntaxError is raised if no context variable is passed in which to store the tag
        result.
        """
        self.assertRaisesMessage(
                TemplateSyntaxError,
                'user_has_perm requires: the request, a permission string and an optional object, '
                'plus a variable name to store its result in.',
                *[Template, """{% load cims_auth_tags %}{% user_has_perm request perm_str %}"""]
                )
