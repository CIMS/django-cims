from django.apps import AppConfig


class TestsAuthConfig(AppConfig):
    name = 'tests.auth'
    label = 'tests_auth'
