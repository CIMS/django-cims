"""
Test generic view classes/mixins.
"""
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.test import RequestFactory, TestCase, override_settings

from cims import generic
from cims.auth.mixins import ObjectPermissionRequiredMixin
from cims.auth.models import UserObjectPermission
from tests.helpers import get_perm, setup_view
from tests.models import Author

User = get_user_model()


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class ObjectPermissionRequiredMixinTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        class AuthorView(ObjectPermissionRequiredMixin, generic.ModelFormWithInlinesMixin):
            model = Author

        cls.AuthorView = AuthorView
        cls.request_factory = RequestFactory()

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.request_factory.get('/')
        self.request.user = self.user

    def test_get_permission_required_not_configured(self):
        """Returns an empty list if `permission_required` not configured."""
        view = setup_view(self.AuthorView(), self.request)
        self.assertListEqual([], view.get_permission_required())

    def test_get_object_permission_required_not_configured(self):
        """Returns an empty list if `object_permission_required` not configured."""
        view = setup_view(self.AuthorView(), self.request)
        self.assertListEqual([], view.get_object_permission_required())

    def test_get_object_permission_required_string(self):
        """Puts `object_permission_required` in a list if it is a string."""
        class AuthorView(self.AuthorView):
            object_permission_required = 'foo'

        view = setup_view(AuthorView(), self.request)
        self.assertListEqual(['foo'], view.get_object_permission_required())

    def test_get_object_permission_required_list(self):
        """Passes `object_permission_required` through if it is not None nor a string."""
        class AuthorView(self.AuthorView):
            object_permission_required = ['bar']

        view = setup_view(AuthorView(), self.request)
        self.assertListEqual(['bar'], view.get_object_permission_required())

    @patch('cims.auth.models.User.is_authenticated', return_value=True)
    def test_handle_no_permission_user_authenticated(self, is_auth):
        """Raise a `PermissionDenied` exception if the user is authenticated."""
        view = setup_view(self.AuthorView(), self.request)
        self.assertRaisesMessage(
                PermissionDenied,
                'You do not have permission to perform this action.',
                view.handle_no_permission
                )

    def test_has_permission_not_configured(self):
        """
        Raise `ImproperlyConfigured` exception if neither `permission_required` nor
        `object_permission_required` are set.
        """
        view = setup_view(self.AuthorView(), self.request)
        self.assertRaisesMessage(
                ImproperlyConfigured,
                'At least one of AuthorView.permission_required or '
                'AuthorView.object_permission_required must be configured, or you must override '
                'AuthorView.get_permission_required() or '
                'AuthorView.get_object_permission_required()',
                view.has_permission
                )

    def test_has_permission_no_object_permission_user_passes(self):
        """
        If `permission_required` is configured and `object_permission_required` is not the object
        permission test should be skipped and the access check will succeed. `self.get_object()`
        will raise an exception if the object permission test is attempted.
        """
        class AuthorView(self.AuthorView):
            permission_required = 'tests.add_author'

        self.request.user.user_permissions.add(get_perm('tests', 'author', 'add_author'))
        view = setup_view(AuthorView(), self.request)
        self.assertTrue(view.has_permission())

    def test_has_permission_no_object_permission_user_fails(self):
        """
        If `permission_required` is configured and `object_permission_required` is not the object
        permission test should be skipped and the access check will fail as the user lacks
        permission.
        """
        class AuthorView(self.AuthorView):
            permission_required = 'tests.add_author'

        view = setup_view(AuthorView(), self.request)
        self.assertFalse(view.has_permission())

    def test_has_permission_object_permission_only_user_succeeds(self):
        """
        If `object_permission_required` is configured and `permission_required` is not the object
        permission test determines the outcome of the test, in this case success.
        """
        class AuthorView(self.AuthorView):
            object_permission_required = 'tests.change_author'

        author = Author.objects.validated_create(name='Author')
        perm = get_perm('tests', 'author', 'change_author')
        UserObjectPermission.objects.create(
                user=self.user,
                permission=perm,
                content_type=ContentType.objects.get_for_model(Author),
                object_id=author.pk
                )
        view = setup_view(AuthorView(), self.request, **{'pk': author.pk})
        self.assertTrue(view.has_permission())

    def test_has_permission_object_permission_only_user_fails(self):
        """
        If `object_permission_required` is configured and `permission_required` is not the object
        permission test determines the outcome of the test, in this case failure.
        """
        class AuthorView(self.AuthorView):
            object_permission_required = 'tests.change_author'

        author = Author.objects.validated_create(name='Author')
        view = setup_view(AuthorView(), self.request, **{'pk': author.pk})
        self.assertFalse(view.has_permission())

    def test_has_permission_model_and_object_model_fails(self):
        """
        Return False if both permission checks don't pass.
        """
        class AuthorView(self.AuthorView):
            permission_required = 'tests.add_author'
            object_permission_required = 'tests.change_author'

        author = Author.objects.validated_create(name='Author')
        perm = get_perm('tests', 'author', 'change_author')
        UserObjectPermission.objects.create(
                user=self.user,
                permission=perm,
                content_type=ContentType.objects.get_for_model(Author),
                object_id=author.pk
                )
        view = setup_view(AuthorView(), self.request, **{'pk': author.pk})
        self.assertFalse(view.has_permission())

    def test_has_permission_model_and_object_model_succeeds(self):
        """
        Return True if both permission checks pass.
        """
        class AuthorView(self.AuthorView):
            permission_required = 'tests.add_author'
            object_permission_required = 'tests.change_author'

        author = Author.objects.validated_create(name='Author')
        self.request.user.user_permissions.add(get_perm('tests', 'author', 'add_author'))
        perm = get_perm('tests', 'author', 'change_author')
        UserObjectPermission.objects.create(
                user=self.user,
                permission=perm,
                content_type=ContentType.objects.get_for_model(Author),
                object_id=author.pk
                )
        view = setup_view(AuthorView(), self.request, **{'pk': author.pk})
        self.assertTrue(view.has_permission())
