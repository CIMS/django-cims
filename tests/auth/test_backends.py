from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Permission, AnonymousUser
from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.core.exceptions import PermissionDenied
from django.test import override_settings, TestCase

from cims.models import ReviewStatus
from cims.auth.models import (ConditionalPermissionsMap, ObjectStatusPermissionsMap, Role,
                              RoleObjectPermission, UserObjectPermission)
from cims.auth.backends import (CIMSBackend, ConditionalPermissionsMapBackend,
                                ObjectStatusPermissionsMapBackend)

from tests import helpers
from tests.models import BasicModel
from tests.auth import maps # noqa
from tests.auth.models import Cuisine, Ingredient, Pie, Pizza


User = get_user_model()


class TestCIMSBackend(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.backend = CIMSBackend()
        pwd = make_password('password')
        Role.objects.create(name='r1')
        Role.objects.create(name='r2')
        Role.objects.create(name='r3')
        Role.objects.create(name='r4')
        Role.objects.create(name='r5')
        Role.objects.create(name='r6')
        User.objects.create_user(username='u1', password=pwd)
        User.objects.create_user(username='u2', password=pwd)
        User.objects.create_user(username='u3', password=pwd)

    def test_perm_filter(self):
        """Check returns values_list with correct fields."""
        perm = Permission.objects.get(pk=1)
        perm_filter = {'id': 1}
        self.assertEqual(
                [(perm.content_type.app_label, perm.codename)],
                list(self.backend._perm_filter(**perm_filter))
                )

    def test_pfp_filter(self):
        """Check returns values_list with correct fields."""
        pfp = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        self.assertEqual(
                [(
                    pfp.content_type.app_label,
                    pfp.codename,
                    pfp.model_permission.codename
                    )],
                list(self.backend._pfp_filter())
                )

    def test_get_user_obj_permissions_no_perms(self):
        """No perms returns empty set."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        obj_ctype = ContentType.objects.get_for_model(User)
        self.assertDictEqual(
                dict(),
                self.backend._get_user_obj_permissions(u1, u2, obj_ctype)
                )

    def test_get_user_obj_permissions_has_model_perms(self):
        """Returns model level permissions on an object possessed by user."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'delete_user')
        perm3 = helpers.get_perm('cims_auth', 'user', 'add_user')
        for perm in [perm1, perm2]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        UserObjectPermission.objects.create(
                user=u3,
                permission=perm3,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.assertDictEqual(
                {
                    'cims_auth.delete_user': set(),
                    'cims_auth.change_user': set()
                },
                self.backend._get_user_obj_permissions(u1, u2, obj_ctype)
                )

    def test_get_user_obj_permissions_has_field_perms(self):
        """Returns field level permissions on an object possessed by user."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        for perm in [perm1, perm2]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        UserObjectPermission.objects.create(
                user=u3,
                permission=perm3,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.assertDictEqual(
                {
                    'cims_auth.change_user': {'cims_auth.change_user__email',
                                              'cims_auth.change_user__is_staff'}
                    },
                self.backend._get_user_obj_permissions(u1, u2, obj_ctype)
                )

    def test_get_user_obj_permissions_has_both_perms(self):
        """Returns permissions on an object possessed by user."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm3 = helpers.get_perm('cims_auth', 'user', 'delete_user')
        perm4 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm5 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm6 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        for perm in [perm1, perm2, perm4, perm5]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        UserObjectPermission.objects.create(
                user=u3,
                permission=perm3,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        UserObjectPermission.objects.create(
                user=u3,
                permission=perm6,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.assertDictEqual(
                {
                    'cims_auth.add_user': set(),
                    'cims_auth.change_user': {'cims_auth.change_user__email',
                                              'cims_auth.change_user__is_staff'}
                    },
                self.backend._get_user_obj_permissions(u1, u2, obj_ctype)
                )

    def test_get_user_perms_has_both_perms(self):
        """Returns permissions possessed by user."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm3 = helpers.get_perm('cims_auth', 'user', 'delete_user')
        perm4 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm5 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm6 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        u1.user_permissions.add(*[perm1, perm2, perm4, perm5])
        u2.user_permissions.add(*[perm3, perm6])
        self.assertDictEqual(
                {
                    'cims_auth.add_user': set(),
                    'cims_auth.change_user': {'cims_auth.change_user__email',
                                              'cims_auth.change_user__is_staff'}
                    },
                self.backend._get_user_permissions(u1)
                )

    def test_get_role_permissions(self):
        """No perms == empty dict."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        self.assertDictEqual(
                dict(),
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_preserve_cache(self):
        """Operating on cache so make sure haven't accidentally modified it."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        self.backend._get_role_permissions(u1)
        self.assertListEqual(
                [set([r1]), set([r2])],
                u1._roles_hierarchical_cache
                )

    def test_get_role_permissions_add_within_set(self):
        """
        Field permissions within a set of roles at the same distance from
        the user are additive.
        """
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        r3 = Role.objects.get(name='r3')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        r1.parent_roles.add(r3)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        r2.permissions.add(perm1, perm2)
        r3.permissions.add(perm3)
        self.assertDictEqual(
                {
                    'cims_auth.change_user': {'cims_auth.change_user__email',
                                              'cims_auth.change_user__is_staff',
                                              'cims_auth.change_user__is_active'}
                    },
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_override_within_set(self):
        """
        A model permissions within a set of roles at the same distance from the
        user will override any equivalent field permissions within that set of
        roles.
        """
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        r3 = Role.objects.get(name='r3')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        r1.parent_roles.add(r3)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        r2.permissions.add(perm1)
        r3.permissions.add(perm2, perm3)
        self.assertDictEqual(
                {
                    'cims_auth.change_user': set(),
                    },
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_add_between_sets(self):
        """
        Field permissions between graph adjacent sets of roles possessed by the
        user are additive.
        """
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        r1.permissions.add(perm1, perm2)
        r2.permissions.add(perm3)
        self.assertDictEqual(
                {
                    'cims_auth.change_user': {'cims_auth.change_user__email',
                                              'cims_auth.change_user__is_staff',
                                              'cims_auth.change_user__is_active'}
                    },
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_override_parent_set(self):
        """
        A model permission from a role which is closer to the user in the role
        graph will override field permissions from more distant roles.
        """
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        r1.permissions.add(perm1)
        r2.permissions.add(perm2, perm3)
        self.assertDictEqual(
                {
                    'cims_auth.change_user': set(),
                    },
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_override_parent_side_effect_1(self):
        """
        When overriding field permissions unrelated entries should be unchanged
        """
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        perm4 = helpers.create_pfp('auth', 'group', 'change_group', 'name')
        perm5 = helpers.get_perm('cims_auth', 'user', 'add_user')
        r1.permissions.add(perm1)
        r2.permissions.add(perm2, perm3, perm4, perm5)
        self.assertDictEqual(
                {
                    'cims_auth.change_user': set(),
                    'cims_auth.add_user': set(),
                    'auth.change_group': {'auth.change_group__name'}
                    },
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_pfp_override_parent_set(self):
        """
        A field permission from a role which is closer to the user in the role
        graph will override a model permissions from more distant roles.
        """
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        r1.permissions.add(perm2, perm3)
        r2.permissions.add(perm1)
        self.assertDictEqual(
                {
                    'cims_auth.change_user': {'cims_auth.change_user__is_staff',
                                              'cims_auth.change_user__is_active'}
                    },
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_pfp_override_side_effect_1(self):
        """
        Overriding model permissions shouldn't change unrelated permissions.
        """
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        perm4 = helpers.create_pfp('auth', 'group', 'change_group', 'name')
        perm5 = helpers.get_perm('cims_auth', 'user', 'add_user')
        r1.permissions.add(perm2, perm3)
        r2.permissions.add(perm1, perm4, perm5)
        self.assertDictEqual(
                {
                    'cims_auth.add_user': set(),
                    'auth.change_group': {'auth.change_group__name'},
                    'cims_auth.change_user': {'cims_auth.change_user__is_staff',
                                              'cims_auth.change_user__is_active'},
                    },
                self.backend._get_role_permissions(u1)
                )

    def test_get_role_permissions_obj(self):
        """Check getting object based permissions."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        obj_ctype = ContentType.objects.get_for_model(User)
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'add_user', 'is_staff')
        perm2 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        perm4 = helpers.get_perm('cims_auth', 'user', 'add_user')
        for perm in [perm1, perm2]:
            RoleObjectPermission.objects.create(
                    role=r1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        for perm in [perm3, perm4]:
            RoleObjectPermission.objects.create(
                    role=r2,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        self.assertDictEqual(
                {
                    'cims_auth.change_user': set(),
                    'cims_auth.add_user': {'cims_auth.add_user__is_staff'},
                    },
                self.backend._get_role_obj_permissions(u1, obj=u2, obj_ctype=obj_ctype)
                )

    def test_get_role_permissions_obj_multiples(self):
        """
        Check `RoleObjectPermission` filter returns multiple entries if
        present.
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        obj_ctype = ContentType.objects.get_for_model(User)
        u1.roles.add(r1)
        r1.parent_roles.add(r2)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        perm3 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm4 = helpers.get_perm('cims_auth', 'user', 'add_user')
        for perm in [perm1, perm2]:
            RoleObjectPermission.objects.create(
                    role=r1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        for perm in [perm3, perm4]:
            RoleObjectPermission.objects.create(
                    role=r2,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        self.assertDictEqual(
                {
                    'cims_auth.add_user': set(),
                    'cims_auth.change_user': {'cims_auth.change_user__is_staff',
                                              'cims_auth.change_user__is_active'},
                    },
                self.backend._get_role_obj_permissions(u1, obj=u2, obj_ctype=obj_ctype)
                )

    def test_get_permissions_inactive(self):
        """Inactive user returns empty set."""
        u1 = User.objects.get(username='u1')
        u1.is_active = False
        self.assertSetEqual(
                set(),
                self.backend._get_permissions(u1, None, 'user')
                )

    def test_get_permissions_anonymous(self):
        """Anonymous user returns empty set."""
        u1 = AnonymousUser()
        u1.is_active = True
        self.assertSetEqual(
                set(),
                self.backend._get_permissions(u1, None, 'user')
                )

    def test_get_permissions_superuser(self):
        """Superuser returns all model perms."""
        u1 = User.objects.get(username='u1')
        u1.is_superuser = True
        chk_set = set()
        for perm in Permission.objects.all():
            chk_set.add(helpers.get_perm_str(perm))
        self.assertSetEqual(
                chk_set,
                self.backend._get_permissions(u1, None, 'user')
                )

    def test_get_permissions_from_name_user(self):
        """Get user permissions when `from_name` set to 'user'."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'add_user')
        u1.user_permissions.add(perm1)
        r1.permissions.add(perm2)
        self.assertSetEqual(
                {helpers.get_perm_str(perm1)},
                self.backend._get_permissions(u1, None, 'user')
                )

    def test_get_permissions_from_name_role(self):
        """Get role permissions when `from_name` set to 'role'."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'add_user')
        u1.user_permissions.add(perm1)
        r1.permissions.add(perm2)
        self.assertSetEqual(
                {helpers.get_perm_str(perm2)},
                self.backend._get_permissions(u1, None, 'role')
                )

    def test_get_permissions_user_cache(self):
        """Sets cache attributes."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'add_user')
        u1.user_permissions.add(perm1)
        r1.permissions.add(perm2)
        self.backend._get_permissions(u1, None, 'user')
        self.backend._get_permissions(u1, None, 'role')
        self.assertTrue(hasattr(u1, '_cims_user_perm_cache'))
        self.assertTrue(hasattr(u1, '_cims_role_perm_cache'))
        self.assertDictEqual(
                {helpers.get_perm_str(perm1): set()},
                u1._cims_user_perm_cache
                )
        self.assertDictEqual(
                {helpers.get_perm_str(perm2): set()},
                u1._cims_role_perm_cache
                )

    def test_get_permissions_obj_superuser(self):
        """Superuser ignores object permissions."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u1.is_superuser = True
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_active')
        for perm in [perm1, perm2, perm3]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        chk_set = set()
        for perm in Permission.objects.filter(perfieldpermission__isnull=True):
            chk_set.add(helpers.get_perm_str(perm))
        self.assertSetEqual(
                chk_set,
                self.backend._get_permissions(u1, u2, 'user')
                )

    def test_get_permissions_obj_cache(self):
        """
        Sets cache attributes for object permissions. Cache has correct
        structure:

        { obj_ctype.id:
            { obj.id:
                { model_perm: set(pfp1, pfp2)}
            }
        }
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        UserObjectPermission.objects.create(
                user=u1,
                permission=perm1,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        RoleObjectPermission.objects.create(
                role=r1,
                permission=perm2,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.backend._get_permissions(u1, u2, 'user')
        self.backend._get_permissions(u1, u2, 'role')
        self.assertTrue(hasattr(u1, '_cims_user_obj_perm_cache'))
        self.assertTrue(hasattr(u1, '_cims_role_obj_perm_cache'))
        self.assertDictEqual(
                {
                    obj_ctype.id: {
                        u2.id: {'cims_auth.change_user': {'cims_auth.change_user__email'}}
                        }
                    },
                u1._cims_user_obj_perm_cache
                )
        self.assertDictEqual(
                {
                    obj_ctype.id: {
                        u2.id: {'cims_auth.change_user': {'cims_auth.change_user__is_staff'}}
                        }
                    },
                u1._cims_role_obj_perm_cache
                )

    def test_get_permissions_obj_return_set(self):
        """Returns flattened set of permissions."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        for perm in [perm1, perm2]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        self.assertSetEqual(
                {'cims_auth.change_user', 'cims_auth.change_user__email',
                 'cims_auth.change_user__is_staff'},
                self.backend._get_permissions(u1, u2, 'user')
                )

    def test_get_permissions_obj_if_no_perms(self):
        """If no perms applicable to this object return model perms."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'add_user')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm4 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        u1.user_permissions.add(perm1, perm2)
        for perm in [perm3, perm4]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u3.id,
                    )
        self.assertSetEqual(
                {'cims_auth.change_user', 'cims_auth.add_user'},
                self.backend._get_permissions(u1, u2, 'user')
                )

    def test_get_all_permissions_inactive(self):
        """Inactive user returns empty set."""
        u1 = User.objects.get(username='u1')
        u1.is_active = False
        self.assertSetEqual(
                set(),
                self.backend.get_all_permissions(u1, None)
                )

    def test_get_all_permissions_anonymous(self):
        """Anonymous user returns empty set."""
        u1 = AnonymousUser()
        u1.is_active = True
        self.assertSetEqual(
                set(),
                self.backend.get_all_permissions(u1, None)
                )

    def test_get_all_permissions_cache(self):
        """Correctly merges user and role permissions into cache structure."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.get_perm('auth', 'group', 'change_group')
        perm4 = helpers.get_perm('cims_auth', 'user', 'add_user')
        perm5 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm6 = helpers.get_perm('auth', 'group', 'add_group')
        perm7 = helpers.create_pfp('auth', 'group', 'change_group', 'name')
        u1.user_permissions.add(perm1, perm2, perm3, perm4)
        r1.permissions.add(perm5, perm6, perm7)
        self.backend.get_all_permissions(u1)
        self.assertDictEqual(
                {
                    'cims_auth.change_user': {'cims_auth.change_user__email',
                                              'cims_auth.change_user__is_staff'},
                    'cims_auth.add_user': set(),
                    'auth.add_group': set(),
                    'auth.change_group': set(),
                    },
                u1._cims_perm_cache
                )

    def test_get_all_permissions_obj_cache(self):
        """
        Correctly merges user and role object permissions into cache structure.
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.get_perm('cims_auth', 'user', 'delete_user')
        perm4 = helpers.get_perm('cims_auth', 'user', 'add_user')
        perm5 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm6 = helpers.create_pfp('cims_auth', 'user', 'delete_user', 'email')
        perm7 = Permission.objects.create(
                content_type=obj_ctype,
                codename='foo_user',
                )
        for perm in [perm1, perm2, perm3, perm4]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        for perm in [perm5, perm6, perm7]:
            RoleObjectPermission.objects.create(
                    role=r1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        self.backend.get_all_permissions(u1, u2)
        self.assertDictEqual(
                {
                    obj_ctype.id: {
                        u2.id: {
                            'cims_auth.change_user': {'cims_auth.change_user__email',
                                                      'cims_auth.change_user__is_staff'},
                            'cims_auth.add_user': set(),
                            'cims_auth.delete_user': set(),
                            'cims_auth.foo_user': set(),
                            },
                        },
                    },
                u1._cims_obj_perm_cache
                )

    def test_get_all_permissions_obj_merge(self):
        """
        Correctly merges object and model permissions. E.g. can add objects but
        only edit objects they've added. Doesn't bork model perm cache.
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.get_perm('cims_auth', 'user', 'delete_user')
        perm4 = helpers.get_perm('cims_auth', 'user', 'add_user')
        for perm in [perm1, perm2]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        u1.user_permissions.add(perm3)
        r1.permissions.add(perm4)
        self.assertSetEqual(
                {'cims_auth.change_user', 'cims_auth.change_user__email',
                 'cims_auth.change_user__is_staff', 'cims_auth.add_user', 'cims_auth.delete_user'},
                self.backend.get_all_permissions(u1, u2)
                )
        self.assertDictEqual(
                {'cims_auth.add_user': set(), 'cims_auth.delete_user': set()},
                u1._cims_perm_cache,
                )

    def test_has_perm_inactive(self):
        """Return false for inactive users."""
        u1 = User.objects.get(username='u1')
        u1.is_active = False
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        self.assertFalse(self.backend.has_perm(u1, helpers.get_perm_str(perm)))

    def test_has_perm_anonymous(self):
        """Return false active anonymous users."""
        u1 = AnonymousUser()
        u1.is_active = True
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        self.assertFalse(self.backend.has_perm(u1, helpers.get_perm_str(perm)))

    def test_has_perm_no_perm(self):
        """Return False if user doesn't have model permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'change_user')
        u1.user_permissions.add(perm2)
        self.assertFalse(self.backend.has_perm(u1, helpers.get_perm_str(perm1)))

    def test_has_perm_no_pfp(self):
        """Return False if user doesn't have field permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        u1.user_permissions.add(perm2)
        self.assertFalse(self.backend.has_perm(u1, helpers.get_perm_str(perm1)))

    def test_has_perm_model_perm(self):
        """Return True if user has model permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        u1.user_permissions.add(perm1)
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm1)))

    def test_has_perm_model_perm_if_pfp(self):
        """
        Return True for model permission if user has field permission for that
        model permission.
        """
        u1 = User.objects.get(username='u1')
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        u1.user_permissions.add(perm2)
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm1)))

    def test_has_perm_field_perm(self):
        """Return True if user has field permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        u1.user_permissions.add(perm1)
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm1)))

    def test_has_perm_field_perm_if_mperm(self):
        """
        Return True for field permission if user has model permission only.
        """
        u1 = User.objects.get(username='u1')
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        u1.user_permissions.add(perm1)
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm2)))

    def test_has_perm_obj_perm(self):
        """Return True if user has object permission."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        obj_ctype = ContentType.objects.get_for_model(User)
        UserObjectPermission.objects.create(
                user=u1,
                permission=perm,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm), u2))

    def test_has_perm_no_obj_perm(self):
        """Return False if user doesn't have object permission."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        self.assertFalse(self.backend.has_perm(u1, helpers.get_perm_str(perm), u2))

    def test_has_perm_obj_perm_if_mperm(self):
        """
        Return True if user has model level perm but not specific object
        permission.
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        u1.user_permissions.add(perm)
        self.assertTrue(self.backend.has_perm(
                u1,
                helpers.get_perm_str(perm),
                u2
                ))

    def test_has_perm_obj_perm_not_mperm(self):
        """User has object permission but test is for model level permission (obj==None)."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'delete_user')
        obj_ctype = ContentType.objects.get_for_model(User)
        UserObjectPermission.objects.create(
                user=u1,
                permission=perm1,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        # User has permission
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm1)))
        # User doesn't have permission
        self.assertFalse(self.backend.has_perm(u1, helpers.get_perm_str(perm2)))

    def test_has_perm_update_cache(self):
        """Update the cache for new object id."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        obj_ctype = ContentType.objects.get_for_model(User)
        UserObjectPermission.objects.create(
                user=u1,
                permission=perm,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm), u2))
        UserObjectPermission.objects.create(
                user=u1,
                permission=perm,
                content_type=obj_ctype,
                object_id=u3.id,
                )
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm), u3))

    def test_has_perm_cache_merge(self):
        """
        Correctly merges object and model permissions. E.g. can add objects but
        only edit objects they've added. Doesn't bork model perm cache.
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        obj_ctype = ContentType.objects.get_for_model(User)
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.get_perm('cims_auth', 'user', 'delete_user')
        perm4 = helpers.get_perm('cims_auth', 'user', 'add_user')
        for perm in [perm1, perm2]:
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        u1.user_permissions.add(perm3)
        r1.permissions.add(perm4)
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm1), u2))
        self.assertTrue(self.backend.has_perm(u1, helpers.get_perm_str(perm4), u2))
        self.assertDictEqual(
                {'cims_auth.add_user': set(), 'cims_auth.delete_user': set()},
                u1._cims_perm_cache,
                )

    def test_has_module_perms_inactive(self):
        """Return false for inactive users."""
        u1 = User.objects.get(username='u1')
        u1.is_active = False
        self.assertFalse(self.backend.has_module_perms(u1, 'cims_auth'))

    def test_has_module_perms_anonymous(self):
        """Return false active anonymous users."""
        u1 = AnonymousUser()
        u1.is_active = True
        self.assertFalse(self.backend.has_module_perms(u1, 'cims_auth'))

    def test_has_module_perms_no_perm(self):
        """Return False if user doesn't have a permission in the app."""
        u1 = User.objects.get(username='u1')
        self.assertFalse(self.backend.has_module_perms(u1, 'cims_auth'))

    def test_has_module_perms_has_perm(self):
        """Return True if user has a permission in the app."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        u1.user_permissions.add(perm1)
        self.assertTrue(self.backend.has_module_perms(u1, 'cims_auth'))

    def test_has_module_perms_has_uobj_perm(self):
        """Return True if user has a user object permission in the app."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        obj_ctype = ContentType.objects.get_for_model(User)
        UserObjectPermission.objects.create(
                user=u1,
                permission=perm1,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.assertTrue(self.backend.has_module_perms(u1, 'cims_auth'))

    def test_has_module_perms_has_robj_perm(self):
        """Return True if user has a role object permission in the app."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        obj_ctype = ContentType.objects.get_for_model(User)
        RoleObjectPermission.objects.create(
                role=r1,
                permission=perm1,
                content_type=obj_ctype,
                object_id=u2.id,
                )
        self.assertTrue(self.backend.has_module_perms(u1, 'cims_auth'))

    def test_get_pfps_inactive(self):
        """Return None for inactive users."""
        u1 = User.objects.get(username='u1')
        u1.is_active = False
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        self.assertTrue(self.backend.get_pfps(u1, perm, User) is None)

    def test_get_pfps_anonymous(self):
        """Return None for active anonymous users."""
        u1 = AnonymousUser()
        u1.is_active = True
        perm = helpers.get_perm('cims_auth', 'user', 'change_user')
        self.assertTrue(self.backend.get_pfps(u1, perm, User) is None)

    def test_get_pfps_no_perm(self):
        """Return None if user doesn't have applicable permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        perm2 = helpers.get_perm('cims_auth', 'user', 'change_user')
        self.assertTrue(self.backend.get_pfps(u1, perm1, User) is None)
        self.assertTrue(self.backend.get_pfps(u1, perm1, u1) is None)
        u1 = User.objects.get(username='u1')
        u1.user_permissions.add(perm2)
        self.assertTrue(self.backend.get_pfps(u1, perm1, User) is None)
        self.assertTrue(self.backend.get_pfps(u1, perm1, u1) is None)

    def test_get_pfps_no_pfps(self):
        """
        Return set() if user has model permission. Pass a PerFieldPermission to check passing
        PerFieldPermission as argument.
        """
        u1 = User.objects.get(username='u1')
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.get_perm('cims_auth', 'user', 'change_user')
        u1.user_permissions.add(perm2)
        self.assertEqual(set(), self.backend.get_pfps(u1, perm1, User))
        self.assertEqual(set(), self.backend.get_pfps(u1, perm1, u1))

    def test_get_pfps_field_perm(self):
        """Return set containing pfp 'app_label.codename' strings if user has field permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'add_user', 'is_superuser')
        u1.user_permissions.add(perm1, perm2)
        obj_ctype = ContentType.objects.get_for_model(User)
        UserObjectPermission.objects.create(
                user=u1,
                permission=perm3,
                content_type=obj_ctype,
                object_id=u1.id,
                )
        self.assertEqual(
                {helpers.get_perm_str(perm1), helpers.get_perm_str(perm2)},
                self.backend.get_pfps(u1, perm1.model_permission, User),
                )
        self.assertEqual(
                {helpers.get_perm_str(perm1), helpers.get_perm_str(perm2)},
                self.backend.get_pfps(u1, perm1.model_permission, u1),
                )

    def test_get_pfps_obj_pfps(self):
        """
        Return set containing pfp 'app_label.codename' strings if user has field permission. Set
        should not contain non-object permissions or object permissions that apply to a different
        action.
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'password')
        perm4 = helpers.create_pfp('cims_auth', 'user', 'add_user', 'is_superuser')
        u1.user_permissions.add(perm3)
        obj_ctype = ContentType.objects.get_for_model(User)
        for perm in (perm1, perm2, perm4):
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        self.assertEqual(
                {helpers.get_perm_str(perm1), helpers.get_perm_str(perm2)},
                self.backend.get_pfps(u1, perm1, u2),
                )

    def test_get_pfps_obj_perm(self):
        """
        Return set() indicating access to all fields on object. Should not contain non-object
        permissions or object permissions that apply to a different action.
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        perm1 = helpers.get_perm('cims_auth', 'user', 'change_user')
        perm2 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'is_staff')
        perm3 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'password')
        perm4 = helpers.create_pfp('cims_auth', 'user', 'add_user', 'is_superuser')
        u1.user_permissions.add(perm2, perm3)
        obj_ctype = ContentType.objects.get_for_model(User)
        for perm in (perm1, perm4):
            UserObjectPermission.objects.create(
                    user=u1,
                    permission=perm,
                    content_type=obj_ctype,
                    object_id=u2.id,
                    )
        self.assertEqual(set(), self.backend.get_pfps(u1, perm1, u2))

    def test_has_pfps_none(self):
        """Return False if user doesn't have applicable permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.get_perm('cims_auth', 'user', 'add_user')
        self.assertFalse(self.backend.has_pfps(u1, perm1, User))

    def test_has_pfps_empty_set(self):
        """Return False if user has full model permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        perm2 = helpers.get_perm('cims_auth', 'user', 'change_user')
        u1.user_permissions.add(perm2)
        self.assertFalse(self.backend.has_pfps(u1, perm1, User))

    def test_has_pfps_field_perms(self):
        """Return True if user has field permission."""
        u1 = User.objects.get(username='u1')
        perm1 = helpers.create_pfp('cims_auth', 'user', 'change_user', 'email')
        u1.user_permissions.add(perm1)
        self.assertTrue(self.backend.has_pfps(u1, perm1, User))


@override_settings(
        AUTHENTICATION_BACKENDS=[
            'cims.auth.backends.CIMSBackend',
            'cims.auth.backends.ConditionalPermissionsMapBackend',
            ],
        PFP_MODELS=[('tests_auth', 'pizza'), ('tests_auth', 'pie')],
        PFP_IGNORE_PERMS={}
        )
class TestConditionalPermissionBackend(TestCase):
    """Confirm operation of `ConditionalPermissionsMapBackend`."""

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')
        cls.map1 = ConditionalPermissionsMap.objects.create(
                description='Master chefs',
                test_model=ContentType.objects.get_for_model(Cuisine),
                test='Is master chef',
                )
        cls.map2 = ConditionalPermissionsMap.objects.create(
                description='Sous chefs',
                test_model=ContentType.objects.get_for_model(Cuisine),
                test='Is sous chef',
                )
        perm1 = helpers.get_perm('tests_auth', 'pizza', 'add_pizza')
        perm2 = helpers.get_perm('tests_auth', 'pizza', 'change_pizza')
        perm3 = helpers.get_perm('tests_auth', 'pie', 'add_pie')
        perm4 = helpers.get_perm('tests_auth', 'pizza', 'change_pizza__toppings')
        perm5 = helpers.get_perm('tests_auth', 'pie', 'change_pie__fillings')
        cls.map1.conditional_permissions.add(perm1, perm2, perm3)
        cls.map2.conditional_permissions.add(perm4, perm5)
        cls.backend = ConditionalPermissionsMapBackend()
        # Pizzas etc.
        cls.italian = Cuisine.objects.create(name='Italian')
        cls.american = Cuisine.objects.create(name='American')
        cls.pizza = Pizza.objects.create(
                name='Capriciosa',
                cuisine=cls.italian,
                )
        cls.pie = Pie.objects.create(name='Apple Pie', cuisine=cls.american)

    def setUp(self):
        self.italian.refresh_from_db()
        self.american.refresh_from_db()

    @override_settings(
            AUTHENTICATION_BACKENDS=['cims.auth.backends.ConditionalPermissionsMapBackend'],
            )
    def test_init_no_cimsbackend(self):
        """Configuration assertion fails."""
        self.assertRaisesMessage(
                AssertionError,
                ('ConditionalPermissionsMapBackend requires cims.auth.backends.CIMSBackend be '
                 'configured before it in django.settings.AUTHENTICATION_BACKENDS.'),
                ConditionalPermissionsMapBackend,
                )

    @override_settings(
            AUTHENTICATION_BACKENDS=[
                'cims.auth.backends.ConditionalPermissionsMapBackend',
                'cims.auth.backends.CIMSBackend',
                ],
            )
    def test_init_before_cimsbackend(self):
        """Configuration assertion fails."""
        self.assertRaisesMessage(
                AssertionError,
                ('ConditionalPermissionsMapBackend requires cims.auth.backends.CIMSBackend be '
                 'configured before it in django.settings.AUTHENTICATION_BACKENDS.'),
                ConditionalPermissionsMapBackend,
                )

    def test_get_all_permissions_invalid_requests(self):
        """Conditions where we automatically return an empty set do so."""
        # Anonymous user
        user = AnonymousUser()
        self.assertSetEqual(set(), self.backend.get_all_permissions(user, obj=self.pizza))
        # No object provided
        user = User.objects.create_user(username='user', password='sekret')
        self.italian.master_chef = user
        self.italian.save()
        self.assertSetEqual(set(), self.backend.get_all_permissions(user))
        # Object hasn't been saved yet
        pizza = Pizza(name='Margarita', cuisine=self.italian)
        self.assertSetEqual(set(), self.backend.get_all_permissions(user, obj=pizza))
        # User isn't active
        user.is_active = False
        self.assertSetEqual(set(), self.backend.get_all_permissions(user, obj=self.pizza))
        # Superuser
        user.is_active = True
        user.is_superuser = True
        self.assertSetEqual(set(), self.backend.get_all_permissions(user, obj=self.pizza))
        # Never reached point where we created a permission cache for the user
        self.assertFalse(hasattr(user, '_cims_conditional_cache'))

    def test_get_all_permissions_mappers(self):
        """Permissions are retrieved from appropriate mappers."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        self.italian.master_chef = user1
        self.italian.sous_chef = user1
        self.italian.save()
        # Perms from multiple mappers merge properly
        self.assertSetEqual(
                {'tests_auth.add_pizza', 'tests_auth.change_pizza'},
                self.backend.get_all_permissions(user1, self.pizza),
                )
        ctype_id = ContentType.objects.get_for_model(self.pizza).id
        self.assertDictEqual(
                {
                    ctype_id: {
                        self.pizza.id: {
                            'tests_auth.add_pizza': set(),
                            'tests_auth.change_pizza': set(),
                            },
                        },
                    },
                user1._cims_conditional_cache,
                )
        # Perms are retrieved from cache if it exists
        perm3 = helpers.get_perm('tests_auth', 'pizza', 'delete_pizza')
        self.map1.conditional_permissions.add(perm3)
        self.assertSetEqual(
                {'tests_auth.add_pizza', 'tests_auth.change_pizza'},
                self.backend.get_all_permissions(user1, self.pizza),
                )

    def test_has_perm_invalid_requests(self):
        """Conditions where we automatically return False do so."""
        user = User.objects.create_user(username='user', password='sekret')
        self.italian.master_chef = user
        self.italian.save()
        perm_str = 'tests_auth.add_pizza'
        # Object hasn't been saved yet
        pizza = Pizza(name='Margarita', cuisine=self.italian)
        self.assertFalse(self.backend.has_perm(user, perm_str, obj=pizza))
        # User isn't active
        user.is_active = False
        self.assertFalse(self.backend.has_perm(user, perm_str, obj=self.pizza))
        # Superuser
        user.is_active = True
        user.is_superuser = True
        self.assertRaisesMessage(
                AssertionError,
                'ConditionalPermissionsMapBackend.has_perm() got an unexpected superuser.',
                *[self.backend.has_perm, user, perm_str],
                **{'obj': self.pizza}
                )
        # Never reached point where we created a permission cache for the user
        self.assertFalse(hasattr(user, '_cims_conditional_cache'))

    def test_has_perm(self):
        """Return correctly if user has permission."""
        # Anonymous user
        anon_user = AnonymousUser()
        self.assertFalse(self.backend.has_perm(anon_user, 'tests_auth.delete_pie', obj=self.pie))
        # Actual user
        user1 = User.objects.create_user(username='user1', password='sekret')
        # Correctly populates cache and tests permission
        self.american.sous_chef = user1
        self.american.save()
        self.assertTrue(self.backend.has_perm(user1, 'tests_auth.change_pie', obj=self.pie))
        self.assertTrue(
                self.backend.has_perm(user1, 'tests_auth.change_pie__fillings', obj=self.pie)
                )
        self.assertFalse(self.backend.has_perm(user1, 'tests_auth.change_pie__name', obj=self.pie))
        self.assertFalse(self.backend.has_perm(user1, 'tests_auth.delete_pie', obj=self.pie))
        ctype_id = ContentType.objects.get_for_model(self.pie).id
        self.assertDictEqual(
                {
                    ctype_id: {
                        self.pie.id: {
                            'tests_auth.change_pie': {'tests_auth.change_pie__fillings'},
                            },
                        },
                    },
                user1._cims_conditional_cache,
                )
        # Answers from cache
        perm2 = helpers.get_perm('tests_auth', 'pie', 'change_pie__name')
        self.map2.conditional_permissions.add(perm2)
        self.assertFalse(self.backend.has_perm(user1, 'tests_auth.change_pie__name', obj=self.pie))
        # Answers True if looking for model permission granted by map
        self.assertTrue(self.backend.has_perm(user1, 'tests_auth.change_pie'))
        # Answers False if looking for model permission not granted by map
        self.assertFalse(self.backend.has_perm(user1, 'tests_auth.delete_pie'))

    def test_has_module_perm_invalid_requests(self):
        """Conditions where we automatically return False do so."""
        # Inactive user
        user = User.objects.create_user(username='user', password='sekret')
        user.is_active = False
        self.italian.master_chef = user
        self.italian.save()
        self.assertFalse(self.backend.has_module_perms(user, 'tests_auth'))
        # Superuser
        user.is_active = True
        user.is_superuser = True
        self.assertRaisesMessage(
                AssertionError,
                'ConditionalPermissionsMapBackend.has_module_perms() got an unexpected superuser.',
                *[self.backend.has_module_perms, user, 'tests_auth']
                )
        # Didn't create cache
        self.assertFalse(hasattr(user, '_cims_conditional_cache'))
        # Anonymous user
        anon_user = AnonymousUser()
        self.assertFalse(self.backend.has_module_perms(anon_user, 'tests_auth'))

    def test_has_module_perm_no_perm(self):
        """
        Return correctly if user doesn't have a permission in app_label from a conditional
        permission.
        """
        user1 = User.objects.create_user(username='user1', password='sekret')
        self.american.sous_chef = user1
        self.american.save()
        # No perms in app_label
        self.assertFalse(self.backend.has_module_perms(user1, 'cims'))

    def test_has_module_perm_via_model_perm(self):
        """
        Return correctly if user has permission in app_label from a conditional permission granted
        via model level permission.
        """
        user1 = User.objects.create_user(username='user1', password='sekret')
        self.american.sous_chef = user1
        self.american.save()
        self.assertTrue(self.backend.has_module_perms(user1, 'tests_auth'))


@override_settings(
        AUTHENTICATION_BACKENDS=[
            'cims.auth.backends.ObjectStatusPermissionsMapBackend',
            'cims.auth.backends.CIMSBackend',
            ],
        PFP_MODELS=[('tests_auth', 'pizza'), ('tests_auth', 'pie'), ('tests_auth', 'cuisine')],
        PFP_IGNORE_PERMS={}
        )
class TestObjectStatusPermissionBackend(TestCase):
    """Confirm operation of `ObjectStatusPermissionsMapBackend`."""

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')
        cls.role = Role.objects.create(name='Multigrain role')
        cls.draft = ReviewStatus.objects.validated_create(name='Draft')
        cls.submitted = ReviewStatus.objects.validated_create(name='Submitted')
        cls.published = ReviewStatus.objects.validated_create(name='Published')
        perm1 = helpers.get_perm('tests_auth', 'pie', 'change_pie__fillings')
        perm2 = helpers.get_perm('tests_auth', 'pizza', 'change_pizza__toppings')
        perm3 = helpers.get_perm('tests_auth', 'cuisine', 'change_cuisine')
        perm4 = helpers.get_perm('tests_auth', 'ingredient', 'change_ingredient')
        cls.mapper = ObjectStatusPermissionsMap.objects.validated_create(
                description='Draft pizzas.',
                check_content_type=ContentType.objects.get_for_model(Cuisine),
                related_content_types=[
                    ContentType.objects.get_for_model(Pie),
                    ContentType.objects.get_for_model(Pizza),
                    ContentType.objects.get_for_model(Ingredient),
                    ],
                permitted_roles=[cls.role],
                allowed_states=[cls.draft],
                denied_permissions=[perm1, perm2, perm3, perm4],
                )
        cls.backend = ObjectStatusPermissionsMapBackend()
        # Pizzas etc.
        cls.italian = Cuisine.objects.create(name='Italian', status=cls.draft)
        cls.american = Cuisine.objects.create(name='American', status=cls.published)
        cls.pizza = Pizza.objects.create(
                name='Capriciosa',
                cuisine=cls.italian,
                )
        cls.pie = Pie.objects.create(name='Apple Pie', cuisine=cls.american)
        cls.apple = Ingredient.objects.create(name='Apple')
        cls.pie.fillings.add(cls.apple)

    @override_settings(
            AUTHENTICATION_BACKENDS=['cims.auth.backends.ObjectStatusPermissionsMapBackend'],
            )
    def test_init_no_cimsbackend(self):
        """Configuration assertion fails."""
        self.assertRaisesMessage(
                AssertionError,
                ('ObjectStatusPermissionsMapBackend must be configured before '
                 'cims.auth.backends.CIMSBackend in django.settings.AUTHENTICATION_BACKENDS.'),
                ObjectStatusPermissionsMapBackend,
                )

    @override_settings(
            AUTHENTICATION_BACKENDS=[
                'cims.auth.backends.CIMSBackend',
                'cims.auth.backends.ObjectStatusPermissionsMapBackend',
                ],
            )
    def test_init_after_cimsbackend(self):
        """Configuration assertion fails."""
        self.assertRaisesMessage(
                AssertionError,
                ('ObjectStatusPermissionsMapBackend must be configured before '
                 'cims.auth.backends.CIMSBackend in django.settings.AUTHENTICATION_BACKENDS.'),
                ObjectStatusPermissionsMapBackend,
                )

    def test_has_perm_invalid_requests(self):
        """Conditions where we automatically return False do so."""
        user = User.objects.create_user(username='user', password='sekret')
        perm1 = helpers.get_perm('tests_auth', 'pizza', 'add_pizza')
        user.user_permissions.add(perm1)
        perm_str = helpers.get_perm_str(perm1)
        # Object is None
        self.assertFalse(self.backend.has_perm(user, perm_str, obj=self.pizza))
        # Object hasn't been saved yet
        pizza = Pizza(name='Margarita')
        self.assertFalse(self.backend.has_perm(user, perm_str, obj=pizza))
        # User isn't active
        user.is_active = False
        self.assertFalse(self.backend.has_perm(user, perm_str, obj=self.pizza))
        # Superuser
        user.is_active = True
        user.is_superuser = True
        self.assertFalse(self.backend.has_perm(user, perm_str, obj=self.pizza))

    def test_has_perm(self):
        """Return correctly if user has permission."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        # Object with same ContentType as check_content_type, status denies access, check model and
        # field permissions
        self.assertRaises(
                PermissionDenied,
                *[self.backend.has_perm, user1, 'tests_auth.change_cuisine'],
                **{'obj': self.american}
                )
        self.assertRaises(
                PermissionDenied,
                *[self.backend.has_perm, user1, 'tests_auth.change_cuisine__name'],
                **{'obj': self.american}
                )
        # Object with different ContentType as check_content_type, but is in related_content_types.
        # Status denies access to field permission but not model permission
        self.assertRaises(
                PermissionDenied,
                *[self.backend.has_perm, user1, 'tests_auth.change_pie__fillings'],
                **{'obj': self.pie}
                )
        self.assertFalse(self.backend.has_perm(user1, 'tests_auth.change_pie', obj=self.pie))
        # Object in related_content_types but no lookup method
        self.assertFalse(
                self.backend.has_perm(user1, 'tests_auth.change_ingredient', obj=self.apple)
                )
        # Unrelated model instance
        unrelated_obj = BasicModel.objects.create(text='Unrelated')
        self.assertFalse(
                self.backend.has_perm(user1, 'tests_auth.change_BasicModel', obj=unrelated_obj)
                )
        # Status in allowed_states
        self.assertFalse(
                self.backend.has_perm(user1, 'tests_auth.change_pizza__toppings', obj=self.pizza)
                )
        # User in permitted roles
        user2 = User.objects.create_user(username='user2', password='sekret')
        user2.roles.add(self.role)
        self.assertFalse(
                self.backend.has_perm(user2, 'tests_auth.change_cuisine', obj=self.american)
                )
        # User in permitted users
        user3 = User.objects.create_user(username='user3', password='sekret')
        self.mapper.permitted_users.add(user3)
        self.assertFalse(
                self.backend.has_perm(user3, 'tests_auth.change_cuisine', obj=self.american)
                )
        # Anonymous user
        anon_user = AnonymousUser()
        anon_user.is_active = True
        self.assertRaises(
                PermissionDenied,
                *[self.backend.has_perm, anon_user, 'tests_auth.change_cuisine'],
                **{'obj': self.american}
                )
