from django.conf import settings
from django.db import models
from cims.models import ReviewStatus


# Models for testing ConditionalPermissionsMap
class CuisineManager(models.Manager):

    def get_cuisine_for_object(self, obj):
        if isinstance(obj, (Pie, Pizza)):
            return obj.cuisine
        return None


class Cuisine(models.Model):
    name = models.CharField(max_length=80)
    master_chef = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='mastered_cuisines',
            )
    sous_chef = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='understudy_cuisines',
            )
    status = models.ForeignKey(ReviewStatus, on_delete=models.SET_NULL, null=True, blank=True)
    objects = CuisineManager()


class Ingredient(models.Model):
    name = models.CharField(max_length=80)


class Pizza(models.Model):
    cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=80)
    toppings = models.ManyToManyField(Ingredient)


class Pie(models.Model):
    cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=80)
    fillings = models.ManyToManyField(Ingredient)
