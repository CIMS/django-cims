from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.test import TestCase

from cims.auth.models import Role, UserObjectPermission
from tests.helpers import get_perm

User = get_user_model()


class RoleManagerTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        Role.objects.create(name='r1')
        Role.objects.create(name='r2')
        Role.objects.create(name='r3')
        Role.objects.create(name='r4')
        Role.objects.create(name='r5')
        Role.objects.create(name='r6')
        pwd = make_password('password')
        User.objects.create_user(username='u1', password=pwd)

    def test_get_by_natural_key(self):
        r1 = Role.objects.get(name='r1')
        self.assertEqual(r1.id, Role.objects.get_by_natural_key('r1').id)

    def test_get_roles_for_user_wrong_type(self):
        """Raise Exception if user argument is non-user object."""
        self.assertRaisesMessage(
                TypeError,
                ('Object passed in is not an instance of the current user '
                    'model.'),
                args=[Role.objects.get_roles_for_user, int(1)],
                )

    def test_get_roles_for_user_no_roles(self):
        """User with no roles returns an empty set."""
        u1 = User.objects.get(username='u1')
        self.assertSetEqual(set(), Role.objects.get_roles_for_user(u1))

    def test_get_roles_for_user_caching(self):
        """Check cache is set and result returned from cache."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        self.assertSetEqual(set(), Role.objects.get_roles_for_user(u1))
        self.assertTrue(hasattr(u1, '_roles_cache'))
        u1.roles.add(r1)
        self.assertSetEqual(set(), Role.objects.get_roles_for_user(u1))

    def test_get_roles_for_user_add_role(self):
        """Return roles with direct membership."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        u1.roles.add(r2)
        chk_set = {r1, r2}
        self.assertSetEqual(chk_set, Role.objects.get_roles_for_user(u1))
        
    def test_get_roles_for_user_parent_role(self):
        """Role with parent role, return both."""  
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r2.child_roles.add(r1)
        chk_set = {r1, r2}
        self.assertSetEqual(chk_set, Role.objects.get_roles_for_user(u1))
        
    def test_get_roles_for_user_child(self):
        """Shouldn't include child roles in result."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.child_roles.add(r2)
        chk_set = {r1}
        self.assertSetEqual(chk_set, Role.objects.get_roles_for_user(u1))
        
    def test_get_roles_for_user_grandparents(self):
        """Traverse graph of ancestors, return set includes grandparents."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        r3 = Role.objects.get(name='r3')
        r4 = Role.objects.get(name='r4')
        r5 = Role.objects.get(name='r5')
        u1.roles.add(r1)
        r2.child_roles.add(r1)
        r3.child_roles.add(r1)
        r4.child_roles.add(r2)
        r5.child_roles.add(r3)
        chk_set = {r1, r2, r3, r4, r5}
        self.assertSetEqual(chk_set, Role.objects.get_roles_for_user(u1))
        
    def test_get_roles_for_user_loop(self):
        """Traversal of graph with cycle doesn't get caught in loop."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        r3 = Role.objects.get(name='r3')
        r4 = Role.objects.get(name='r4')
        r5 = Role.objects.get(name='r5')
        u1.roles.add(r1)
        r2.child_roles.add(r1)
        r3.child_roles.add(r1)
        r4.child_roles.add(r2)
        r5.child_roles.add(r3)
        r1.child_roles.add(r5)
        chk_set = {r1, r2, r3, r4, r5}
        self.assertSetEqual(chk_set, Role.objects.get_roles_for_user(u1))

    def test_get_roles_harchy_no_roles(self):
        """No role membership returns empty list."""
        u1 = User.objects.get(username='u1')
        self.assertListEqual([], Role.objects.get_role_hierarchy_for_user(u1))

    def test_get_roles_harchy_cache(self):
        """Cache is created and results returned from cache."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        self.assertListEqual([], Role.objects.get_role_hierarchy_for_user(u1))
        self.assertTrue(hasattr(u1, '_roles_hierarchical_cache'))
        u1.roles.add(r1)
        self.assertListEqual([], Role.objects.get_role_hierarchy_for_user(u1))

    def test_get_roles_harchy_role(self):
        """Direct membership only returns list with single set."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        self.assertListEqual([{r1}], Role.objects.get_role_hierarchy_for_user(u1))

    def test_get_roles_harchy_grandparents(self):
        """Create correct list of sets from graph traversal."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        r3 = Role.objects.get(name='r3')
        r4 = Role.objects.get(name='r4')
        r5 = Role.objects.get(name='r5')
        u1.roles.add(r1)
        u1.roles.add(r2)
        r3.child_roles.add(r1)
        r4.child_roles.add(r2)
        r4.child_roles.add(r3)
        r5.child_roles.add(r4)
        self.assertListEqual(
                [{r1, r2}, {r3, r4}, {r5}],
                Role.objects.get_role_hierarchy_for_user(u1),
                )

    def test_get_roles_harchy_child(self):
        """Graph traversal should not include child roles."""
        u1 = User.objects.get(username='u1')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        r1.child_roles.add(r2)
        self.assertListEqual([{r1}], Role.objects.get_role_hierarchy_for_user(u1))


class RoleTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        Role.objects.create(name='r1')
        Role.objects.create(name='r2')
        Role.objects.create(name='r3')
        Role.objects.create(name='r4')
        Role.objects.create(name='r5')
        pwd = make_password('password')
        User.objects.create_user(username='u1', password=pwd)
        User.objects.create_user(username='u2', password=pwd)
        User.objects.create_user(username='u3', password=pwd)
        User.objects.create_user(username='u4', password=pwd)
        User.objects.create_user(username='u5', password=pwd)

    def test_get_users_no_users(self):
        """Role with no users."""
        r1 = Role.objects.get(name='r1')
        self.assertSetEqual(set(), r1.all_users)

    def test_get_users_has_users(self):
        """Return users who are members of role."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        u2.roles.add(r1)
        self.assertSetEqual({u1, u2}, r1.all_users)

    def test_get_users_caching(self):
        """Cache populated properly, results returned from cache."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        u1.roles.add(r1)
        self.assertSetEqual({u1}, r1.all_users)
        u2.roles.add(r1)
        self.assertSetEqual({u1}, r1.all_users)

    def test_get_users_traverse_graph(self):
        """Include users from child roles."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        u4 = User.objects.get(username='u4')
        u5 = User.objects.get(username='u5')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        r3 = Role.objects.get(name='r3')
        r4 = Role.objects.get(name='r4')
        r5 = Role.objects.get(name='r5')
        u1.roles.add(r1)
        u2.roles.add(r1)
        u2.roles.add(r2)
        u1.roles.add(r3)
        u3.roles.add(r3)
        u2.roles.add(r5)
        u3.roles.add(r4)
        u4.roles.add(r4)
        u5.roles.add(r5)
        r1.child_roles.add(r2)
        r1.child_roles.add(r3)
        r2.child_roles.add(r4)
        r3.child_roles.add(r5)
        self.assertSetEqual({u1, u2, u3, u4, u5}, r1.all_users)

    def test_get_users_ignore_parent(self):
        """Graph traversal should ignore parent roles."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        u1.roles.add(r1)
        u2.roles.add(r2)
        r2.child_roles.add(r1)
        self.assertSetEqual({u1}, r1.all_users)

    def test_get_users_loop(self):
        """
        A role graph with cycles should return all users in the cycle, and exit
        processing...
        """
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        r1 = Role.objects.get(name='r1')
        r2 = Role.objects.get(name='r2')
        r3 = Role.objects.get(name='r3')
        u1.roles.add(r1)
        u2.roles.add(r2)
        u3.roles.add(r3)
        r1.child_roles.add(r2)
        r2.child_roles.add(r3)
        r3.child_roles.add(r1)
        self.assertSetEqual({u1, u2, u3}, r1.all_users)


class TestBaseObjectPermission(TestCase):
    
    def test_save(self):
        """
        Attempting to save an object permission with a different permission
        content type to the object referenced should raise a `ValidationError`.
        """
        perm = get_perm('cims_auth', 'user', 'change_user')
        ctype = ContentType.objects.get(app_label='auth', model='permission')
        u1 = User.objects.create_user(username='u1', password='password')
        uperm = UserObjectPermission(
                user=u1,
                permission=perm,
                content_type=ctype,
                object_id=1,
                )
        self.assertRaisesMessage(
                ValidationError,
                "Cannot persist permission not designed for this class "
                "(permission's type is {} and object's type is {})".format(
                    uperm.content_type, ctype 
                    ),
                args=[uperm.save],
                )
