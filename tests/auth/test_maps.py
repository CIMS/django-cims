"""
Tests for ConditionalPermissionsMap and related objects.
"""
from django.contrib.auth.models import AnonymousUser
from django.test import TestCase

from cims.auth import maps
from cims.auth.models import User
from cims.models import Course, CourseInstance, CourseVersion, OrganisationalUnit, ReviewStatus


@maps.register('Test test test')
def _test_test_test(*args, **kwargs):
    return False


class TestRegisterDecorator(TestCase):

    def test_register_test_func(self):
        """cims.auth.maps.register adds the function to the dict of test functions."""
        user = AnonymousUser()
        self.assertIn('Test test test', maps.mapper_tests)
        self.assertFalse(maps.mapper_tests['Test test test'](user))

    def test_register_twice(self):
        """Registering the same name twice raises an exception."""
        def _test_test():
            return True

        wrapper = maps.register('Test test test')

        self.assertRaisesMessage(
                maps.AlreadyRegistered,
                'Mapper test function name Test test test already registered.',
                *[wrapper, _test_test]
                )


# Test application test functions
class TestIsClassConvener(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user1 = User.objects.create_user(username='user1', password='sekret')
        cls.user2 = User.objects.create_user(username='user2', password='sekret')
        cls.course = Course.objects.validated_create(code='TEST123456')
        cls.status = ReviewStatus.objects.validated_create(name='Approved')
        cv = CourseVersion.objects.validated_create(
                course=cls.course,
                status=cls.status,
                version_descriptor='Test Version',
                )
        ou = OrganisationalUnit.objects.validated_create(name='Test OU', short_name='TOU')
        cls.ci1 = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance1',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )
        cls.ci1.conveners.add(cls.user1)
        cls.ci2 = CourseInstance.objects.validated_create(
                courseversion=cv,
                instance_descriptor='Test Instance2',
                taught_by=[ou.pk],
                start_date='2017-01-01',
                end_date='2017-01-02',
                )

    def test_test_obj_wrong_class(self):
        """Passing a test object of the wrong type raises exception."""
        test_func = maps.mapper_tests['Is class convener']
        self.assertRaisesMessage(
                AssertionError,
                'Attempting to check class convener on an object that isn\'t a CourseInstance.',
                *[test_func, self.user1, self.user2]
                )

    def test_model_wide_check(self):
        """Test returns correct results when test_obj is None."""
        test_func = maps.mapper_tests['Is class convener']
        # User1 is convener of a course - True
        self.assertTrue(test_func(self.user1))
        # User2 isn't convener of a course - False
        self.assertFalse(test_func(self.user2))

    def test_specific_object_check(self):
        """Test returns correct results."""
        test_func = maps.mapper_tests['Is class convener']
        # User1 is convener of this course - True
        self.assertTrue(test_func(self.user1, self.ci1))
        # User1 isn't convener of this course - False
        self.assertFalse(test_func(self.user1, self.ci2))


# Test application test functions
class TestIsCourseCustodian(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user1 = User.objects.create_user(username='user1', password='sekret')
        cls.user2 = User.objects.create_user(username='user2', password='sekret')
        cls.course = Course.objects.validated_create(code='TEST123456')
        cls.status = ReviewStatus.objects.validated_create(name='Approved')
        cls.cv1 = CourseVersion.objects.validated_create(
                course=cls.course,
                status=cls.status,
                version_descriptor='Test Version',
                )
        cls.cv1.custodians.add(cls.user1)
        cls.cv2 = CourseVersion.objects.validated_create(
                course=cls.course,
                status=cls.status,
                version_descriptor='Test Version 2',
                )

    def test_test_obj_wrong_class(self):
        """Passing a test object of the wrong type raises exception."""
        test_func = maps.mapper_tests['Is course custodian']
        self.assertRaisesMessage(
                AssertionError,
                'Attempting to check course custodian on an object that isn\'t a CourseVersion.',
                *[test_func, self.user1, self.user2]
                )

    def test_model_wide_check(self):
        """Test returns correct results when test_obj is None."""
        test_func = maps.mapper_tests['Is course custodian']
        # User1 is custodian of a course - True
        self.assertTrue(test_func(self.user1))
        # User2 isn't custodian of a course - False
        self.assertFalse(test_func(self.user2))

    def test_specific_object_check(self):
        """Test returns correct results."""
        test_func = maps.mapper_tests['Is course custodian']
        # User1 is custodian of this course - True
        self.assertTrue(test_func(self.user1, self.cv1))
        # User1 isn't custodian of this course - False
        self.assertFalse(test_func(self.user1, self.cv2))
