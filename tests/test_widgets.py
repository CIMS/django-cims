"""
Test fields from CIMS application.
"""
import datetime
from django.test import override_settings, TestCase

from cims import widgets


@override_settings(LANGUAGE_CODE='en-au', TIME_ZONE='Australia/ACT', USE_I18N=True, USE_L10N=True,
                   USE_TZ=True)
class DateInputTest(TestCase):

    def test_dateinput_returns_isoformat(self):
        """
        self.format_value() should return an ISO formatted date so html5 date pickers will
        understand the value.
        """
        widget = widgets.DateInput()
        today = datetime.date.today()
        self.assertEqual(today.isoformat(), widget.format_value(today))

    def test_dateinput_override_format(self):
        """We can still override the date format if we want to."""
        widget = widgets.DateInput(format='%d/%m/%Y')
        today = datetime.date.today()
        self.assertEqual(today.strftime('%d/%m/%Y'), widget.format_value(today))
