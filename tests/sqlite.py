#!/usr/bin/env python
# SQLite database setting for test suite.

DATABASES = {
    "default": {'ENGINE': 'django.db.backends.sqlite3'},
    }

SECRET_KEY = "cims_tests_secret_key"

PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher',)
