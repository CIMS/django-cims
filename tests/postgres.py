#!/usr/bin/env python
# Postgres database settings for test suite.

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'cims_tox',
        'USER': 'cims_tox',
        'PASSWORD': "",
        'HOST': 'postgres',
        'PORT': '5432',
    },
}

SECRET_KEY = "cims_tests_secret_key"

PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher',)
