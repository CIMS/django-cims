"""
Tests for CIMS formsets.
"""
from django.contrib.auth import get_user_model
from django.test import RequestFactory, TestCase

from cims.models import (Course, CourseVersion, LearningOutcome, ReviewStatus, StandardObjective,
                         StandardsBody, StandardsFramework, Weight, WeightedLOToSO, WeightingScheme)
from cims.viewhelpers import (CourseVersionMappingHelper, DetailViewTabHelper, MappingTabHelper,
                              RelatedModelTabHelper)
from .helpers import get_perm
from .models import Author, Book

User = get_user_model()


class DetailViewTabHelperTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        class BookTabHelper(DetailViewTabHelper):
            label = 'Books!'
            template = 'totally_a_template.html'

        cls.BookTabHelper = BookTabHelper
        cls.author = Author.objects.validated_create(name='Herman Melville')
        cls.book = Book.objects.validated_create(title='Moby Dick', author=cls.author)
        cls.factory = RequestFactory()

    def test_no_template_configured(self):
        """ValueError raised if no template is configure."""
        class BookTabHelper(DetailViewTabHelper):
            pass

        self.assertRaisesMessage(
                ValueError,
                'BookTabHelper has no template specified',
                *[BookTabHelper, False, False]
                )

    def test_simple_methods(self):
        """Basic function tests of simple methods."""
        request = self.factory.get('/')
        request.user = User.objects.create_user(username='user', password='sekret')
        helper = self.BookTabHelper(Author, self.book, request=request)
        # init() sets a bunch of attributes
        self.assertEqual(helper.parent_model, Author)
        self.assertEqual(helper.instance, self.book)
        self.assertEqual(helper.request, request)
        self.assertEqual(helper.permission_required, [])
        # get_context() returns dict containing self
        context = helper.get_context()
        self.assertEqual(context['tab_helper'], helper)
        # get_label() returns self.label
        self.assertEqual(helper.get_label(), helper.label)
        # has_permission() checks permissions
        request.user.user_permissions.add(get_perm('tests', 'book', 'change_book'))
        self.assertTrue(helper.has_permission(helper.request))


class RelatedModelTabHelperTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.wscheme1 = WeightingScheme.objects.validated_create(name='wscheme1')
        cls.wscheme2 = WeightingScheme.objects.validated_create(name='wscheme2')
        cls.weight1 = Weight.objects.validated_create(weightingscheme=cls.wscheme1, value='10')
        cls.weight2 = Weight.objects.validated_create(weightingscheme=cls.wscheme1, value='20')
        cls.weight3 = Weight.objects.validated_create(weightingscheme=cls.wscheme2, value='30')

    def test_no_model(self):
        """Raises exception if no model configured."""
        class WeightHelper(RelatedModelTabHelper):
            pass

        self.assertRaisesMessage(
                ValueError,
                'WeightHelper has no model class specified.',
                *[WeightHelper, WeightingScheme, self.wscheme1]
                )

    def test_no_template(self):
        """Raises exception if no template configured."""
        class WeightHelper(RelatedModelTabHelper):
            template = None
            model = Weight

        self.assertRaisesMessage(
                ValueError,
                'WeightHelper has no template specified.',
                *[WeightHelper, WeightingScheme, self.wscheme1]
                )

    def test_init(self):
        """__init__ sets some attributes correctly."""
        class WeightHelper(RelatedModelTabHelper):
            model = Weight

        helper = WeightHelper(WeightingScheme, self.wscheme1)
        self.assertEqual(helper.opts, Weight._meta)
        self.assertEqual(helper.label, Weight._meta.verbose_name_plural)
        self.assertEqual(helper.object_detail_template, 'cims/weight_detail.html')

    def test_queryset(self):
        """Queryset is filtered according to relationship and ordered (if ordering is set)."""
        class WeightHelper(RelatedModelTabHelper):
            model = Weight
            ordering = ['-value']

        helper = WeightHelper(WeightingScheme, self.wscheme1)
        self.assertQuerysetEqual(
                helper.queryset,
                [repr(self.weight2), repr(self.weight1)]
                )

    def test_get_context(self):
        """Context is updated with additional data."""
        class WeightHelper(RelatedModelTabHelper):
            model = Weight
            object_detail_template = 'Foo1'
            linked_objects_template = 'Foo2'
            linked_objects_title = 'Foo3'

        helper = WeightHelper(WeightingScheme, self.wscheme1)
        context = helper.get_context()
        self.assertEqual('cims.change_weight', context['update_permission'])
        self.assertEqual('Foo1', context['related_object_template'])
        self.assertEqual('Foo2', context['linked_objects_template'])
        self.assertEqual('Foo3', context['linked_objects_title'])


class MappingTabHelperTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        class MappingHelper(MappingTabHelper):
            source_model = LearningOutcome
            target_model = StandardObjective
            mapping_model = WeightedLOToSO

        cls.Helper = MappingHelper

    def test_init_no_source_model(self):
        """No source model configure raises exception"""
        class MappingHelper(MappingTabHelper):
            pass

        self.assertRaisesMessage(
                AssertionError,
                'MappingHelper must have source, mapping and target models configured.',
                *[MappingHelper, None, None]
                )

    def test_init_no_target_model(self):
        """No target model configure raises exception"""
        class MappingHelper(MappingTabHelper):
            source_model = LearningOutcome

        self.assertRaisesMessage(
                AssertionError,
                'MappingHelper must have source, mapping and target models configured.',
                *[MappingHelper, None, None]
                )

    def test_init_no_mapping_model(self):
        """No mapping model configure raises exception"""
        class MappingHelper(MappingTabHelper):
            source_model = LearningOutcome
            target_model = StandardObjective

        self.assertRaisesMessage(
                AssertionError,
                'MappingHelper must have source, mapping and target models configured.',
                *[MappingHelper, None, None]
                )

    def test_unimplemented_methods(self):
        """Exceptions are raised by unimplemented methods."""
        helper = self.Helper(None, None)
        self.assertRaisesMessage(
                NotImplementedError,
                'Helper.create_mapping_table_row() requires implementation.',
                *[helper.create_mapping_table_row, None, None]
                )
        self.assertRaisesMessage(
                NotImplementedError,
                'Helper.get_courseversion() requires implementation.',
                helper.get_courseversion
                )
        self.assertRaisesMessage(
                NotImplementedError,
                'Helper.get_target_model_tuples() requires implementation.',
                *[helper.get_target_model_tuples, None]
                )


class MappingTabHelperAddedTest(TestCase):
    """
    To test MappingTabHelper.get_context() we need to use a concrete class so use one of the
    ones create in CIMS. These tests are split out so they don't clutter other tests for
    MappingTabHelper.
    """
    @classmethod
    def setUpTestData(cls):
        # Objective relationships
        cls.std_body = StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        cls.std_fwork1 = StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 1',
                )
        cls.node1 = StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 1',
                short_name='N1',
                )
        cls.node2 = StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 2',
                short_name='N2',
                )
        cls.node3 = StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 3',
                short_name='N3',
                )
        # Course objects
        course = Course.objects.validated_create(code='Test1234')
        status = ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                standards_frameworks=[cls.std_fwork1]
                )
        cv2 = CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version 2',
                status=status,
                )
        cls.lo1 = LearningOutcome.objects.validated_create(
                courseversion=cls.cv1,
                outcome='I just know.',
                )
        cls.lo2 = LearningOutcome.objects.validated_create(
                courseversion=cls.cv1,
                outcome='That something good.',
                )
        cls.lo3 = LearningOutcome.objects.validated_create(
                courseversion=cv2,
                outcome='Blurp.'
                )
        # Weighting relationships
        cls.scheme1 = WeightingScheme.objects.validated_create(name='Valid Scheme')
        cls.scheme2 = WeightingScheme.objects.validated_create(name='X Invalid Scheme')
        cls.weight1 = Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='High'
                )
        cls.weight2 = Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='Low'
                )
        cls.weight3 = Weight.objects.validated_create(
                weightingscheme=cls.scheme2,
                value='2'
                )
        CourseVersion.objects.validated_update(
                cls.cv1,
                **{
                    'weightingschemes': [cls.scheme1, cls.scheme2],
                    'objectives': [cls.node1, cls.node2, cls.node3]
                    }
                )
        # Mappings
        WeightedLOToSO.objects.validated_create(
                learningoutcome=cls.lo1,
                standardobjective=cls.node1,
                weights=[cls.weight1, cls.weight3]
                )
        WeightedLOToSO.objects.validated_create(
                learningoutcome=cls.lo1,
                standardobjective=cls.node2,
                )
        WeightedLOToSO.objects.validated_create(
                learningoutcome=cls.lo2,
                standardobjective=cls.node1,
                )
        WeightedLOToSO.objects.validated_create(
                learningoutcome=cls.lo2,
                standardobjective=cls.node3,
                weights=[cls.weight2]
                )

    def test_get_context_creates_mappings(self):
        """The various mapping tables are created correctly"""
        helper = CourseVersionMappingHelper(CourseVersion, self.cv1)
        context = helper.get_context()
        mappings = context['mappings']
        self.assertTrue(len(mappings), 2)
        # Mappings against weighting schemes

        def check_mapping(zipped_mapping):
            for mapping, result in zipped_mapping:
                with self.subTest(mapping=mapping, result=result):
                    # Convert flattened values_list() queryset to a list so we can compare it
                    mapping = mapping[:-1] + (list(mapping[-1]),)
                    self.assertEqual(mapping, result)

        # wscheme1 - Link with weights from multiple schemes gets value from correct scheme,
        # unweighted linked gets None, No link gets None
        mapping_result = zip(
                mappings[0][1],
                [
                    (str(self.lo1), self.lo1.outcome, [self.weight1.value, None, None]),
                    (str(self.lo2), self.lo2.outcome, [None, None, self.weight2.value]),
                    ]
                )
        check_mapping(mapping_result)

        # Mappings against wscheme2
        mapping_result = zip(
                mappings[1][1],
                [
                    (str(self.lo1), self.lo1.outcome, [self.weight3.value, None, None]),
                    (str(self.lo2), self.lo2.outcome, [None, None, None]),
                    ]
                )
        check_mapping(mapping_result)

        # Mappings with no weight. Weighted links should be False, unweighted links should by
        # True, no link should be False
        mappings = context['extra_mappings']
        mapping_result = zip(
                mappings[0][2],
                [
                    (str(self.lo1), self.lo1.outcome, [False, True, False]),
                    (str(self.lo2), self.lo2.outcome, [True, False, False]),
                    ]
                )
        check_mapping(mapping_result)

        # Currently unlinked relationships. No link should be True, everything else False.
        mapping_result = zip(
                mappings[1][2],
                [
                    (str(self.lo1), self.lo1.outcome, [False, False]),
                    (str(self.lo2), self.lo2.outcome, [False, True]),
                    ]
                )
        check_mapping(mapping_result)

    def test_get_context_updates_dict(self):
        """The context dict get updated properly with entries other than mappings."""
        helper = CourseVersionMappingHelper(CourseVersion, self.cv1)
        context = helper.get_context()
        self.assertEqual(LearningOutcome._meta, context['source_model_opts'])
        self.assertEqual(StandardObjective._meta, context['target_model_opts'])
        self.assertEqual(
                [
                    (str(self.node1), self.node1.description),
                    (str(self.node2), self.node2.description),
                    (str(self.node3), self.node3.description)
                    ],
                context['target_objects']
                )
        self.assertQuerysetEqual(
                context['weightingschemes'],
                [repr(self.scheme1), repr(self.scheme2)]
                )


class CourseVersionMappingHelperTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Objective relationships
        cls.std_body = StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        cls.std_fwork1 = StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 1',
                )
        cls.std_fwork2 = StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 2',
                )
        # Course objects
        course = Course.objects.validated_create(code='Test1234')
        status = ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                standards_frameworks=[cls.std_fwork1]
                )

    def test_single_framework(self):
        """
        If only a single StandardsFramework is associated with the CourseVersion then the
        framework selection attributes aren't added to the context.
        """
        helper = CourseVersionMappingHelper(CourseVersion, self.cv1)
        context = helper.get_context()
        self.assertNotIn('has_multiple_frameworks', context)
        self.assertNotIn('framework', context)
        self.assertNotIn('frameworks_form', context)

    def test_multiple_frameworks(self):
        """
        If multiple StandardsFrameworks are associated with the CourseVersion then the
        framework selection attributes are added to the context.
        """
        self.cv1.standards_frameworks.add(self.std_fwork2)
        request = RequestFactory().get('/')
        helper = CourseVersionMappingHelper(CourseVersion, self.cv1, request=request)
        context = helper.get_context()
        self.assertTrue(context['has_multiple_frameworks'])
        self.assertEqual(self.std_fwork1, context['framework'])
        self.assertDictEqual(
                context['frameworks_form'].initial,
                {'framework': self.std_fwork1.pk}
                )

    def test_multiple_frameworks_with_get_parameter(self):
        """
        If multiple StandardsFrameworks are associated with the CourseVersion then the
        framework selected by the user is used to set context attributes.
        """
        self.cv1.standards_frameworks.add(self.std_fwork2)
        request = RequestFactory().get('/', data={'framework': self.std_fwork2.pk})
        helper = CourseVersionMappingHelper(CourseVersion, self.cv1, request=request)
        context = helper.get_context()
        self.assertEqual(self.std_fwork2, context['framework'])
        self.assertEqual(context['frameworks_form'].data['framework'], str(self.std_fwork2.pk))
