from decimal import Decimal

import reversion
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from cims.fields import BleachField
from cims.models import CIMSBaseModel, ValidatedManager


class BasicModel(models.Model):
    """A really basic model for testing."""
    text = models.CharField(max_length=80)


class TestTimeStamp(CIMSBaseModel):
    """Model to assist testing time stamp behaviour."""
    name = models.CharField(max_length=80, blank=True)


class PeroxideBlonde(CIMSBaseModel):
    """Model to assist testing bleachfield behaviour."""
    dirty_text = BleachField()


# Models for Validation framework tests
class PizzaBase(CIMSBaseModel):
    name = models.CharField(max_length=80)


class Topping(CIMSBaseModel):
    name = models.CharField(max_length=80)


class Pizza(CIMSBaseModel):
    name = models.CharField(max_length=80)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    toppings = models.ManyToManyField(Topping, blank=True)
    bases = models.ManyToManyField(PizzaBase, blank=True)

    def clean(self):
        if Decimal(self.price) > Decimal('30.00'):
            raise ValidationError('You think people will pay ${} for a pizza?'.format(self.price))

    @staticmethod
    @receiver(m2m_changed, dispatch_uid='nPp9gbp7UD')
    def clean_options(sender, **kwargs):
        instance = kwargs['instance']
        if sender is Pizza.bases.through \
                and kwargs['action'] is 'pre_add' \
                and not kwargs['reverse']:
            if (
                    PizzaBase.objects.filter(
                            pk__in=kwargs['pk_set'],
                            name__iexact='gluten free'
                            ).exists()
                    and instance.toppings.filter(name__iexact='sausage').exists()
                    ):
                raise ValidationError('Sausage is not gluten free, can\'t combine with GF base.')

        if sender is Pizza.toppings.through \
                and kwargs['action'] is 'pre_add' \
                and not kwargs['reverse']:
            if Topping.objects.filter(pk__in=kwargs['pk_set'], name__iexact='sausage').exists() \
                    and instance.bases.filter(name__iexact='gluten free').exists():
                raise ValidationError('Sausage is not gluten free, can\'t combine with GF base.')

    def validate_other(self):
        num_toppings = self.toppings.count()
        if num_toppings < 3 or num_toppings > 7:
            raise ValidationError('Pizzas with fewer than three toppings are tasteless, those with '
                                  'greater than 7 are overcomplicated mush.')
        if (self.bases.filter(name='Gluten Free').exists()
                and self.toppings.filter(name='Sausage').exists()):
            raise ValidationError('Sausage is not gluten free, can\'t combine with GF base.')


# Models for tests that require Foreign Keys
@reversion.register()
class Author(CIMSBaseModel):
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name

    def validate_other(self):
        if self.name == 'Ray Bradbury':
            raise ValidationError('Ray Bradbury is already taken!')


class BookManager(ValidatedManager):
    select_related_fields = ['author']


@reversion.register()
class Book(CIMSBaseModel):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    title = models.CharField(max_length=80)
    pages = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(0)])

    objects = BookManager()

    def __str__(self):
        return self.title


# Models for tests that require through model relationships
class Weight(CIMSBaseModel):
    task = models.ForeignKey('Task', on_delete=models.CASCADE)
    objective = models.ForeignKey('Objective', on_delete=models.CASCADE)


class Task(CIMSBaseModel):
    task = models.TextField()
    objectives = models.ManyToManyField('Objective', through=Weight)

    def __str__(self):
        return self.task


class Objective(CIMSBaseModel):
    description = models.TextField()

    def __str__(self):
        return self.description


# GenericForeignKey model
class Annotation(CIMSBaseModel):
    content_type = models.ForeignKey(
            ContentType,
            on_delete=models.CASCADE,
            related_name='test_notes'
            )
    object_id = models.CharField('object ID', max_length=255)
    annotated_object = GenericForeignKey('content_type', 'object_id')
    note = models.CharField(
            max_length=250,
            help_text='Free Text note to associate with this object.'
            )
