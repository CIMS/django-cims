"""
Test fields from CIMS application.
"""
import datetime

from django.test import TestCase

from cims.fields import AutoCreatedField
from .models import PeroxideBlonde


class TestAutoCreatedField(TestCase):

    def test_date_created_field(self):
        field = AutoCreatedField()
        self.assertFalse(field.editable)
        # Field default is call to django.utils.timezone.now()
        self.assertTrue(isinstance(field.default(), datetime.datetime))


class TestBleachField(TestCase):

    def test_bleach_wipes_badtags(self):
        badtext = "abc</div>def"
        cleantext = "abc&lt;/div&gt;def"
        dummy = PeroxideBlonde.objects.create(dirty_text=badtext)
        dummy.clean_fields()
        self.assertEqual(dummy.dirty_text, cleantext)

    def test_bleach_preserves_safetags(self):
        safetext = """
            <a href="example.com">link text with <em>nested emphasis</em></a>
            <b>boldly</b> go <strong>forth!</strong>

            <ol>
                <li>First, do this</li>
                <li>Then, do that</li>
                <li>Now you're doing cooking by the book!</li>
            </ol>
        """
        dummy = PeroxideBlonde.objects.create(dirty_text=safetext)
        dummy.clean_fields()
        self.assertEqual(dummy.dirty_text, safetext)
