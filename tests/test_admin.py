from django.contrib.admin.sites import AdminSite
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.core.management import call_command
from django.http import HttpRequest
from django.test import override_settings, TestCase

from cims import models
from cims.admin import CIMSBaseModelAdmin, get_leaf_objectives, UserAdmin
from cims.auth.models import User, UserObjectPermission

from .helpers import get_perm
from .models import Topping


class TestAdminFunctions(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_get_leaf_objectives(self):
        """
        Given a CourseVersion id, should return a queryset of related StandardObjectives which
        are leaves in the StandardObjective forest.
        """
        status = models.ReviewStatus.objects.create(name='Stately')
        course = models.Course.objects.create(code='Test Course')
        course_version = models.CourseVersion.objects.create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )
        standard_body = models.StandardsBody.objects.create(name='Engineers Australia')
        framework = models.StandardsFramework.objects.create(
                standardsbody=standard_body,
                name='Stage 1 Competencies',
                short_name='Stage 1',
                )
        course_version.standards_frameworks.add(framework)
        # empy result set
        self.assertFalse(get_leaf_objectives(course_version.id))
        # roots which are also leaves, ignore unrelated objective
        models.StandardObjective.objects.create(name='so0')
        so1 = models.StandardObjective.objects.create(
                standardsframework=framework,
                name='so1',
                )
        so2 = models.StandardObjective.objects.create(
                standardsframework=framework,
                name='so2',
                )
        self.assertSetEqual(
                set([so1, so2]),
                set(get_leaf_objectives(course_version.id)),
                )
        # leaves which have ancestors
        so3 = models.StandardObjective.objects.create(name='so3', parent=so1)
        so4 = models.StandardObjective.objects.create(name='so4', parent=so1)
        so5 = models.StandardObjective.objects.create(name='so5', parent=so4)
        self.assertSetEqual(
                set([so2, so3, so5]),
                set(get_leaf_objectives(course_version.id)),
                )


class TestCIMSBaseModelAdmin(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.adminsite = AdminSite(name='test_adminsite')

    @override_settings(
            AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
            )
    def test_has_change_permission(self):
        """Permits access to index and model listing admin pages if user has object permissions."""
        topping_admin = CIMSBaseModelAdmin(Topping, self.adminsite)
        user1 = User.objects.create_user(username='user1', password='sekret')
        request = HttpRequest()
        request.user = user1
        # User with no permissions
        self.assertFalse(topping_admin.has_change_permission(request))
        # User has required permission
        user2 = User.objects.create_user(username='user2', password='sekret')
        request.user = user2
        perm1 = get_perm('tests', 'topping', 'change_topping')
        user2.user_permissions.add(perm1)
        self.assertTrue(topping_admin.has_change_permission(request))
        # User has required permission on an object and we're not testing a specific object
        user3 = User.objects.create_user(username='user3', password='sekret')
        request.user = user3
        topping = Topping.objects.create(name='Ice Cream!')
        topping_ctype = ContentType.objects.get_for_model(topping)
        UserObjectPermission.objects.create(
                user=user3,
                permission=perm1,
                content_type=topping_ctype,
                object_id=topping.id,
                )
        self.assertTrue(topping_admin.has_change_permission(request))


@override_settings(
        AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'],
        PFP_MODELS=[('cims_auth', 'user')],
        PFP_IGNORE_PERMS={},
        )
class TestUserAdmin(TestCase):

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')
        cls.adminsite = AdminSite(name='test_adminsite')

    def test_set_unusable_password(self):
        """List action sets unusable password for selected users."""
        user_admin = UserAdmin(User, self.adminsite)
        user1 = User.objects.create_user(username='user1', password='sekret')
        request = HttpRequest()
        request.user = user1
        # User without permission
        self.assertRaisesMessage(
                PermissionDenied,
                'You do not have permission to perform this action.',
                *[user_admin.set_unusable_password, request, User.objects.none()]
                )
        # User has required permission
        user2 = User.objects.create_user(username='user2', password='sekret')
        perm1 = get_perm('cims_auth', 'user', 'change_user')
        user2.user_permissions.add(perm1)
        request.user = user2
        user_admin.set_unusable_password(request, User.objects.filter(pk=user1.pk))
        user1.refresh_from_db()
        self.assertFalse(user1.has_usable_password())
