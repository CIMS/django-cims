import reversion

from datetime import date, datetime, timedelta
from decimal import Decimal
from itertools import repeat, zip_longest
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import connection, transaction
from django.test import TestCase
from django.urls import reverse
from reversion.models import Version

from cims import models
from . import PostgreSQLTestCase
from .models import Author, Book, Pizza, PizzaBase, TestTimeStamp, Topping

User = get_user_model()


class TestValidatedManager(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.cheese = Topping.objects.validated_create(name='Cheese')
        cls.mushrooms = Topping.objects.validated_create(name='Mushrooms')
        cls.capsicums = Topping.objects.validated_create(name='Capsicums')
        cls.salami = Topping.objects.validated_create(name='salami')
        cls.sausage = Topping.objects.validated_create(name='Sausage')
        cls.traditional = PizzaBase.objects.validated_create(name='Traditional')
        cls.dc = PizzaBase.objects.validated_create(name='Deep Crust')
        cls.gf = PizzaBase.objects.validated_create(name='Gluten Free')

    def test_validated_create(self):
        # Compliant pizza passes
        pizza_the_first = Pizza.objects.validated_create(
                name='Proper Pizza',
                price='10.95',
                toppings=[self.salami, self.capsicums, self.mushrooms, self.cheese],
                bases=[self.traditional, self.dc],
                )
        self.assertTrue(Pizza.objects.filter(id=pizza_the_first.id))
        self.assertEqual('Proper Pizza', pizza_the_first.name)
        self.assertEqual(Decimal('10.95'), pizza_the_first.price)
        self.assertSetEqual(
                {self.salami, self.capsicums, self.mushrooms, self.cheese},
                set(pizza_the_first.toppings.all()),
                )
        self.assertSetEqual(
                {self.traditional, self.dc},
                set(pizza_the_first.bases.all()),
                )
        # Standard validation should fail
        self.assertRaisesMessage(
                ValidationError,
                "{'price': ['Ensure that there are no more than 2 decimal places.']}",
                Pizza.objects.validated_create,
                **{
                    'name': 'Overprecise pizza',
                    'price': '13.955',
                    }
                )
        self.assertFalse(Pizza.objects.filter(name='Overprecise pizza').exists())
        self.assertRaisesMessage(
                ValidationError,
                "{'__all__': ['You think people will pay $31.00 for a pizza?']}",
                Pizza.objects.validated_create,
                **{
                    'name': 'Overpriced pizza',
                    'price': '31.00',
                    }
                )
        self.assertFalse(Pizza.objects.filter(name='Overpriced pizza').exists())
        # Extended validation fails.
        # validate_other raises exception()
        self.assertRaisesMessage(
                ValidationError,
                ('Pizzas with fewer than three toppings are tasteless, those with greater than 7 '
                 'are overcomplicated mush.'),
                Pizza.objects.validated_create,
                **{
                    'name': 'Tasteless Pizza',
                    'price': '8.95',
                    'toppings': [self.mushrooms, self.cheese],
                    'bases': [self.traditional, self.dc],
                    }
                )
        self.assertFalse(Pizza.objects.filter(name='Tasteless Pizza').exists())
        # m2m_changed signal receiver raises exception
        self.assertRaisesMessage(
                ValidationError,
                'Sausage is not gluten free, can\'t combine with GF base.',
                Pizza.objects.validated_create,
                **{
                    'name': 'GF Pizza',
                    'price': '8.95',
                    'toppings': [self.mushrooms, self.cheese, self.sausage],
                    'bases': [self.traditional, self.gf],
                    }
                )
        self.assertFalse(Pizza.objects.filter(name='GF Pizza').exists())

    def test_validated_update(self):
        # Compliant pizza passes
        pizza_the_first = Pizza.objects.validated_create(
                name='Proper Pizza',
                price='10.95',
                toppings=[self.salami, self.capsicums, self.mushrooms, self.cheese],
                bases=[self.traditional, self.dc],
                )
        # Standard validation should fail
        self.assertRaisesMessage(
                ValidationError,
                "{'price': ['Ensure that there are no more than 2 decimal places.']}",
                *[Pizza.objects.validated_update, pizza_the_first],
                **{
                    'name': 'Overprecise pizza',
                    'price': '13.955',
                    }
                )
        self.assertRaisesMessage(
                ValidationError,
                "{'__all__': ['You think people will pay $31.00 for a pizza?']}",
                *[Pizza.objects.validated_update, pizza_the_first],
                **{
                    'name': 'Overpriced pizza',
                    'price': '31.00',
                    }
                )
        # Extended validation fails. Refresh from db to clear values set above.
        pizza_the_first.refresh_from_db()
        self.assertRaisesMessage(
                ValidationError,
                ('Pizzas with fewer than three toppings are tasteless, those with greater than '
                 '7 are overcomplicated mush.'),
                *[Pizza.objects.validated_update, pizza_the_first],
                **{
                    'toppings': [self.mushrooms, self.cheese],
                    }
                )
        self.assertEqual(pizza_the_first.toppings.count(), 4)

    def test_get_create_url_ui_url_exists(self):
        """If a UI based create url exists for this model return it."""
        opts = Author._meta
        self.assertEqual(
                reverse('{}:{}.create'.format(opts.app_label, opts.model_name)),
                Author.objects.get_create_url()
                )

    def test_get_create_url_no_ui_url_exists(self):
        """If no UI based create url exists for this model return the API create url."""
        opts = Book._meta
        self.assertEqual(
                reverse('{}-list'.format(opts.model_name)),
                Book.objects.get_create_url()
                )

    def test_get_queryset(self):
        """
        If `select_related_fields` is set the default queryset automatically retrieves those
        related objects, so accessing the objects doesn't create an additional DB query.
        """
        author = Author.objects.validated_create(name='Author')
        Book.objects.validated_create(author=author, title='Book')
        book = Book.objects.first()
        num_queries = len(connection.queries)
        getattr(book, 'author')
        self.assertEqual(len(connection.queries), num_queries)


class TestCustomManagers(TestCase):

    @classmethod
    def setUpTestData(cls):
        status = models.ReviewStatus.objects.create(name='Stately')
        cls.std_body = models.StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        cls.std_fwork = models.StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 1',
                )
        cls.node1 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork,
                name='Node 1',
                short_name='N1',
                )
        cls.school = models.OrganisationalUnit.objects.validated_create(
                name='Foo School',
                short_name='FS'
                )
        cls.course = models.Course.objects.validated_create(code='Test Course')
        cls.cv = models.CourseVersion.objects.create(
                course=cls.course,
                version_descriptor='Test course version',
                status=status,
                )
        cls.cv.standards_frameworks.add(cls.std_fwork)
        cls.cv.objectives.add(cls.node1)
        cls.ci = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv,
                instance_descriptor='Test instance',
                start_date=date.today(),
                end_date=date.today(),
                taught_by=[cls.school],
                )
        cls.lo = models.LearningOutcome.objects.validated_create(
                courseversion=cls.cv,
                outcome='Something good.',
                )
        cls.schedule = models.Schedule.objects.validated_create(
                courseinstance=cls.ci,
                session_name='week1',
                )
        cls.task = models.Task.objects.validated_create(
                courseinstance=cls.ci,
                title='Things you can do.',
                )
        role_title = models.CourseRelatedRoleTitle.objects.validated_create(title='Tutor')
        user = User.objects.create_user(username='user', password='sekret')
        cls.related_role = models.CourseRelatedRole.objects.validated_create(
                user=user,
                courseinstance=cls.ci,
                role_title=role_title,
                )
        cls.weightedlotoso = models.WeightedLOToSO.objects.validated_create(
                learningoutcome=cls.lo,
                standardobjective=cls.node1
                )
        cls.weightedtasktolo = models.WeightedTaskToLO.objects.validated_create(
                task=cls.task,
                learningoutcome=cls.lo
                )

    def test_coursemanager_get_course_for_object(self):
        """CourseManager returns the appropriately related Course."""
        # Non-applicable model
        self.assertIsNone(models.Course.objects.get_course_for_object(self.std_body))
        # CourseVersion
        self.assertEqual(self.course.pk, models.Course.objects.get_course_for_object(self.cv).pk)
        # CourseInstance
        self.assertEqual(self.course.pk, models.Course.objects.get_course_for_object(self.ci).pk)
        # LearningOutcome
        self.assertEqual(self.course.pk, models.Course.objects.get_course_for_object(self.lo).pk)
        # Task
        self.assertEqual(self.course.pk, models.Course.objects.get_course_for_object(self.task).pk)
        # Schedule
        self.assertEqual(
                self.course.pk,
                models.Course.objects.get_course_for_object(self.schedule).pk,
                )
        # CourseRelatedRole
        self.assertEqual(
                self.course.pk,
                models.Course.objects.get_course_for_object(self.related_role).pk,
                )

    def test_courseversionmanager_get_courseversion_for_object(self):
        """CourseManager returns the appropriately related Course."""
        # Non-applicable model
        self.assertIsNone(models.CourseVersion.objects.get_courseversion_for_object(self.std_body))
        # CourseInstance
        self.assertEqual(
                self.cv.pk,
                models.CourseVersion.objects.get_courseversion_for_object(self.ci).pk
                )
        # LearningOutcome
        self.assertEqual(
                self.cv.pk,
                models.CourseVersion.objects.get_courseversion_for_object(self.lo).pk
                )
        # Task
        self.assertEqual(
                self.cv.pk,
                models.CourseVersion.objects.get_courseversion_for_object(self.task).pk
                )
        # Schedule
        self.assertEqual(
                self.cv.pk,
                models.CourseVersion.objects.get_courseversion_for_object(self.schedule).pk,
                )
        # CourseRelatedRole
        self.assertEqual(
                self.cv.pk,
                models.CourseVersion.objects.get_courseversion_for_object(self.related_role).pk,
                )
        # WeightedLOtoSO
        self.assertEqual(
                self.cv.pk,
                models.CourseVersion.objects.get_courseversion_for_object(self.weightedlotoso).pk,
                )
        # Annotation attached to a CourseVersion
        annotation = models.Annotation.objects.validated_create(
                object_id=self.cv.pk,
                content_type=ContentType.objects.get_for_model(models.CourseVersion),
                note='Noted.'
                )
        self.assertEqual(
                self.cv.pk,
                models.CourseVersion.objects.get_courseversion_for_object(annotation).pk
                )
        # Annotation attached to an instance of another model
        annotation = models.Annotation.objects.validated_create(
                object_id=self.course.pk,
                content_type=ContentType.objects.get_for_model(models.Course),
                note='Noted.'
                )
        self.assertIsNone(models.CourseVersion.objects.get_courseversion_for_object(annotation))

    def test_courseinstancemanager_get_courseinstance_for_object(self):
        """CourseInstanceManager returns the appropriatedly related Course."""
        # Non-applicable model
        self.assertIsNone(
                models.CourseInstance.objects.get_courseinstance_for_object(self.std_body)
                )
        # Task
        self.assertEqual(
                self.ci.pk,
                models.CourseInstance.objects.get_courseinstance_for_object(self.task).pk
                )
        # Schedule
        self.assertEqual(
                self.ci.pk,
                models.CourseInstance.objects.get_courseinstance_for_object(self.schedule).pk,
                )
        # CourseRelatedRole
        self.assertEqual(
                self.ci.pk,
                models.CourseInstance.objects.get_courseinstance_for_object(self.related_role).pk,
                )
        # WeightedTaskToLO
        self.assertEqual(
                self.ci.pk,
                models.CourseInstance.objects.get_courseinstance_for_object(
                        self.weightedtasktolo
                        ).pk
                )
        # Annotation attached to a CourseInstance
        annotation = models.Annotation.objects.validated_create(
                object_id=self.ci.pk,
                content_type=ContentType.objects.get_for_model(models.CourseInstance),
                note='Noted.'
                )
        self.assertEqual(
                self.ci.pk,
                models.CourseInstance.objects.get_courseinstance_for_object(annotation).pk
                )
        # Annotation attached to an instance of another model
        annotation = models.Annotation.objects.validated_create(
                object_id=self.cv.pk,
                content_type=ContentType.objects.get_for_model(models.CourseVersion),
                note='Noted.'
                )
        self.assertIsNone(models.CourseInstance.objects.get_courseinstance_for_object(annotation))


class TestTimeStampedModel(TestCase):

    @patch('django.utils.timezone.now')
    def test_time_stamped_model(self, now):
        """
        `created` and `last_modified` fields are set on object creation, `last_modified` is set on
        update.
        """
        now.return_value = datetime.now()
        obj = TestTimeStamp.objects.create(name='Stampy')
        self.assertEqual(now.return_value, obj.created)
        self.assertEqual(now.return_value, obj.last_modified)
        now.return_value = datetime.now()
        obj.name = 'Thumper'
        obj.save()
        self.assertNotEqual(now.return_value, obj.created)
        self.assertEqual(now.return_value, obj.last_modified)


class CIMSBaseModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.author = Author.objects.validated_create(name='Author')

    def test_get_absolute_url(self):
        """Default get_absolute_url returns an API url."""
        self.assertEqual(self.author.get_api_absolute_url(), self.author.get_absolute_url())

    def test_get_ui_absolute_url(self):
        """Default ui url is named `<app_label>:<model_name>.detail`."""
        opts = Author._meta
        self.assertEqual(
                reverse('{}:{}.detail'.format(
                    opts.app_label, opts.model_name),
                    args=[self.author.pk]
                    ),
                self.author.get_ui_absolute_url(),
                )

    def test_get_ui_update_url(self):
        """Default ui update url is named `<app_label>:<model_name>.update`."""
        opts = Author._meta
        self.assertEqual(
                reverse('{}:{}.update'.format(
                    opts.app_label, opts.model_name),
                    args=[self.author.pk]
                    ),
                self.author.get_ui_update_url(),
                )


class TestValidateStartEndDates(TestCase):
    one_day_more = timedelta(days=1)
    start_date = date.today()

    def test_end_date_gte_start_date(self):
        """Raise exception if end_date is not greater than or equal to start_date."""
        self.assertRaisesMessage(
                ValidationError,
                ('End date must be greater than or equal to the start date. end_date >= '
                 'start_date.'),
                args=[models.validate_start_end_dates, self.start_date, self.start_date -
                      self.one_day_more],
                )

    def test_interval_not_timedelta(self):
        """Raise exception if interval is not a timedelta."""
        self.assertRaisesMessage(
                ValidationError,
                'Minimum interval, if specified, must be datetime.timedelta >= 1 day.',
                args=[models.validate_start_end_dates, self.start_date, self.start_date +
                      self.one_day_more, 1],)

    def test_negative_interval(self):
        """Raise exception if interval is a negative timedelta."""
        one_day_less = timedelta(days=-1)
        self.assertRaisesMessage(
                ValidationError,
                'Minimum interval, if specified, must be datetime.timedelta >= 1 day.',
                args=[models.validate_start_end_dates, self.start_date, self.start_date +
                      self.one_day_more, one_day_less],
                )

    def test_end_date_lt_start_date_plus_interval(self):
        """Raise exception if end_date is smaller than start_date plus the interval."""
        many_days_more = timedelta(days=13)
        self.assertRaisesMessage(
                ValidationError,
                ('End date must be greater than or equal to the start date plus the minimum '
                 'interval of 13 days. end_date >= start_date + interval.'),
                args=[models.validate_start_end_dates, self.start_date, self.start_date +
                      self.one_day_more, many_days_more],
                )

    def test_start_date_not_date(self):
        self.assertRaisesMessage(
                ValidationError,
                'Can\'t validate start/end dates if one of them is not a date.',
                args=[models.validate_start_end_dates, None, self.start_date + self.one_day_more],
                )

    def test_end_date_not_date(self):
        self.assertRaisesMessage(
                ValidationError,
                'Can\'t validate start/end dates if one of them is not a date.',
                args=[models.validate_start_end_dates, self.start_date, 42],
                )


class TestStandardsBody(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_str(self):
        std_body = models.StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        self.assertEqual(str(std_body), std_body.name)

    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            std_body = models.StandardsBody.objects.validated_create(
                    name='Engineers Australia',
                    short_name='EA',
                    )
        self.assertTrue(Version.objects.get_for_object(std_body).exists())


class TestStandardsFramework(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.std_body = models.StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        with reversion.create_revision():
            cls.std_fwork1 = models.StandardsFramework.objects.validated_create(
                    standardsbody=cls.std_body,
                    name='EA Stage 1',
                    )
        cls.std_fwork2 = models.StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 2',
                )
        cls.node_1 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 1',
                short_name='N1',
                )
        cls.node_1_1 = models.StandardObjective.objects.validated_create(
                parent=cls.node_1,
                name='Node 1.1',
                short_name='N1.1',
                )
        cls.node_2 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork2,
                name='Node 2',
                short_name='N2',
                )
        cls.node_2_1 = models.StandardObjective.objects.validated_create(
                parent=cls.node_2,
                name='Node 2.1',
                short_name='N2.1',
                )
        cls.node_3 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork2,
                name='Node 3',
                short_name='N3',
                )

    def test_str(self):
        self.assertEqual(str(self.std_fwork1), 'EA Stage 1')
        self.std_fwork1.name = ''.join(repeat('0123456789', 5))
        self.assertEqual(str(self.std_fwork1), '{}...'.format(self.std_fwork1.name[:37]))
        self.std_fwork1.short_name = 'EA Stage Eleventy'
        self.assertEqual(str(self.std_fwork1), 'EA Stage Eleventy')

    def test_reversion(self):
        """Confirm an initial revision is created."""
        self.assertTrue(Version.objects.get_for_object(self.std_fwork1).exists())

    def test_get_root_objectives(self):
        """Returns queryset containing only root nodes for trees associated with this framework."""
        self.assertQuerysetEqual(self.std_fwork1.get_root_objectives(), [repr(self.node_1)])

    def test_get_objective_trees(self):
        """
        Returns a list of TreeQuerySets for each StandardObjective tree linked to the framework.
        """
        expected_list = [
                self.node_2.get_descendants(include_self=True),
                self.node_3.get_descendants(include_self=True)
                ]
        for qs1, qs2 in zip_longest(
                expected_list,
                self.std_fwork2.get_objective_trees(ordering=['pk']),
                fillvalue=models.StandardsFramework.objects.none()):
            with self.subTest(qs1=qs1, qs2=qs2):
                self.assertEqual(list(qs1), list(qs2))


class TestStandardObjective(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.std_body = models.StandardsBody.objects.create(
                name='Engineers Australia',
                short_name='EA',
                )
        cls.std_fwork = models.StandardsFramework.objects.create(
                standardsbody=cls.std_body,
                name='EA Stage 1',
                )
        with reversion.create_revision():
            cls.node1 = models.StandardObjective.objects.create(
                    standardsframework=cls.std_fwork,
                    name='Node 1',
                    short_name='N1',
                    description='The thingies that they have.',
                    )
        cls.node2 = models.StandardObjective.objects.create(
                name='Node 2',
                short_name='N2',
                description='Sublevel N2',
                parent=cls.node1,
                )
        cls.node3 = models.StandardObjective.objects.create(
                name='Node 3',
                short_name='N3',
                description='Sub-seblevel N3',
                parent=cls.node2,
                )
        cls.node4 = models.StandardObjective.objects.create(
                name='Node 4',
                short_name='N4',
                description='Sublevel N4',
                parent=cls.node1,
                )

    def test_str(self):
        self.assertEqual('EA Stage 1 - N1', str(self.node1))
        self.node1.short_name = ''
        self.assertEqual('EA Stage 1 - Node 1', str(self.node1))
        self.node1.name = ''.join(repeat('0123456789', 5))
        self.assertEqual('EA Stage 1 - {}...'.format(self.node1.name[:37]), str(self.node1))

    def test_reversion(self):
        """Confirm an initial revision is created."""
        self.assertTrue(Version.objects.get_for_object(self.node1).exists())

    def test_clean(self):
        """
        Raise ValidationError if creating a node and haven't linked to a
        standards framework or parent.
        """
        std_obj = models.StandardObjective.objects.create(
                name='Foo',
                description='Has Foo',
                )
        self.assertRaisesMessage(
                ValidationError,
                'A Standard Objective bust be linked to either a Standards Framework (making it '
                'the root of a tree), or a parent objective.',
                std_obj.clean,
                )

    def test_save_set_framework_for_non_root(self):
        """If the node is a child set the framework to that of the parent node."""
        fwork2 = models.StandardsFramework.objects.validated_create(
                standardsbody=self.std_body,
                name='Stage 2',
                )
        objective = models.StandardObjective.objects.create(
                name='Objective',
                description='I object!',
                parent=self.node1,
                standardsframework=fwork2
                )
        self.assertEqual(objective.standardsframework_id, self.std_fwork.pk)

    def test_get_absolute_url(self):
        """Return the url for the parent standardsframework."""
        self.assertEqual(self.node1.get_absolute_url(), self.std_fwork.get_absolute_url())

    def test_children_are_leaves(self):
        """True if at least one child is a leaf node."""
        self.assertTrue(self.node1.children_are_leaves)

    def test_children_are_leaves_leaf(self):
        """False if node is a leaf."""
        self.assertFalse(self.node3.children_are_leaves)

    def test_children_not_leaves(self):
        """False if no children are leaves."""
        self.node4.move_to(self.node3)
        self.assertFalse(self.node1.children_are_leaves)

    def test_leaf_count(self):
        """Ignore nodes that aren't leaves."""
        self.assertEqual(2, self.node1.leaf_count)

    def test_leaf_count_leaf(self):
        """Leaf nodes have no leaves."""
        self.assertEqual(0, self.node4.leaf_count)


class TestTask(TestCase):

    @classmethod
    def setUpTestData(cls):
        status = models.ReviewStatus.objects.validated_create(name='Stately')
        cls.school = models.OrganisationalUnit.objects.validated_create(
                name='Foo School',
                short_name='FS'
                )
        cls.course = models.Course.objects.validated_create(code='Test Course')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=cls.course,
                version_descriptor='Test course version',
                status=status
                )
        cls.cv2 = models.CourseVersion.objects.validated_create(
                course=cls.course,
                version_descriptor='Testify to this course version ',
                status=status
                )
        cls.ci = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test instance',
                start_date=date.today(),
                end_date=date.today(),
                taught_by=[cls.school],
                )

    def test_defaults(self):
        """New Tasks have a default order of 0. An initial revision is created."""
        with reversion.create_revision():
            task = models.Task.objects.validated_create(courseinstance=self.ci, title='New task')
        self.assertEqual(task.order, 0)
        self.assertTrue(Version.objects.get_for_object(task).exists())


class CourseTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        stately = models.ReviewStatus.objects.validated_create(name='Stately')
        approved = models.ReviewStatus.objects.validated_create(name='Approved')
        cls.school = models.OrganisationalUnit.objects.validated_create(
                name='Foo School',
                short_name='FS'
                )
        with reversion.create_revision():
            cls.course1 = models.Course.objects.validated_create(code='Test Course1')
        cls.course2 = models.Course.objects.validated_create(code='Test Course2')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=cls.course1,
                version_descriptor='Test course version',
                status=approved
                )
        cls.cv2 = models.CourseVersion.objects.validated_create(
                course=cls.course2,
                version_descriptor='Testify to this course version ',
                status=stately
                )

    def test_reversion(self):
        """Confirm an initial revision is created."""
        self.assertTrue(Version.objects.get_for_object(self.course1).exists())

    def test_get_current_courseversion_no_version(self):
        """A course with no approved versions raises ObjectDoesNotExist."""
        self.assertRaisesMessage(
                ObjectDoesNotExist,
                'CourseVersion matching query does not exist.',
                *[self.course2.get_current_courseversion]
                )

    def test_get_current_courseversion_approved(self):
        """Returns the current approved CourseVersion."""
        self.assertEqual(self.cv1, self.course1.get_current_courseversion())

    def test_get_current_courseversion_selected_related(self):
        """Includes the selected related objects in the queryset."""
        current_version = self.course1.get_current_courseversion(select_related=['status'])
        num_queries = len(connection.queries)
        getattr(current_version, 'status')
        self.assertEqual(len(connection.queries), num_queries)


class TestCourseVersion(TestCase):

    @classmethod
    def setUpTestData(cls):
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.ou = models.OrganisationalUnit.objects.validated_create(
                name='Test OU',
                short_name='TOU'
                )
        course = models.Course.objects.validated_create(code='Test1234')
        with reversion.create_revision():
            cls.cv1 = models.CourseVersion.objects.validated_create(
                    course=course,
                    version_descriptor='Test course version',
                    status=status,
                    )

    def test_reversion(self):
        """Confirm an initial revision is created."""
        self.assertTrue(Version.objects.get_for_object(self.cv1).exists())

    def test_standardobjective_validation(self):
        """
        StandardObjectives must come from those associated with StandardsFrameworks linked to the
        CourseVersion.
        """
        std_body = models.StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        std_fwork1 = models.StandardsFramework.objects.validated_create(
                standardsbody=std_body,
                name='Stage 1',
                )
        std_fwork2 = models.StandardsFramework.objects.validated_create(
                standardsbody=std_body,
                name='Stage 3',
                )
        node_1_1 = models.StandardObjective.objects.validated_create(
                standardsframework=std_fwork1,
                name='Node 1',
                short_name='N1',
                )
        node_1_11 = models.StandardObjective.objects.validated_create(
                parent=node_1_1,
                name='Node 1.2',
                short_name='N1.2',
                )
        node_1_12 = models.StandardObjective.objects.validated_create(
                parent=node_1_1,
                name='Node 1.1',
                short_name='N1.1',
                )
        node_1_2 = models.StandardObjective.objects.validated_create(
                standardsframework=std_fwork1,
                name='Node 2',
                short_name='N2',
                )
        node_1_21 = models.StandardObjective.objects.validated_create(
                parent=node_1_2,
                name='Node 2.1',
                short_name='N2.1',
                )
        node_2_1 = models.StandardObjective.objects.validated_create(
                standardsframework=std_fwork2,
                name='Node 3',
                short_name='N3',
                )
        node_2_2 = models.StandardObjective.objects.validated_create(
                standardsframework=std_fwork2,
                name='Node 4',
                short_name='N4',
                )
        node_2_3 = models.StandardObjective.objects.validated_create(
                standardsframework=std_fwork2,
                name='Node 5',
                short_name='N5',
                )
        # Add objective without a framework fails.
        self.assertRaisesMessage(
                ValidationError,
                ('StandardObjectives linked to this CourseVersion must be leaf objectives '
                 'associated with a StandardsFramework that is itself linked to this '
                 'CourseVersion.'),
                *[models.CourseVersion.objects.validated_update, self.cv1],
                **{
                    'objectives': [node_2_3],
                    }
                )
        # Adding objectives from a framework with a flat hierarchy succeeds
        self.cv1 = models.CourseVersion.objects.validated_update(
                self.cv1,
                **{
                    'standards_frameworks': [std_fwork2],
                    'objectives': [node_2_1, node_2_2],
                    }
                )
        # Adding objective from outside selected framework fails
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    ('StandardObjectives linked to this CourseVersion must be leaf objectives '
                     'associated with a StandardsFramework that is itself linked to this '
                     'CourseVersion.'),
                    self.cv1.objectives.add,
                    *[node_1_1, node_2_1, node_2_2]
                    )
        # And from the reverse relationship
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    ('StandardObjectives linked to this CourseVersion must be leaf objectives '
                     'associated with a StandardsFramework that is itself linked to this '
                     'CourseVersion.'),
                    node_1_1.courseversion_set.add,
                    *[self.cv1]
                    )
        # Adding non-leaf objectives from a framework with a tree structure fails
        with transaction.atomic():
            self.cv1.standards_frameworks.add(std_fwork1)
            self.assertRaisesMessage(
                    ValidationError,
                    ('StandardObjectives linked to this CourseVersion must be leaf objectives '
                     'associated with a StandardsFramework that is itself linked to this '
                     'CourseVersion.'),
                    self.cv1.objectives.add,
                    *[node_1_1]
                )
        # And the reverse
        with transaction.atomic():
            self.cv1.standards_frameworks.add(std_fwork1)
            self.assertRaisesMessage(
                    ValidationError,
                    ('StandardObjectives linked to this CourseVersion must be leaf objectives '
                     'associated with a StandardsFramework that is itself linked to this '
                     'CourseVersion.'),
                    node_1_1.courseversion_set.add,
                    *[self.cv1]
                    )
        # Adding leaf objectives from a framework with a tree structure succeeds
        self.cv1.standards_frameworks.add(std_fwork1)
        self.cv1 = models.CourseVersion.objects.validated_update(
                self.cv1,
                **{'objectives': [node_1_11, node_1_12, node_1_21]}
                )
        # Add stuff from multiple frameworks...
        self.cv1.standards_frameworks.add(std_fwork2)
        self.cv1 = models.CourseVersion.objects.validated_update(
                self.cv1,
                **{'objectives': [node_1_11, node_1_12, node_1_21, node_2_1, node_2_3]}
                )

    def test_get_ias_bad_date(self):
        """
        Passing a date argument that isn't datetime.date/datetime.datetime/None raises
        exception.
        """
        self.assertRaisesMessage(
                AssertionError,
                'Date parameter must be datetime.date or datetime.datetime.',
                *[self.cv1.get_indicative_assessments, self.cv1]
                )

    def test_get_instance_for_year_default_year(self):
        """
        Returns the CourseInstance with the earliest start date from the current year if no year
        argument is passed.
        """
        today = date.today()
        ci1 = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Test instance',
                start_date=date(today.year, 6, 1),
                end_date=date(today.year, 7, 1),
                taught_by=[self.ou],
                )
        models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Test instance',
                start_date=date(today.year, 9, 1),
                end_date=date(today.year, 10, 1),
                taught_by=[self.ou],
                )
        self.assertEqual(ci1, self.cv1.get_instance_for_year())

    def test_get_instance_for_year_specified_year(self):
        """
        Returns the CourseInstance with the earliest start date from the year passed as an argument.
        """
        today = date.today()
        models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Test instance',
                start_date=date(today.year, 6, 1),
                end_date=date(today.year, 7, 1),
                taught_by=[self.ou],
                )
        ci2 = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Test instance',
                start_date=date(1999, 9, 1),
                end_date=date(1999, 10, 1),
                taught_by=[self.ou],
                )
        self.assertEqual(ci2, self.cv1.get_instance_for_year(year=1999))


class TestCourseVersionPostgres(PostgreSQLTestCase):

    @classmethod
    def setUpTestData(cls):
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.ou = models.OrganisationalUnit.objects.validated_create(
                name='Test OU',
                short_name='TOU'
                )
        course = models.Course.objects.validated_create(code='Test1234')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )

    def test_get_ias(self):
        """
        get_indicative_assessments() returns correctly formatted dict() drawing info from the
        right objects.
        """
        # Setup...
        online = models.DeliveryMode.objects.validated_create(mode='Online')
        in_person = models.DeliveryMode.objects.validated_create(mode='In Person')
        models.DeliveryMode.objects.validated_create(mode='Undelivered')
        today = date.today()
        old_start = today - timedelta(weeks=7)
        old_finish = today - timedelta(weeks=3)
        recent_start = today - timedelta(weeks=2)
        recent_finish = today + timedelta(weeks=2)
        future_start = today + timedelta(weeks=5)
        future_finish = today + timedelta(weeks=7)
        old_session = models.TeachingSession.objects.validated_create(
                name='Old',
                start_date=old_start,
                end_date=old_finish,
                )
        recent_session = models.TeachingSession.objects.validated_create(
                name='Recent',
                start_date=recent_start,
                end_date=recent_finish,
                )
        future_session = models.TeachingSession.objects.validated_create(
                name='Future',
                start_date=future_start,
                end_date=future_finish,
                )
        ci_old = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Old Instance',
                start_date=old_start,
                end_date=old_finish,
                delivery_mode=online,
                teachingsession=old_session
                )
        models.Task.objects.validated_create(
                courseinstance=ci_old,
                title='Final Exam',
                weighting=100,
                )
        ci_recent_online = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Recent Online Instance',
                start_date=recent_start,
                end_date=recent_finish,
                delivery_mode=online,
                teachingsession=recent_session
                )
        models.Task.objects.validated_create(
                courseinstance=ci_recent_online,
                title='Assignment',
                weighting=10,
                )
        models.Task.objects.validated_create(
                courseinstance=ci_recent_online,
                title='Mid-semester test',
                weighting=30,
                )
        models.Task.objects.validated_create(
                courseinstance=ci_recent_online,
                title='Final Exam',
                weighting=60,
                )
        ci_recent_inperson = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Recent Inperson Instance',
                start_date=recent_start,
                end_date=recent_finish,
                delivery_mode=in_person,
                teachingsession=recent_session
                )
        models.Task.objects.validated_create(
                courseinstance=ci_recent_inperson,
                title='Tutorials',
                weighting=10,
                )
        models.Task.objects.validated_create(
                courseinstance=ci_recent_inperson,
                title='Lab',
                weighting=20,
                )
        models.Task.objects.validated_create(
                courseinstance=ci_recent_inperson,
                title='Lab',
                weighting=20,
                )
        models.Task.objects.validated_create(
                courseinstance=ci_recent_inperson,
                title='Final Exam',
                weighting=50,
                )
        # Create a class that isn't tied to a teaching session and runs within the start/end dates
        # of one. This shouldn't appear in any results
        ci_nosession_online = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Intensive online course',
                start_date=recent_start + timedelta(weeks=1),
                end_date=today + timedelta(weeks=1),
                delivery_mode=online,
                )
        models.Task.objects.validated_create(
                courseinstance=ci_nosession_online,
                title='Final Exam',
                weighting=100,
                )
        ci_future_inperson = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Future Inperson Instance',
                start_date=future_start,
                end_date=future_finish,
                delivery_mode=in_person,
                teachingsession=future_session
                )
        models.Task.objects.validated_create(
                courseinstance=ci_future_inperson,
                title='Final Exam',
                weighting=100,
                )
        self.assertDictEqual(
                self.cv1.get_indicative_assessments(),
                {
                    'Online': 'Assignment 10.00%; Mid-semester test 30.00%; Final Exam 60.00%',
                    'In Person': 'Tutorials 10.00%; Lab 40.00%; Final Exam 50.00%',
                    }
                )
        # Test with datetime argument and empty delivery mode
        today = datetime.today() - timedelta(weeks=5)
        self.assertDictEqual(
                self.cv1.get_indicative_assessments(date=today),
                {'Online': 'Final Exam 100.00%'},
                )
        # If a class not linked to a TeachingSession starts after the closest session finishes but
        # before the current date we use it.
        today = date.today() + timedelta(weeks=3)
        ci_nosession_inperson = models.CourseInstance.objects.validated_create(
                courseversion=self.cv1,
                instance_descriptor='Intensive holiday course',
                start_date=today,
                end_date=today + timedelta(weeks=1),
                delivery_mode=in_person,
                )
        models.Task.objects.validated_create(
                courseinstance=ci_nosession_inperson,
                title='Final Exam',
                weighting=100,
                )
        today = today + timedelta(days=4)
        self.assertDictEqual(
                self.cv1.get_indicative_assessments(date=today),
                {
                    'In Person': 'Final Exam 100.00%',
                    'Online': 'Assignment 10.00%; Mid-semester test 30.00%; Final Exam 60.00%',
                    },
                )


class TestCourseInstance(TestCase):

    @classmethod
    def setUpTestData(cls):
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        course = models.Course.objects.validated_create(code='Test Course')
        cls.cv = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )

    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            ci = models.CourseInstance.objects.validated_create(
                    courseversion=self.cv,
                    instance_descriptor='Test instance',
                    start_date=date.today(),
                    end_date=date.today(),
                    )
        self.assertTrue(Version.objects.get_for_object(ci).exists())

    # def test_clean(self):
        # dm1 = models.DeliveryMode.objects.create(mode='Online')
        # dm2 = models.DeliveryMode.objects.create(mode='In Person')
        # CourseVersion delivery mode is blank
        # course_instance = models.CourseInstance.objects.create(
        #         courseversion=self.cv,
        #         instance_descriptor='Test instance',
        #         start_date=date.today(),
        #         end_date=date.today(),
        #         delivery_mode=dm2,
        #         )
        # delivery modes differ
        # self.cv.delivery_mode = dm2
        # self.cv.save()
        # course_instance.delivery_mode = dm1
        # self.assertRaisesMessage(
        #         ValidationError,
        #         'CourseInstance delivery mode must match that of its parent CourseVersion.',
        #         course_instance.clean,
        #         )


class TestLearningOutcome(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )

    def test_defaults(self):
        """The default order is set to 0. A revision is made."""
        with reversion.create_revision():
            lo = models.LearningOutcome.objects.validated_create(
                    courseversion=self.cv,
                    outcome='Stuff.'
                    )
        self.assertEqual(lo.order, 0)
        self.assertTrue(Version.objects.get_for_object(lo).exists())

    def test_str(self):
        """Output of __str__ varies depending on values of outcome attributes"""
        lo = models.LearningOutcome(courseversion=self.cv, outcome='Stuff.')
        self.assertEqual(lo.outcome, str(lo))
        lo.outcome = ''.join(repeat('0123456789', 5))
        self.assertEqual(lo.outcome[:37] + '...', str(lo))
        lo.outcome_short_form = 'Short stuff.'
        self.assertEqual(lo.outcome_short_form, str(lo))


class TestWeightedLOToSO(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )
        # Objective relationships
        cls.std_body = models.StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        cls.std_fwork1 = models.StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 1',
                )
        cls.std_fwork2 = models.StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 3',
                )
        cls.node1 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 1',
                short_name='N1',
                )
        cls.node2 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 2',
                short_name='N2',
                )
        cls.node3 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork2,
                name='Node 3',
                short_name='N3',
                )
        models.CourseVersion.objects.validated_update(
                cls.cv,
                **{
                    'standards_frameworks': [cls.std_fwork1],
                    'objectives': [cls.node1, cls.node2],
                    }
                )
        cls.lo1 = models.LearningOutcome.objects.validated_create(
                courseversion=cls.cv,
                outcome='Good stuff',
                )
        # Weighting relationship
        cls.scheme1 = models.WeightingScheme.objects.validated_create(name='Valid Scheme')
        cls.scheme2 = models.WeightingScheme.objects.validated_create(name='Invalid Scheme')
        cls.weight1 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='High'
                )
        cls.weight2 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme2,
                value='Low'
                )
        cls.weight3 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='Middle'
                )
        models.CourseVersion.objects.validated_update(
                cls.cv,
                **{'weightingschemes': [cls.scheme1]})

    def test_validate_unique(self):
        """
        Instance with same LearningOutcome/StandardObjective links as existing object raises
        exception.
        """
        models.WeightedLOToSO.objects.validated_create(
                learningoutcome=self.lo1,
                standardobjective=self.node1,
                )
        self.assertRaises(
                ValidationError,
                models.WeightedLOToSO.objects.validated_create,
                **{'learningoutcome': self.lo1, 'standardobjective': self.node1}
                )

    def test_clean_passes_defaults(self):
        """
        StandardObjectives/Weights must come from the set linked to the CourseVersion.
        Confirm a revision is made.
        """
        with reversion.create_revision():
            weightedlotoso = models.WeightedLOToSO.objects.validated_create(
                    learningoutcome=self.lo1,
                    standardobjective=self.node1,
                    weights=[self.weight1],
                    )
        self.assertTrue(Version.objects.get_for_object(weightedlotoso).exists())

    def test_clean_standardobjective_not_linked(self):
        """
        Attempting to add a link to a StandardObjective which isn't linked to the parent
        CourseVersion raises an exception.
        """
        self.assertRaisesMessage(
                ValidationError,
                ('{\'__all__\': [\'The StandardObjective "objective" must come from the set linked '
                 'to "learningoutcome\\\'s" CourseVersion.\']}'),
                models.WeightedLOToSO.objects.validated_create,
                **{'learningoutcome': self.lo1, 'standardobjective': self.node3}
                )

    def test_clean_weights_pre_add_weight_from_non_associated_scheme(self):
        """
        Creating/adding with a Weight whose WeightingScheme isn't linked to the parent
        CourseVersion raises an exception.
        """
        weightedlotoso = models.WeightedLOToSO.objects.validated_create(
                learningoutcome=self.lo1,
                standardobjective=self.node1,
                )
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    ('[\'Weights must come from a WeightingScheme associated with '
                     '"learningoutcome\\\'s" CourseVersion.\']'),
                    weightedlotoso.weights.add,
                    *[self.weight1, self.weight2]
                    )
        self.assertFalse(weightedlotoso.weights.all())
        # And from the reverse end of the relationship
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    ('[\'Weights must come from a WeightingScheme associated with '
                     '"learningoutcome\\\'s" CourseVersion.\']'),
                    self.weight2.weightedlotoso_set.add,
                    weightedlotoso
                    )
        self.weight1.weightedlotoso_set.add(weightedlotoso)

    def test_clean_weights_pre_add_multiple_weights_same_scheme(self):
        """
        Creating/updating with multiple Weights from the same WeightingScheme raises an exception.
        """
        weightedlotoso = models.WeightedLOToSO.objects.validated_create(
                learningoutcome=self.lo1,
                standardobjective=self.node1,
                )
        # Add multiple Weights from same scheme
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    '[\'You can only apply one weight from each weighting scheme.\']',
                    weightedlotoso.weights.add,
                    *[self.weight1, self.weight3]
                    )
        self.assertFalse(weightedlotoso.weights.all())
        weightedlotoso.weights.add(self.weight1)
        # Add from existing scheme
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    '[\'You can only apply one weight from each weighting scheme.\']',
                    weightedlotoso.weights.add,
                    self.weight3
                    )
        # Add from reverse end of relationship
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    '[\'You can only apply one weight from each weighting scheme.\']',
                    self.weight3.weightedlotoso_set.add,
                    weightedlotoso
                    )


class TestWeightedTaskToLO(TestCase):

    @classmethod
    def setUpTestData(cls):
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )
        cv2 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version 2',
                status=status,
                )
        ci = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test instance',
                start_date=date.today(),
                end_date=date.today(),
                )
        cls.lo1 = models.LearningOutcome.objects.validated_create(
                courseversion=cls.cv1,
                outcome='Something good.',
                )
        cls.lo2 = models.LearningOutcome.objects.validated_create(
                courseversion=cv2,
                outcome='Is gonna happen.',
                )
        cls.task = models.Task.objects.create(
                courseinstance=ci,
                title='Things you can do.',
                )
        # Weighting relationships
        cls.scheme1 = models.WeightingScheme.objects.validated_create(name='Valid Scheme')
        cls.scheme2 = models.WeightingScheme.objects.validated_create(name='Invalid Scheme')
        cls.weight1 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='High'
                )
        cls.weight2 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme2,
                value='Low'
                )
        cls.weight3 = models.Weight.objects.validated_create(
                weightingscheme=cls.scheme1,
                value='Middle'
                )
        models.CourseVersion.objects.validated_update(
                cls.cv1,
                **{'weightingschemes': [cls.scheme1]}
                )

    def test_validate_unique(self):
        """Instance with same Task/LearningOutcome links as existing object raises exception"""
        models.WeightedTaskToLO.objects.validated_create(
                task=self.task,
                learningoutcome=self.lo1,
                )
        self.assertRaises(
                ValidationError,
                models.WeightedTaskToLO.objects.validated_create,
                **{'task': self.task, 'learningoutcome': self.lo1}
                )

    def test_clean_passes_defaults(self):
        """
        LearningOutcomes/Weights must come from the set linked to the CourseVersion.
        A revision is created.
        """
        with reversion.create_revision():
            weightedtasktolo = models.WeightedTaskToLO.objects.validated_create(
                    task=self.task,
                    learningoutcome=self.lo1,
                    weights=[self.weight1],
                    )
        self.assertTrue(Version.objects.get_for_object(weightedtasktolo).exists())

    def test_clean_learningoutcome_wrong_courseversion(self):
        """Add with incorrect LearningOutcome relationship raises exception."""
        self.assertRaisesMessage(
                ValidationError,
                ('{\'__all__\': [\'The linked LearningOutcome must come from the set associated '
                 'with the parent CourseVersion of "task".\']}'),
                models.WeightedTaskToLO.objects.validated_create,
                **{'task': self.task, 'learningoutcome': self.lo2}
                )

    def test_clean_weights_pre_add_weight_from_non_associated_scheme(self):
        """
        Creating/adding with a Weight whose WeightingScheme isn't linked to the parent
        CourseVersion raises an exception.
        """
        weightedtasktolo = models.WeightedTaskToLO.objects.validated_create(
                task=self.task,
                learningoutcome=self.lo1,
                )
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    ('[\'Weights must come from a WeightingScheme associated with "task\\\'s" '
                     'CourseVersion.\']'),
                    weightedtasktolo.weights.add,
                    self.weight2
                    )
        # And from the reverse end of the relationship
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    ('[\'Weights must come from a WeightingScheme associated with "task\\\'s" '
                     'CourseVersion.\']'),
                    self.weight2.weightedtasktolo_set.add,
                    weightedtasktolo
                    )
        self.weight1.weightedtasktolo_set.add(weightedtasktolo)

    def test_clean_weights_pre_add_multiple_weights_same_scheme(self):
        """
        Creating/updating with multiple Weights from the same WeightingScheme raises an exception.
        """
        weightedtasktolo = models.WeightedTaskToLO.objects.validated_create(
                task=self.task,
                learningoutcome=self.lo1,
                )
        # Add multiple Weights from same scheme
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    '[\'You can only apply one weight from each weighting scheme.\']',
                    weightedtasktolo.weights.add,
                    *[self.weight1, self.weight3]
                    )
        weightedtasktolo.weights.add(self.weight1)
        # Add from existing scheme
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    '[\'You can only apply one weight from each weighting scheme.\']',
                    weightedtasktolo.weights.add,
                    self.weight3
                    )
        # Add from reverse end of relationship
        with transaction.atomic():
            self.assertRaisesMessage(
                    ValidationError,
                    '[\'You can only apply one weight from each weighting scheme.\']',
                    self.weight3.weightedtasktolo_set.add,
                    weightedtasktolo
                    )


class TestWeightingScheme(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            weightingscheme = models.WeightingScheme.objects.validated_create(name='Test Scheme')
        self.assertTrue(Version.objects.get_for_object(weightingscheme).exists())


class TestWeight(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.wscheme = models.WeightingScheme.objects.validated_create(name='Test Scheme')

    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            weight = models.Weight.objects.validated_create(
                    weightingscheme=self.wscheme,
                    value='High'
                    )
        self.assertTrue(Version.objects.get_for_object(weight).exists())


class TestOrganisationalUnit(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            ou = models.OrganisationalUnit.objects.validated_create(
                    name='Test OU',
                    short_name='TOU'
                    )
        self.assertTrue(Version.objects.get_for_object(ou).exists())


class TestTeachingSession(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            teachingsession = models.TeachingSession.objects.validated_create(
                    name='Semester 1, THE FUTURE',
                    start_date=date.today(),
                    end_date=date.today() + timedelta(days=1)
                    )
        self.assertTrue(Version.objects.get_for_object(teachingsession).exists())


class TestAcademicCareer(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            career = models.AcademicCareer.objects.validated_create(career='McDonalds')
        self.assertTrue(Version.objects.get_for_object(career).exists())


class TestDeliveryMode(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            mode = models.DeliveryMode.objects.validated_create(mode='Fast')
        self.assertTrue(Version.objects.get_for_object(mode).exists())


class TestCourseClassification(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            classification = models.CourseClassification.objects.validated_create(
                    classification='Postgraduate'
                    )
        self.assertTrue(Version.objects.get_for_object(classification).exists())


class TestSchedule(TestCase):
    @classmethod
    def setUpTestData(cls):
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )
        cls.ci = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test instance',
                start_date=date.today(),
                end_date=date.today(),
                )

    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            schedule = models.Schedule.objects.validated_create(
                    courseinstance=self.ci,
                    session_name='Session the first.'
                    )
        self.assertTrue(Version.objects.get_for_object(schedule).exists())


class TestCourseRelatedRoleTitle(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            role_title = models.CourseRelatedRoleTitle.objects.validated_create(title='Poobah')
        self.assertTrue(Version.objects.get_for_object(role_title).exists())


class TestCourseRelatedRole(TestCase):
    @classmethod
    def setUpTestData(cls):
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )
        cls.ci = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test instance',
                start_date=date.today(),
                end_date=date.today(),
                )

    def test_reversion(self):
        """Confirm an initial revision is created."""
        user = User.objects.create_user(username='User', password='sekret')
        role_title = models.CourseRelatedRoleTitle.objects.validated_create(title='Poobah')
        with reversion.create_revision():
            role = models.CourseRelatedRole.objects.validated_create(
                    user=user,
                    courseinstance=self.ci,
                    role_title=role_title
                    )
        self.assertTrue(Version.objects.get_for_object(role).exists())


class TestReviewStatus(TestCase):
    def test_reversion(self):
        """Confirm an initial revision is created."""
        with reversion.create_revision():
            status = models.ReviewStatus.objects.validated_create(name='Approved')
        self.assertTrue(Version.objects.get_for_object(status).exists())
