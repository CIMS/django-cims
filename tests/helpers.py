from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from perfieldperms.models import PerFieldPermission


def create_pfp(app_label, model, perm_cname, field):
    c_type = ContentType.objects.get(
            app_label=app_label,
            model=model,
            )
    perm_obj = Permission.objects.get(content_type=c_type, codename=perm_cname)
    codename='{}__{}'.format(perm_cname, field)
    try:
        pfp = Permission.objects.get(
                content_type=c_type,
                codename=codename,
                )
    except ObjectDoesNotExist:
        pfp = PerFieldPermission(
                content_type=c_type,
                codename=codename,
                name='{} - {}'.format(perm_obj.name, field),
                model_permission=perm_obj,
                field_name=field,
                )
        pfp.save()
    return pfp


def dict_a_from_b(dict_a: dict, dict_b: dict) -> dict:
    """Tries to recreate dict A using items from dict B."""
    return {key: dict_b[key] for key in dict_a if key in dict_b}


def get_perm(app_label, model, perm_cname):
    return Permission.objects.get(
            content_type__app_label=app_label,
            content_type__model=model,
            codename=perm_cname,
            )


def get_perm_str(perm):
    return '{}.{}'.format(perm.content_type.app_label, perm.codename)


def setup_view(view, request, *args, **kwargs):
    """
    Mimic as_view() but return view instance instead of callable to enable unit testing.

    args and kwargs are as you would pass to `reverse()`.
    """
    view.request = request
    view.args = args
    view.kwargs = kwargs
    return view
