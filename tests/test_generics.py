"""
Test generic view classes/mixins.
"""
import unittest
from collections import OrderedDict
from unittest.mock import MagicMock, Mock, patch

from django.contrib.admin.models import LogEntry
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured
from django.forms.models import BaseInlineFormSet, inlineformset_factory, modelform_factory
from django.test import RequestFactory, SimpleTestCase, TestCase, override_settings
from django.views.generic import TemplateView
from reversion.models import Revision

from cims import generic
from cims.auth.models import UserObjectPermission
from cims.forms import CIMSBaseModelForm
from cims.formsets import AnnotationManager, CIMSBaseGenericInlineFormset, CIMSInlineFormSetManager
from cims.models import Annotation
from .helpers import get_perm, setup_view
from .models import Author, Book

User = get_user_model()


class MiscFunctionsTest(unittest.TestCase):
    def test_make_dict_of_model_fields(self):
        """Return a dict of structure {'field_name': Field()} for a given model."""
        self.assertDictEqual(
                generic.make_dict_of_model_fields(Author),
                {'name': Author._meta.get_field('name')}
                )


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class CIMSSingleObjectMixinTest(TestCase):
    """Mixin adds additional context."""
    @classmethod
    def setUpTestData(cls):
        class AuthorView(generic.CIMSSingleObjectMixin):
            model = Author

        cls.AuthorView = AuthorView
        cls.request_factory = RequestFactory()
        cls.author = Author.objects.validated_create(name='Author')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.request_factory.get('/')
        self.request.user = self.user

    def test_get_context_data_existing_object(self):
        """
        Additional context on top of that added for a new object is added when presented with an
        existing object, comprising detail and update links.
        """
        view = setup_view(self.AuthorView(), self.request, **{'pk': self.author.pk})
        view.object = self.author
        context = view.get_context_data()
        self.assertEqual(context['title'], 'author: {}'.format(self.author))
        self.assertDictEqual(
                context['obj_links'],
                {
                    'create': '/author/add/',
                    'detail': '/testapi/authortester/{}/'.format(self.author.pk),
                    'update': '/testapi/authortester/{}/'.format(self.author.pk)
                    }
                )

    def test_get_context_data_user_lacks_permissions(self):
        """
        Additional context relating to permissions is set false if user lacks permissions.
        Change permission must be object specific or whole of model.
        """
        ghost_writer = Author.objects.validated_create(name='Ghost Writer')
        UserObjectPermission.objects.create(
                user=self.user,
                permission=get_perm('tests', 'author', 'change_author'),
                object_id=ghost_writer.pk,
                content_type=ContentType.objects.get_for_model(Author)
                )
        view = setup_view(self.AuthorView(), self.request, **{'pk': self.author.pk})
        view.object = self.author
        context = view.get_context_data()
        self.assertFalse(context['has_add_permission'])
        self.assertFalse(context['has_change_permission'])

    def test_get_context_data_user_has_permissions(self):
        """Additional context relating to permissions is set True if user has permissions."""
        self.user.user_permissions.add(
                get_perm('tests', 'author', 'add_author'),
                get_perm('tests', 'author', 'change_author')
                )
        view = setup_view(self.AuthorView(), self.request, **{'pk': self.author.pk})
        view.object = self.author
        context = view.get_context_data()
        self.assertTrue(context['has_add_permission'])
        self.assertTrue(context['has_change_permission'])


class ModelFormWithInlinesMixinTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        class AuthorView(generic.ModelFormWithInlinesMixin, TemplateView):
            model = Author
            template_name = 'tests/blank.html'
            fields = '__all__'

        cls.AuthorView = AuthorView

        class BookManager(CIMSInlineFormSetManager):
            parent_model = Author
            model = Book
            fields = '__all__'

        cls.BookManager = BookManager
        cls.request_factory = RequestFactory()

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.request_factory.get('/')
        self.request.user = self.user

    def test_get_inlines_no_inlines(self):
        """If no inlines configured return an empty list."""
        view = setup_view(self.AuthorView(), self.request)
        self.assertListEqual([], view.get_inlines())

    def test_get_inlines_inlines(self):
        """Return inlines."""
        class AuthorView(self.AuthorView):
            inlines = ['AnInline']

        view = setup_view(AuthorView(), self.request)
        self.assertListEqual(['AnInline'], view.get_inlines())

    def test_get_form_class_is_properly_subclassed(self):
        """
        If the specified/generated form class is not a subclass of CIMSBaseModelForm it is
        regenerated so it is.
        """
        view = setup_view(self.AuthorView(), self.request)
        self.assertTrue(issubclass(view.get_form_class(), CIMSBaseModelForm))

    def test_get_form_class_passed_through(self):
        """If the specified form class is a subclass of CIMSBaseModelForm it is passed through."""
        AuthorForm = modelform_factory(Author, form=CIMSBaseModelForm, fields='__all__')

        class AuthorView(self.AuthorView):
            inlines = ['AnInline']
            fields = None
            form_class = AuthorForm

        view = setup_view(AuthorView(), self.request)
        self.assertTrue(issubclass(view.get_form_class(), AuthorForm))

    def test_get_form_kwargs(self):
        """Request user is added to form kwargs."""
        view = setup_view(self.AuthorView(), self.request)
        kwargs = view.get_form_kwargs()
        self.assertEqual(kwargs['user'], self.user)

    def test_construct_inlines(self):
        """An inline is added to the list of inlines if the user has appropriate permissions."""
        class AuthorView(self.AuthorView):
            inlines = [self.BookManager]

        self.user.user_permissions.add(
                get_perm('tests', 'book', 'add_book'),
                )
        author = Author.objects.validated_create(name='Author')
        view = setup_view(AuthorView(), self.request, **{'pk': author.pk})
        view.object = author
        formsets = view.construct_inlines()
        self.assertTrue('books' in formsets)
        self.assertTrue(isinstance(formsets['books'][0], CIMSInlineFormSetManager))
        self.assertTrue(isinstance(formsets['books'][1], BaseInlineFormSet))

    def test_construct_inlines_no_permissions(self):
        """If the user lacks permissions for the inline it isn't added to the list."""
        class AuthorView(self.AuthorView):
            inlines = [self.BookManager]

        author = Author.objects.validated_create(name='Author')
        view = setup_view(AuthorView(), self.request, **{'pk': author.pk})
        view.object = author
        formsets = view.construct_inlines()
        self.assertDictEqual(OrderedDict(), formsets)

    @patch('django.contrib.messages.success')
    def test_form_valid(self, msgs):
        """Creates a revision, creates a LogEntry, logs a message, returns a redirect."""
        view = setup_view(self.AuthorView(), self.request)
        view.get_success_url = Mock(return_value='/')
        view.object = None
        form = view.get_form_class()(data={'name': 'Author'})
        form.is_valid()
        response = view.form_valid(form)
        self.assertEqual(response.status_code, 302)
        revision = Revision.objects.first()
        self.assertEqual(revision.comment, '[{"added": {}}]')
        self.assertEqual(revision.user_id, self.user.pk)
        logentry = LogEntry.objects.first()
        self.assertEqual(logentry.change_message, '[{"added": {}}]')
        self.assertEqual(logentry.user_id, self.user.pk)
        msgs.assert_called_once_with(self.request, 'Author created successfully.')

    @patch('django.contrib.messages.success')
    def test_form_valid_update(self, msgs):
        """Logs a change message when updating the object."""
        author = Author.objects.validated_create(name='Author')
        view = setup_view(self.AuthorView(), self.request, **{'pk': author.pk})
        view.get_success_url = Mock(return_value='/')
        view.object = author
        form = view.get_form_class()(data={'name': 'Nom de plume'})
        form.is_valid()
        response = view.form_valid(form)
        self.assertEqual(response.status_code, 302)
        msgs.assert_called_once_with(self.request, 'Nom de plume updated successfully.')

    @patch('django.contrib.messages.error')
    def test_form_valid_form_validation_error(self, msgs):
        """
        Returns HTTP 200 when extended model validation fails following form save. Failed
        validation adds form and formsets to context. Adds failure message.
        """
        author = Author.objects.validated_create(name='Author')
        view = setup_view(self.AuthorView(), self.request, **{'pk': author.pk})
        view.object = author
        form = view.get_form_class()(data={'name': 'Ray Bradbury'})
        form.is_valid()
        response = view.form_valid(form)
        self.assertEqual(response.status_code, 200)
        self.assertIn('form', response.context_data)
        self.assertIn('formsets', response.context_data)
        msgs.assert_called_once_with(
                self.request,
                'The form failed validation, please fix the highlighted errors.'
                )

    @patch('django.contrib.messages.error')
    def test_form_valid_formset_validation_error(self, msgs):
        """Returns HTTP 200 when formset validation fails, logs message"""
        view = setup_view(self.AuthorView(), self.request)
        view.object = None
        form = view.get_form_class()(data={'name': 'Author'})
        form.is_valid()
        FormSet = inlineformset_factory(Author, Book, fields='__all__', extra=1)
        formset = FormSet(data={
            'book_set-0-title': 'Hard Times',
            'book_set-0-pages': '-1',
            'book_set-TOTAL_FORMS': '1',
            'book_set-INITIAL_FORMS': '0',
            'book_set-MAX_NUM_FORMS': '1000',
            'book_set-MIN_NUM_FORMS': '0',
            })
        formset.is_valid()
        formsets = {'books': (None, formset)}
        view.construct_inlines = Mock(return_value=formsets)
        response = view.form_valid(form)
        self.assertEqual(response.status_code, 200)
        msgs.assert_called_once_with(
                self.request,
                'The form failed validation, please fix the highlighted errors.'
                )

    @patch('django.contrib.messages.success')
    def test_form_valid_formset_delete_invalid_entry(self, msgs):
        """
        When a form is submitted with a formset entry that is invalid but marked for deletion, the
        submission succeeds.
        """
        author = Author.objects.validated_create(name='Author')
        view = setup_view(self.AuthorView(), self.request, **{'pk': author.pk})
        view.get_success_url = Mock(return_value='/')
        view.object = author
        form = view.get_form_class()(data={'name': 'Nom de plume'})
        form.is_valid()
        FormSet = inlineformset_factory(Author, Book, fields='__all__', extra=1)
        formset = FormSet(data={
            'book_set-0-title': 'Hard Times',
            'book_set-0-pages': '-1',
            'book_set-0-DELETE': 'on',
            'book_set-TOTAL_FORMS': '1',
            'book_set-INITIAL_FORMS': '0',
            'book_set-MAX_NUM_FORMS': '1000',
            'book_set-MIN_NUM_FORMS': '0',
            })
        formset.is_valid()
        formsets = {'books': (None, formset)}
        view.construct_inlines = Mock(return_value=formsets)
        response = view.form_valid(form)
        self.assertEqual(response.status_code, 302)
        msgs.assert_called_once_with(self.request, 'Nom de plume updated successfully.')

    @patch('cims.generic.formset_change_message', return_value='')
    def test_add_formset_change_messages(self, msgs):
        """`formset_change_message() called once for each formset."""
        view = setup_view(self.AuthorView(), self.request)
        view.add_formset_change_messages([{}, {}], '')
        self.assertEqual(2, msgs.call_count)

    def test_save_formsets(self):
        """Saves the formset."""
        author = Author.objects.validated_create(name='Author')
        view = setup_view(self.AuthorView(), self.request, **{'pk': author.pk})
        FormSet = inlineformset_factory(Author, Book, fields='__all__', extra=1)
        formset = FormSet(
                data={
                    'book_set-0-title': 'Hard Times',
                    'book_set-TOTAL_FORMS': '1',
                    'book_set-INITIAL_FORMS': '0',
                    'book_set-MAX_NUM_FORMS': '1000',
                    'book_set-MIN_NUM_FORMS': '0',
                    },
                instance=author
                )
        formset.is_valid()
        formsets = [formset]
        view.save_formsets(formsets)
        self.assertEqual('Hard Times', Book.objects.first().title)

    def test_save_formsets_no_save_method(self):
        """Raises ImproperlyConfigured if given a FormSet without a save() method."""
        view = setup_view(self.AuthorView(), self.request)
        formsets = ['books']
        self.assertRaisesMessage(
                ImproperlyConfigured,
                'AuthorView.save_formsets() must be overriden if handling FormSets without save() '
                'methods.',
                *[view.save_formsets, formsets]
                )


class ProcessFormWithInlinesViewTest(SimpleTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        class AuthorView(generic.ModelFormWithInlinesMixin, generic.ProcessFormWithInlinesView):
            model = Author
            template_name = 'tests/blank.html'
            fields = '__all__'

        cls.AuthorView = AuthorView
        cls.request_factory = RequestFactory()

    def setUp(self):
        self.request = self.request_factory.get('/')

    def test_get(self):
        """GET passes form, formsets and kwargs to context."""
        view = setup_view(self.AuthorView(), self.request)
        view.get_form = Mock(return_value=42)
        view.get_context_data = Mock()
        view.render_to_response = Mock()
        kwargs = {'foo': 'bar'}
        view.get(self.request, **kwargs)
        args, kwargs = view.get_context_data.call_args
        self.assertDictEqual(kwargs, {'form': 42, 'formsets': OrderedDict(), 'foo': 'bar'})

    def test_post(self):
        """Valid form results in call to `form_valid()`."""
        form = modelform_factory(Author, fields='__all__')(data={'name': 'Nom de plume'})
        view = setup_view(self.AuthorView(), self.request)
        view.get_form = Mock(return_value=form)
        view.form_valid = Mock()
        view.post(self.request)
        view.form_valid.assert_called_once_with(form)

    def test_post_form_invalid(self):
        """Invalid form results in call to forms_invalid with form and formsets as kwargs."""
        author = Author(name='Author')
        form = modelform_factory(Author, fields='__all__')(
                data={},
                instance=author
                )
        view = setup_view(self.AuthorView(), self.request)
        view.get_form = Mock(return_value=form)
        view.forms_invalid = Mock()
        view.post(self.request)
        view.forms_invalid.assert_called_once_with(form, OrderedDict())


class BaseDetailViewTest(SimpleTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        class AuthorView(generic.BaseDetailView):
            model = Author

        cls.AuthorView = AuthorView
        cls.request_factory = RequestFactory()

    def setUp(self):
        self.request = self.request_factory.get('/')

    def test_build_layout_no_fields(self):
        """Returns an empty list if neither `fields` nor `layout` is specified."""
        view = setup_view(self.AuthorView(), self.request)
        self.assertFalse(view.build_layout())

    def test_build_layout_no_layout(self):
        """Returns a  list with a single layout entry when no layout is specified."""
        author = Author(name='Author')
        view = setup_view(self.AuthorView(), self.request)
        view.object = author
        view.fields = ['name']
        self.assertListEqual(
                [(None, [(Author._meta.get_field('name'), 'Author')])],
                view.build_layout(),
                )

    def test_build_layout_with_layout(self):
        """Returns a  list with fields allocated as per the `layout` attribute."""
        author = Author(name='Author')
        view = setup_view(self.AuthorView(), self.request)
        view.object = author
        view.fields = ['name']
        view.layout = [(None, []), ('Name', ['name'])]
        self.assertListEqual(
                [
                    (None, []),
                    ('Name', [(Author._meta.get_field('name'), 'Author')])
                    ],
                view.build_layout(),
                )

    def test_get_extra_links(self):
        """Returns an empty dict."""
        view = setup_view(self.AuthorView(), self.request)
        self.assertDictEqual({}, view.get_extra_links())

    def test_get_related_model_helpers_no_helpers(self):
        """Return an empty list if no helpers configured."""
        view = setup_view(self.AuthorView(), self.request)
        helpers = view.get_tab_helpers()
        self.assertListEqual(helpers, [])

    def test_get_related_model_helpers(self):
        author = Author(name='Author')
        view = setup_view(self.AuthorView(), self.request)
        view.object = author
        helper = MagicMock()
        view.tab_helpers = [helper]
        helpers = view.get_tab_helpers()
        helper.assert_called_once_with(view.model, author, self.request)
        self.assertListEqual(helpers, [helper()])

    def test_get_context_data(self):
        """Adds `tab_helpers` to context."""
        author = Author(name='Author')
        view = setup_view(self.AuthorView(), self.request)
        view.object = author
        context = view.get_context_data()
        self.assertIn('tab_helpers', context)


class CIMSCreateViewTest(TestCase):
    """Mixin adds additional context."""
    @classmethod
    def setUpTestData(cls):
        class AuthorView(generic.CIMSCreateView):
            model = Author
            fields = '__all__'

        cls.AuthorView = AuthorView
        cls.request_factory = RequestFactory()
        cls.author = Author.objects.validated_create(name='Author')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.request_factory.get('/')
        self.request.user = self.user

    def test_get_context_data_new_object(self):
        """Additional context of title, model options is added."""
        view = setup_view(self.AuthorView(), self.request)
        view.object = None
        context = view.get_context_data()
        self.assertEqual(context['title'], 'Add new author')
        self.assertEqual(context['opts'], view.model._meta)


class CIMSUpdateViewTest(TestCase):
    """Mixin adds additional context."""
    @classmethod
    def setUpTestData(cls):
        class AuthorView(generic.CIMSUpdateView):
            model = Author
            fields = '__all__'

        cls.AuthorView = AuthorView
        cls.request_factory = RequestFactory()
        cls.author = Author.objects.validated_create(name='Author')

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.request_factory.get('/')
        self.request.user = self.user

    def test_construct_inlines(self):
        """An AnnotationManager is appended to the inlines dict"""
        view = setup_view(self.AuthorView(), self.request)
        view.object = self.author
        note_inline = view.construct_inlines().popitem()
        self.assertTrue(isinstance(note_inline[1][0], AnnotationManager))
        self.assertTrue(isinstance(note_inline[1][1], CIMSBaseGenericInlineFormset))

    def test_get_context_data(self):
        """Additional context of Annotation queryset, Annotation inline added."""
        note = Annotation.objects.validated_create(
                object_id=self.author.pk,
                content_type=ContentType.objects.get_for_model(self.author),
                note='The best note.'
                )
        Annotation.objects.validated_create(
                object_id=99999,
                content_type=ContentType.objects.get_for_model(self.author),
                note='The best note.'
                )
        view = setup_view(self.AuthorView(), self.request, **{'pk': self.author.pk})
        view.object = self.author
        context = view.get_context_data(form=view.get_form(), formsets=view.construct_inlines())
        self.assertListEqual([note], list(context['annotations']))
        self.assertTrue(isinstance(context['annotations_inline'][0], AnnotationManager))
        self.assertTrue(isinstance(context['annotations_inline'][1], CIMSBaseGenericInlineFormset))
        # Annotations inline has been removed from the inlines dict so it can be rendered separately
        self.assertFalse(context['formsets'])
