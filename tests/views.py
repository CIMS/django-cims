from django.views.generic import CreateView, DetailView, UpdateView
from django.template.response import TemplateResponse

from .models import Author


def blank(request):
    """View to use when we don't care about the response but want to run middelware etc."""
    return TemplateResponse(request, 'tests/blank.html')


class AuthorCreate(CreateView):
    model = Author


class AuthorDetail(DetailView):
    model = Author


class AuthorUpdate(UpdateView):
    model = Author
