from bs4 import BeautifulSoup
from django.test import TestCase
from requests.exceptions import HTTPError

from cims.contrib.pandcscraper.pandc_scraper import PandCScraper


class TestPandCScraper(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_init(self):
        """Parts of __init__() that will break other things if they fail."""
        # Missing page raises exception
        self.assertRaisesMessage(
                HTTPError,
                ('The page http://programsandcourses.anu.edu.au/course/COMP0001/ could not be '
                 'found'),
                *[PandCScraper, 'comp0001']
                )
        # Gets something that looks like a year from the page
        scraper = PandCScraper('comp1100')
        self.assertTrue(int(scraper.year) > 2000)

    def test_get_course_dict(self):
        """Exercise most of the parts of the scraper. Hard to test operation beyond this as it
        depends on particular page/whether layout of PandC gets tweaked."""
        scraper = PandCScraper('comp1100')
        course_dict = scraper.get_course_dict()
        self.assertEqual(course_dict['code'], 'COMP1100')
        self.assertEqual(course_dict['title'], 'Programming as Problem Solving')
        self.assertTrue(course_dict['learning_outcomes'])
        self.assertTrue(course_dict['indicative_assessments'])
        self.assertTrue(course_dict['mode'])
        self.assertTrue(course_dict['conveners'])
        self.assertTrue(course_dict['req_and_incompat'])
        self.assertTrue(course_dict['offerings'])
        self.assertTrue(course_dict['offered_by'])

    def test_summary_detail(self):
        """Retrieve text value(s) from a list element in a summary block."""
        scraper = PandCScraper('comp1100')
        scraper.soup = BeautifulSoup('', 'html5lib')
        # Entire block missing
        self.assertIsNone(scraper.get_summary_detail('Offered By'))

        with open('tests/contrib/pandcscraper/data/summary_block.html') as f:
            html = f.read()
        scraper.soup = BeautifulSoup(html, 'html5lib')

        # Single value
        self.assertListEqual(
                ['Research School of Computer Science'],
                scraper.get_summary_detail('Offered by')
                )
        # Multiple values
        self.assertListEqual(
                ['Advanced', 'Specialist'],
                scraper.get_summary_detail('Classification')
                )
        # Text is link
        self.assertListEqual(
                ['COMP4620'],
                scraper.get_summary_detail('Co-taught Course')
                )
        # No values
        self.assertListEqual(
                [],
                scraper.get_summary_detail('Areas of interest')
                )
        # Empty span
        self.assertListEqual(
                [],
                scraper.get_summary_detail('Mode of delivery')
                )
        # Entry doesn't exist
        self.assertIsNone(scraper.get_summary_detail('FooBar'))
