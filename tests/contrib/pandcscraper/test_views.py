from django.urls import reverse

from cims.models import CourseVersion, Course, ReviewStatus

from tests import PostgreSQLTestCase


class TestPandCDiffView(PostgreSQLTestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_pandc_diff(self):
        comp1100 = Course.objects.validated_create(code='COMP1100')
        # Course doesn't exist
        url = reverse('pandcscraper:pandcdiff', args=['engn4626'])
        response = self.client.get(url)
        self.assertContains(
                response,
                '',
                status_code=404,
                )
        # No CourseVersion exists for this course code
        url = reverse('pandcscraper:pandcdiff', args=['comp1100'])
        response = self.client.get(url)
        self.assertContains(
                response,
                ('No appropriate CourseVersion found to compare to Programs and Courses. This '
                 'implies none have been approved.'),
                )
        # Error retrieveing PandC page
        status = ReviewStatus.objects.validated_create(name='Approved')
        CourseVersion.objects.validated_create(
                course=comp1100,
                version_descriptor='Test course version',
                status=status,
                )
        url = url + '?year=2010'
        response = self.client.get(url)
        self.assertContains(
                response,
                'Trying to retrieve the page from Programs and Courses failed. The error was:',
                )
        # Basic operation succeeds
        url = reverse('pandcscraper:pandcdiff', args=['comp1100'])
        response = self.client.get(url)
        self.assertContains(response, 'Section Comparisons')
