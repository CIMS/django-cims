"""
Tests for CIMS utility scripts etc.
"""
from django.test import TestCase
from django.contrib.auth import get_user_model

from cims.utils.misc import nasty_search

from .models import Author, Book


User = get_user_model()


class NastySearchTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        author = Author.objects.validated_create(name='Iain M. Banks')
        author2 = Author.objects.validated_create(name='Eric Berne')
        cls.book1 = Book.objects.validated_create(author=author, title='Consider Phlebas')
        cls.book2 = Book.objects.validated_create(author=author, title='The Player of Games')
        cls.book3 = Book.objects.validated_create(author=author2, title='Games People Play')

    def test_matches_substring(self):
        """Search matches substrings."""
        search_vector = ['title']
        q_filters = nasty_search('player', search_vector)
        self.assertQuerysetEqual(Book.objects.filter(q_filters), [repr(self.book2)])

    def test_matches_any_field_in_vector(self):
        """If multiple fields are in the search vector then a match in any field is sufficient."""
        search_vector = ['title', 'author__name']
        q_filters = nasty_search('player', search_vector)
        self.assertQuerysetEqual(Book.objects.filter(q_filters), [repr(self.book2)])
        pass

    def test_matches_all_search_tokens(self):
        """If multiple tokens exist in the search string a single record must match all of them."""
        search_vector = ['title', 'author__name']
        q_filters = nasty_search('play banks', search_vector)
        self.assertQuerysetEqual(Book.objects.filter(q_filters), [repr(self.book2)])

    def test_trigram_stuff(self):
        """Need to do lots of DB setup stuff before we can do this..."""
        pass


class MiscTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass
