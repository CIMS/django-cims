"""
Tests for CIMS formsets.
"""
import unittest
from datetime import date
from unittest.mock import Mock, patch

from crispy_forms.helper import FormHelper
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured
from django.forms import (BaseInlineFormSet, Form, modelform_factory,
                          modelformset_factory)
from django.test import RequestFactory, TestCase, override_settings

from cims import models
from cims.auth.models import UserObjectPermission
from cims.forms import ValidatedModelForm
from cims.formsets import (BaseFormSetManager, BaseModelFormSetManager,
                           CIInlineWeightedTaskToLOManager, CIMSBaseInlineFormSet,
                           CIMSInlineFormSetManager, CIMSModelFormSetManager,
                           CVInlineWeightedLOToSOManager, InlineFormSetPermissionsMixin,
                           M2MValidatingInlineFormSet, ObjectPermsModelFormSet,
                           formset_change_message, BaseGenericInlineFormSetManager)
from .helpers import get_perm
from .models import Author, Book, Topping, Annotation

User = get_user_model()


class FormsetChangeMessageTest(unittest.TestCase):

    class Formset(object):
        pass

    def test_no_changes(self):
        """
        If none of the required attributes are on the object then nothing is added to the
        change_message.
        """
        formset = self.Formset()
        self.assertEqual([], formset_change_message(formset, []))

    def test_add_message(self):
        """
        New objects result in a dict() detailing the addition being added to the change_mesage.
        """
        formset = self.Formset()
        formset.new_objects = [Author(name='Author')]
        self.assertListEqual(
                [{'added': {'name': 'author', 'object': 'Author'}}],
                formset_change_message(formset, [])
                )

    def test_change_message(self):
        """
        Changed objects result in a dict() detailing the change being added to the change_mesage.
        """
        formset = self.Formset()
        formset.changed_objects = [(Author(name='Author'), ['name'])]
        self.assertListEqual(
                [{'changed': {'name': 'author', 'object': 'Author', 'fields': ['name']}}],
                formset_change_message(formset, [])
                )

    def test_deleted_message(self):
        """
        Deleted objects result in a dict() detailing the changes being added to the change_mesage.
        """
        formset = self.Formset()
        formset.deleted_objects = [Author(name='Author')]
        self.assertListEqual(
                [{'deleted': {'name': 'author', 'object': 'Author'}}],
                formset_change_message(formset, [])
                )


class BaseFormSetManagerTest(unittest.TestCase):

    def test_init(self):
        """Request parameter gets set as attribute"""
        manager = BaseFormSetManager(request=True)
        self.assertTrue(manager.request)

    def test_construct_formset_set_attributes(self):
        """
        Adds the following attributes to the generated formset:
        * crispy forms helper
        * crispy forms helper for the formset empty_form
        * help_text
        """
        class Manager(BaseFormSetManager):
            form_class = Form
            crispy_helper = FormHelper
            empty_form_crispy_helper = FormHelper
            help_text = 'Test help text.'

        manager = Manager()
        # manager.request = RequestFactory().get('/')
        formset = manager.construct_formset()
        self.assertTrue(isinstance(formset.helper, FormHelper))
        self.assertTrue(isinstance(formset.empty_form_helper, FormHelper))
        self.assertEqual(formset.help_text, manager.help_text)

    def test_get_factory_kwargs(self):
        """All the attributes are properly set in the kwargs dict."""
        class Manager(BaseFormSetManager):
            extra = 1
            max_num = 2
            min_num = 3
            can_order = 4
            can_delete = 5
            validate_max = 6
            validate_min = 7

        manager = Manager()
        self.assertEqual(
                {'extra': 1, 'max_num': 2, 'min_num': 3, 'can_order': 4, 'can_delete': 5,
                 'validate_max': 6, 'validate_min': 7},
                manager.get_factory_kwargs()
                )

    def test_get_factory_kwargs_add_formset_class(self):
        """If set the formset class is added to the formset factory kwargs dict"""
        class Manager(BaseFormSetManager):
            formset_class = True

        manager = Manager()
        self.assertTrue(manager.get_factory_kwargs()['formset'])

    def test_get_formset_kwargs(self):
        """
        If set the following attributes are added to the formset kwargs dict:
        * initial
        * prefix
        * data
        * files
        """
        class Manager(BaseFormSetManager):
            initial = 1
            prefix = 2

        request = RequestFactory().post('/', {'foo': True})
        request.FILES.update = {'foo_file': 3}
        manager = Manager(request)
        self.assertEqual(
                {'form_kwargs': {}, 'initial': 1, 'prefix': 2, 'data': request.POST,
                 'files': request.FILES},
                manager.get_formset_kwargs()
                )


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class InlineFormSetPermissionsMixinTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()

        class BookMixin(InlineFormSetPermissionsMixin):
            model = Book
            can_delete = True

        cls.BookMixin = BookMixin

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.factory.get('/')
        self.request.user = self.user

    def tests_init_require_request(self):
        """Raise exception if valid request isn't passed as a parameter."""
        self.assertRaisesMessage(
                ImproperlyConfigured,
                'BookMixin requires a valid request argument to perform permissions checks.',
                self.BookMixin
                )

    def test_init_set_num_forms_no_permission(self):
        """max_num and extra are set to 0 if the user lacks add permissions."""
        self.assertEqual(None, self.BookMixin.max_num)
        self.assertEqual(2, self.BookMixin.extra)
        mixin = self.BookMixin(request=self.request)
        self.assertEqual(0, mixin.max_num)
        self.assertEqual(0, mixin.extra)

    def test_get_formset_kwargs(self):
        """The request gets added to the formset kwargs dict."""
        mixin = self.BookMixin(
                request=self.request,
                )
        self.assertEqual(mixin.get_formset_kwargs()['request'], self.request)

    def test_get_queryset_blank(self):
        """User without change permissions gets blank queryset."""
        author = Author.objects.validated_create(name='A Habit')
        Book.objects.validated_create(author=author, title='The Great White Snail')
        mixin = self.BookMixin(
                request=self.request,
                )
        self.assertFalse(mixin.get_queryset())

    def test_get_queryset(self):
        """User with change permission gets non-empty queryset."""
        self.user.user_permissions.add(get_perm('tests', 'book', 'change_book'))
        author = Author.objects.validated_create(name='A Habit')
        book1 = Book.objects.validated_create(author=author, title='The Great White Snail')
        mixin = self.BookMixin(
                request=self.request,
                )
        mixin.ordering = ['id']
        kwargs = mixin.get_formset_kwargs()
        self.assertQuerysetEqual(
                kwargs['queryset'],
                [repr(book1)],
                )

    def test_has_add_permission(self):
        self.user.user_permissions.add(get_perm('tests', 'book', 'add_book'))
        mixin = self.BookMixin(request=self.request)
        self.assertTrue(mixin.has_add_permission())

    def test_has_add_permission_set_opts(self):
        """Passing in different model meta tests permissions against a different model."""
        self.user.user_permissions.add(get_perm('tests', 'author', 'add_author'))
        mixin = self.BookMixin(request=self.request)
        self.assertTrue(mixin.has_add_permission(opts=Author._meta))

    def test_has_add_permission_no_permission(self):
        mixin = self.BookMixin(request=self.request)
        self.assertFalse(mixin.has_add_permission())

    def test_has_change_permission(self):
        self.user.user_permissions.add(get_perm('tests', 'book', 'change_book'))
        mixin = self.BookMixin(request=self.request)
        self.assertTrue(mixin.has_change_permission())

    def test_has_change_permission_set_opts(self):
        """Passing in different model meta tests permissions against a different model."""
        self.user.user_permissions.add(get_perm('tests', 'author', 'change_author'))
        mixin = self.BookMixin(request=self.request)
        self.assertTrue(mixin.has_change_permission(opts=Author._meta))

    def test_has_change_permission_no_permission(self):
        mixin = self.BookMixin(request=self.request)
        self.assertFalse(mixin.has_change_permission())

    def test_has_delete_permission(self):
        """User has delete permissions and deletion enabled self.can_delete is set True."""
        self.assertTrue(self.BookMixin.can_delete)
        self.user.user_permissions.add(get_perm('tests', 'book', 'delete_book'))
        mixin = self.BookMixin(request=self.request)
        self.assertTrue(mixin.has_delete_permission())
        self.assertTrue(mixin.can_delete)

    def test_has_delete_permission_set_opts(self):
        """Passing in different model meta tests permissions against a different model."""
        self.user.user_permissions.add(get_perm('tests', 'author', 'delete_author'))
        mixin = self.BookMixin(request=self.request)
        self.assertTrue(mixin.has_delete_permission(opts=Author._meta))

    def test_has_delete_permission_no_permission(self):
        """
        If user lacks delete permissions and deletion is enabled self.can_delete is set false.
        """
        self.assertTrue(self.BookMixin.can_delete)
        mixin = self.BookMixin(request=self.request)
        self.assertFalse(mixin.has_delete_permission())
        self.assertFalse(mixin.can_delete)

    def test_has_permission(self):
        """If the user has at least one of the required permissions return True."""
        self.user.user_permissions.add(get_perm('tests', 'book', 'add_book'))
        mixin = self.BookMixin(request=self.request)
        self.assertTrue(mixin.has_permission())

    def test_doesnt_have_permission(self):
        """If the user doesn't have any of the required permissions return False."""
        mixin = self.BookMixin(request=self.request)
        self.assertFalse(mixin.has_permission())


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class BaseModelFormSetManagerTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.author = Author.objects.validated_create(name='A Habit')
        cls.book1 = Book.objects.validated_create(author=cls.author, title='The Great White Snail')
        cls.book2 = Book.objects.validated_create(
                author=cls.author,
                title='Poking The Great White Snail'
                )
        cls.book3 = Book.objects.validated_create(
                author=cls.author,
                title='With a Very Pointy Stick'
                )

        class BookManager(BaseModelFormSetManager):
            model = Book
            can_delete = True

        cls.Manager = BookManager

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.factory.get('/')
        self.request.user = self.user

    def test_init_set_attributes(self):
        """__init__() properly sets self.parent_object."""
        manager = self.Manager(
                request=self.request,
                instance=self.author,
                )
        self.assertEqual(manager.parent_object, self.author)

    def test_init_model_attribute_set(self):
        """The class must have a model attribute configured."""
        class BadManager(BaseModelFormSetManager):
            pass

        self.assertRaisesMessage(
                ImproperlyConfigured,
                'BadManager must have a model configured.',
                BadManager
                )

    def test_queryset_ordering(self):
        """
        The default queryset is retrieved and ordering applied if ordering is set. Also tests that
        the queryset is added to the formset kwargs dict.
        """
        manager = self.Manager(
                request=self.request,
                instance=self.author,
                )
        manager.ordering = ['-title']
        kwargs = manager.get_formset_kwargs()
        self.assertTrue(kwargs['queryset'].exists())
        self.assertQuerysetEqual(
                kwargs['queryset'],
                [repr(self.book3), repr(self.book1), repr(self.book2)],
                )

    def test_get_label(self):
        """Returns the model's verbose_name_plural attribute."""
        manager = self.Manager(
                request=self.request,
                instance=self.author,
                )
        self.assertEqual(Book._meta.verbose_name_plural, manager.get_label())

    def test_get_factory_kwargs(self):
        """Formset factory kwargs are correctly set in the kwargs dict."""
        class Manager(BaseModelFormSetManager):
            model = Book
            exclude = 1
            fields = 2
            formfield_callback = 3
            widgets = 4
            localized_fields = 5
            labels = 6
            help_texts = 7
            error_messages = 8
            field_classes = 9
            form_class = 10
            formset_class = 11

        manager = Manager(self.request)
        kwargs_dict = {
            'exclude': 1,
            'fields': 2,
            'formfield_callback': 3,
            'widgets': 4,
            'localized_fields': 5,
            'labels': 6,
            'help_texts': 7,
            'error_messages': 8,
            'field_classes': 9,
            'form': 10,
            'formset': 11,
            }
        factory_kwargs = manager.get_factory_kwargs()
        for key, value in kwargs_dict.items():
            with self.subTest(key=key):
                self.assertEqual(factory_kwargs[key], value)


class CIMSModelFormSetManagerTest(unittest.TestCase):
    def test_permission_checks_before_save(self):
        """
        Permission checks must occur before database operations, check the MRO ensures this
        i.e.InlineFormSetPermissionsMixin is second in the MRO.
        """
        self.assertEqual(
                1,
                CIMSModelFormSetManager.__mro__.index(InlineFormSetPermissionsMixin)
                )


class BaseGenericInlineFormSetManagerTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()

        class AnnotationManager(BaseGenericInlineFormSetManager):
            model = Annotation
            can_delete = True

        cls.Manager = AnnotationManager

    def setUp(self):
        self.request = self.factory.get('/')

    def test_get_factory_kwargs(self):
        """Formset factory kwargs are correctly set in the kwargs dict."""
        manager = self.Manager(self.request)
        factory_kwargs = manager.get_factory_kwargs()
        kwargs_keys = list(factory_kwargs.keys())
        kwargs_keys.sort()
        keys = ['formset', 'ct_field', 'fk_field', 'fields', 'exclude', 'extra',
                'can_order', 'can_delete', 'max_num', 'formfield_callback', 'validate_max',
                'for_concrete_model', 'min_num', 'validate_min']
        keys.sort()
        self.assertListEqual(keys, kwargs_keys)
        for key, value in {'ct_field': 'content_type', 'fk_field': 'object_id',
                           'for_concrete_model': True}.items():
            with self.subTest(key=key):
                self.assertEqual(factory_kwargs[key], value)


@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class CIMSInlineFormsetManagerTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.author = Author.objects.validated_create(name='A Habit')

        class BookManager(CIMSInlineFormSetManager):
            model = Book
            parent_model = Author
            can_delete = True

        cls.Manager = BookManager

    def setUp(self):
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = self.factory.get('/')
        self.request.user = self.user

    def test_get_form_kwargs(self):
        """User is added to kwargs dict."""
        manager = self.Manager(
                request=self.request,
                instance=self.author,
                )
        kwargs = manager.get_form_kwargs()
        self.assertIn('user', kwargs)

    def test_permission_checks_before_save(self):
        """
        Permission checks must occur before database operations, check the MRO ensures this
        i.e.InlineFormSetPermissionsMixin is second in the MRO.
        """
        self.assertEqual(
                1,
                CIMSInlineFormSetManager.__mro__.index(InlineFormSetPermissionsMixin)
                )


# Formset class tests
@override_settings(AUTHENTICATION_BACKENDS=['cims.auth.backends.CIMSBackend'])
class ObjectPermsModelFormSetTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.ToppingForm = modelform_factory(Topping, fields=['name'])
        cls.ToppingFormSet = modelformset_factory(
                Topping,
                form=ValidatedModelForm,
                formset=ObjectPermsModelFormSet,
                fields=['name'],
                )

    def setUp(self):
        self.cheese = Topping.objects.validated_create(name='Cheese')
        self.user = User.objects.create_user(username='user', password='sekret')
        self.request = Mock()
        self.request.user = self.user

    def test_init_requires_request(self):
        """Raise an exception if a request is not passed as a parameter."""
        self.assertRaisesMessage(
                AssertionError,
                'ToppingFormFormSet requires a not-None request parameter.',
                self.ToppingFormSet
                )

    def test_save_new_success(self):
        """
        Saving an new form succeeds if the user has add permissions for that object.
        """
        formset = self.ToppingFormSet(request=self.request)
        perm = get_perm('tests', 'topping', 'add_topping')
        self.user.user_permissions.add(perm)
        data = {'name': 'mushrooms'}
        form = self.ToppingForm(data=data)
        form.is_valid()
        mushrooms = formset.save_new(form)
        self.assertEqual(data['name'], mushrooms.name)

    @patch('django.contrib.messages.warning')
    def test_save_new_fails(self, msgs):
        """
        Saving a new form fails if the user does not have add permissions for that object.
        """
        formset = self.ToppingFormSet(request=self.request)
        data = {'name': 'mushrooms'}
        form = self.ToppingForm(data=data)
        form.is_valid()
        formset.save_new(form)
        msgs.assert_called_once_with(
                self.request,
                'New {} were not created as you lack permission to create new entries.'.format(
                        Topping._meta.verbose_name_plural
                        )
                )

    def test_save_existing_success(self):
        """
        Saving an existing form succeeds if the user has change permissions for that object.
        """
        formset = self.ToppingFormSet(request=self.request)
        perm = get_perm('tests', 'topping', 'change_topping')
        UserObjectPermission.objects.create(
                user=self.user,
                permission=perm,
                content_type=ContentType.objects.get_for_model(Topping),
                object_id=self.cheese.pk
                )
        data = {'name': 'mushrooms'}
        form = self.ToppingForm(data=data, instance=self.cheese)
        form.is_valid()
        cheese = formset.save_existing(form, self.cheese)
        self.assertEqual(data['name'], cheese.name)

    @patch('django.contrib.messages.warning')
    def test_save_existing_fails(self, msgs):
        """
        Saving an existing form fails if the user does not have change permissions for that object.
        """
        formset = self.ToppingFormSet(request=self.request)
        data = {'name': 'mushrooms'}
        form = self.ToppingForm(data=data, instance=self.cheese)
        form.is_valid()
        cheese = formset.save_existing(form, self.cheese)
        self.assertEqual(self.cheese.name, cheese.name)
        msgs.assert_called_once_with(
                self.request,
                '{} was not updated as you lack permission.'.format(cheese)
                )

    def test_delete_existing_success(self):
        """
        Deleting an existing object succeeds if the user has delete permissions for that object.
        """
        formset = self.ToppingFormSet(request=self.request)
        perm = get_perm('tests', 'topping', 'delete_topping')
        UserObjectPermission.objects.create(
                user=self.user,
                permission=perm,
                content_type=ContentType.objects.get_for_model(Topping),
                object_id=self.cheese.pk
                )
        formset.delete_existing(self.cheese)
        self.assertFalse(Topping.objects.filter(pk=self.cheese.pk).exists())

    @patch('django.contrib.messages.warning')
    def test_delete_existing_fails(self, msgs):
        """
        Deleting an existing object fails if the user does not have delete permissions for that
        object.
        """
        formset = self.ToppingFormSet(request=self.request)
        formset.delete_existing(self.cheese)
        self.assertTrue(Topping.objects.filter(pk=self.cheese.pk).exists())
        msgs.assert_called_once_with(
                self.request,
                '{} was not deleted as you lack permission.'.format(self.cheese)
                )


class CIMSBaseInlineFormSetTest(unittest.TestCase):
    def test_save_new_mro(self):
        """
        Correct functioning of our extended validation requires the use of ValidatedModelForm. If
        class inheritance for CIMSBaseInlineFormSet isn't structured properly then
        BaseInlineFormSet.save_new() could be called before M2MValidatingModelFormSet.save_new(),
        which will cause an assertion in ValidatedModelForm.save() to fail.
        ."""
        self.assertTrue(
                CIMSBaseInlineFormSet.__mro__.index(M2MValidatingInlineFormSet)
                < CIMSBaseInlineFormSet.__mro__.index(BaseInlineFormSet)
                )


# Concrete formset manager tests
class CVInlineWeightedLOToSOManagerTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Objective relationships
        cls.std_body = models.StandardsBody.objects.validated_create(
                name='Engineers Australia',
                short_name='EA',
                )
        cls.std_fwork1 = models.StandardsFramework.objects.validated_create(
                standardsbody=cls.std_body,
                name='Stage 1',
                )
        cls.node1 = models.StandardObjective.objects.validated_create(
                standardsframework=cls.std_fwork1,
                name='Node 1',
                short_name='N1',
                )
        # Course objects
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                standards_frameworks=[cls.std_fwork1],
                )
        cls.cv1.objectives.add(cls.node1)
        cv2 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version 2',
                status=status,
                standards_frameworks=[cls.std_fwork1],
                )
        cv2.objectives.add(cls.node1)
        cls.lo1 = models.LearningOutcome.objects.validated_create(
                courseversion=cls.cv1,
                outcome='Something good.',
                )
        cls.lo2 = models.LearningOutcome.objects.validated_create(
                courseversion=cv2,
                outcome='Is gonna happen.',
                )
        cls.weighted_mapping1 = models.WeightedLOToSO.objects.validated_create(
                learningoutcome=cls.lo1,
                standardobjective=cls.node1
                )
        cls.weighted_mapping2 = models.WeightedLOToSO.objects.validated_create(
                learningoutcome=cls.lo2,
                standardobjective=cls.node1
                )

    def test_get_queryset(self):
        """Queryset is filtered of objects not related to the parent object"""
        request = RequestFactory().get('/')
        request.user = User.objects.create_user(username='user', password='sekret')
        request.user.user_permissions.add(
                get_perm('cims', 'weightedlotoso', 'change_weightedlotoso')
                )
        manager = CVInlineWeightedLOToSOManager(request=request, instance=self.cv1)
        self.assertQuerysetEqual(manager.get_queryset(), [repr(self.weighted_mapping1)])


class CIInlineWeightedTaskToLOTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Course objects
        school = models.OrganisationalUnit.objects.validated_create(
                name='Foo School',
                short_name='FS'
                )
        course = models.Course.objects.validated_create(code='Test1234')
        status = models.ReviewStatus.objects.validated_create(name='TestStatus')
        cls.cv1 = models.CourseVersion.objects.validated_create(
                course=course,
                version_descriptor='Test course version',
                status=status,
                )
        cls.lo1 = models.LearningOutcome.objects.validated_create(
                courseversion=cls.cv1,
                outcome='Something good.',
                )
        cls.ci = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Test instance',
                start_date=date.today(),
                end_date=date.today(),
                taught_by=[school],
                )
        ci2 = models.CourseInstance.objects.validated_create(
                courseversion=cls.cv1,
                instance_descriptor='Another test instance',
                start_date=date.today(),
                end_date=date.today(),
                taught_by=[school],
                )
        cls.task = models.Task.objects.validated_create(
                courseinstance=cls.ci,
                title='The correct task.'
                )
        task2 = models.Task.objects.validated_create(
                courseinstance=ci2,
                title='The incorrect task.'
                )
        cls.weighted_mapping1 = models.WeightedTaskToLO.objects.validated_create(
                task=cls.task,
                learningoutcome=cls.lo1,
                )
        cls.weighted_mapping2 = models.WeightedTaskToLO.objects.validated_create(
                task=task2,
                learningoutcome=cls.lo1,
                )

    def test_get_queryset(self):
        """Queryset is filtered of objects not related to the parent object"""
        request = RequestFactory().get('/')
        request.user = User.objects.create_user(username='user', password='sekret')
        request.user.user_permissions.add(
                get_perm('cims', 'weightedtasktolo', 'change_weightedtasktolo')
                )
        manager = CIInlineWeightedTaskToLOManager(request=request, instance=self.ci)
        self.assertQuerysetEqual(manager.get_queryset(), [repr(self.weighted_mapping1)])
