"""cims_dev URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from . import views
# from django.contrib import admin


tests_pattens = ([
        url(r'^author/add/$', views.AuthorCreate.as_view(),
            name='author.create'),
        url(r'^author/(?P<pk>[0-9]+)/$', views.AuthorDetail.as_view(),
            name='author.detail'),
        url(r'^author/(?P<pk>[0-9]+)/edit/$', views.AuthorUpdate.as_view(),
            name='author.update'),
        url(r'^blank/', views.blank),
        ],
        'tests'
        )

urlpatterns = [
        url(r'^', include(tests_pattens)),
        url(r'^testapi/', include('tests.api.urls')),
        url(r'^pandcscraper/', include('cims.contrib.pandcscraper.urls')),
        url(r'^api/', include('cims.api.urls')),
        # url(r'^', include('cims.reg_urls')),
        url(r'^', include('django.contrib.auth.urls')),
        url(r'^', include('cims.urls')),
        # url(r'^admin/', admin.site.urls),
        ]
