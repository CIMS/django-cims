import os
from setuptools import find_packages, setup


# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__),
    os.pardir)))
    
setup(
        name='django-cims',
        version='0.2.1',
        packages=find_packages(exclude=['tests*', 'scripts*']),
        include_package_data=True,
        license='Apache-2.0',
        description='University College course information management system.',
        url='https://gitlab.anu.edu.au/CIMS/django-cims',
        author='Lincoln Smith',
        author_email='lincoln.smith@anu.edu.au',
        classifiers=[
            'Programming Language :: Python',
            'Programming Language :: Python :: 3 :: Only',
            'License :: OSI Approved :: Apache Software License',
            'Operating System :: OS Independent',
            'Development Status :: 3 - Alpha',
            'Environment :: Web Environment',
            'Framework :: Django :: 1.11',
            'Framework :: Django :: 2.0',
            'Framework :: Django :: 2.1',
            'Intended Audience :: Developers',
            'Topic :: Internet :: WWW/HTTP',
            'Topic :: Software Development :: Libraries :: Python Modules',
            ],
        install_requires=['django>=1.11'],
        python_requires='~=3.4',
        long_description="""\
""",
        )

