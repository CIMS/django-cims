import logging, time

class RequestLoggerMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        start_time = time.perf_counter()
        response = self.get_response(request)
        logger = logging.getLogger('cims.request')
        try:
            remote_addr = request.META.get('HTTP_X_FORWARDED_FOR') or request.META.get('REMOTE_ADDR')
            uname = '-'
            if hasattr(request, 'user'):
                uname = request.user.username
            elapsed = (time.perf_counter() - start_time) * 1000
            content_len = len(response.content)
            logger.info('{} {} {} {} {} {} {:.2f}ms'.format(
                    remote_addr,
                    uname,
                    request.method,
                    request.get_full_path(),
                    response.status_code,
                    content_len,
                    elapsed,
                    ))
        except Exception as e:
            logger.error('RequestLoggerMiddleware error: {}'.format(e))

        return response
