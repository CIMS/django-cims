from django.contrib.sites.shortcuts import get_current_site


def site(request):
    """
    Add a site object to the request context. Useful for obtaining e.g. the site root.
    """
    site = get_current_site(request)
    protocol = 'https' if request.is_secure() else 'http'

    return {
            'site': site,
            'site_root': '{}://{}'.format(protocol, site.domain),
            }
