from django.apps import AppConfig


class CimsConfig(AppConfig):
    name = 'cims'

    def ready(self):
        from cims import signals
        super().ready()
