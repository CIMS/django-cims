"""Crispy Forms FormHelper classes"""
from crispy_forms.helper import FormHelper


# Crispy Forms FormHelper classes
class BaseFormHelper(FormHelper):
    """
    Since we often have multiple forms on a page by default disable rendering of form tags and the
    CSRF block. Disable HTML5 required attributes by default as they don't play nicely in dynamic
    formsets.
    """
    form_tag = False
    disable_csrf = True
    html5_required = False


class DefaultFormHelper(BaseFormHelper):
    """
    The default formhelper for rendering the form for the core object of the
    view, so make it render the CSRF block by default so we don't forget it. Turn on HTML5 required
    attributes for this part of the form.
    """
    disable_csrf = False
    html5_required = True


class HorizontalFormHelper(DefaultFormHelper):
    """The default formhelper to use if you need a horizontal form i.e. labels beside inputs."""
    form_class = 'form-horizontal'
    label_class = 'col-2'
    field_class = 'col-6'


class ListgroupFormsetHelper(BaseFormHelper):
    """Formset that lays out forms in a bootstrap list group."""
    template = 'bootstrap4/listgroup_formset.html'


class ListgroupFormHelper(BaseFormHelper):
    """FormHelper that renders a form inside a list group element."""
    template = 'bootstrap4/listgroup_form.html'


class TableInlineFormSetHelper(BaseFormHelper):
    """Formset that lays out forms in a table."""
    template = 'bootstrap4/table_inline_formset.html'


class TableInlineCollapseFormSetHelper(BaseFormHelper):
    """Formset that lays out forms as collapsible regions in a table."""
    template = 'bootstrap4/table_inline_collapse_formset.html'


class TableInlineModalFormSetHelper(BaseFormHelper):
    """Formset that lays out forms without the View/Edit prefix column."""
    template = 'bootstrap4/table_inline_modal_formset.html'


class TableInlineFormHelper(BaseFormHelper):
    """FormHelper that renders a form inside a table row."""
    template = 'bootstrap4/table_inline_form.html'
