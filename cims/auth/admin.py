from django.contrib import admin
from django.contrib.auth.models import Group

from cims.auth.maps import run_mappers
from cims.auth.models import ConditionalPermissionsMap
from cims.auth.utils import check_user_obj_permissions, get_perm_with_pfps


def admin_check_obj_permissions(user_obj, app_label, codename):
    """
    Return True if `user_obj` has any `Permission` or `PerFieldPermission` matching
    /`app_label`.`codename`*/ on any object.
    """
    permissions = get_perm_with_pfps(app_label, codename)
    # Standard object permissions lookup
    obj_filters = {
            'user': {'permission__in': permissions},
            'role': {'permission__in': permissions},
            }
    if check_user_obj_permissions(user_obj, obj_filters):
        return True

    # Conditional maps lookup
    mappers = ConditionalPermissionsMap.objects.filter(
            conditional_permissions__in=permissions,
            )
    return run_mappers(mappers, user_obj)


# Admin classes
admin.site.unregister(Group)
