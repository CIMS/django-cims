from django.apps import AppConfig


class CIMSAuthConfig(AppConfig):
    name = 'cims.auth'
    label = 'cims_auth'

    def ready(self):
        import cims.auth.signals
        super().ready()
