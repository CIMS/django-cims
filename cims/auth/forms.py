"""
CIMS Auth forms.
"""
from django.forms import ChoiceField, ModelForm, ModelMultipleChoiceField
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

from cims.auth.maps import mapper_tests
from cims.auth.models import ConditionalPermissionsMap, ObjectStatusPermissionsMap, User, Role


class CIMSUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User


class CIMSUserChangeForm(UserChangeForm):
    """
    Customised UserChangeForm

    Additional Fields
    -----------------
    roles : cims.auth.models.Role
            A reverse relation from Role due to original user model config. Added here to make
            management a bit easier.
    """
    roles = ModelMultipleChoiceField(
            Role.objects.all(),
            widget=FilteredSelectMultiple('Roles', False),
            required=False,
            )

    class Meta(UserChangeForm.Meta):
        model = User

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.initial['roles'] = self.instance.roles.all()

    def save(self, *args, **kwargs):
        instance = super().save(*args, **kwargs)
        if instance.pk:
            instance.roles.set(self.cleaned_data.get('roles'))
        return instance


def _create_test_func_list():
    func_list = []
    for func_name in mapper_tests:
        func_list.append((func_name, func_name))
    return func_list


class ConditionalPermissionsMapForm(ModelForm):
    """Extension to ModelForm to force the use of a select input for a CharField."""
    test = ChoiceField(
            choices=_create_test_func_list(),
            help_text='This test will be run against the check model above.',
            )
    app_label_filter = ('cims',)

    class Meta:
        model = ConditionalPermissionsMap
        fields = ('description', 'test_model', 'test', 'conditional_permissions')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.app_label_filter is not None:
            self.fields['test_model'].queryset = ContentType.objects.filter(
                    app_label__in=self.app_label_filter,
                    )
            self.fields['conditional_permissions'].queryset = Permission.objects.filter(
                    content_type__app_label__in=self.app_label_filter,
                    )


class ObjectStatusPermissionsMapForm(ModelForm):
    """
    Extension to ModelForm to allow customisation of which models are included in select boxes
    based on app_label.
    """
    app_label_filter = ('cims',)

    class Meta:
        model = ObjectStatusPermissionsMap
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.app_label_filter is not None:
            self.fields['check_content_type'].queryset = ContentType.objects.filter(
                    app_label__in=self.app_label_filter,
                    )
            self.fields['denied_permissions'].queryset = Permission.objects.filter(
                    content_type__app_label__in=self.app_label_filter,
                    )
            self.fields['related_content_types'].queryset = ContentType.objects.filter(
                    app_label__in=self.app_label_filter,
                    )
