import logging
from django.contrib.auth.signals import user_logged_in, user_login_failed
from django.dispatch import receiver

auth_logger = logging.getLogger('cims.auth')

@receiver(user_logged_in)
def log_login_success(sender, request, user, **kwargs):
    auth_logger.info('{} LOGIN SUCCESS'.format(user.username))

@receiver(user_login_failed)
def log_login_failure(sender, credentials, **kwargs):
    auth_logger.warn('{} LOGIN FAILURE'.format(credentials['username']))

