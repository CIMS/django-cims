"""
"""
from cims.models import CourseInstance, CourseVersion


class AlreadyRegistered(Exception):
    pass


# Dict of registered mapper test functions
mapper_tests = {}


def run_mappers(mappers, user_obj, test_obj=None, **kwargs):
        """
        Run tests from an iterable of ConditionalPermissionsMap objects, return True if a test
        passes.
        """
        for mapper in mappers:
            if mapper.perform_test(user_obj, test_obj, **kwargs):
                return True
        return False


def register(name):
    """
    Add a test function to a dict of mapper test functions.

    Parameters
    ----------
    name : str
           Description of what the test function checks.
    """
    def _register_wrapper(test_func):
        """

        :param test_func: Function that performs the actual test.
        :return:
        """
        if name in mapper_tests:
            raise AlreadyRegistered('Mapper test function name {} already '
                                    'registered.'.format(name))

        mapper_tests.update({name: test_func})
    return _register_wrapper


# Test functions
@register('Is class convener')
def is_class_convener(user, test_obj=None, **kwargs):
    """Returns if the `user` is in the set of conveners of the `check_obj`."""
    if test_obj is None:
        # Checking if the user is convener of at least one class
        return CourseInstance.objects.filter(conveners=user).exists()

    # Checking user is convener of this class
    assert isinstance(test_obj, CourseInstance), ('Attempting to check class convener on an '
                                                  'object that isn\'t a CourseInstance.')
    return test_obj.conveners.filter(pk=user.pk).exists()


@register('Is course custodian')
def is_course_custodian(user, test_obj=None, **kwargs):
    """Returns if the `user` is in the set of custodians of the `check_obj`."""
    if test_obj is None:
        # Checking if the user is convener of at least one class
        return CourseVersion.objects.filter(custodians=user).exists()

    # Checking user is convener of this class
    assert isinstance(test_obj, CourseVersion), ('Attempting to check course custodian on an '
                                                 'object that isn\'t a CourseVersion.')
    return test_obj.custodians.filter(pk=user.pk).exists()
