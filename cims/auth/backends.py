"""
Backends for CIMS authentication/authorisation.

Some notes about how permissions are applied in Django when object permissions are enabled:
    - Permissions can be allocated at the model or object level.
    - Allocation at the model level implies the permission applies to all instances of that model.
    - The interface for checking permissions is via the has_perm() method of a backend.
    - When object permissions are enabled, checking a permission with obj=None means you're asking
      "Does this user have this permission on AT LEAST ONE OBJECT." It is typically used as an
      early check to avoid expensive further processing.
    - Checking a permission with obj=<something not None> means you're asking "Does this user have
      this permission for this object specifically." Note that the user may have that permission as
      a result of allocation at the model or object level.
"""
from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from perfieldperms.models import PerFieldPermission
from perfieldperms.utils import cache_to_set, make_model_perm_sets, make_pfp_perm_sets, merge_caches

from cims.auth.maps import run_mappers
from cims.auth.models import (ConditionalPermissionsMap, ObjectStatusPermissionsMap, Role,
                              RoleObjectPermission)
from cims.auth.utils import check_user_obj_permissions, get_perm_str, get_perm_with_pfps


class CIMSBackend(object):
    """
    Provide permission checking hooks for PerFieldPermissions.
    """
    mbackend = ModelBackend()

    def authenticate(self, request, username=None, password=None, **kwargs):
        """Just use standard ModelBackend for now."""
        return self.mbackend.authenticate(request, username=username, password=password, **kwargs)

    def get_user(self, user_id):
        """Just use standard ModelBackend for now."""
        return self.mbackend.get_user(user_id)

    @staticmethod
    def _perm_filter(**filters):
        """
        Wrapper around `Permission` manager to get queryset of specific fields according to
        `filters`.
        """
        perms = Permission.objects.filter(**filters).values_list(
                'content_type__app_label',
                'codename',
                )
        return perms

    @staticmethod
    def _pfp_filter(**filters):
        """ Wrapper around `PerFieldPermission` manager to get queryset of specific
        fields according to `filters`.
        """
        pfps = PerFieldPermission.objects.filter(**filters).values_list(
                'content_type__app_label',
                'codename',
                'model_permission__codename',
                )
        return pfps

    def _get_perm_set(self, perm_filters, pfp_filters, flip=False):
        """
        Get `Permission` and `PerFieldPermission` querysets from given filters `perm_filters` and
        `pfp_filters` respectively, and assemble into structure of permissions suitable for storing
        in the cache.
        """
        # Set model level perms first, means we can potentially ignore some field level perms later
        perms = self._perm_filter(**perm_filters)
        pfps = self._pfp_filter(**pfp_filters)
        if flip:
            perm_sets = make_pfp_perm_sets(pfps)
            perm_sets = make_model_perm_sets(perms, perm_sets)
        else:
            perm_sets = make_model_perm_sets(perms)
            perm_sets = make_pfp_perm_sets(pfps, perm_sets)
        return perm_sets

    def _get_user_obj_permissions(self, user_obj, obj, obj_ctype):
        user_obj_perms = user_obj.objectpermissions.filter(
                object_id=obj.id,
                content_type=obj_ctype,
                )
        perm_filters = {
                'perfieldpermission__isnull': True,
                'userobjectpermission__in': user_obj_perms,
                }
        pfp_filters = {'userobjectpermission__in': user_obj_perms}
        return self._get_perm_set(perm_filters, pfp_filters)

    def _get_user_permissions(self, user_obj):
        perm_filters = {
                'perfieldpermission__isnull': True,
                'user': user_obj,
                }
        pfp_filters = {'user': user_obj}
        return self._get_perm_set(perm_filters, pfp_filters)

    def _get_role_permissions(self, user_obj, obj=None, obj_ctype=None):
        perms = dict()
        roles = Role.objects.get_role_hierarchy_for_user(user_obj)
        # Algorithm for role permission resolution is:
        # - Get list of sets of roles where each set contains roles at equal
        #   distance from the user.
        # - Process list in decreasing distance order
        # - Per-field permissions are additive within a set of roles
        # - A model level permission in a set overrides any equivalent field
        #   level perms
        # - Per-field permissions are additive between adjacent sets
        # - A closer resultant set containing per field permissions will
        #   override a more distant set comprising only a model permission
        #
        # Going to pop the list so copy to avoid borking cache.
        roles = roles.copy()
        while roles:
            role_set = roles.pop()
            role_ids = [role.id for role in role_set]
            if obj is not None:
                role_obj_perms = RoleObjectPermission.objects.filter(
                        object_id=obj.id,
                        content_type=obj_ctype,
                        role__in=role_ids,
                        )
                perm_filters = {
                        'perfieldpermission__isnull': True,
                        'roleobjectpermission__in': role_obj_perms,
                        }
                pfp_filters = {'roleobjectpermission__in': role_obj_perms}
            else:
                perm_filters = {
                        'perfieldpermission__isnull': True,
                        'role__in': role_ids,
                        }
                pfp_filters = {'role__in': role_ids}

            perm_sets = self._get_perm_set(perm_filters, pfp_filters, flip=True)
            perms = merge_caches(perm_sets, perms)
        return perms

    _get_role_obj_permissions = _get_role_permissions

    def _get_permissions(self, user_obj, obj, from_name):
        """
        Lookup role or user permissions for `user_obj` depending on the
        contents of `from_name`. `from_name` can be "user" or "role", and
        calls `_get_user_permissions` and `_get_user_obj_permissions`,
        `_get_role_permissions` and `_get_role_obj_permissions`
        respectively.
        """
        if not user_obj.is_active or user_obj.is_anonymous:
            return set()

        # Lookup non-object perms, need them regardless
        cache_name = '_cims_{}_perm_cache'.format(from_name)
        if not hasattr(user_obj, cache_name):
            if user_obj.is_superuser:
                perms = Permission.objects.filter(
                        perfieldpermission__isnull=True,
                        ).values_list('content_type__app_label', 'codename')
                perm_sets = make_model_perm_sets(perms)
            else:
                perm_sets = getattr(self, '_get_{}_permissions'.format(from_name))(user_obj)

            setattr(user_obj, cache_name, perm_sets)

        # No need to look up obj perms for superuser, they get them all from
        # model level.
        # Obj perm cache has structure:
        # { obj_ctype.id:
        #       { obj.id:
        #           { model_perm: set(pfp1, pfp2)}
        #       }
        # }
        if (obj is not None and obj.pk is not None) and not user_obj.is_superuser:
            obj_ctype = ContentType.objects.get_for_model(obj)
            obj_cache_name = '_cims_{}_obj_perm_cache'.format(from_name)
            obj_cache = getattr(user_obj, obj_cache_name, dict())
            if obj_ctype.id not in obj_cache:
                obj_cache[obj_ctype.id] = dict()
            if obj.id not in obj_cache[obj_ctype.id]:
                perm_sets = getattr(
                        self,
                        '_get_{}_obj_permissions'.format(from_name))(user_obj, obj, obj_ctype)
                obj_cache[obj_ctype.id][obj.id] = perm_sets
            # If perms for this obj exist return, otherwise fall through to
            # model level.
            setattr(user_obj, obj_cache_name, obj_cache)
            if obj_cache[obj_ctype.id][obj.id]:
                return cache_to_set(obj_cache[obj_ctype.id][obj.id])

        return cache_to_set(getattr(user_obj, cache_name))

    def get_user_permissions(self, user_obj, obj=None):
        """Get permissions granted only to this user."""
        return self._get_permissions(user_obj, obj, 'user')

    def get_group_permissions(self, user_obj, obj=None):
        """Get permissions granted to roles this user is a member of."""
        return self._get_permissions(user_obj, obj, 'role')

    get_role_permissions = get_group_permissions

    def get_all_permissions(self, user_obj, obj=None):
        """Get all user and role based permissions for this user. Creates a
        cache of permissions for life of user object.
        """
        if not user_obj.is_active or user_obj.is_anonymous:
            return set()

        if not hasattr(user_obj, '_cims_perm_cache'):
            self.get_user_permissions(user_obj, obj)
            self.get_role_permissions(user_obj, obj)
            # Copy the group perm cache, then update it from the user cache
            # effectively overriding those settings
            user_obj._cims_perm_cache = user_obj._cims_role_perm_cache.copy()
            user_obj._cims_perm_cache.update(user_obj._cims_user_perm_cache)
            # The below will merge caches instead of overwriting
            # user_obj._pfp_perm_cache = merge_caches(
            #        user_obj._pfp_user_perm_cache,
            #        user_obj._pfp_perm_cache,
            #        )

        if obj is not None and obj.pk is not None:
            obj_ctype = ContentType.objects.get_for_model(obj)
            obj_cache = getattr(user_obj, '_cims_obj_perm_cache', None)
            if obj_cache is None:
                obj_cache = user_obj._cims_obj_perm_cache = dict()
            if obj_ctype.id not in obj_cache:
                obj_cache[obj_ctype.id] = dict()
            if obj.id not in obj_cache[obj_ctype.id]:
                # Get perms for this object and insert them in the cache
                self.get_user_permissions(user_obj, obj)
                self.get_role_permissions(user_obj, obj)
                role_obj_perms = user_obj._cims_role_obj_perm_cache[obj_ctype.id][obj.id].copy()
                obj_cache[obj_ctype.id][obj.id] = role_obj_perms
                user_obj_perms = user_obj._cims_user_obj_perm_cache[obj_ctype.id][obj.id]
                obj_cache[obj_ctype.id][obj.id].update(user_obj_perms)

            # If actually have perms applicable to this object merge them with
            # model permissions to get effective permissions else fall through.
            if obj_cache[obj_ctype.id][obj.id]:
                mcache = user_obj._cims_perm_cache.copy()
                mcache.update(obj_cache[obj_ctype.id][obj.id])
                return cache_to_set(mcache)

        return cache_to_set(user_obj._cims_perm_cache)

    def has_perm(self, user_obj, perm, obj=None):
        """
        Returns true if `user_obj` has `perm` in their set of all permissions.
        """
        if not user_obj.is_active or user_obj.is_anonymous:
            return False
        if not hasattr(user_obj, '_cims_perm_cache'):
            self.get_all_permissions(user_obj, obj)

        # Looking up permissions for a specific object
        if obj is not None and obj.pk is not None:
            obj_ctype = ContentType.objects.get_for_model(obj)
            obj_cache = getattr(user_obj, '_cims_obj_perm_cache', None)
            if (obj_cache is None
                    or obj_ctype.id not in obj_cache
                    or obj.id not in obj_cache[obj_ctype.id]):
                self.get_all_permissions(user_obj, obj)
            # Merge model and object permissions to get effective perms
            cache = user_obj._cims_perm_cache.copy()
            cache.update(user_obj._cims_obj_perm_cache[obj_ctype.id][obj.id])
        else:
            cache = user_obj._cims_perm_cache

        # Looking for model level perm, no need to go deeper
        if perm in cache:
            return True
        perm_key = perm.rsplit('__', maxsplit=1)[0]
        # A user has a field level permission if the model level parent is set
        # and no fields are set, or the field permission is set.
        if perm_key in cache and (not cache[perm_key] or perm in cache[perm_key]):
            return True

        # If we got to here and obj==None then we're checking if the user has this permission on at
        # least one object, and evidently they haven't been allocated it at the model level or they
        # would have passed the checks above. Check if they have it via any object level
        # permissions.
        if obj is None:
            app_label, codename = perm.split('.', maxsplit=1)
            # Get this permission and all it's potential per-field permissions in a queryset, if a
            # user can e.g. change one field on one object they have change permissions
            permission = Permission.objects.get(
                    codename=codename,
                    content_type__app_label=app_label,
                    )
            permissions = Permission.objects.filter(
                    Q(id=permission.id) | Q(perfieldpermission__model_permission=permission)
                    )
            obj_filters = {
                    'user': {'permission__in': permissions},
                    'role': {'permission__in': permissions},
                    }
            if check_user_obj_permissions(user_obj, obj_filters):
                return True

        return False

    def has_module_perms(self, user_obj, app_label):
        """
        Returns True if `user_obj` has any permissions in the given
        `app_label`. Since this is largely used by Admin site knock back
        anonymous users straight away.
        """
        if not user_obj.is_active or user_obj.is_anonymous:
            return False
        if not hasattr(user_obj, '_cims_perm_cache'):
            self.get_all_permissions(user_obj)
        for perm in user_obj._cims_perm_cache.keys():
            if perm[:perm.index('.')] == app_label:
                return True

        # Does user have any object permissions in app_label
        filters = {
                'user': {'content_type__app_label': app_label},
                'role': {'content_type__app_label': app_label},
                }
        return check_user_obj_permissions(user_obj, filters)

    def get_pfps(self, user_obj, permission, model):
        """
        Returns the set of per-field permissions owned by `user_obj` matching `permission` and
        model/object `model` in "app_label.codename" form, or None if the user lacks any
        permissions applying to the model/object.
        """
        if not user_obj.is_active:
            return None

        if isinstance(permission, PerFieldPermission):
            permission = permission.model_permission
        content_type = ContentType.objects.get_for_model(model)
        perm_key = '{}.{}'.format(content_type.app_label, permission.codename)
        # Look for object level PFPs
        if not isinstance(model, type) and model.pk is not None:
            obj = model
            # all_perms = None
            obj_cache = getattr(user_obj, '_cims_obj_perm_cache', None)
            if (obj_cache is None
                    or content_type.id not in obj_cache
                    or obj.id not in obj_cache[content_type.id]):
                all_perms = self.get_all_permissions(user_obj, obj)
            # Check for empty sets/None to catch anonymous users
            if obj_cache or all_perms:
                obj_cache = user_obj._cims_obj_perm_cache
                if perm_key in obj_cache[content_type.id][obj.id]:
                    return obj_cache[content_type.id][obj.id][perm_key]

        # Only looking at model level PFPs or there were no applicable object perms
        # Catch anonymous users
        if not hasattr(user_obj, '_cims_perm_cache'):
            if not self.get_all_permissions(user_obj):
                return None
        cache = user_obj._cims_perm_cache
        return cache.get(perm_key, None)

    def has_pfps(self, user_obj, permission, model):
        """
        Returns if the user has any perfield permissions applicable to the given
        permission and model/object.

        NOTE: this method will return False if the user has
        unrestricted access to the model/object for this permission
        """
        return bool(self.get_pfps(user_obj, permission, model))


class ConditionalPermissionsMapBackend(object):
    """
    Backend that checks if a user has additional permissions based on passing a
    ConditionalPermissionsMap test.
    """
    def __init__(self, *args, **kwargs):
        backends = settings.AUTHENTICATION_BACKENDS
        cimsbackend = 'cims.auth.backends.CIMSBackend'
        thisbackend = 'cims.auth.backends.ConditionalPermissionsMapBackend'

        # Ensure we're properly configured
        assert (cimsbackend in backends
                and backends.index(cimsbackend) < backends.index(thisbackend)), (
                        'ConditionalPermissionsMapBackend requires {} be configured before it in '
                        'django.settings.AUTHENTICATION_BACKENDS.'.format(cimsbackend))

    def authenticate(self, username=None, password=None, **kwargs):
        """We don't do this."""
        return None

    def get_user(self, user_id):
        """We don't do this."""
        return None

    def get_all_permissions(self, user_obj, obj=None):
        """
        Get `Permissions` for this `obj` allocated to this `user_obj` via
        `ConditionalPermissionsMap`.
        """
        # PermissionMappers only operate on saved objects. If user is superuser CIMSBackend will
        # have handled everything so bail now.
        if (not user_obj.is_active or user_obj.is_anonymous or user_obj.is_superuser
                or obj is None or obj.id is None):
            return set()

        # Check if we've already cached permissions for this object, retrieve and add to cache if
        # not
        obj_ctype = ContentType.objects.get_for_model(obj)
        conditional_cache = getattr(user_obj, '_cims_conditional_cache', dict())
        if obj_ctype.id not in conditional_cache:
            conditional_cache[obj_ctype.id] = dict()
        if obj.id not in conditional_cache[obj_ctype.id]:
            # Get maps that apply to this object
            perm_maps = ConditionalPermissionsMap.objects.filter(
                    conditional_permissions__content_type=obj_ctype
                    ).select_related('test_model')
            # Get resultant permissions from mappers. Because all mappers are 'equal' don't
            # overwrite model permissions with field permissions
            effective_perms = dict()
            for mapper in perm_maps:
                test_obj = None
                # Get the object we're going perform the test on
                if mapper.test_model == obj_ctype:
                    test_obj = obj
                else:
                    TestClass = mapper.test_model.model_class()
                    get_method = getattr(
                            TestClass.objects,
                            'get_{}_for_object'.format(mapper.test_model.model),
                            None,
                            )
                    if get_method is not None:
                        test_obj = get_method(obj)

                # If the test passes the user gets the permissions
                if test_obj is not None and mapper.perform_test(user_obj, test_obj):
                    filtered_permissions = mapper.conditional_permissions.filter(
                            content_type=obj_ctype,
                            )
                    perms = filtered_permissions.filter(
                            perfieldpermission__isnull=True,
                            ).values_list('content_type__app_label', 'codename')
                    pfps = PerFieldPermission.objects.filter(
                            content_type=obj_ctype,
                            id__in=filtered_permissions,
                            ).values_list(
                                    'content_type__app_label',
                                    'codename',
                                    'model_permission__codename',
                                    )
                    # Put permissions into a cache structure
                    perm_sets = make_model_perm_sets(perms)
                    perm_sets = make_pfp_perm_sets(pfps, perm_sets=perm_sets)
                    effective_perms = merge_caches(
                            perm_sets,
                            effective_perms,
                            overwrite_empty=False
                            )

            conditional_cache[obj_ctype.id][obj.id] = effective_perms
            setattr(user_obj, '_cims_conditional_cache', conditional_cache)

        return cache_to_set(conditional_cache[obj_ctype.id][obj.id])

    def has_perm(self, user_obj, perm, obj=None):
        """
        Return True if `user_obj` has `Permission` `perm` for `obj` granted by a
        `ConditionalPermissionsMap`.
        """
        # PermissionMappers only operate on saved objects
        if not user_obj.is_active or (obj is not None and obj.id is None):
            return False

        assert not user_obj.is_superuser, ('ConditionalPermissionsMapBackend.has_perm() got an '
                                           'unexpected superuser.')

        # If obj==None then we're checking if the user has this permission via a
        # ConditionalPermissionsMap on at least one object
        if obj is None:
            # Get this permission and all its potential per-field permissions in a queryset, if a
            # user can e.g. change one field on one object they have change permissions
            app_label, codename = perm.split('.', maxsplit=1)
            permissions = get_perm_with_pfps(app_label, codename)
            # Get any ConditionalPermissionsMaps that grant permissions from the set we're
            # interested in
            mappers = ConditionalPermissionsMap.objects.filter(
                    conditional_permissions__in=permissions,
                    )
            return run_mappers(mappers, user_obj)

        # Check if permissions are cached, get them if not
        obj_ctype = ContentType.objects.get_for_model(obj)
        cache = getattr(user_obj, '_cims_conditional_cache', None)
        if (cache is None
                or obj_ctype.id not in cache
                or obj.id not in cache[obj_ctype.id]):
            self.get_all_permissions(user_obj, obj=obj)
            cache = user_obj._cims_conditional_cache

        cache = cache[obj_ctype.id][obj.id]

        # Looking for model level perm, no need to go deeper
        if perm in cache:
            return True
        perm_key = perm.rsplit('__', maxsplit=1)[0]
        # A user has a field level permission if the model level parent is set
        # and no fields are set, or the field permission is set.
        return perm_key in cache and (not cache[perm_key] or perm in cache[perm_key])

    def has_module_perms(self, user_obj, app_label):
        """
        Return True if `user_obj` is granted any `Permissions` in `app_label` by a
        `ConditionalPermissionsMap`.
        """
        if not user_obj.is_active or user_obj.is_anonymous:
            return False

        assert not user_obj.is_superuser, ('ConditionalPermissionsMapBackend.has_module_perms() '
                                           'got an unexpected superuser.')

        # Get the ConditionalPermissionMaps that grant permissions in this app_label
        mappers = ConditionalPermissionsMap.objects.filter(
                conditional_permissions__content_type__app_label=app_label,
                )
        return run_mappers(mappers, user_obj)


class ObjectStatusPermissionsMapBackend(object):
    """
    Backend that uses cims.auth.ObjectStatusPermissionsMap to deny access to a model instance based
    on the value of a status field of it's own, or of a related model.
    """
    def __init__(self, *args, **kwargs):
        backends = settings.AUTHENTICATION_BACKENDS
        cimsbackend = 'cims.auth.backends.CIMSBackend'
        thisbackend = 'cims.auth.backends.ObjectStatusPermissionsMapBackend'

        # Ensure we're properly configured
        assert (cimsbackend in backends
                and backends.index(cimsbackend) > backends.index(thisbackend)), (
                        'ObjectStatusPermissionsMapBackend must be configured before {} in '
                        'django.settings.AUTHENTICATION_BACKENDS.'.format(cimsbackend)
                        )

        self.permission_mappers = ObjectStatusPermissionsMap.objects.all()

    def authenticate(self, username=None, password=None, **kwargs):
        """We don't do this."""
        return None

    def get_user(self, user_id):
        """We don't do this."""
        return None

    def get_all_permissions(self, user_obj, obj=None):
        """This backend denies permissions, doesn't grant them."""
        return set()

    get_group_permissions = get_all_permissions
    get_user_permissions = get_all_permissions

    def has_perm(self, user_obj, perm, obj=None):
        """
        Raises PermissionDenied if the status of the object being checked matches one of the
        mappers, and the user isn't exempted.
        """
        # Only operate on saved objects. Ignore inactive users or superusers
        if not user_obj.is_active or user_obj.is_superuser or obj is None or obj.pk is None:
            return False

        for mapper in self.permission_mappers:
            # If user is permitted skip this mapper
            if mapper.permitted_users.filter(pk=user_obj.pk).exists():
                break
            role_ids = [r.pk for r in Role.objects.get_roles_for_user(user_obj)]
            roles_qs = mapper.permitted_roles.filter(pk__in=role_ids)
            if roles_qs.exists():
                break

            # Work out which object we're going to check the status of or skip this mapper if it
            # isn't relevant to this object
            obj_ctype = ContentType.objects.get_for_model(obj)
            if obj_ctype == mapper.check_content_type:
                check_obj = obj
            elif mapper.related_content_types.filter(pk=obj_ctype.pk).exists():
                CheckObj = mapper.check_content_type.model_class()
                # If no way to look up a check object from a related object skip this mapper
                obj_getter = getattr(
                        CheckObj.objects,
                        'get_{}_for_object'.format(mapper.check_content_type.model),
                        None
                        )
                if obj_getter is None:
                    break
                check_obj = obj_getter(obj)
                if check_obj is None:
                    break
            else:
                break

            # Check the status of our object against allowed list. If that fails and our permission
            # is in the denied list for this mapper raise a PermissionDenied exception to shortcut
            # the permission checking process.
            check_field = getattr(check_obj, mapper.check_field_name)
            if not mapper.allowed_states.filter(pk=check_field.pk).exists():
                model_perm = perm.split('__', maxsplit=1)[0]
                denied_perm_strings = [
                        get_perm_str(p) for p in
                        mapper.denied_permissions.all().select_related('content_type')
                        ]
                if perm in denied_perm_strings or model_perm in denied_perm_strings:
                    raise PermissionDenied()
        # If we didn't deny the permission then we rely on other backends to allow it.
        return False

    def has_module_perms(self, user_obj, app_label):
        """
        This backend has no input to module perms, only concerned with denying access to specific
        objects.
        """
        return False
