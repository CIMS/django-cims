"""
View mixins
"""
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ImproperlyConfigured, PermissionDenied


class ObjectPermissionRequiredMixin(PermissionRequiredMixin):
    """
    Extend PermissionRequiredMixin to check object permissions.

    Attributes
    ----------
    object_permission_required : list
                                 List of permissions to test against the specific object in this
                                 view.
    permission_denied_message : str
                                Message displayed to user when an action is denied.
    """
    object_permission_required = None
    permission_denied_message = 'You do not have permission to perform this action.'

    def get_permission_required(self):
        """
        Configuration check moved to self.has_permission(), so catch exception here and return an
        empty list.
        """
        try:
            perms = list(super().get_permission_required())
        except ImproperlyConfigured:
            perms = []

        return perms

    def get_object_permission_required(self):
        """
        Return the list of object permissions.
        """
        if isinstance(self.object_permission_required, str):
            return [self.object_permission_required]

        if self.object_permission_required is None:
            self.object_permission_required = []
        return self.object_permission_required

    def handle_no_permission(self):
        """Always raise an exception for an authenticated user."""
        if self.request.user.is_authenticated:
            raise PermissionDenied(self.get_permission_denied_message())
        return super().handle_no_permission()

    def has_permission(self):
        """
        Override to check at least one of permission_required or object_permission_required is
        configured, and to check object specific permissions.
        """
        if self.permission_required is None and self.object_permission_required is None:
            raise ImproperlyConfigured('At least one of {0}.permission_required or '
                                       '{0}.object_permission_required must be configured, '
                                       'or you must override {0}.get_permission_required() or '
                                       '{0}.get_object_permission_required()'.format(
                                           self.__class__.__name__
                                           )
                                       )
        perms = self.get_permission_required()
        obj_perms = self.get_object_permission_required()
        user = self.request.user
        # Check model perms, and if object perms are configured check them. If no object perms
        # configured then skip that step as we may not have an object e.g. add operation
        return (user.has_perms(perms)
                and (not obj_perms or user.has_perms(obj_perms, obj=self.get_object()))
                )
