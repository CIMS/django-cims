"""
Template tags provided by cims.auth.
"""
from django import template


register = template.Library()


@register.tag(name='user_has_perm')
def user_has_perm(parser, token):
    """
    A wrapper around user.has_perm() so we can check object permissions in a template. Takes the
    request, the permission string and an optional object as arguments, stores the result in the
    given context variable.'
    """
    tokens = token.split_contents()
    # Pop variable name, then check previous token matches the required 'as' to verify syntax. Then
    # try and map tokens to variables.
    try:
        variable_name = tokens.pop()
        if tokens.pop() != 'as':
            raise ValueError()
        try:
            tag_name, request, perm, obj = tokens
        except ValueError:  # obj argument wasn't passed
            tag_name, request, perm = tokens
            obj = None
    except ValueError:
        raise template.TemplateSyntaxError(
                '{} requires: the request, a permission string and an optional object, '
                'plus a variable name to store its result in.'.format(token.split_contents()[0])
                )

    return UserHasPermNode(request, perm, obj, variable_name)


class UserHasPermNode(template.Node):
    """
    Render the template output for the `user_has_perm` tag.

    Parameters
    ----------
    request : django.http.HttpRequest
              Request object for this request.
    perm : str
           A string identifying the permission to check.
    obj : The object to check permission against.
    variable_name : str
                    The template context variable in which to store the result of the permission
                    check.
    """
    def __init__(self, request, perm, obj, variable_name):
        self.request = template.Variable(request)
        self.perm = template.Variable(perm)
        if obj is None:
            self.obj = None
        else:
            self.obj = template.Variable(obj)
        self.variable_name = variable_name

    def render(self, context):
        request = self.request.resolve(context)
        perm = self.perm.resolve(context)
        obj = self.obj
        if obj is not None:
            obj = self.obj.resolve(context)
        # Set the context variable that stores our real result, and return an empty string to the
        # template rendering
        context[self.variable_name] = request.user.has_perm(perm, obj)
        return ''
