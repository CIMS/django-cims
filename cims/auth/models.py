from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, Permission
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from cims.auth.maps import mapper_tests
from cims.models import CIMSBaseModel, ReviewStatus


class User(AbstractUser):
    """Custom user model."""
    def __str__(self):
        if self.first_name or self.last_name:
            return self.get_full_name()
        else:
            return self.get_username()

    def get_absolute_url(self):
        return reverse_lazy('cims:user.profile', args=[self.pk])


class RoleManager(models.Manager):
    """
    The manager for the Role model.
    """
    def get_by_natural_key(self, name):
        return self.get(name=name)

    def _get_roles_for_user(self, user_obj):
        """
        Get all the roles for a given user. Since walking the graph generates a
        number of DB queries equal to the diameter of the resulting graph, caches
        the results in a flat list as well as a list of sets, in order of
        increasing distance from the user.

        Currently the query doesn't prune already visited nodes, relying on set
        operations to avoid duplication. Pruning could be done but would require
        more bookkeeping in the loop.
        """
        if not isinstance(user_obj, get_user_model()):
            raise TypeError("""Object passed in is not an instance of
                    the current user model.""")
        visited = set()
        roles_hierarchy = []
        roles = self.filter(users=user_obj)
        unvisited = set(roles)
        # Breadth first search of Role graph.
        while unvisited:
            roles_hierarchy.append(unvisited.difference(visited))
            visited.update(roles_hierarchy[-1])
            # Get the parents of our current set of roles
            roles = self.filter(child_roles__in=roles)
            unvisited = set(roles).difference(visited)
        # Kind of expensive to do this search so cache
        user_obj._roles_cache = visited
        user_obj._roles_hierarchical_cache = roles_hierarchy

    def get_roles_for_user(self, user_obj):
        """Get a set of all roles for a given user."""
        if not isinstance(user_obj, get_user_model()):
                return []
        if not hasattr(user_obj, '_roles_cache'):
            self._get_roles_for_user(user_obj)
        return user_obj._roles_cache

    def get_role_hierarchy_for_user(self, user_obj):
        """Get the hierarchical list of set of roles for the given user."""
        if not isinstance(user_obj, get_user_model()):
                return []
        if not hasattr(user_obj, '_roles_hierarchical_cache'):
            self._get_roles_for_user(user_obj)
        return user_obj._roles_hierarchical_cache


class Role(models.Model):
    """
    Roles. An replacement for the Django Group model that enables inheritance.
    """
    name = models.CharField(_('name'), max_length=80, unique=True)
    users = models.ManyToManyField(
            settings.AUTH_USER_MODEL,
            verbose_name=_('users'),
            blank=True,
            related_name='roles',
            )
    permissions = models.ManyToManyField(
            Permission,
            verbose_name=_('permissions'),
            blank=True,
            )
    child_roles = models.ManyToManyField(
            'self',
            symmetrical=False,
            blank=True,
            help_text=_(
                'Any roles selected here will inherit permissions from this '
                'role.'
                ),
            related_name='parent_roles',
            )

    class Meta:
        verbose_name = _('role')
        verbose_name_plural = _('roles')

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,)

    objects = RoleManager()

    def _get_users(self):
        """Get a set of all the users who are members of this role."""
        users = set(self.users.all())
        roles = self.child_roles.all().prefetch_related('child_roles__users')
        visited = set([self])
        unvisited = set(roles)
        while unvisited:
            while unvisited:
                # Add the users from each role to our set
                role = unvisited.pop()
                users.update(set(role.users.all()))
            roles = Role.objects.filter(
                    parent_roles__in=roles
                    ).prefetch_related('users')
            unvisited = set(roles).difference(visited)
        self._roles_all_users = users

    @property
    def all_users(self):
        if not hasattr(self, '_roles_all_users'):
            self._get_users()
        return self._roles_all_users


# Below largely cut'n'paste from Django Guardian, modified to suit our
# purposes.
class BaseObjectPermission(models.Model):
    """
    """
    permission = models.ForeignKey(Permission, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(_('object ID'), max_length=255)
    content_object = GenericForeignKey()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if self.content_type != self.permission.content_type:
            raise ValidationError("Cannot persist permission not designed for this class "
                                  "(permission's type is {} and object's type is "
                                  "{})".format(self.permission.content_type, self.content_type)
                                  )
        return super().save(*args, **kwargs)


class UserObjectPermission(BaseObjectPermission):
    """
    """
    user = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete=models.CASCADE,
            verbose_name=_('user'),
            related_name='objectpermissions',
            )

    class Meta:
        unique_together = ['user', 'permission', 'content_type', 'object_id']


class RoleObjectPermission(BaseObjectPermission):
    """
    """
    role = models.ForeignKey(
            Role,
            on_delete=models.CASCADE,
            verbose_name=_('role'),
            related_name='objectpermissions',
            )

    class Meta:
        unique_together = ['role', 'permission', 'content_type', 'object_id']


# setattr(Group, 'add_obj_perm',
#        lambda self, perm, obj: GroupObjectPermission.objects.assign_perm(perm, self, obj))
# setattr(Group, 'del_obj_perm',
#        lambda self, perm, obj: GroupObjectPermission.objects.remove_perm(perm, self, obj))


# Dependent Permissions
class ConditionalPermissionsMap(CIMSBaseModel):
    """
    Stores a mapping between a ContentType, some test function and a set of additional permissions.
    This allows for the allocation of additional permissions when a user passes some test. This
    simplifies the allocation of permissions to e.g. conveners.

    Roles like class conveners are tricky to accommodate in a role based auth system without it
    becoming unwieldy. The relationship is per-object but entitles the user to additional
    permissions to the class object and its related objects. Managing the proliferation of roles and
    permissions in this situation would be a mess unless automated.
    """
    description = models.TextField(help_text='Describe the purpose of this map')
    test_model = models.ForeignKey(
            ContentType,
            on_delete=models.CASCADE,
            help_text='The test will be performed against an instance of this model.'
            )
    test = models.CharField(
            max_length=40,
            help_text='This test will be run against the test model above.'
            )
    conditional_permissions = models.ManyToManyField(
            Permission,
            blank=True,
            related_name='conditional_mappers',
            help_text=('Permissions to apply to the test model and its related objects if the '
                       'test above passes.'),
            )

    def __str__(self):
        return self.description[:80]

    def perform_test(self, user, test_obj=None, **kwargs):
        test_func = mapper_tests[self.test]
        return test_func(user, test_obj, **kwargs)


class ObjectStatusPermissionsMap(CIMSBaseModel):
    """
    Stores a mapping between the value of a field on a model and a set of `Permissions` which will
    be denied.

    The imagined use case is that in a workflow, as an object moves through the process and its
    workflor status changes people will acquire or lose access to part or all of the object (and
    potentially, related objects as well).
    """
    description = models.TextField(help_text='Describe the purpose of this map')
    check_content_type = models.ForeignKey(
            ContentType,
            on_delete=models.CASCADE,
            help_text='The model of the object whose status we will check.'
            )
    check_field_name = models.CharField(
            max_length=80,
            default='status',
            help_text='Name of the field whose value we will check.',
            )
    allowed_states = models.ManyToManyField(
            ReviewStatus,
            help_text='The set of object states that will pass this check.',
            )
    denied_permissions = models.ManyToManyField(
            Permission,
            help_text='The permissions that will be denied if this check fails.',
            )
    related_content_types = models.ManyToManyField(
            ContentType,
            blank=True,
            related_name='obj_perm_map_related_ctype',
            help_text='Models that have a status dependency on the check Model.',
            )
    permitted_roles = models.ManyToManyField(
            Role,
            blank=True,
            help_text='Roles that will automatically pass this check.',
            )
    permitted_users = models.ManyToManyField(
            settings.AUTH_USER_MODEL,
            blank=True,
            help_text='Users that will automatically pass this check.',
            )

    def __str__(self):
        return self.description[:80]
