"""
Utility functions for cims.auth module.
"""
from django.contrib.auth.models import Permission
from django.db.models import Q

from cims.auth.models import Role, RoleObjectPermission, UserObjectPermission


def get_perm_str(permission):
    """
    Return the `app_label`.`codename` ID string for a `Permission`.

    Parameters
    ----------
    permission : Permission
                 The `Permission` to generate the ID string for.
    """
    return '{}.{}'.format(permission.content_type.app_label, permission.codename)


def check_user_obj_permissions(user_obj, filters=None):
    """
    Return if any rows from `UserObjectPermission` or `RoleObjectPermission` exist that match the
    given filters.

    Parameters
    ----------
    user_obj : settings.AUTH_USERMODEL
           The user to check permissions for
    filters : dict, optional
              Dict with structure:
              {
                'user': {<UserObjectPermission queryset filters},
                'role': {<RoleObjectPermission queryset filters),
              }
    """
    if isinstance(filters, dict):
        user_filters = filters.get('user', dict())
        role_filters = filters.get('role', dict())
    else:
        user_filters = role_filters = dict()

    user_qs = UserObjectPermission.objects.filter(
            user=user_obj,
            **user_filters
            )
    roles = Role.objects.get_roles_for_user(user_obj)
    roles_qs = RoleObjectPermission.objects.filter(
            role__in=roles,
            **role_filters
            )
    return user_qs.exists() or roles_qs.exists()


def get_perm_with_pfps(app_label, codename):
    """
    Get a permission from its `app_label` and `codename` as well as all it's potential per-field
    permissions in a queryset.
    """
    permission = Permission.objects.get(
            codename=codename,
            content_type__app_label=app_label,
            )
    return Permission.objects.filter(
            Q(id=permission.id) | Q(perfieldpermission__model_permission=permission)
            )
