import re
from datetime import date

import reversion
from django.contrib import messages
from django.contrib.admin.models import CHANGE
from django.contrib.auth import get_permission_codename, get_user_model
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.views import redirect_to_login
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres import search as pg_search
from django.core.exceptions import ValidationError
from django.db.models import Prefetch, Q
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.timezone import now
from django.views import generic
from django.views.decorators.http import require_POST
from perfieldperms.utils import get_unpermitted_fields
from reversion.models import Version

from cims import forms, formsets, models, viewhelpers as helpers
from cims.generic import CIMSCreateView, CIMSDetailView, CIMSUpdateView
from cims.utils.logging import construct_change_message, create_logentry
from cims.utils.misc import get_fields_for_model, instance_to_dict, nasty_search


# Concrete views
def home(request):
    return render(request, 'cims/home.html')


def get_assistance(request: HttpRequest):
    """Help/contact landing page."""
    subject = 'CECS CIMS - Assistance requested'
    body = ('Useful Info:\n\nReferer URL: {}\n\nPlease enter your request below\n'
            '-------------------------------'.format(request.META.get('HTTP_REFERER', '')))
    return render(request, 'cims/help.html', {'email_subject': subject, 'email_body': body})


def test_500_error_view(request):
    """Return a 500 server error to test notifications, logging, stats."""
    assert False, 'This better not be enabled in production...'
    # return HttpResponse(status=500)


def course_standards_map(request: HttpRequest, version_id: str):

    """
    View for course version learning outcome -> external standard mapping.
    """
    version = get_object_or_404(models.CourseVersion, pk=version_id)
    # Tasks comparison comes from a course instance, so optionally specifify a
    # year to get an instance from via GET query argument
    if request.GET.get('year') and re.match('^[0-9]{4}$', request.GET['year']):
        year = int(request.GET['year'])
    else:
        year = date.today().year

    frameworks = version.standards_frameworks.all()
    return render(request, 'cims/course_standards_map.html', {
        'frameworks': frameworks,
        'title': '{} - {}'.format(version.course.code, version.title),
        'version': version,
        'year': year,
        })


@require_POST
@login_required
@permission_required('cims.change_courseinstance')
def courseinstance_import(request: HttpRequest, pk: str):
    """
    Import field values from one CourseInstance to another.

    Copies field values from the POST'd CourseInstance ID to the the one identified by `pk`.
    Avoids overwriting existing values. Optionally copies related model instances and associates
    the new copies with the target CourseInstance.

    :param request: The request object for this request
    :param pk: The primary key identifying the target CourseInstance
    """
    msgs = {
            'no_permission': 'You lack the required permission to update this {}.',
            'form_invalid': 'The import form failed validation.',
            'courserelatedrole_permissions': ('Roles were not imported as you lack required '
                                              'permissions.'),
            'schedule_permissions': 'Schedules were not imported as you lack required permissions.',
            'task_permissions': 'Tasks were not imported as you lack required permissions.',
            'failed_validation': ('The import was aborted as the target failed validation after '
                                  'import. The error was: {}'),
            'success': '{} updated with data imported from {}',
            'change': 'Imported from {},  version id {}.',
            }

    opts = models.CourseInstance._meta
    target = get_object_or_404(models.CourseInstance, pk=pk)
    if not request.user.has_perm('cims.change_courseinstance', target):
        return redirect_to_login(request.get_full_path())

    import_form = forms.CourseInstanceImportForm(request.POST, user=request.user)
    if not import_form.is_valid():
        messages.error(request, msgs['form_invalid'])
        return redirect(target.get_absolute_url())

    source = import_form.cleaned_data['source_courseinstance']

    # Exclude fields the user lacks permissions for from the import, and fields it makes no sense
    # to import
    excluded_fields = {'courseversion', 'class_number', 'instance_descriptor'}
    excluded_fields.update(
            set(get_unpermitted_fields(models.CourseInstance, request.user, target))
            )

    # Make sure we're only copying concrete, editable fields. We don't want to copy e.g.
    # creation timestamps
    fields = [f.name for f in get_fields_for_model(models.CourseInstance)]
    update_kwargs = instance_to_dict(source, fields=fields, exclude=excluded_fields)

    # Try to update this instance. Catch any validation errors outside the reversion block so the
    # exception ensures the database transaction is wound back.
    try:
        with reversion.create_revision():
            source_current_version = Version.objects.get_for_object(source).first()
            change_message = msgs['change'].format(str(source), source_current_version.pk)
            reversion.set_comment(change_message)
            reversion.set_user(request.user)
            create_logentry(request, target, change_message, action=CHANGE)

            # Update
            target = models.CourseInstance.objects.validated_update(target, **update_kwargs)

            # For the related models the user could optionally import, check if they want to import
            # them and then try to do so. Form validation performed above performs a model level
            # permissions check so checking the form cleaned_data tells us both if they want to
            # import and if they (likely) can.
            related_models = [
                (model._meta.model_name, model)
                for model in (models.CourseRelatedRole, models.Schedule, models.Task)
                ]
            for model_name, Model, in related_models:
                can_import = import_form.cleaned_data['import_{}'.format(model_name)]
                if not can_import:
                    continue

                # Check object permissions for everything we're going to delete
                del_perm = 'cims.{}'.format(get_permission_codename('delete', Model._meta))
                # Work out the field for the reverse relation
                related_name = [f for f in opts.related_objects
                                if f.one_to_many and not f.concrete
                                and f.related_model._meta.model_name == model_name
                                ].pop().get_accessor_name()
                target_manager = getattr(target, related_name)
                source_manager = getattr(source, related_name)
                for obj in target_manager.all().order_by():
                    can_import = can_import and request.user.has_perm(del_perm, obj=obj)

                # Is it possible to import Task -> LearningOutcome mappings, and does the user
                # have sufficient permissions to do so?
                if Model is models.Task:
                    import_mappings = source.courseversion_id == target.courseversion_id
                    import_mappings or messages.warning(
                            request,
                            'Task -> Learning Outcome mappings were not imported. These mappings '
                            'can only be imported when the source and target Classes have a common '
                            'parent Course Version.'
                            )
                    if import_mappings:
                        mapping_add_perm = 'cims.{}'.format(
                                get_permission_codename('add', models.WeightedTaskToLO._meta)
                                )
                        import_mappings = import_mappings\
                                          and request.user.has_perm(mapping_add_perm)
                        import_mappings or messages.warning(
                                request,
                                'Task -> Learning Outcome mappings were not imported as you lack '
                                'required permissions.'
                                )
                else:
                    import_mappings = False

                # Delete the existing instances and import from the source
                if can_import:
                    target_manager.all().delete()
                    for obj in source_manager.all().order_by():
                        # If we're handling Tasks, get the mapping objects for this Task
                        if import_mappings:
                            mappings = [
                                (weightedtasktolo, weightedtasktolo.weights.all())
                                for weightedtasktolo in obj.weightedtasktolo_set.all()
                                ]
                            for weightedtasktolo, _ in mappings:
                                weightedtasktolo.pk = None

                        # Set the pk to None and update with the new parent object
                        obj.pk = None
                        obj = Model.objects.validated_update(obj, courseinstance=target)
                        create_logentry(request, obj, construct_change_message(create=True))
                        if import_mappings:
                            for weightedtasktolo, weights in mappings:
                                models.WeightedTaskToLO.objects.validated_update(
                                        weightedtasktolo,
                                        **{'task': obj, 'weights': weights}
                                        )
                                create_logentry(
                                        request,
                                        weightedtasktolo,
                                        construct_change_message(create=True)
                                        )
                else:
                    messages.warning(request, msgs['{}_permissions'.format(model_name)])

    except ValidationError as e:
        messages.error(request, msgs['failed_validation'].format(''.join(e.messages)))
        return redirect(target.get_update_url())

    messages.success(request, msgs['success'].format(str(target), str(source)))
    return redirect(target.get_update_url())


def user_profile(request):
    """Output the user's profile, or redirect to the login page if not logged in."""
    if request.user.is_authenticated:
        return UserProfile.as_view()(request, pk=request.user.pk)
    else:
        return HttpResponseRedirect(reverse('cims:login'))


# class PDFTemplateResponseMixin(TemplateResponseMixin):
#    """
#    Extends `TemplateResponseMixin` to return the content rendered as a pdf if the the query
#    parameter `pdf` is set.
#    """
#    filename = None
#    pdfkit_opts = None
#
#    def get_filename(self, *args, **kwargs):
#        return self.filename
#
#    def get_pdfkit_opts(self, *args, **kwargs):
#        return self.pdfkit_opts
#
#    def render_to_response(self, context, **response_kwargs):
#        if self.request.GET.get('pdf', default=False):
#            return PDFTemplateResponse(
#                    request=self.request,
#                    template=self.get_template_names(),
#                    context=context,
#                    using=self.template_engine,
#                    filename=self.get_filename(),
#                    pdfkit_opts=self.get_pdfkit_opts(),
#                    )
#        return super().render_to_response(context, **response_kwargs)
#
#
# class PDFDetailView(generic.DetailView):
#    """
#    Extend `DetailView` to allow returning a pdf as the response based on query parameter `pdf`
#    being set.
#
#    When rendering to a pdf sets a context variable `rendering_to_pdf` to True.
#    """
#    pdfkit_options = None
#    url_name = None
#    rendering_to_pdf = False
#    pdf_content_type = 'application/pdf'
#
#    def get_context_data(self, **kwargs):
#        context = super().get_context_data(**kwargs)
#        if self.rendering_to_pdf:
#            context['rendering_to_pdf'] = True
#        return context
#
#    def get_filename(self, *args, **kwargs):
#        """Construct a filename for the returned pdf."""
#        return None
#
#    def get_uri_args(self):
#        """Get arguments used to generate a URI for the requested pdf."""
#        return [self.object.id]
#
#    def get_pdf_url(self, request):
#        """Construct the URI for the content to be rendered as a pdf."""
#        uri = reverse(self.url_name, args=self.get_uri_args())
#        return '{}?rendering_to_pdf=1'.format(request.build_absolute_uri(uri))
#
#    def get(self, request, *args, **kwargs):
#        """If `pdf` is set in the query parameters respond with a pdf rendering of the content."""
#        if request.GET.get('pdf', default=False):
#            self.object = self.get_object()
#            url = self.get_pdf_url(request)
#            pdf = pdfkit.from_url(url, False, options=self.pdfkit_options)
#            response = HttpResponse(pdf, content_type=self.pdf_content_type)
#            filename = self.get_filename()
#            if filename is not None:
#                response['Content-Disposition'] = 'attachment;filename="{}"'.format(filename)
#            return response
#
#        if request.GET.get('rendering_to_pdf', False):
#                self.rendering_to_pdf = True
#        return super().get(request, *args, **kwargs)


class ConvenerList(generic.ListView):
    model = get_user_model()
    ordering = ['first_name', 'last_name', 'username']
    template_name = 'cims/convener_list.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        q_filters = []
        approved_versions = models.CourseVersion.objects.filter(
                status__in=models.ReviewStatus.objects.get_published_states()
                )
        courseinstances = models.CourseInstance.objects.filter(
                start_date__year__gte=date.today().year,
                courseversion__in=approved_versions,
                )
        # Filter based on search string
        search_form = forms.BasicSearchForm(self.request.GET)
        if search_form.is_valid():
            search_string = search_form.cleaned_data['q']
            if search_string:
                search_vector = ['first_name', 'last_name',
                                 'courseinstance__courseversion__course__code']
                q_filters.append(nasty_search(search_string, search_vector))

        queryset = queryset.filter(*q_filters, courseinstance__in=courseinstances).distinct()

        # Retrieve the CourseInstances starting this year and into the future and stick in a
        # attribute for use in the template
        queryset = queryset.prefetch_related(
                Prefetch(
                    'courseinstance_set',
                    queryset=courseinstances.order_by(
                            'courseversion__course__code',
                            '-start_date',
                            'class_number'
                            ),
                    to_attr='courseinstances',
                    )
                )
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        # Pass the GET data to the form so the previous search string is retained.
        context['search_form'] = forms.BasicSearchForm(self.request.GET)
        context['title'] = 'Conveners'
        context['courseinstance_opts'] = models.CourseInstance._meta
        return context


class CourseList(generic.ListView):
    model = models.Course
    ordering = 'code'

    def get_queryset(self):
        queryset = super().get_queryset()
        approved_versions = models.CourseVersion.objects.filter(
                status__in=models.ReviewStatus.objects.get_published_states()
                )
        # Filter based on search string
        search_form = forms.BasicSearchForm(self.request.GET)
        if search_form.is_valid():
            search_string = search_form.cleaned_data['q']
            if search_string:
                course_vector = ['code']
                courseversion_vector = ['courseversion__title', 'courseversion__version_descriptor']
                course_filter = nasty_search(search_string, course_vector)
                courseversion_filter = nasty_search(search_string, courseversion_vector)
                queryset = queryset.filter(
                        course_filter | Q(
                            courseversion_filter, courseversion__in=approved_versions
                            )
                        )

                # search_query = pg_search.SearchQuery(search_string)
                # search_vector = pg_search.SearchVector('code', 'courseversion__title',
                #                                        'courseversion__version_descriptor')
                # Annotate the queryset with the text search vectors, then filter on the search
                # terms using only approved CourseVersions
                # queryset = queryset.annotate(search=search_vector)
                # queryset = queryset.filter(
                #         courseversion__in=approved_versions,
                #         search=search_query
                #         )
        # Retrieve the current approved CourseVersion for each Course, and stuff it in an attribure
        # `current_version`
        queryset = queryset.distinct().prefetch_related(
                Prefetch(
                    'courseversion_set',
                    queryset=approved_versions,
                    to_attr='current_version',
                    )
                )
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        # Pass the GET data to the form so the previous search string is retained.
        context['search_form'] = forms.BasicSearchForm(self.request.GET)
        context['title'] = 'Courses'
        return context


class CourseDetail(CIMSDetailView):
    model = models.Course
    fields = ['code']
    slug_field = 'code'
    slug_url_kwarg = 'code'
    tab_helpers = [helpers.CourseRelatedCVHelper]

    def get_extra_links(self):
        return {
                'Compare against P&C': reverse_lazy(
                    'pandcscraper:pandcdiff',
                    args=[self.object.code]
                    )
                }


class CourseVersionDetail(CIMSDetailView):
    """
    Attributes:
    fields -- model fields we want to display in a table in the view
             template.
    """
    model = models.CourseVersion
    fields = ('course', 'version_descriptor', 'status', 'status_freetext', 'approval_date',
              'approval_description', 'title', 'description', 'delivery_mode', 'custodians',
              'course_group', 'unit_value', 'classification', 'academiccareer',
              'standards_frameworks', 'objectives', 'weightingschemes', 'prerequisite_courses',
              'corequisite_courses', 'incompatible_courses', 'assumed_knowledge',
              'research_led_teaching', 'required_resources', 'additional_costs', 'participation',
              'field_trips', 'recommended_resources', 'other_information',)
    layout = [
            (None, ['course', 'version_descriptor', 'status', 'status_freetext', 'approval_date',
                    'approval_description', 'title', 'description', 'delivery_mode', 'custodians',
                    'course_group', 'unit_value', 'classification', 'academiccareer',
                    'assumed_knowledge']),
            ('standards', ['standards_frameworks', 'objectives', 'weightingschemes']),
            ('requisite statements', ['prerequisite_courses', 'corequisite_courses',
                                      'incompatible_courses']),
            ('miscellaneous statements', ['research_led_teaching', 'required_resources',
                                          'additional_costs', 'participation', 'field_trips',
                                          'recommended_resources', 'other_information']),
            ]
    tab_helpers = [helpers.CourseVersionRelatedLOHelper,
                   helpers.CourseVersionRelatedCIHelper,
                   helpers.CourseVersionMappingHelper]

    def get_extra_links(self):
        opts = self.model._meta
        return {
                'Accreditation mapping tables': reverse_lazy(
                    '{}:objectives.mapping'.format(opts.app_label),
                    args=[self.object.pk]
                    ),
                'Compare against P&C': reverse_lazy(
                    'pandcscraper:pandcdiff.version',
                    args=[self.object.course.code, self.object.pk]
                    )
                }


class CourseVersionCreate(CIMSCreateView):
    model = models.CourseVersion
    form_class = forms.CourseVersionForm
    permission_required = ('cims.add_courseversion',)
    inlines = [formsets.LearningOutcomeManager]


class CourseVersionUpdate(CIMSUpdateView):
    model = models.CourseVersion
    form_class = forms.CourseVersionForm
    object_permission_required = ('cims.change_courseversion',)
    inlines = [formsets.LearningOutcomeManager, formsets.CVInlineWeightedLOToSOManager]


class CourseInstanceDetail(CIMSDetailView):
    model = models.CourseInstance
    tab_helpers = [helpers.CourseInstanceRelatedScheduleHelper,
                   helpers.CourseInstanceRelatedTaskHelper,
                   helpers.CourseInstanceMappingHelper]
    fields = ('courseversion', 'instance_descriptor', 'start_date', 'end_date', 'taught_by',
              'conveners', 'class_number', 'teachingsession', 'delivery_mode', 'delivery_group',
              'inherent_requirements', 'staff_feedback_statement', 'course_url', 'workload',
              'contact_methods', 'prescribed_texts', 'preliminary_reading_list',
              'indicative_reading_list', 'exams', 'exam_material', 'online_submission',
              'late_submission_policy', 'resubmission_policy', 'assignment_return',
              'reference_requirements', 'hard_copy_submission', 'other_information')
    layout = [
            (None, ['courseversion', 'instance_descriptor', 'start_date', 'end_date', 'taught_by',
                    'conveners', 'class_number', 'teachingsession', 'delivery_mode',
                    'delivery_group', 'course_url', 'inherent_requirements', 'workload',
                    'contact_methods', 'prescribed_texts']),
            ('miscellaneous statements', ['preliminary_reading_list', 'indicative_reading_list',
                                          'exams', 'exam_material', 'online_submission',
                                          'late_submission_policy', 'resubmission_policy',
                                          'assignment_return', 'reference_requirements',
                                          'hard_copy_submission', 'staff_feedback_statement',
                                          'other_information']),
            ]

    def get_extra_links(self):
        opts = self.model._meta
        return {
                'Generate course outline': reverse_lazy(
                    '{}:courseinstance.outline'.format(opts.app_label),
                    args=[self.object.pk]
                    ),
                }


class CourseInstanceCreate(CIMSCreateView):
    model = models.CourseInstance
    permission_required = ('cims.add_courseinstance',)
    form_class = forms.CourseInstanceForm
    inlines = [formsets.ScheduleManager, formsets.TaskManager]


class CourseInstanceUpdate(CIMSUpdateView):
    model = models.CourseInstance
    object_permission_required = ('cims.change_courseinstance',)
    form_class = forms.CourseInstanceForm
    inlines = [formsets.ScheduleManager, formsets.TaskManager,
               formsets.CIInlineWeightedTaskToLOManager]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        opts = self.model._meta
        context['import_form'] = forms.CourseInstanceImportForm(user=self.request.user)
        context['import_view'] = reverse_lazy(
                '{}:courseinstance.import'.format(opts.app_label),
                args=[self.object.pk]
                )
        return context


class CourseOutline(generic.DetailView):
    """Generate a course outline from a given CourseInstance."""
    model = models.CourseInstance
    # context_object_name = 'courseinstance'
    template_name = 'cims/course_outline.html'
    # url_name = 'cims:courses.instances.outline'
    # pdfkit_options = {
    #        'footer-left': '[page] | The Australian National University',
    #        'footer-font-size': '8',
    #        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # rendering_to_pdf = self.request.GET.get('pdf', False)
        context.update(
                {
                    # 'rendering_to_pdf': rendering_to_pdf,
                    'courseversion': self.object.courseversion,
                    'outcomes': self.object.courseversion.learningoutcome_set.all(),
                    'assessment_tasks': self.object.task_set.filter(weighting__gt=0),
                    'tasks': self.object.task_set.all(),
                    'schedule': self.object.schedule_set.all(),
                    'conveners': self.object.conveners.all(),
                    'title': 'Course outline for {}'.format(self.object),
                    })
        return context

    # def get_filename(self, *args, **kwargs):
    #    course = get_object_or_404(models.Course, courseversion__courseinstance=self.object)
    #    return '{}.pdf'.format(course.code)


class LearningOutcomeDetail(CIMSDetailView):
    model = models.LearningOutcome
    tab_helpers = [helpers.LORelatedObjectiveHelper, helpers.LearningOutcomeMappingHelper]
    fields = ['courseversion', 'outcome', 'outcome_short_form', 'order']


class LearningOutcomeCreate(CIMSCreateView):
    model = models.LearningOutcome
    form_class = forms.LearningOutcomeForm
    permission_required = ('cims.add_learningoutcome',)
    inlines = []


class LearningOutcomeUpdate(CIMSUpdateView):
    model = models.LearningOutcome
    form_class = forms.LearningOutcomeForm
    object_permission_required = ('cims.change_learningoutcome',)
    inlines = [formsets.LOInlineWeightedLOToSOManager]


class ScheduleDetail(CIMSDetailView):
    model = models.Schedule
    fields = ['courseinstance', 'session_order', 'session_number', 'session_name',
              'summary', 'assessment']


class ScheduleCreate(CIMSCreateView):
    model = models.Schedule
    form_class = forms.ScheduleForm
    permission_required = ('cims.add_schedule',)


class ScheduleUpdate(CIMSUpdateView):
    model = models.Schedule
    form_class = forms.ScheduleForm
    object_permission_required = ('cims.change_schedule',)


class StandardsBodyList(generic.ListView):
    """List standards bodies and their associated frameworks."""
    model = models.StandardsBody
    ordering = 'name'

    def get_queryset(self):
        """Prefetch associated frameworks."""
        queryset = super().get_queryset()
        # Filter based on search string
        if self.request.GET.get('q'):
            search_form = forms.BasicSearchForm(self.request.GET)
            search_form.is_valid()
            search_string = search_form.cleaned_data['q']
            if search_string:
                search_query = pg_search.SearchQuery(search_string)
                search_vector = pg_search.SearchVector('code', 'courseversion__title',
                                                       'courseversion__version_descriptor')
                # Annotate the queryset with the text search vectors, then filter on the search
                # terms using only approved CourseVersions
                queryset = queryset.annotate(search=search_vector)
                queryset = queryset.filter(search=search_query)
        queryset = queryset.prefetch_related('standardsframework_set')
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        # Pass the GET data to the form so the previous search string is retained.
        context['search_form'] = forms.BasicSearchForm(self.request.GET)
        context['title'] = 'Standards Bodies'
        return context


class StandardsBodyDetail(CIMSDetailView):
    model = models.StandardsBody
    template_name = 'cims/standardsbody_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        return context


class StandardsFrameworkDetail(CIMSDetailView):
    model = models.StandardsFramework
    template_name = 'cims/standardsframework_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        return context


class TaskDetail(CIMSDetailView):
    model = models.Task
    tab_helpers = [helpers.TaskRelatedLOHelper, helpers.TaskMappingHelper]
    fields = ['courseinstance', 'title', 'description', 'order', 'task_url', 'is_exam', 'is_hurdle',
              'weighting', 'presentation_requirements', 'due_date', 'due_time', 'return_date',
              'hurdle_requirements', 'individual_in_group']


class TaskCreate(CIMSCreateView):
    model = models.Task
    form_class = forms.TaskForm
    permission_required = ('cims.add_task',)


class TaskUpdate(CIMSUpdateView):
    model = models.Task
    form_class = forms.TaskForm
    object_permission_required = ('cims.change_task',)
    inlines = [formsets.TaskInlineWeightedTaskToLOManager]


class UserProfile(generic.DetailView):
    """
    View for displaying a profile view for a user, summarising what courses they are a convener of
    etc.
    """
    model = get_user_model()
    template_name = 'cims/user_profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.object
        courseinstance_opts = models.CourseInstance._meta
        context['courseversion_opts'] = models.CourseVersion._meta
        context['courseinstance_opts'] = courseinstance_opts
        courseinstance_ctype = ContentType.objects.get_for_model(models.CourseInstance)
        context['current_courseinstances'] = {
            ci: models.Annotation.objects.filter(
                    content_type=courseinstance_ctype,
                    object_id=ci.pk,
                    ).order_by('-last_modified')
            for ci in
            user.courseinstance_set.filter(start_date__year__gte=now().year).order_by('-start_date')
            }
        context['past_courseinstances'] = {
            ci: models.Annotation.objects.filter(
                    content_type=courseinstance_ctype,
                    object_id=ci.pk,
                    ).order_by('-last_modified')
            for ci in
            user.courseinstance_set
                .exclude(start_date__year__gte=now().year)
                .order_by('-start_date')
            }
        context['current_courseversions'] = user.courseversion_set.filter(
                status__in=models.ReviewStatus.objects.get_published_states()
                ).order_by('course__code')
        context['archived_courseversions'] = user.courseversion_set.filter(
                status__name__startswith='Archived'
                ).order_by('course__code')
        context['courseinstance_change_permission'] = '{}.{}'.format(
                courseinstance_opts.app_label,
                get_permission_codename('change', courseinstance_opts)
                )

        role_titles = models.CourseRelatedRoleTitle.objects.values_list(
                'title', flat=True
                ).order_by('title')
        related_roles = {}
        for title in role_titles:
            roles_qs = models.CourseInstance.objects.filter(
                    relatedroles__user=user,
                    start_date__year__gte=now().year,
                    relatedroles__role_title__title=title
                    )
            if roles_qs:
                related_roles[title] = roles_qs
        context['related_roles'] = related_roles

        old_roles = {}
        for title in role_titles:
            roles_qs = models.CourseInstance.objects.filter(
                    relatedroles__user=user,
                    start_date__year__lt=now().year,
                    relatedroles__role_title__title=title
                    )
            if roles_qs:
                old_roles[title] = roles_qs
        context['old_roles'] = old_roles

        context['title'] = user.get_full_name() or user.get_username()
        return context
