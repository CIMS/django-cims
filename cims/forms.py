from crispy_forms.bootstrap import Accordion, AccordionGroup
from crispy_forms.layout import Field, Layout
from django import forms
from django.contrib.auth import get_permission_codename
from django.core.exceptions import ValidationError
from django.db import transaction
from perfieldperms.forms import PFPModelForm

from cims import models
from cims.crispy_helpers import DefaultFormHelper, HorizontalFormHelper
from cims.widgets import DateInput, TimeInput


# Base/Generic Form classes
class ValidatedModelForm(forms.ModelForm):
    """
    Extends `ModelForm` to perform additional validation during `save()`.

    Cannot be used with a ModelAdmin due to ModelAdmin's dependency on delayed commit saves.
    """
    def save(self, commit=True):
        """
        Wrap the save in a transaction and then perform additional validation. Rollback if
        validation fails.
        """
        assert commit, "ValidatedModelForm doesn't support delayed commit saves."

        try:
            with transaction.atomic():
                instance = super().save(commit)
                instance.validate(full_clean=False)
        except ValidationError as e:
            self.add_error(None, e)

        return self.instance


# class CIMSBaseModelForm(PFPModelForm):
class CIMSBaseModelForm(ValidatedModelForm, PFPModelForm):
    """
    Base ModelForm. Currently just an extension of PFPModelForm.
    """
    crispy_helper = HorizontalFormHelper

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = self.crispy_helper()


# Data model ModelForms
class CourseInstanceForm(CIMSBaseModelForm):
    class Meta:
        model = models.CourseInstance
        fields = ('courseversion', 'instance_descriptor', 'start_date', 'end_date', 'taught_by',
                  'conveners', 'class_number', 'teachingsession', 'delivery_mode',
                  'delivery_group', 'course_url', 'inherent_requirements', 'workload',
                  'contact_methods', 'prescribed_texts', 'preliminary_reading_list',
                  'indicative_reading_list', 'exams', 'exam_material', 'online_submission',
                  'late_submission_policy', 'resubmission_policy', 'assignment_return',
                  'reference_requirements', 'hard_copy_submission', 'staff_feedback_statement',
                  'other_information')
        widgets = {'start_date': DateInput, 'end_date': DateInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Setup Crispyforms config
        self.helper.layout = Layout(
                Field('courseversion', tabindex='1'), 'instance_descriptor', 'start_date',
                'end_date', 'taught_by', 'conveners', 'class_number', 'teachingsession',
                'delivery_mode', 'delivery_group', 'course_url', 'inherent_requirements',
                'workload', 'contact_methods', 'prescribed_texts',
                Accordion(
                    AccordionGroup('Miscellaneous Statements', 'preliminary_reading_list',
                                   'indicative_reading_list', 'exams', 'exam_material',
                                   'online_submission', 'late_submission_policy',
                                   'resubmission_policy', 'assignment_return',
                                   'reference_requirements', 'hard_copy_submission',
                                   'staff_feedback_statement', 'other_information',
                                   css_class='mb-4',
                                   ),
                    )
                )


class InlineCourseInstanceForm(CIMSBaseModelForm):
    class Meta:
        model = models.CourseInstance
        fields = ['instance_descriptor', 'start_date', 'end_date', 'taught_by']


class CourseVersionForm(CIMSBaseModelForm):
    class Meta:
        model = models.CourseVersion
        fields = ('course', 'version_descriptor', 'status', 'status_freetext', 'approval_date',
                  'approval_description', 'title', 'description', 'delivery_mode', 'custodians',
                  'course_group', 'unit_value', 'classification', 'academiccareer',
                  'standards_frameworks', 'objectives', 'weightingschemes', 'prerequisite_courses',
                  'corequisite_courses', 'incompatible_courses', 'assumed_knowledge',
                  'research_led_teaching', 'required_resources', 'additional_costs',
                  'participation', 'field_trips', 'recommended_resources', 'other_information',)
        widgets = {'approval_date': DateInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Set the queryset on the objectives field
        objectives = self.fields['objectives']
        if self.instance.pk is None:
            objectives.queryset = models.StandardObjective.objects.none()
        else:
            objectives.queryset = self.instance.get_allowed_standardobjectives()

        # Setup Crispyforms config
        self.helper.layout = Layout(
                Field('course', tabindex='1'), 'version_descriptor', 'status', 'status_freetext',
                'approval_date', 'approval_description', 'title', 'description', 'delivery_mode',
                'custodians', 'course_group', 'unit_value', 'classification', 'academiccareer',
                'assumed_knowledge',
                Accordion(
                    AccordionGroup('Standards', 'standards_frameworks', 'objectives',
                                   'weightingschemes',
                                   css_class='mb-4',
                                   ),
                    AccordionGroup('Requisite statements', 'prerequisite_courses',
                                   'corequisite_courses', 'incompatible_courses',
                                   css_class='mb-4',
                                   ),
                    AccordionGroup('Miscellaneous statements', 'research_led_teaching',
                                   'required_resources', 'additional_costs', 'participation',
                                   'field_trips', 'recommended_resources', 'other_information',
                                   css_class='mb-4',
                                   ),
                    )
                )


class LearningOutcomeForm(CIMSBaseModelForm):
    class Meta:
        model = models.LearningOutcome
        fields = ('courseversion', 'outcome', 'outcome_short_form', 'order')


class ScheduleForm(CIMSBaseModelForm):
    class Meta:
        model = models.Schedule
        fields = ('courseinstance', 'session_name', 'session_order', 'session_number',
                  'summary', 'assessment')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Setup Crispyforms config
        self.helper.layout = Layout(
                Field('courseinstance', tabindex='1'), 'session_name', 'session_order',
                'session_number', 'summary', 'assessment'
                )


class TaskForm(CIMSBaseModelForm):
    class Meta:
        model = models.Task
        fields = ('courseinstance', 'title', 'description', 'weighting', 'order', 'is_hurdle',
                  'is_exam', 'task_url', 'presentation_requirements', 'due_date', 'due_time',
                  'hurdle_requirements', 'individual_in_group')
        widgets = {'due_date': DateInput, 'due_time': TimeInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Setup Crispyforms config
        self.helper.layout = Layout(
                Field('courseinstance', tabindex='1'), 'title', 'description', 'weighting', 'order',
                'is_hurdle', 'is_exam', 'task_url', 'presentation_requirements', 'due_date',
                'due_time', 'hurdle_requirements', 'individual_in_group',
                )


class WeightedLOToSOForm(CIMSBaseModelForm):
    class Meta:
        model = models.WeightedLOToSO
        fields = ('learningoutcome', 'standardobjective', 'weights')


class WeightedTaskToLOForm(CIMSBaseModelForm):
    class Meta:
        model = models.WeightedTaskToLO
        fields = ('task', 'learningoutcome', 'weights')


# Often we want to filter the querysets of forms used in formsets. Functions to assist with that.
_form_queryset_setters = {}


def _get_empty_form_field_querysets(form_class: forms.Form, fields: list) -> dict:
    """
    Set the queryset for each field in fields to an empty queryset.

    :param form_class: The Form class whose querysets we're modifying.
    :param fields: The fields in the form whose querysets we want to modify.
    :return: Dict of empty querysets
    """
    querysets = {}
    for field_name in fields:
        if hasattr(form_class.base_fields[field_name], 'queryset'):
            field_model = form_class.base_fields[field_name].queryset.model
            querysets[field_name] = field_model.objects.none()
    return querysets


def _set_weightedtasktolo_form_querysets(form_class: forms.Form, fields: list, **kwargs) -> dict:
    """
    Helper function that sets the querysets for a cims.models.WeightedTaskToLO model form. Needs
    the parent CourseInstance passed in as part of kwargs.
    """
    courseinstance = kwargs.pop('courseinstance')  # type: cims.models.CourseInstance
    if courseinstance and getattr(courseinstance, 'pk', False):
        courseversion = courseinstance.courseversion
        querysets = {
            'task': courseinstance.task_set.all(),
            'learningoutcome': courseversion.learningoutcome_set.all(),
            'weights': models.Weight.objects.filter(weightingscheme__courseversion=courseversion)
            }
    else:
        querysets = _get_empty_form_field_querysets(form_class, fields)
    return querysets


_form_queryset_setters['_set_weightedtasktolo_form_querysets'] = _set_weightedtasktolo_form_querysets


def _set_weightedlotoso_form_querysets(form_class: forms.Form, fields: list, **kwargs) -> dict:
    """
    Helper function that sets the querysets for a cims.models.WeightedLOToSO model form. Needs
    the parent CourseVersion passed in as part of kwargs.
    """
    courseversion = kwargs.pop('courseversion')  # type: cims.models.CourseVersion
    if courseversion and getattr(courseversion, 'pk', False):
        querysets = {
            'learningoutcome': courseversion.learningoutcome_set.all(),
            'standardobjective': courseversion.objectives.all(),
            'weights': models.Weight.objects.filter(weightingscheme__courseversion=courseversion)
            }
    else:
        querysets = _get_empty_form_field_querysets(form_class, fields)
    return querysets


_form_queryset_setters['_set_weightedlotoso_form_querysets'] = _set_weightedlotoso_form_querysets


def set_form_querysets(form_class: forms.Form, fields: list, **kwargs) -> None:
    """
    Modify the base field querysets of model choice fields in form_class. Hand off the actual
    creation of querysets to functions specific to each model.

    :param form_class: The form class whose base fields we want to modify.
    :param fields: The model choice fields whose queryset we want to change
    :param kwargs: Arguments to pass to the queryset creating helper function.
    """
    queryset_function = _form_queryset_setters[
        '_set_{}_form_querysets'.format(form_class.Meta.model._meta.model_name)
        ]
    querysets = queryset_function(form_class, fields, **kwargs)
    for field_name in fields:
        if hasattr(form_class.base_fields[field_name], 'queryset'):
            form_class.base_fields[field_name].queryset = querysets[field_name]


# Other forms
class CourseInstanceImportForm(forms.Form):
    """Form used in CourseInstance import dialog in CourseInstance form view."""
    source_courseinstance = forms.ModelChoiceField(
            models.CourseInstance.objects.all(),
            label='Source {}'.format(models.CourseInstance._meta.verbose_name)
            )
    import_courserelatedrole = forms.BooleanField(
            required=False,
            initial=False,
            disabled=True,
            label='Import roles'
            )
    import_schedule = forms.BooleanField(
            required=False,
            initial=False,
            disabled=True,
            label='Import schedules'
            )
    import_task = forms.BooleanField(
            required=False,
            initial=False,
            disabled=True,
            label='Import tasks'
            )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.helper = DefaultFormHelper()
        self.helper.layout = Layout('source_courseinstance', 'import_courserelatedrole',
                                    'import_schedule', 'import_task')
        # Enable the related model checkboxes if the user has sufficient permissions
        if user:
            for Model in [models.CourseRelatedRole, models.Schedule, models.Task]:
                opts = Model._meta
                del_perm = '{}.{}'.format(opts.app_label, get_permission_codename('delete', opts))
                add_perm = '{}.{}'.format(opts.app_label, get_permission_codename('add', opts))
                if user.has_perms([add_perm, del_perm]):
                    field_name = 'import_{}'.format(opts.model_name)
                    self.fields[field_name].disabled = False
                    self.fields[field_name].initial = True


class BasicSearchForm(forms.Form):
    """Provides a text field for a basic search form."""
    q = forms.CharField(required=False, label='Filter')
