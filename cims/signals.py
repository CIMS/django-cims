from django.core.exceptions import ValidationError
from django.db.models import Count, Exists, F, OuterRef, QuerySet
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from cims.models import (CourseVersion, StandardObjective, StandardsFramework, Weight,
                         WeightedLOToSO, WeightedTaskToLO, WeightingScheme)


# Validate mapping models
def _validate_mapping_weights_forward(
        instance,
        pk_set: list,
        courseversion,
        not_linked_msg: dict,
        too_many_msg: dict
        ):
    """
    Validate Weights for mapping models when updates are made in the forward direction.

    :param instance: The model instance whose Weights we are updating
    :param pk_set: The pks of the Weights being added
    :param courseversion: The parent CourseVersion, used to determine which Weights are allowed.
    :param not_linked_msg: The error message when invalid Weights are added.
    :param too_many_msg: The error message when multiple Weights from the same WeightingScheme
    are added.
    """
    if Weight.objects \
            .filter(pk__in=pk_set) \
            .exclude(weightingscheme__in=courseversion.weightingschemes.all()) \
            .exists():
        raise ValidationError(not_linked_msg)

    # Check we don't end up with  multiple Weights from the same scheme
    weight_from_existing_scheme = WeightingScheme.objects \
        .filter(weight__in=pk_set) \
        .filter(weight__in=instance.weights.all()) \
        .exists()
    new_weights_same_scheme = Weight.objects \
        .filter(pk__in=pk_set) \
        .values('weightingscheme_id') \
        .annotate(weightingscheme_members=Count('pk')) \
        .filter(weightingscheme_members__gt=1) \
        .exists()
    if weight_from_existing_scheme or new_weights_same_scheme:
        raise ValidationError(too_many_msg)


def _validate_mapping_weights_reverse(
        instance,
        pk_set: list,
        mapping_model,
        valid_scheme: QuerySet,
        not_linked_msg: dict,
        too_many_msg: dict
        ):
    """
    Validate Weights for mapping models when updates are made using the reverse relation.

    :param instance: The Weight model instance whose reverse relation we are updating
    :param pk_set: The pks of the mapping instances being added
    :param mapping_model: The mapping Model whose instances we're adding this Weight to.
    :param valid_scheme: A QuerySet used as a subquery to determine if this Weight's
    WeightingScheme is valid for a given mapping model instance.
    :param not_linked_msg: The error message when this Weight is invalid for at least one of the
    mapping instances.
    :param too_many_msg: The error message when a Weight from this Weight's WeightingScheme is
    already linked to one of the mapping instances.
    """
    if mapping_model.objects \
            .filter(pk__in=pk_set) \
            .annotate(valid_scheme=Exists(valid_scheme)) \
            .filter(valid_scheme=False) \
            .exists():
        raise ValidationError(not_linked_msg)

    # Check Weights from this Weight's WeightingScheme aren't already associated with any
    # of the WeightedLOToSOs
    if mapping_model.objects \
            .filter(pk__in=pk_set, weights__weightingscheme=instance.weightingscheme) \
            .exists():
        raise ValidationError(too_many_msg)


@receiver(m2m_changed, dispatch_uid='QnPjB2MpVc')
def weightedlotoso_clean_weights_pre_add(sender, **kwargs):
    """
    Validate the weights about to be linked.

    The linked Weights must come from a WeightingScheme associated with the CourseVersion linked
    via the LearningOutcome.
    An instance can only link to one Weight from each WeightingScheme.
    """
    if not (sender is WeightedLOToSO.weights.through and kwargs['action'] is 'pre_add'):
        return

    not_linked_msg = {'weights': 'Weights must come from a WeightingScheme associated with '
                                 '"learningoutcome\'s" CourseVersion.'}
    too_many_msg = {'weights': 'You can only apply one weight from each weighting scheme.'}

    if kwargs['reverse']:
        # Check that the WeightingScheme for this Weight is in the set of Schemes allowed by
        # the parent CourseVersion of each WeightedLOToSO we're adding this Weight to.
        instance = kwargs['instance']  # type: Weight
        valid_scheme = WeightingScheme.objects.filter(
                pk=instance.weightingscheme_id,
                courseversion__learningoutcome__weightedlotoso=OuterRef('pk')
                )
        _validate_mapping_weights_reverse(
                instance,
                kwargs['pk_set'],
                WeightedLOToSO,
                valid_scheme,
                not_linked_msg,
                too_many_msg
                )
    else:
        # Check the weights all come from valid WeightingSchemes
        instance = kwargs['instance']  # type: WeightedLOToSO
        courseversion = CourseVersion.objects.get(
                learningoutcome=instance.learningoutcome_id
                )
        _validate_mapping_weights_forward(
                instance,
                kwargs['pk_set'],
                courseversion,
                not_linked_msg,
                too_many_msg)


@receiver(m2m_changed, dispatch_uid='sWnp5wV6dM')
def weightedtasktolo_clean_weights_pre_add(sender, **kwargs):
    """
    Validate the weights about to be linked.

    Weights must come from a WeightingScheme associated with the CourseVersion linked
    via the Task.
    An instance can only link to one Weight from each WeightingScheme.
    """
    if not (sender is WeightedTaskToLO.weights.through and kwargs['action'] is 'pre_add'):
        return

    not_linked_msg = {'weights': 'Weights must come from a WeightingScheme associated with '
                                 '"task\'s" CourseVersion.'}
    too_many_msg = {'weights': 'You can only apply one weight from each weighting scheme.'}

    if kwargs['reverse']:
        # Check that the WeightingScheme for this Weight is in the set of Schemes allowed by
        # the parent CourseVersion of each WeightedTaskToLO we're adding this Weight to.
        instance = kwargs['instance']  # type: Weight
        valid_scheme = WeightingScheme.objects.filter(
                pk=instance.weightingscheme_id,
                courseversion__courseinstance__task__weightedtasktolo=OuterRef('pk')
                )
        _validate_mapping_weights_reverse(
                instance,
                kwargs['pk_set'],
                WeightedTaskToLO,
                valid_scheme,
                not_linked_msg,
                too_many_msg
                )
    else:
        # Check the weights all come from valid WeightingSchemes
        instance = kwargs['instance']  # type: WeightedTaskToLO
        courseversion = CourseVersion.objects.get(
                pk=instance.task.courseinstance.courseversion_id
                )
        _validate_mapping_weights_forward(
                instance,
                kwargs['pk_set'],
                courseversion,
                not_linked_msg,
                too_many_msg)


@receiver(m2m_changed, dispatch_uid='qeBzFK6hwd')
def courseversion_clean_objectives_pre_add(sender, **kwargs):
    """
    Linked StandardObjectives must be leaf objectives from objective trees linked to
    StandardFrameworks that are themselves linked to this CourseVersion.
    """
    if not (sender is CourseVersion.objectives.through and kwargs['action'] is 'pre_add'):
        return

    if kwargs['reverse']:
        objective = kwargs['instance']  # type: StandardObjective
        # Checking from the reverse relation so check this StandardObjective's framework is valid
        # for each of the CourseVersions we're trying to add to, if any aren't raise an exception
        valid_framework = StandardsFramework.objects \
            .filter(courseversion=OuterRef('pk'), pk=objective.standardsframework_id)
        bad_framework = CourseVersion.objects \
            .filter(pk__in=kwargs['pk_set']) \
            .annotate(valid_framework=Exists(valid_framework)) \
            .filter(valid_framework=False) \
            .exists()

        if not objective.is_leaf_node() or bad_framework:
            raise ValidationError(
                    {'objectives': 'StandardObjectives linked to this CourseVersion must be '
                                   'leaf objectives associated with a StandardsFramework that '
                                   'is itself linked to this CourseVersion.'}
                    )
    else:
        courseversion = kwargs['instance']  # type: CourseVersion
        valid_trees = StandardObjective.objects \
            .filter(standardsframework_id__in=courseversion.standards_frameworks.all()) \
            .values_list('tree_id', flat=True) \
            .order_by()
        # Exclude valid objectives i.e. leaf nodes in valid trees. The F() statement checks if a
        # node is a leaf.
        bad_objectives = StandardObjective.objects \
            .filter(pk__in=kwargs['pk_set']) \
            .exclude(tree_id__in=valid_trees, lft=F('rght') - 1) \
            .exists()
        if bad_objectives:
            raise ValidationError(
                    {'objectives': 'StandardObjectives linked to this CourseVersion must be '
                                   'leaf objectives associated with a StandardsFramework that '
                                   'is itself linked to this CourseVersion.'}
                    )
