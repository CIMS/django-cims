"""
HTML input widgets.
"""
from django.forms import widgets
from django.utils import formats
from django.utils.encoding import force_text


# Encourage use of html5 input types
class DateInput(widgets.DateInput):
    input_type = 'date'

    def format_value(self, value):
        """HTML5 date inputs expect values in ISO format so disable localisation."""
        if value is not None:
            return force_text(
                    formats.localize_input(
                        value,
                        self.format or formats.get_format(self.format_key, use_l10n=False)[0]
                        )
                    )


class DateTimeInput(widgets.DateTimeInput):
    input_type = 'datetime-local'


class TimeInput(widgets.TimeInput):
    input_type = 'time'
