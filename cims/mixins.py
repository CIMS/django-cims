"""
Mixins for the CIMS application
"""
from django.views.generic.base import TemplateResponseMixin


class PDFResponseMixin(TemplateResponseMixin):
    """Extend `TemplateResponseMixin` to render the response content as a PDF document."""
    pass

