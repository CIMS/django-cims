"""
Formsets for the CIMS application.
"""

from crispy_forms.layout import Div, Field, Layout
from django import forms
from django.contrib import messages
from django.contrib.auth import get_permission_codename
from django.contrib.contenttypes.forms import (BaseGenericInlineFormSet,
                                               generic_inlineformset_factory)
from django.core.exceptions import ImproperlyConfigured
from django.forms.formsets import formset_factory
from django.forms.models import BaseInlineFormSet, inlineformset_factory, modelformset_factory
from django.utils.encoding import force_text

from cims import models
from cims.crispy_helpers import (BaseFormHelper, TableInlineCollapseFormSetHelper,
                                 TableInlineFormSetHelper, TableInlineModalFormSetHelper)
from cims.forms import (CIMSBaseModelForm, InlineCourseInstanceForm, set_form_querysets)
from cims.widgets import DateInput, TimeInput


def formset_change_message(formset, change_message):
    """Append change message details for a `formset` to `change_message`."""
    if hasattr(formset, 'new_objects'):
        for added_object in formset.new_objects:
            change_message.append({
                'added': {
                    'name': force_text(added_object._meta.verbose_name),
                    'object': force_text(added_object),
                    }
                })

    if hasattr(formset, 'changed_objects'):
        for changed_object, changed_fields in formset.changed_objects:
            change_message.append({
                'changed': {
                    'name': force_text(changed_object._meta.verbose_name),
                    'object': force_text(changed_object),
                    'fields': changed_fields,
                    }
                })

    if hasattr(formset, 'deleted_objects'):
        for deleted_object in formset.deleted_objects:
            change_message.append({
                'deleted': {
                    'name': force_text(deleted_object._meta.verbose_name),
                    'object': force_text(deleted_object),
                    }
                })
    return change_message


# Base/Generic Formset classes
class ObjectPermsModelFormSet(forms.BaseModelFormSet):
    """
    Extension to BaseModelFormSet to check object permissions on save/delete.

    This requires that we have access to the request so we can get the request user.

    Parameters
    ----------
    request : HttpRequest
              The Request associated with the views using this formset.
    """
    def __init__(self, data=None, request=None, **kwargs):
        assert request is not None, ('{} requires a not-None request '
                                     'parameter.'.format(self.__class__.__name__))
        self.request = request
        super().__init__(data=data, **kwargs)

    def get_add_permission(self):
        opts = self.model._meta
        codename = get_permission_codename('add', opts)
        return '{}.{}'.format(opts.app_label, codename)

    def get_change_permission(self):
        opts = self.model._meta
        codename = get_permission_codename('change', opts)
        return '{}.{}'.format(opts.app_label, codename)

    def get_delete_permission(self):
        opts = self.model._meta
        codename = get_permission_codename('delete', opts)
        return '{}.{}'.format(opts.app_label, codename)

    def save_new(self, form, commit=True):
        """
        Only save this form if the user has permission to create the object.
        """
        if self.request.user.has_perm(self.get_add_permission()):
            return super().save_new(form, commit=commit)
        messages.warning(
                self.request,
                'New {} were not created as you lack permission to create new entries.'.format(
                        self.model._meta.verbose_name_plural
                        )
                )

    def save_existing(self, form, instance, commit=True):
        """
        Only save this form if the user has permission to update the object.
        """
        if self.request.user.has_perm(self.get_change_permission(), obj=instance):
            return super().save_existing(form, instance, commit=commit)
        messages.warning(
                self.request,
                '{} was not updated as you lack permission.'.format(instance)
                )
        return instance

    def delete_existing(self, obj, commit=True):
        """Only delete this object if the user has permission to do so."""
        if self.request.user.has_perm(self.get_delete_permission(), obj=obj):
            super().delete_existing(obj, commit=commit)
        else:
            messages.warning(self.request, '{} was not deleted as you lack permission.'.format(obj))


class CIMSBaseModelFormSet(ObjectPermsModelFormSet):
    """Rebrand the ObjectPermsModelFormSet."""


class M2MValidatingInlineFormSet(forms.BaseInlineFormSet):
    """
    InlineFormSet that ensures commit is correctly passed through to each form when saving new
    objects.
    """
    def save_new(self, form, commit=True):
        """
        django.forms.BaseInlineFormSet overrides the commit value to enable tinkering with FK
        values. Testing suggests this isn't required and it breaks how we do some m2m validation
        so avoid it.
        """
        # Ensure the latest copy of the related instance is present on each form (it may  have
        # been  saved after the formset was originally instantiated).
        setattr(form.instance, self.fk.name, self.instance)
        # Effectively skip the immediate parent class
        return form.save(commit=True)


class CIMSBaseInlineFormSet(ObjectPermsModelFormSet, M2MValidatingInlineFormSet):
    """
    Base InlineModelFormSet that adds permission checking to create, update and delete actions.
    Overrides save_new so our extended validation is performed.
    """


class CIMSBaseGenericInlineFormset(BaseGenericInlineFormSet, ObjectPermsModelFormSet):
    """
    Combines object permission checks with a FormSet that handles models using GenericForeignKeys.
    """


# Classes that act as wrappers of sorts around formset factory functions.
class BaseFormSetManager:
    """
    Base class for constructing a FormSet within a view, largely a wrapper around formset_factory().

    Largely borrowed from django-extra-views, tidied up in a few spots

    Attributes:
    -----------
    crispy_helper : crispy_forms.helper.FormHelper, optional
                    Helper class to assist with rendering the formset.
    empty_form_crispy_helper : crispy_forms.helper.FormHelper, optional
                               Helper class to use with the formset empty_form as the empty_form is
                               generated dynamically, and so won't have the layout from the formset
                               helper applied to it.
    help_text : string, optional
                Help text to display to the user to help them understand/use the formset.

    Parameters
    ----------
    request : django.http.HttpRequest
              Request object for this request. Used to get the request user so we can check
              permissions.
    """

    initial = ()
    form_class = None
    formset_class = None
    success_url = None
    extra = 2
    max_num = None
    min_num = None
    validate_max = False
    validate_min = False
    can_order = False
    can_delete = False
    prefix = None
    crispy_helper = None
    empty_form_crispy_helper = None
    help_text = None

    def __init__(self, request=None, *args, **kwargs):
        self.request = request

    def construct_formset(self):
        """
        Returns an instance of the formset
        """
        FormSet = self.get_formset()
        formset = FormSet(**self.get_formset_kwargs())
        # Attach our helper to the formset. If we're using a custom layout then attach that to the
        # helper.
        if self.crispy_helper:
            formset.helper = self.get_layout(self.crispy_helper())
        # Because formset.empty_form is generated dynamically we need to provide a helper for it as
        # the formset helper won't attach a layout to it.
        if self.empty_form_crispy_helper:
            formset.empty_form_helper = self.get_layout(self.empty_form_crispy_helper())
        if self.help_text:
            formset.help_text = self.help_text
        return formset

    def get_initial(self):
        """
        Returns the initial data to use for formsets on this view.
        """
        return self.initial

    def get_formset_class(self):
        """
        Returns the formset class to use in the formset factory
        """
        return self.formset_class

    def get_form_kwargs(self):
        """
        Returns extra keyword arguments to pass to the constructor for each form in the formset.
        Note that the arguments to each form are the same. If they need to be different see
        django.formsets.BaseFormSet.get_form_kwargs().
        """
        return {}

    def get_form_class(self):
        """
        Returns the form class to use with the formset in this view
        """
        return self.form_class

    def get_formset(self):
        """
        Returns the formset class from the formset factory
        """
        return formset_factory(self.get_form_class(), **self.get_factory_kwargs())

    def get_formset_kwargs(self):
        """
        Returns the keyword arguments for instantiating the formset.
        """
        kwargs = {'form_kwargs': self.get_form_kwargs()}

        # We have to check whether initial has been set rather than blindly passing it along,
        # This is because Django 1.3 doesn't let inline formsets accept initial, and no versions
        # of Django let generic inline formset handle initial data.
        initial = self.get_initial()
        if initial:
            kwargs['initial'] = initial

        if self.prefix:
            kwargs['prefix'] = self.prefix

        if self.request and self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
                })
        return kwargs

    def get_factory_kwargs(self):
        """
        Returns the keyword arguments for calling the formset factory
        """
        kwargs = {
            'extra': self.extra,
            'max_num': self.max_num,
            'min_num': self.min_num,
            'can_order': self.can_order,
            'can_delete': self.can_delete,
            'validate_max': self.validate_max,
            'validate_min': self.validate_min,
            }

        formset_class = self.get_formset_class()
        if formset_class:
            kwargs.update({'formset': formset_class})

        return kwargs

    def get_layout(self, helper):
        """
        Get the Crispy Forms Layout object for this formset. By default do nothing and use the
        auto-generated layout.
        """
        return helper


class BaseModelFormSetManager(BaseFormSetManager):
    """
    Base class that implements functionality shared by model based formsets.

    Should be subclassed before use.

    Attributes
    ----------
    ordering : list
               An iterable of field names to use to order items in the model queryset/formset.

    Parameters
    ----------
    instance : django.db.models.Model
               Instance of the related parent model being processed in this request.
    """
    model = None
    fields = None
    exclude = None
    formfield_callback = None
    widgets = None
    localized_fields = None
    labels = None
    help_texts = None
    error_messages = None
    field_classes = None
    ordering = None

    def __init__(self, instance=None, *args, **kwargs):
        if self.model is None:
            raise ImproperlyConfigured(
                    '{} must have a model configured.'.format(self.__class__.__name__)
                    )
        super().__init__(*args, **kwargs)
        self.parent_object = instance

    def get_factory_kwargs(self):
        """
        Returns the keyword arguments for calling the formset factory
        """
        kwargs = super().get_factory_kwargs()
        kwargs.update({
            'exclude': self.exclude,
            'fields': self.fields,
            'formfield_callback': self.formfield_callback,
            'widgets': self.widgets,
            'localized_fields': self.localized_fields,
            'labels': self.labels,
            'help_texts': self.help_texts,
            'error_messages': self.error_messages,
            'field_classes': self.field_classes,
            })
        if self.get_form_class():
            kwargs['form'] = self.get_form_class()
        if self.get_formset_class():
            kwargs['formset'] = self.get_formset_class()
        return kwargs

    def get_formset_kwargs(self):
        """
        Returns the keyword arguments for instantiating the formset.
        """
        kwargs = super().get_formset_kwargs()
        queryset = self.get_queryset()
        ordering = self.get_ordering()
        if ordering:
            queryset = queryset.order_by(*ordering)
        kwargs.update({'queryset': queryset})
        return kwargs

    def get_formset(self):
        return modelformset_factory(self.model, **self.get_factory_kwargs())

    def get_label(self):
        """Return the label we want to use to identify this formset in the output template."""
        return self.model._meta.verbose_name_plural

    def get_ordering(self):
        """Return the field names to use for queryset ordering."""
        return self.ordering

    def get_queryset(self):
        """
        Set the base queryset for the formset, setting it blank if the user lacks change
        permissions.
        """
        return self.model._default_manager.get_queryset()


class BaseInlineModelFormSetManager(BaseModelFormSetManager):
    """
    Base class for constructing an inline formSet within a view, basically a wrapper around
    inlineformset_factory().

    Largely borrowed from django-extra-views, with attribute set made complete, some changes to
    support form_kwargs parameter for inlineformset_factory().

    Attributes
    ----------
    See the parameter links for django.forms.models.inlineformset_factory()
    """
    parent_model = None
    fk_name = None
    formset_class = BaseInlineFormSet

    def get_inline_model(self):
        """
        Returns the inline model to use with the inline formset
        """
        return self.model

    def get_formset_kwargs(self):
        """
        Returns the keyword arguments for instantiating the formset.
        """
        kwargs = super().get_formset_kwargs()
        # We don't pass a queryset to an InlineFormSet
        # kwargs.pop('queryset', None)
        # We pass a model instance
        kwargs['instance'] = self.parent_object
        return kwargs

    def get_factory_kwargs(self):
        """
        Returns the keyword arguments for calling the formset factory
        """
        kwargs = super().get_factory_kwargs()
        kwargs.update({'fk_name': self.fk_name})
        return kwargs

    def get_formset(self):
        """
        Returns the formset class from the inline formset factory
        """
        return inlineformset_factory(
                self.parent_model,
                self.get_inline_model(),
                **self.get_factory_kwargs()
                )


class BaseGenericInlineFormSetManager(BaseInlineModelFormSetManager):
    """
    Base FormSetManager for creating FormSets for models that use GenericForeignKeys.
    """
    ct_field = 'content_type'
    fk_field = 'object_id'
    for_concrete_model = True

    def get_factory_kwargs(self):
        kwargs = super().get_factory_kwargs()
        # Get rid of factory arguments that aren't used by generic_inlineformset_factory.
        for key in ('widgets', 'localized_fields', 'labels', 'help_texts', 'error_messages',
                    'field_classes', 'fk_name'):
            kwargs.pop(key, None)
        kwargs.update({
            'ct_field': self.ct_field,
            'fk_field': self.fk_field,
            'for_concrete_model': self.for_concrete_model
            })
        return kwargs

    def get_formset(self):
        return generic_inlineformset_factory(
                self.get_inline_model(),
                **self.get_factory_kwargs()
                )


class InlineFormSetPermissionsMixin(BaseModelFormSetManager):
    """
    Mixin to allow setting a model attribute on the FormSetManager class and checking basic
    add/update/delete permissions against it. This has to be the last mixin/superclass added
    otherwise permission checks might not happen before database operations.
    """
    def __init__(self, *args, **kwargs):
        request = kwargs.get('request', None)
        if request is None:
            raise ImproperlyConfigured(
                    '{} requires a valid request argument to perform permissions checks.'.format(
                        self.__class__.__name__
                        )
                    )
        super().__init__(*args, **kwargs)
        self.user = request.user

        # Set some some values based on whether the request user has the required permissions
        if not self.has_add_permission():
            self.max_num = 0
            self.extra = 0
        self.can_delete = self.can_delete and self.has_delete_permission()

    def get_formset_kwargs(self):
        """
        Add the request to the formset kwargs so CIMSBaseModelFormset can do permission checks.
        """
        formset_kwargs = super().get_formset_kwargs()
        formset_kwargs.update({'request': self.request})
        return formset_kwargs

    def has_permission(self):
        """
        Return if the user has the minimum set of permissions required to use the formset
        generated by this inline.
        """
        return (self.has_add_permission()
                or self.has_change_permission()
                or self.has_delete_permission())

    def has_add_permission(self, opts=None):
        """
        Return if the user has permission to add objects to this inline. `opts` is used to check
        permissions against a different model, handy with through model inlines.
        """
        opts = opts or self.model._meta
        codename = get_permission_codename('add', opts)
        return self.user.has_perm('{}.{}'.format(opts.app_label, codename))

    def has_change_permission(self, opts=None):
        """Return if the user has permission to change objects from this inline."""
        opts = opts or self.model._meta
        codename = get_permission_codename('change', opts)
        return self.user.has_perm('{}.{}'.format(opts.app_label, codename))

    def has_delete_permission(self, opts=None):
        """Return if the user has permission to delete objects from this inline."""
        opts = opts or self.model._meta
        codename = get_permission_codename('delete', opts)
        return self.user.has_perm('{}.{}'.format(opts.app_label, codename))

    def get_queryset(self):
        """
        Set the base queryset for the formset, setting it blank if the user lacks change
        permissions.
        """
        qs = super().get_queryset()
        if not self.has_change_permission():
            qs = qs.none()
        return qs


class CIMSModelFormSetManager(InlineFormSetPermissionsMixin, BaseModelFormSetManager):
    """
    Default FormSetManager for ModelFormsets which require the request in order to check
    permissions on save operations.
    """
    extra = 0
    form_class = CIMSBaseModelForm
    formset_class = CIMSBaseModelFormSet

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Add the user to the form kwargs so it can check permissions on fields.
        kwargs.update({'user': self.user})
        return kwargs


class CIMSInlineFormSetManager(InlineFormSetPermissionsMixin, BaseInlineModelFormSetManager):
    """
    Default FormSetManager for inline ModelFormsets which require the request in order to check
    permissions on save operations.
    """
    extra = 0
    form_class = CIMSBaseModelForm
    formset_class = CIMSBaseInlineFormSet

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Add the user to the form kwargs so it can check permissions on fields.
        kwargs.update({'user': self.user})
        return kwargs


class CIMSGenericInlineFormSetManager(InlineFormSetPermissionsMixin,
                                      BaseGenericInlineFormSetManager):
    extra = 0
    form_class = CIMSBaseModelForm
    formset_class = CIMSBaseGenericInlineFormset

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Add the user to the form kwargs so it can check permissions on fields.
        kwargs.update({'user': self.user})
        return kwargs


# InlineFormSetManagers
class AnnotationManager(CIMSGenericInlineFormSetManager):
    model = models.Annotation
    fields = ['note']
    max_num = 1
    extra = 1
    can_delete = True
    crispy_helper = TableInlineModalFormSetHelper
    ordering = ['-last_modified']
    help_text = 'Use these forms to attach small notes to the object being edited.'


class CourseInstanceManager(CIMSInlineFormSetManager):
    model = models.CourseInstance
    parent_model = models.CourseVersion
    fields = ['instance_descriptor', 'start_date', 'end_date', 'taught_by']
    form_class = InlineCourseInstanceForm
    widgets = {'start_date': DateInput, 'end_date': DateInput}
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    ordering = ['-start_date']


class LearningOutcomeManager(CIMSInlineFormSetManager):
    model = models.LearningOutcome
    parent_model = models.CourseVersion
    fields = ['outcome', 'outcome_short_form', 'order']
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    help_text = """
    These forms are used to create learning outcomes associated with this course version.
    """


class RelatedRoleManager(CIMSInlineFormSetManager):
    model = models.CourseRelatedRole
    parent_model = models.CourseInstance
    fields = ['user', 'role_title']
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    help_text = """
    The features these forms support are currently under development so can be ignored.
    """


class ScheduleManager(CIMSInlineFormSetManager):
    model = models.Schedule
    parent_model = models.CourseInstance
    fields = ['session_name', 'session_order', 'session_number', 'summary', 'assessment']
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    help_text = """
    Use these Schedule forms to construct a high level overview of the course schedule. As an
    example audience consider a prospective student wanting to get a brief idea of the structure of
    the course.  Going beyond what happens - e.g. lectures, labs, tutorials etc. - and what is
    covered - e.g. the lecture/lab topic - is probably more detail than required, but this is up to
    the convener.
    """


class TaskManager(CIMSInlineFormSetManager):
    model = models.Task
    parent_model = models.CourseInstance
    fields = ['title', 'description', 'weighting', 'order', 'due_date', 'task_url', 'is_exam',
              'is_hurdle', 'due_time', 'presentation_requirements', 'return_date',
              'hurdle_requirements', 'individual_in_group']
    can_delete = True
    crispy_helper = TableInlineCollapseFormSetHelper
    empty_form_crispy_helper = BaseFormHelper
    widgets = {'due_date': DateInput, 'due_time': TimeInput}
    help_text = """
    The primary role of these Task forms is to capture basic information about assessment tasks
    during the course. To that end the main information to input is: a title, a brief description
    of the task, what weighting it contributes to the final mark, and when the task is
    due/performed. The additional fields allow for providing more specific detail about the task
    but are not required.  Tasks which are not assessed can also be detailed here if desired e.g.
    labs with no assessable component, by simply ignore the weighting field.
    """

    def get_layout(self, helper):
        helper.layout = Layout(
                Div(
                        Field('title', wrapper_class='col-md-6'),
                        Field('description', wrapper_class='col-md-6'),
                        css_class='form-row'
                        ),
                Div(
                        Field('weighting', wrapper_class='col-md-4'),
                        Field('order', wrapper_class='col-md-4'),
                        css_class='form-row'
                        ),
                Div(
                        Field('due_date', wrapper_class='col-md-4'),
                        Field('due_time', wrapper_class='col-md-4'),
                        css_class='form-row'
                        ),
                Div(
                        Field('return_date', wrapper_class='col-md-4'),
                        Field('task_url', wrapper_class='col-md-4'),
                        css_class='form-row'
                        ),
                'is_exam',
                'is_hurdle',
                Div(
                        Field('presentation_requirements', wrapper_class='col-md-4'),
                        Field('hurdle_requirements', wrapper_class='col-md-4'),
                        Field('individual_in_group', wrapper_class='col-md-4'),
                        css_class='form-row'
                        ),
                )
        return helper


class CVInlineWeightedLOToSOManager(CIMSModelFormSetManager):
    model = models.WeightedLOToSO
    fields = ['learningoutcome', 'standardobjective', 'weights']
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    ordering = ['learningoutcome__order']
    help_text = """
    The forms below allow you to link competencies/objectives to this learning outcome. 
    Available weights come from weighting schemes associated with the higher level course 
    version. Only one weight from each scheme can be attached to an individual link.
    """

    def get_formset(self):
        formset_class = super().get_formset()
        kwargs = {'courseversion': self.parent_object}
        set_form_querysets(formset_class.form, self.fields, **kwargs)
        return formset_class

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(learningoutcome__courseversion=self.parent_object)


class LOInlineWeightedLOToSOManager(CIMSInlineFormSetManager):
    model = models.WeightedLOToSO
    parent_model = models.LearningOutcome
    fields = ['standardobjective', 'weights']
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    ordering = ['standardobjective__name']
    help_text = """
    The forms below allow you to link competencies/objectives to this learning outcome. Available 
    weights come from weighting schemes associated with the higher level Course Version. Only one 
    weight from each scheme can be associated with an individual link.
    """

    def get_formset(self):
        formset_class = super().get_formset()
        kwargs = {'courseversion': self.parent_object.courseversion}
        set_form_querysets(formset_class.form, self.fields, **kwargs)
        return formset_class


class CIInlineWeightedTaskToLOManager(CIMSModelFormSetManager):
    model = models.WeightedTaskToLO
    fields = ['task', 'learningoutcome', 'weights']
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    ordering = ['task__order']
    help_text = """
    The forms below allow you to link Tasks to Learning Outcomes. Available weights come from 
    weighting schemes associated with the higher level Course Version.
    """

    def get_formset(self):
        formset_class = super().get_formset()
        kwargs = {'courseinstance': self.parent_object}
        set_form_querysets(formset_class.form, self.fields, **kwargs)
        return formset_class

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(task__courseinstance=self.parent_object)


class TaskInlineWeightedTaskToLOManager(CIMSInlineFormSetManager):
    model = models.WeightedTaskToLO
    parent_model = models.Task
    fields = ['learningoutcome', 'weights']
    can_delete = True
    crispy_helper = TableInlineFormSetHelper
    ordering = ['learningoutcome__order']
    help_text = """
    The forms below allow you to link this task to learning outcomes. Available weights come from 
    weighting schemes associated with the higher level Course Version. Only one weight from each 
    scheme can be associated with an individual link.
    """

    def get_formset(self):
        formset_class = super().get_formset()
        kwargs = {'courseinstance': self.parent_object.courseinstance}
        set_form_querysets(formset_class.form, self.fields, **kwargs)
        return formset_class
