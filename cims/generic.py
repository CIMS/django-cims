"""
Generic views mixins/classes.
"""
from collections import OrderedDict

import reversion
from django.contrib import messages
from django.contrib.admin.models import ADDITION, CHANGE
from django.contrib.admin.utils import construct_change_message
from django.contrib.auth import get_permission_codename
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.forms import models as model_forms
from django.forms.formsets import all_valid
from django.http import HttpResponseRedirect
from django.views import generic
from django.views.generic.detail import SingleObjectMixin, SingleObjectTemplateResponseMixin
from django.views.generic.edit import FormView, ModelFormMixin

from cims import forms
from cims.auth.mixins import ObjectPermissionRequiredMixin
from cims.formsets import AnnotationManager, formset_change_message
from cims.models import Annotation
from cims.utils.logging import create_logentry, json_dumps_list
from cims.utils.misc import get_fields_for_model


def make_dict_of_model_fields(model):
    """Return a dict of structure {'field_name': Field()} for a given model."""
    return dict([(f.name, f) for f in get_fields_for_model(model)])


class CIMSViewError(Exception):
    """
    Exception for when we don't want to raise/catch a more standard exception in case we
    interfere with something generated deep in the plumbing of Django.
    """


class CIMSSingleObjectMixin(SingleObjectMixin):
    """Extend SingleObjectMixin to add context data that we typically want in our templates."""
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        # Add title
        opts = self.model._meta
        context['opts'] = opts
        # Add links to CRU actions
        context['obj_links'] = {
                'create': self.model.objects.get_create_url()
                }
        instance = self.get_object()
        context['title'] = '{}: {}'.format(opts.verbose_name, str(instance))
        context['obj_links'].update(
                {
                    'detail': instance.get_absolute_url(),
                    'update': instance.get_update_url(),
                    }
                )

        # Add what permissions this user has w.r.t. the model to the context
        codename = get_permission_codename('add', opts)
        context['has_add_permission'] = user.has_perm('{}.{}'.format(opts.app_label, codename))
        codename = get_permission_codename('change', opts)
        # Check object permissions
        obj = self.get_object()
        context['has_change_permission'] = user.has_perm(
                '{}.{}'.format(opts.app_label, codename),
                obj=obj
                )
        return context


class ProcessFormWithInlinesView(FormView):
    """
    View mixin that renders a form with its inline formsets on GET and process it on POST.
    """
    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form and formsets.
        """
        form = self.get_form()
        formsets = self.construct_inlines()
        return self.render_to_response(
                self.get_context_data(form=form, formsets=formsets, **kwargs)
                )

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        formsets = self.construct_inlines()
        return self.forms_invalid(form, formsets)

    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)


class ModelFormWithInlinesMixin(ModelFormMixin):
    """
    Extend ModelFormMixin to support processing inline formsets.

    Constructs formsets from a set of manager classes, saves formsets, creates a revision, and
    makes changelog entries of what has changed.

    Attributes
    ----------
    inlines : list
              A list of cims.formsets.InlineFormSetManager classes to generate formsets from.
    """
    inlines = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_inlines(self):
        """Return the inline formset classes."""
        return self.inlines or []

    def construct_inlines(self):
        """
        Construct the inline formsets. Return an OrderedDict of formsets with a label as the key.
        """
        formsets = OrderedDict()
        for Inline in self.get_inlines():
            inline = Inline(request=self.request, instance=self.object)
            if inline.has_permission():
                formsets[inline.get_label()] = (inline, inline.construct_formset())

        return formsets

    def add_formset_change_messages(self, formsets, change_message):
        """
        Append details of changes to formset items to change message.

        Parameters
        ----------
        formsets : iterable
                   iterable of formsets whose changes will be added to the change message
        change_message : list
                         List of JSON structures detailing the change message.
        """
        for formset in formsets:
            change_message = formset_change_message(formset, change_message)
        return change_message

    def form_valid(self, form):
        """
        Save the form and formsets that have passed validation.

        Creates a revision storing the changes, rolls back any changes if post-save validation
        fails. Logs changelog messages detailing what was changed.
        """
        # Creating a revision wraps all this in a transaction, so if any forms have an error we
        # need to raise an exception and catch it outside the transaction block, otherwise the
        # transaction won't roll back.
        add = False
        action = CHANGE
        add_message = '{} created successfully.'
        update_message = '{} updated successfully.'
        try:
            with reversion.create_revision():
                if self.object is None or self.object.pk is None:
                    add = True
                    action = ADDITION
                # Save the form, formsets, then run validation one more time to ensure we catch all
                # possible errors.
                self.object = form.save()
                formset_details = self.construct_inlines()
                formsets = [formset for manager, formset in formset_details.values()]
                if all_valid(formsets):
                    formsets = self.save_formsets(formsets)
                    if hasattr(self.object, 'validate'):
                        try:
                            self.object.validate(full_clean=False)
                        except ValidationError as e:
                            form.add_error(None, e)
                # formsets = self.save_formsets(formsets)
                # If we fail post save validation the form was invalid!
                if form.errors or not all_valid(formsets):
                    raise CIMSViewError('I should have been caught...')

                change_message = construct_change_message(form, formsets=[], add=add)
                change_message = self.add_formset_change_messages(formsets, change_message)
                reversion.set_user(self.request.user)
                reversion.set_comment(json_dumps_list(change_message))
                create_logentry(self.request, self.object, change_message, action=action)
        # Catch the CIMSViewsError that was raised to make sure the transaction rolls back
        except CIMSViewError:
            return self.forms_invalid(form, formset_details)

        if add:
            msg = add_message
        else:
            msg = update_message
        messages.success(self.request, msg.format(self.object))
        return HttpResponseRedirect(self.get_success_url())

    def forms_invalid(self, form, formsets):
        """
        If the form or formsets are invalid, re-render the context data with the
        data-filled form, formsets and errors.
        """
        messages.error(
                self.request,
                'The form failed validation, please fix the highlighted errors.'
                )
        return self.render_to_response(self.get_context_data(form=form, formsets=formsets))

    def get_form_class(self):
        """
        Extend to regenerate the form class if not explicitly specified, using CIMSBaseModelForm as
        the base form class so we get per-field permissions.
        """
        Form = super().get_form_class()
        if issubclass(Form, forms.CIMSBaseModelForm):
            return Form
        return model_forms.modelform_factory(
                Form.Meta.model,
                form=forms.CIMSBaseModelForm,
                fields=list(Form.base_fields)
                )

    def get_form_kwargs(self):
        """Add the request user to the form kwargs if we're changing an object."""
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def save_formsets(self, formsets):
        """
        Save each formset. If they're all ModelFormSets we can handle them here, otherwise this
        method will need overriding.
        """
        for formset in formsets:
            if hasattr(formset, 'save') and callable(formset.save):
                formset.save(commit=True)
            else:
                raise ImproperlyConfigured('{0}.save_formsets() must be overriden if handling '
                                           'FormSets without save() methods.'.format(
                                               self.__class__.__name__
                                               )
                                           )
        return formsets


# Base generic views
class BaseCreateWithInlinesView(ModelFormWithInlinesMixin, ProcessFormWithInlinesView):
    """
    Base create view for creating an instance and its related objects.

    Needs subclassing with a response mixin.
    """
    def get(self, request, *args, **kwargs):
        self.object = None
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = None
        return super().post(request, *args, **kwargs)


class CIMSCreateView(UserPassesTestMixin,
                     ObjectPermissionRequiredMixin,
                     SingleObjectTemplateResponseMixin,
                     BaseCreateWithInlinesView):
    """
    Extend BaseCreateWithInlinesView to require a logged in user, and permission to edit this
    object.
    """
    template_name_suffix = '_form'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Add title
        opts = self.model._meta
        context['opts'] = opts
        context['title'] = 'Add new {}'.format(opts.verbose_name)
        return context

    def test_func(self):
        return self.request.user.is_staff


class BaseDetailView(generic.DetailView):
    """
    Add support for related model managers, to help make templating etc. a bit more generic.

    Attributes
    ----------
    tab_helpers: list, optional
                             List of DetailViewTabHelper classes to be rendered in the view.
    fields : list, optional
             A list of names of fields from the model to display in the view. Fields not listed
             here but listed self.layout will not be displayed.
    layout : list, optional
                 A list of two tuples of shape ('name', ('field name', 'field name',..)) to
                 indicate which fields to display in some form of collapsable div or similar, with
                 the name used to title the div. If not empty the first entry should have `None`
                 for a name and contain fields to be displayed outside any accordion.
    """
    tab_helpers = None
    fields = None
    layout = None

    def build_layout(self):
        """
        Use `self.fields` and `self.layout` to build a context variable that looks like:
        [('block', [(field_class, value)])]

        Use this variable to help automate the creation of detail displays of models.
        """
        layout = []
        if self.fields and self.layout:
            for name, fields in self.layout:
                layout.append(
                        (name, [
                            (self.model._meta.get_field(fname), getattr(self.object, fname))
                            for fname in fields
                            ])
                        )
        elif self.fields:
            layout.append(
                    (None, [
                        (self.model._meta.get_field(fname), getattr(self.object, fname))
                        for fname in self.fields
                        ])
                    )

        return layout

    def get_extra_links(self):
        """
        Return a dict of structure {'label': url} to populate a nav dropdown list of extra
        operations to perform with this model instance.
        """
        return {}

    def get_tab_helpers(self) -> list:
        """Get the related model managers."""
        self.tab_helpers = self.tab_helpers or []
        helpers = [Helper(self.model, self.object, self.request) for Helper in self.tab_helpers]
        return [helper for helper in helpers if helper.has_permission(self.request)]

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        # Add the fields, layout and any configured related model helpers.
        context['fields'] = self.fields
        context['layout'] = self.build_layout()
        context['tab_helpers'] = self.get_tab_helpers()
        context['extra_links'] = self.get_extra_links()
        return context


class CIMSDetailView(CIMSSingleObjectMixin, BaseDetailView):
    """Extend the standard DetailView with CIMSSingleObjectMixin"""
    template_name = 'cims/model_detail.html'


class BaseUpdateWithInlinesView(CIMSSingleObjectMixin,
                                ModelFormWithInlinesMixin,
                                ProcessFormWithInlinesView):
    """
    Base update view for updating an instance and its related objects.

    Needs subclassing with a response mixin.
    """
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)


class CIMSUpdateView(LoginRequiredMixin,
                     ObjectPermissionRequiredMixin,
                     SingleObjectTemplateResponseMixin,
                     BaseUpdateWithInlinesView):
    """
    Extend BaseUpdateWithInlinesView to require a logged in user, and permission to edit this
    object.
    """
    template_name_suffix = '_form'

    def construct_inlines(self):
        formsets = super().construct_inlines()
        # Add the AnnotationManager to the inlines list so it gets processed/saved.
        note_manager = AnnotationManager(request=self.request, instance=self.object)
        formsets[note_manager.get_label()] = (note_manager, note_manager.construct_formset())
        return formsets

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Add a queryset of Annotations to be rendered under the page title.
        context['annotations'] = Annotation.objects.filter(
                object_id=self.object.pk,
                content_type=ContentType.objects.get_for_model(self.object)
                )
        # Extract the AnnotationManager from the dict of formset managers so it can be rendered
        # separately. Annotations inline was the last inline to be added to the OrderedDict
        formsets = context['formsets']
        context['annotations_inline'] = formsets.popitem()[1]
        return context
