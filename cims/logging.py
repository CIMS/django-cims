"""
Logging config for CIMS application.
"""
import sys


LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'cims.auth': {
                'format': '[%(asctime)s] %(message)s',
                },
            },
        'handlers': {
            'cims.auth': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'stream': sys.stdout,
                'formatter': 'cims.auth',
                },
            },
        'loggers': {
            'cims': {},
            'cims.auth': {
                'level': 'INFO',
                'handlers': ['cims.auth'],
                },
            },
        }
