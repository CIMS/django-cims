from django import template
from django.contrib.admin.templatetags.admin_modify import submit_row
from itertools import chain


register = template.Library()


@register.simple_tag(takes_context=True)
def render_tab_helper(context, tab_helper):
    """Simple tag to get the current context to the render method of a DetailViewTabHelper"""
    return tab_helper.render(context)


# Tags for creating Learning Outcome <-> Standards mappings
@register.inclusion_tag('cims/standards_mapping_summary.html', takes_context=True)
def standards_mapping_summary(context, objectives):
    context = {
            'objectives': objectives,
            'version': context['version'],
            }
    if objectives and objectives.first().is_leaf_node():
        context.update({'linked_objectives': context['version'].objectives.all()})
    return context


@register.inclusion_tag('cims/lo_standards_mapping_table.html')
def lo_standards_mapping_table(version, nodes):
    """Generate a table of learning outcomes to taxonomy mappings."""
    recurse = False
    if nodes and not nodes[0].is_leaf_node():
        recurse = True

    context = {
            'recurse': recurse,
            'nodes': nodes,
            'version': version,
            }

    if recurse:
        context.update({
            'child_nodes': list(chain.from_iterable([node.get_children() for node in nodes]))
            })
    else:
        context.update({
                'outcomes': version.learningoutcome_set.all().prefetch_related('objectives')
                })
    return context


@register.inclusion_tag('cims/assessment_lo_mapping.html',
                        takes_context=True)
def assessment_lo_mapping(context):
    """Generate assessment task to learning outcome mapping table."""
    tasks = []
    instance = context['version'].get_instance_for_year(context['year'])
    if instance:
        tasks = instance.task_set.filter(weighting__gt=0).order_by('pk')
    return {
            'outcomes': context['version'].learningoutcome_set.all(),
            'tasks': tasks,
            }


@register.inclusion_tag('cims/wizard_submit_row.html', takes_context=True)
def wizard_submit_row(context):
    ctx = submit_row(context)
    return ctx
