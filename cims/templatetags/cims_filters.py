import re

from bleach.linkifier import Linker
from django import template
from django.template.defaultfilters import stringfilter
from django.utils.encoding import force_text
from django.utils.safestring import SafeData, mark_safe

register = template.Library()
linker = Linker(parse_email=True)


@register.filter
def get_item(source_dict, key):
    """
    Return the value of key in source_dict.
    """
    return source_dict.get(key, None)


@register.filter
def stripcomp(str1, str2):
    """Compare two strings once stripped of whitespace."""
    str1 = re.sub(r'\s', '', force_text(str1))
    str2 = re.sub(r'\s', '', force_text(str2))
    return str1 == str2


@register.filter
@stringfilter
def extract_requisite(str1):
    """Extract the course codes from a course prerequisites statement."""
    match = re.search(r'(ENGN|COMP).*?\.', str1)
    if match:
        return match.group()
    return None


@register.filter
@stringfilter
def linkify(str1):
    """Linkify given text using bleach library"""
    lnkd = linker.linkify(str1)
    if isinstance(str1, SafeData):
        return mark_safe(lnkd)
    return lnkd


@register.filter
def fields_have_errors(form, fields):
    """Return if any of `fields` in `form` have errors."""
    for f in fields:
        if f in form.errors:
            return True
    return False


@register.filter
def contains(iterable, item):
    """Return if item is in iterable."""
    return item in iterable


@register.filter
def prefix(string, the_prefix):
    """Add the prefix to the string."""
    return the_prefix + string


@register.filter
def suffix(string, the_suffix):
    """Add the suffix to the string."""
    return string + the_suffix
