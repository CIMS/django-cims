from django.conf.urls import url, include
from cims import views


app_name = 'cims'

urlpatterns = [
    url(r'^$', views.CourseList.as_view(), name='home'),
    url(r'^help/$', views.get_assistance, name='help'),
    url(r'accounts/', include('django.contrib.auth.urls')),
    # Users
    url(r'^accounts/profile/$', views.user_profile,
        name='user.profile_home'),
    url(r'^accounts/(?P<pk>[0-9]+)/$', views.UserProfile.as_view(), name='user.profile'),
    # url(r'^courses/select/$', views.course_select, name='courses.select'),
    url(r'^courses/$', views.CourseList.as_view(), name='courses.list'),
    url(r'^courses/(?P<pk>[0-9]+)/$', views.CourseDetail.as_view(),
        name='course.detail'),
    url(r'^courses/(?P<code>[A-Za-z]{4}[0-9]{4}[A-Za-z]?)/$', views.CourseDetail.as_view(),
        name='course.detail'),
    # Course Versions
    url(r'^courses/versions/add/$', views.CourseVersionCreate.as_view(),
        name='courseversion.create'),
    url(r'^courses/versions/(?P<pk>[0-9]+)/$', views.CourseVersionDetail.as_view(),
        name='courseversion.detail'),
    url(r'^courses/versions/(?P<pk>[0-9]+)/edit/$', views.CourseVersionUpdate.as_view(),
        name='courseversion.update'),
    # Course Instances
    url(r'^courses/instances/add/$', views.CourseInstanceCreate.as_view(),
        name='courseinstance.create'),
    url(r'^courses/instances/(?P<pk>[0-9]+)/$', views.CourseInstanceDetail.as_view(),
        name='courseinstance.detail'),
    url(r'^courses/instances/(?P<pk>[0-9]+)/edit/$', views.CourseInstanceUpdate.as_view(),
        name='courseinstance.update'),
    url(r'^courses/instances/(?P<pk>[0-9]+)/outline/$', views.CourseOutline.as_view(),
        name='courseinstance.outline'),
    url(r'^courses/instances/(?P<pk>[0-9]+)/import/$', views.courseinstance_import,
        name='courseinstance.import'),
    # Standards Bodies etc.
    url(r'^standards/$', views.StandardsBodyList.as_view(), name='standards.list'),
    url(r'^standards/(?P<pk>[0-9]+)/$', views.StandardsBodyDetail.as_view(),
        name='standardsbody.detail'),
    url(r'^standards/frameworks/(?P<pk>[0-9]+)/$', views.StandardsFrameworkDetail.as_view(),
        name='standardsframework.detail'),
    url(r'^courses/versions/(?P<version_id>[0-9]+)/mapping/$', views.course_standards_map,
        name='objectives.mapping'),
    # Conveners
    url(r'^conveners/$', views.ConvenerList.as_view(), name='conveners.list'),
    # Model CLRU views
    # LearningOutcomes
    url(r'^learningoutcomes/add/$', views.LearningOutcomeCreate.as_view(),
        name='learningoutcome.create'),
    url(r'^learningoutcomes/(?P<pk>[0-9]+)/$', views.LearningOutcomeDetail.as_view(),
        name='learningoutcome.detail'),
    url(r'^learningoutcomes/(?P<pk>[0-9]+)/edit/$', views.LearningOutcomeUpdate.as_view(),
        name='learningoutcome.update'),
    # Schedules
    url(r'^schedules/add/$', views.ScheduleCreate.as_view(),
        name='schedule.create'),
    url(r'^schedules/(?P<pk>[0-9]+)/$', views.ScheduleDetail.as_view(),
        name='schedule.detail'),
    url(r'^schedules/(?P<pk>[0-9]+)/edit/$', views.ScheduleUpdate.as_view(),
        name='schedule.update'),
    # Tasks
    url(r'^tasks/add/$', views.TaskCreate.as_view(),
        name='task.create'),
    url(r'^tasks/(?P<pk>[0-9]+)/$', views.TaskDetail.as_view(),
        name='task.detail'),
    url(r'^tasks/(?P<pk>[0-9]+)/edit/$', views.TaskUpdate.as_view(),
        name='task.update'),
    ]
