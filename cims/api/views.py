"""
API views for the CIMS application
"""
from django.contrib.admin.models import CHANGE
from django.core.exceptions import ObjectDoesNotExist
from django_auth_ldap.backend import LDAPBackend
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.filters import OrderingFilter
from rest_framework.mixins import DestroyModelMixin
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from cims.models import (AcademicCareer, Annotation, Course, CourseClassification, CourseVersion,
                         CourseInstance, CourseRelatedRole, CourseRelatedRoleTitle, DeliveryMode,
                         LearningOutcome, OrganisationalUnit, ReviewStatus, Schedule, Task,
                         StandardsBody, StandardsFramework, StandardObjective, TeachingSession,
                         Weight, WeightingScheme, WeightedLOToSO, WeightedTaskToLO)
from cims.auth.models import User, Role
from cims.api import filters
from cims.api.mixins import VersionedPFPUpdateModelMixin
from cims.api.permissions import DjangoObjectPermissionsAnyOfOrNothing
from cims.api.serializers import (AcademicCareerSerializer, AnnotationSerializer, CourseSerializer,
                                  CourseClassificationSerializer, CourseVersionSerializer,
                                  CourseInstanceSerializer, CourseRelatedRoleSerializer,
                                  CourseRelatedRoleTitleSerializer, DeliveryModeSerializer,
                                  LearningOutcomeSerializer, ScheduleSerializer, TaskSerializer,
                                  StandardsBodySerializer, StandardsFrameworkSerializer,
                                  StandardObjectiveSerializer, ReviewStatusSerializer,
                                  TeachingSessionSerializer, UserSerializer, WeightSerializer,
                                  WeightingSchemeSerializer, WeightedLOToSOSerializer,
                                  RoleSerializer, OrganisationalUnitSerializer,
                                  WeightedTaskToLOSerializer)
from cims.api.viewsets import LRViewset, CIMSBaseCLRUViewSet
from cims.utils.logging import construct_change_message, create_logentry


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated, IsAdminUser])
def set_ldap_user(request):
    """
    Allows for prepopulation of a user from LDAP (useful for setting up permission before a user
    logs in for the first time). If used with an existing user sets their local password unusable,
    forcing them to authenticate via LDAP.

    Uses a single POST parameter of `username` to perform both functions.
    """
    if request.method == 'GET':
        return Response()

    add_perm = 'cims_auth.add_user'
    change_perm = 'cims_auth.change_user__password'

    if 'username' in request.data:
        username = request.data.get('username')
    else:
        return Response(
                {'detail': 'A username argument is required.'},
                status=status.HTTP_400_BAD_REQUEST,
                )

    # Check if the user already exists, and if so set an unusable password. Requires permission to
    # change the password field for a user. Log the change and create a revision
    try:
        user_obj = User.objects.get(username=username)
        if request.user.has_perm(change_perm, user_obj):
            user_obj.set_unusable_password()
            user_obj.save()
            message = construct_change_message(['password'])
            create_logentry(request, user_obj, message, action=CHANGE)
            serializer = UserSerializer(user_obj)
            return Response(serializer.data)
        else:
            return Response(
                {'detail': 'You do not have permission to perform this action.'},
                status=status.HTTP_403_FORBIDDEN,
                )
    except ObjectDoesNotExist:
        pass

    # User doesn't exist in our database yet so check if they exist in LDAP, and create them if
    # they do. Requires user add permission
    if request.user.has_perm(add_perm):
        backend = LDAPBackend()
        user_obj = backend.populate_user(username)
        if user_obj is None:
            return Response({'detail': 'Not found.'}, status=status.HTTP_404_NOT_FOUND)

        message = construct_change_message(create=True)
        create_logentry(request, user_obj, message)
        serializer = UserSerializer(user_obj)
        return Response(serializer.data)

    return Response(
            {'detail': 'You do not have permission to perform this action.'},
            status=status.HTTP_403_FORBIDDEN,
            )


# Model based views
class AcademicCareerViewSet(CIMSBaseCLRUViewSet):
    queryset = AcademicCareer.objects.all()
    serializer_class = AcademicCareerSerializer


class AnnotationViewSet(LRViewset):
    queryset = Annotation.objects.all()
    serializer_class = AnnotationSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering = ['created']
    ordering_fields = ['id', 'created']


class CourseViewSet(CIMSBaseCLRUViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.CourseFilter
    ordering = ['code']
    ordering_fields = ['id', 'created']


class CourseClassificationViewSet(CIMSBaseCLRUViewSet):
    queryset = CourseClassification.objects.all()
    serializer_class = CourseClassificationSerializer


class CourseInstanceViewSet(DestroyModelMixin, CIMSBaseCLRUViewSet):
    queryset = CourseInstance.objects.all().prefetch_related(
            'taught_by', 'conveners', 'delivery_group', 'schedule_set', 'task_set', 'relatedroles'
            )
    serializer_class = CourseInstanceSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.CourseInstanceFilter
    ordering_fields = ['id', 'start_date', 'courseversion__course__code']


class CourseRelatedRoleViewSet(CIMSBaseCLRUViewSet):
    queryset = CourseRelatedRole.objects.all()
    serializer_class = CourseRelatedRoleSerializer


class CourseRelatedRoleTitleViewSet(CIMSBaseCLRUViewSet):
    queryset = CourseRelatedRoleTitle.objects.all()
    serializer_class = CourseRelatedRoleTitleSerializer


class CourseVersionViewSet(CIMSBaseCLRUViewSet):
    queryset = CourseVersion.objects.all().prefetch_related(
            'custodians', 'course_group', 'standards_frameworks', 'objectives', 'weightingschemes',
            'learningoutcome_set', 'courseinstance_set'
            )
    serializer_class = CourseVersionSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.CourseVersionFilter
    ordering = ['course__code']
    ordering_fields = ['id', 'course__code']


class DeliveryModeViewSet(CIMSBaseCLRUViewSet):
    queryset = DeliveryMode.objects.all()
    serializer_class = DeliveryModeSerializer


class LearningOutcomeViewSet(DestroyModelMixin, CIMSBaseCLRUViewSet):
    queryset = LearningOutcome.objects.all().prefetch_related('objectives')
    serializer_class = LearningOutcomeSerializer


class OrganisationalUnitViewSet(CIMSBaseCLRUViewSet):
    queryset = OrganisationalUnit.objects.all()
    serializer_class = OrganisationalUnitSerializer


class ReviewStatusViewSet(CIMSBaseCLRUViewSet):
    queryset = ReviewStatus.objects.all()
    serializer_class = ReviewStatusSerializer


class RoleViewSet(CIMSBaseCLRUViewSet):
    # Require users to have add or change permissions to view any roles
    permission_classes = (DjangoObjectPermissionsAnyOfOrNothing,)
    queryset = Role.objects.all()
    serializer_class = RoleSerializer


class ScheduleViewSet(CIMSBaseCLRUViewSet):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer


class StandardsBodyViewSet(CIMSBaseCLRUViewSet):
    queryset = StandardsBody.objects.all()
    serializer_class = StandardsBodySerializer


class StandardsFrameworkViewSet(CIMSBaseCLRUViewSet):
    queryset = StandardsFramework.objects.all()
    serializer_class = StandardsFrameworkSerializer


class StandardObjectiveViewSet(CIMSBaseCLRUViewSet):
    queryset = StandardObjective.objects.all()
    serializer_class = StandardObjectiveSerializer


class TaskViewSet(CIMSBaseCLRUViewSet):
    queryset = Task.objects.all().prefetch_related('learningoutcomes')
    serializer_class = TaskSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.TaskFilter


class TeachingSessionViewSet(CIMSBaseCLRUViewSet):
    queryset = TeachingSession.objects.all()
    serializer_class = TeachingSessionSerializer


class UserViewSet(VersionedPFPUpdateModelMixin, LRViewset):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.UserFilter
    ordering = ['username']
    ordering_fields = ['username', 'first_name', 'last_name', 'email']

    def get_queryset(self):
        # If you can't edit users you only get yourself
        user = self.request.user
        if user.has_perm('cims_auth.change_user'):
            return super().get_queryset()

        return User.objects.filter(pk=user.pk)


class WeightViewSet(CIMSBaseCLRUViewSet):
    queryset = Weight.objects.all()
    serializer_class = WeightSerializer


class WeightingSchemeViewSet(CIMSBaseCLRUViewSet):
    queryset = WeightingScheme.objects.all()
    serializer_class = WeightingSchemeSerializer


class WeightedLOToSOViewSet(CIMSBaseCLRUViewSet):
    queryset = WeightedLOToSO.objects.all()
    serializer_class = WeightedLOToSOSerializer


class WeightedTaskToLOViewSet(CIMSBaseCLRUViewSet):
    queryset = WeightedTaskToLO.objects.all()
    serializer_class = WeightedTaskToLOSerializer
