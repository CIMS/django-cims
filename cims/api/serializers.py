"""
REST framework serializers for the CIMS application API.
"""
from django.db import transaction
from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import (as_serializer_error, ModelSerializer,
                                        PrimaryKeyRelatedField, HyperlinkedModelSerializer)

from cims.api.fields import BleachField
from cims.models import (AcademicCareer, Annotation, Course, CourseClassification, CourseVersion,
                         CourseInstance, DeliveryMode, LearningOutcome, CourseRelatedRoleTitle,
                         CourseRelatedRole, OrganisationalUnit, ReviewStatus, Schedule, Task,
                         StandardsBody, StandardsFramework, StandardObjective, TeachingSession,
                         Weight, WeightingScheme, WeightedLOToSO, WeightedTaskToLO)
from cims.auth.models import Role, User


# ModelSerializer subclasses
class ValidatedModelSerializer(ModelSerializer):
    """
    `ModelSerializer` that performs additional validation of `create()` and `update()`, rolls back
    the database transaction if validation fails.
    """
    def validated_save(self, validated_data, instance=None):
        try:
            with transaction.atomic():
                if instance is None:
                    instance = super().create(validated_data)
                else:
                    instance = super().update(instance, validated_data)
                assert hasattr(instance, 'validate'), ('ValidatedModelSerializer must be used '
                                                       'with a model class using '
                                                       'ValidatedModelMixin.')
                instance.validate()

        # Massage exceptions into rest framework format and re-raise to ensure transaction rolls
        # back
        except (ValidationError, DjangoValidationError) as e:
            if hasattr(e, 'message_dict'):
                e = ValidationError(detail=e.message_dict)
            else:
                e = ValidationError(detail=e.messages)
            raise ValidationError(detail=as_serializer_error(e))

        return instance

    def create(self, validated_data):
        """Wrap parent `create()` in a transaction, and do additional validation."""
        return self.validated_save(validated_data)

    def update(self, instance, validated_data):
        """Wrap parent `update()` in a transaction, and do additional validation."""
        return self.validated_save(validated_data, instance=instance)


class FilteredFieldsModelSerializer(ModelSerializer):
    """
    Filter allowed fields based on `disabled_fields` argument passed __init__.

    Parameters
    ----------
    disabled_fields : List, optional
                      A list of field names to disable
    disable_fields : bool, optional
                     Whether to set fields read_only or remove them from the serializer.
    """
    def __init__(self, *args, **kwargs):
        disabled_fields = kwargs.pop('disabled_fields', None)
        self.disable_fields = kwargs.pop('disable_fields', True)
        super().__init__(*args, **kwargs)
        self.disabled_fields = []
        if disabled_fields:
            self._filter_fields(disabled_fields)

    def _filter_fields(self, disabled_fields):
        """Do the field filtering."""
        # Iterate through fields, disabling or removing if the user lacks sufficient permission
        serializer_fields = list(self.fields.keys())
        for fname in disabled_fields:
            # Serializers might not expose all model fields.
            if fname in serializer_fields:
                self.disabled_fields.append(fname)
                if self.disable_fields:
                    self.fields[fname].read_only = True
                else:
                    self.fields.pop(fname)


# CIMS Base serializer classes - data model serializers must use one of these
class CIMSBaseModelSerializer(ValidatedModelSerializer, FilteredFieldsModelSerializer):
    pass


class CIMSLinkedModelSerializer(CIMSBaseModelSerializer, HyperlinkedModelSerializer):
    pass


# Concrete Serializer classes
class AcademicCareerSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = AcademicCareer
        fields = ('id', 'created', 'last_modified', 'career')


class AnnotationSerializer(CIMSBaseModelSerializer):
    content_type = PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Annotation
        # Don't serialize the GenericForeignKey to avoid having to muck around with generating URLs
        fields = ('id', 'created', 'last_modified', 'content_type', 'object_id', 'note', 'version')
        read_only_fields = ('content_type', 'object_id', 'version')


class CourseSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'created', 'last_modified', 'code')


class CourseClassificationSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = CourseClassification
        fields = ('id', 'created', 'last_modified', 'classification')


class CourseVersionSerializer(CIMSBaseModelSerializer):
    description = BleachField(required=False, allow_blank=True)
    prerequisite_courses = BleachField(required=False, allow_blank=True)
    corequisite_courses = BleachField(required=False, allow_blank=True)
    incompatible_courses = BleachField(required=False, allow_blank=True)
    assumed_knowledge = BleachField(required=False, allow_blank=True)
    research_led_teaching = BleachField(required=False, allow_blank=True)
    required_resources = BleachField(required=False, allow_blank=True)
    additional_costs = BleachField(required=False, allow_blank=True)
    participation = BleachField(required=False, allow_blank=True)
    field_trips = BleachField(required=False, allow_blank=True)
    recommended_resources = BleachField(required=False, allow_blank=True)
    other_information = BleachField(required=False, allow_blank=True)

    class Meta:
        model = CourseVersion
        fields = ('id', 'created', 'last_modified', 'course', 'version_descriptor',
                  'status', 'status_freetext', 'approval_date', 'approval_description', 'title',
                  'description', 'custodians', 'delivery_mode', 'course_group', 'unit_value',
                  'classification', 'academiccareer', 'standards_frameworks', 'objectives',
                  'weightingschemes', 'prerequisite_courses', 'corequisite_courses',
                  'incompatible_courses', 'assumed_knowledge', 'research_led_teaching',
                  'required_resources', 'additional_costs', 'participation', 'field_trips',
                  'recommended_resources', 'other_information', 'learningoutcome_set',
                  'courseinstance_set')
        read_only_fields = ('learningoutcome_set', 'courseinstance_set')


class CourseInstanceSerializer(CIMSBaseModelSerializer):
    inherent_requirements = BleachField(required=False, allow_blank=True)
    workload = BleachField(required=False, allow_blank=True)
    prescribed_texts = BleachField(required=False, allow_blank=True)
    preliminary_reading_list = BleachField(required=False, allow_blank=True)
    indicative_reading_list = BleachField(required=False, allow_blank=True)
    staff_feedback_statement = BleachField(required=False, allow_blank=True)
    exams = BleachField(required=False, allow_blank=True)
    exam_material = BleachField(required=False, allow_blank=True)
    online_submission = BleachField(required=False, allow_blank=True)
    late_submission_policy = BleachField(required=False, allow_blank=True)
    resubmission_policy = BleachField(required=False, allow_blank=True)
    assignment_return = BleachField(required=False, allow_blank=True)
    reference_requirements = BleachField(required=False, allow_blank=True)
    hard_copy_submission = BleachField(required=False, allow_blank=True)
    other_information = BleachField(required=False, allow_blank=True)

    class Meta:
        model = CourseInstance
        fields = ('id', 'created', 'last_modified', 'courseversion', 'instance_descriptor',
                  'start_date', 'end_date', 'taught_by', 'conveners', 'class_number',
                  'teachingsession', 'delivery_mode', 'delivery_group', 'course_url',
                  'inherent_requirements', 'workload', 'contact_methods', 'prescribed_texts',
                  'preliminary_reading_list', 'indicative_reading_list', 'exams', 'exam_material',
                  'online_submission', 'late_submission_policy', 'resubmission_policy',
                  'assignment_return', 'reference_requirements', 'hard_copy_submission',
                  'staff_feedback_statement', 'other_information', 'schedule_set', 'task_set',
                  'relatedroles')
        read_only_fields = ('schedule_set', 'task_set', 'relatedroles')


class DeliveryModeSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = DeliveryMode
        fields = ('id', 'created', 'last_modified', 'mode')


class LearningOutcomeSerializer(CIMSBaseModelSerializer):
    outcome = BleachField()

    class Meta:
        model = LearningOutcome
        fields = ('id', 'created', 'last_modified', 'courseversion', 'outcome',
                  'outcome_short_form', 'order', 'objectives')


class CourseRelatedRoleTitleSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = CourseRelatedRoleTitle
        fields = ('id', 'title')


class CourseRelatedRoleSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = CourseRelatedRole
        fields = ('id', 'courseinstance', 'role_title', 'user')


class OrganisationalUnitSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = OrganisationalUnit
        fields = ('id', 'created', 'last_modified', 'name', 'short_name', 'description',
                  'parent', 'children')


class ReviewStatusSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = ReviewStatus
        fields = ('id', 'name', 'description')


class RoleSerializer(FilteredFieldsModelSerializer, ModelSerializer):
    class Meta:
        model = Role
        fields = ('id', 'name', 'users', 'parent_roles', 'child_roles')


class ScheduleSerializer(CIMSBaseModelSerializer):
    summary = BleachField(required=False, allow_blank=True)
    assessment = BleachField(required=False, allow_blank=True)

    class Meta:
        model = Schedule
        fields = ('id', 'created', 'last_modified', 'courseinstance', 'session_name',
                  'session_order', 'session_number', 'summary', 'assessment')


class StandardsBodySerializer(CIMSBaseModelSerializer):
    class Meta:
        model = StandardsBody
        fields = ('id', 'created', 'last_modified', 'name', 'short_name')


class StandardsFrameworkSerializer(CIMSBaseModelSerializer):
    description = BleachField(required=False, allow_blank=True)

    class Meta:
        model = StandardsFramework
        fields = ('id', 'created', 'last_modified', 'standardsbody', 'name', 'short_name',
                  'description')


class StandardObjectiveSerializer(CIMSBaseModelSerializer):
    description = BleachField(required=False, allow_blank=True)

    class Meta:
        model = StandardObjective
        fields = ('id', 'created', 'last_modified', 'standardsframework', 'parent', 'name',
                  'short_name', 'description')


class TaskSerializer(CIMSBaseModelSerializer):
    description = BleachField(required=False, allow_blank=True)
    presentation_requirements = BleachField(required=False, allow_blank=True)
    hurdle_requirements = BleachField(required=False, allow_blank=True)
    individual_in_group = BleachField(required=False, allow_blank=True)

    class Meta:
        model = Task
        fields = ('id', 'created', 'last_modified', 'courseinstance', 'title', 'description',
                  'order', 'learningoutcomes', 'task_url', 'is_exam', 'is_hurdle',
                  'hurdle_requirements', 'weighting', 'presentation_requirements', 'due_date',
                  'due_time', 'return_date', 'individual_in_group')


class TeachingSessionSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = TeachingSession
        fields = ('id', 'created', 'last_modified', 'session_code', 'name', 'start_date',
                  'end_date')


class UserSerializer(FilteredFieldsModelSerializer, ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'is_active',
                  'is_staff', 'is_superuser', 'last_login', 'date_joined')
        read_only_fields = ('last_login', 'date_joined')


class WeightSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = Weight
        fields = ('id', 'created', 'last_modified', 'weightingscheme', 'value')


class WeightingSchemeSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = WeightingScheme
        fields = ('id', 'created', 'last_modified', 'name', 'description')


class WeightedLOToSOSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = WeightedLOToSO
        fields = ('id', 'created', 'last_modified', 'learningoutcome', 'standardobjective',
                  'weights')


class WeightedTaskToLOSerializer(CIMSBaseModelSerializer):
    class Meta:
        model = WeightedTaskToLO
        fields = ('id', 'created', 'last_modified', 'task', 'learningoutcome', 'weights')


# class ####Serializer(CIMSBaseModelSerializer):
#     class Meta:
#         model = Weight
#         fields = ('id', 'created', 'last_modified')
