from django.conf.urls import url, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.routers import DefaultRouter

from cims.api import views

# Make our api router
router = DefaultRouter()
router.register(r'academiccareer', views.AcademicCareerViewSet)
# router.register(r'annotation', views.AnnotationViewSet)
router.register(r'course', views.CourseViewSet)
router.register(r'courseclassification', views.CourseClassificationViewSet)
router.register(r'courseinstance', views.CourseInstanceViewSet)
router.register(r'courserelatedrole', views.CourseRelatedRoleViewSet)
router.register(r'courserelatedroletitle', views.CourseRelatedRoleTitleViewSet)
router.register(r'courseversion', views.CourseVersionViewSet)
router.register(r'deliverymode', views.DeliveryModeViewSet)
router.register(r'learningoutcome', views.LearningOutcomeViewSet)
router.register(r'organisationalunit', views.OrganisationalUnitViewSet)
router.register(r'reviewstatus', views.ReviewStatusViewSet)
router.register(r'role', views.RoleViewSet)
router.register(r'schedule', views.ScheduleViewSet)
router.register(r'standardsbody', views.StandardsBodyViewSet)
router.register(r'standardsframework', views.StandardsFrameworkViewSet)
router.register(r'standardobjective', views.StandardObjectiveViewSet)
router.register(r'task', views.TaskViewSet)
router.register(r'teachingsession', views.TeachingSessionViewSet)
router.register(r'user', views.UserViewSet)
router.register(r'weight', views.WeightViewSet)
router.register(r'weightingscheme', views.WeightingSchemeViewSet)
router.register(r'weighted_learningoutcome_to_standardobjective', views.WeightedLOToSOViewSet)
router.register(r'weighted_task_to_learningoutcome', views.WeightedTaskToLOViewSet)

# Include patterns generated by the router from viewsets
urlpatterns = [
        url(r'^user/set_ldap_user/', views.set_ldap_user, name='user-set_ldap'),
        url(r'^', include(router.urls)),
        # url(r'^auth/', include('rest_framework.urls')),
        ]

# Create and add a schema view to the API
api_schema = get_schema_view(
        openapi.Info(
                title='CIMS API',
                default_version='v0',
                description='CECS Course Information Management System REST API'
                ),
        )

urlpatterns.append(url(
        r'^schema/swagger(?P<format>\.json|\.yaml)$',
        api_schema.without_ui(cache_timeout=None),
        ))
urlpatterns.append(url(r'^schema/swagger/$', api_schema.with_ui('swagger', cache_timeout=None),
                       name='api-schema-swagger'))
urlpatterns.append(url(r'^schema/redoc/$', api_schema.with_ui('redoc', cache_timeout=None),
                       name='api-schema-redoc'))
