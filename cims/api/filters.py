"""
Filters for API endpoints.
"""
import re

from django_filters import rest_framework as filters

from cims import models
from cims.auth.models import User


def _match_course_level(queryset, name, value):
    """
    Filter a course or related queryset using the level of the course, e.g. 4000, 3, etc.

    :param queryset: Base queryset to filter
    :param name: Field/relation that is being filtered on
    :param value: List of course levels to include in the regex.
    :return: Filtered queryset or empty queryset if filter value doesn't look like a course level
    """
    match = re.findall(r'(\d)0{0,3}', ' '.join(value))
    if match:
        regex = r'[A-Z]{4}(' + '|'.join(match) + r')\d{3}[A-Z]?'
        lookup = '__'.join([name, 'regex'])
        return queryset.filter(**{lookup: regex})
    return queryset.none()


class NumberInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class CourseFilter(filters.FilterSet):
    """Filters for the Course model API endpoint."""
    code__level = NumberInFilter(
            field_name='code',
            method=_match_course_level,
            label='course level'
            )

    class Meta:
        model = models.Course
        fields = {'code': ['iexact', 'icontains']}


class CourseVersionFilter(filters.FilterSet):
    """Filters for the CourseVersion model API endpoint."""
    course__code__icontains = filters.CharFilter(
            field_name='course__code',
            lookup_expr='icontains',
            label='course code contains',
            )
    course__code__level = NumberInFilter(
            field_name='course__code',
            method=_match_course_level,
            label='course level'
            )

    class Meta:
        model = models.CourseVersion
        fields = []


class CourseInstanceFilter(filters.FilterSet):
    """Filters for the CourseInstance model API endpoint."""
    pk = NumberInFilter(field_name='pk', lookup_expr='in', label='ID is in')
    course__code__icontains = filters.CharFilter(
            field_name='courseversion__course__code',
            lookup_expr='icontains',
            label='course code contains',
            )
    course__code__level = NumberInFilter(
            field_name='courseversion__course__code',
            method=_match_course_level,
            label='course level'
            )
    start_date = filters.DateFromToRangeFilter()

    class Meta:
        model = models.CourseInstance
        fields = []


class TaskFilter(filters.FilterSet):
    """Filters for the Task model API endpoint"""
    due_date = filters.DateFromToRangeFilter()

    class Meta:
        model = models.Task
        fields = ['courseinstance']


class UserFilter(filters.FilterSet):
    """Filters for the User mode API endpoint."""
    pk = NumberInFilter(field_name='pk', lookup_expr='in', label='ID is in')

    class Meta:
        model = User
        fields = {
                'username': ['exact'],
                'email': ['exact'],
                'first_name': ['iexact'],
                'last_name': ['iexact'],
                }
