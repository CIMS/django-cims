"""
Authorisation classes for the CIMS API
"""
from rest_framework.permissions import DjangoModelPermissions, DjangoObjectPermissions


class DjangoModelPermissionsAnyOf(DjangoModelPermissions):
    """
    Extends `DjangoModelPermissions` to require at least one of the permissions listed for each
    method, rather than all of them.
    """
    def has_permission(self, request, view):
        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.
        if getattr(view, '_ignore_model_permissions', False):
            return True

        if hasattr(view, 'get_queryset'):
            queryset = view.get_queryset()
        else:
            queryset = getattr(view, 'queryset', None)
            assert queryset is not None, ('Cannot apply DjangoModelPermissions on a view that '
                                          'does not set `.queryset or have a `.get_queryset()` '
                                          'method.')

        if request.user and (request.user.is_authenticated or not self.authenticated_users_only):
            perms = self.get_required_permissions(request.method, queryset.model)
            for perm in perms:
                if request.user.has_perm(perm):
                    return True

        return False


class DjangoObjectPermissionsOrReadOnly(DjangoObjectPermissions):
    """Extends `DjangoObjectPermissions` to allow readonly access to safe methods."""

    authenticated_users_only = False

    def has_object_permission(self, request, view, obj):
        if hasattr(view, 'get_queryset'):
            queryset = view.get_queryset()
        else:
            queryset = getattr(view, 'queryset', None)

        assert queryset is not None, (
            'Cannot apply DjangoObjectPermissionsOrReadOnly on a view that '
            'does not set `.queryset` or have a `.get_queryset()` method.'
        )

        model_cls = queryset.model
        user = request.user

        perms = self.get_required_object_permissions(request.method, model_cls)

        return (request.user
                and user.has_perms(perms, obj)
                and (request.user.is_authenticated or not self.authenticated_users_only)
                )


class DjangoObjectPermissionsAnyOfOrNothing(DjangoModelPermissionsAnyOf, DjangoObjectPermissions):
    """
    Extends `DjangoModelPermissionsAnyOf` to require `add` or `change` permissions to view any
    objects, extends requirement for at least one of listed permissions to object permission
    checks.
    """
    perms_map = {'GET': ['%(app_label)s.add_%(model_name)s',
                         '%(app_label)s.change_%(model_name)s'],
                 'OPTIONS': [],
                 'HEAD': [],
                 'POST': ['%(app_label)s.add_%(model_name)s'],
                 'PUT': ['%(app_label)s.change_%(model_name)s'],
                 'PATCH': ['%(app_label)s.change_%(model_name)s'],
                 'DELETE': ['%(app_label)s.delete_%(model_name)s'],
                 }

    def has_object_permission(self, request, view, obj):
        if hasattr(view, 'get_queryset'):
            queryset = view.get_queryset()
        else:
            queryset = getattr(view, 'queryset', None)

        assert queryset is not None, (
            'Cannot apply DjangoObjectPermissionsOrReadOnly on a view that '
            'does not set `.queryset` or have a `.get_queryset()` method.'
        )

        model_cls = queryset.model

        if request.user and (request.user.is_authenticated or not self.authenticated_users_only):
            perms = self.get_required_object_permissions(request.method, model_cls)
            for perm in perms:
                if request.user.has_perm(perm):
                    return True

        return False
