"""
Mixins for the CIMS REST API.
"""
from django.contrib.admin.models import CHANGE
from django.forms.models import model_to_dict
from perfieldperms.utils import get_unpermitted_fields
from rest_framework import mixins, status
from rest_framework.response import Response
from reversion import create_revision, set_comment, set_user

from cims.utils.logging import construct_change_message, create_logentry


# Serialiser field modifying mixins
class PFPCreateModelMixin(mixins.CreateModelMixin):
    """
    Mixin that extends `create` and `perform_create` to modify the settings of fields the user
    lacks access to, based on per-field permissions.
    """
    def create(self, request, *args, **kwargs):
        """
        Strip out fields the user lacks permission to set. Set notification in returned data that
        some fields have been omitted.
        """
        SerializerClass = self.get_serializer_class()
        disabled_fields = get_unpermitted_fields(SerializerClass.Meta.model, request.user)
        serializer = self.get_serializer(data=request.data, disabled_fields=disabled_fields)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response_data = serializer.data.copy()
        if serializer.disabled_fields:
            response_data['CIMS_NOTE'] = ('The fields {} were not set because you lack required '
                                          'permissions.'.format(serializer.disabled_fields))
        return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        """Strip out fields from the serializer."""
        return serializer.save()


class PFPUpdateModelMixin(mixins.UpdateModelMixin):
    """
    Mixin that extends `create` and `perform_create` to modify the settings of fields the user
    lacks access to, based on per-field permissions.
    """
    def update(self, request, *args, **kwargs):
        """
        Strip out fields the user lacks permission to set. Set notification in returned data that
        some fields have been omitted.
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        SerializerClass = self.get_serializer_class()
        disabled_fields = get_unpermitted_fields(
                SerializerClass.Meta.model,
                request.user,
                instance,
                )
        serializer = self.get_serializer(
                instance,
                data=request.data,
                partial=partial,
                disabled_fields=disabled_fields,
                )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        response_data = serializer.data.copy()
        if serializer.disabled_fields:
            response_data['CIMS_NOTE'] = (
                    'The fields {} were not updated because you lack '
                    'required permissions.'.format(serializer.disabled_fields)
                    )
        return Response(response_data)

    def perform_update(self, serializer):
        """Strip out fields from the serializer."""
        return serializer.save()


# Logging mixins
class LoggingCreateModelMixin(mixins.CreateModelMixin):
    """Mixin for creating a `LogEntry` for the create action."""
    def perform_create(self, serializer):
        """Create the new object and generate a log message."""
        super().perform_create(serializer)
        obj = serializer.instance
        change_message = construct_change_message(create=True)
        return create_logentry(self.request, obj, change_message)


class LoggingUpdateModelMixin(mixins.UpdateModelMixin):
    """Mixin for creating a `LogEntry` for the update action."""
    def perform_update(self, serializer):
        """Perform the update and generate a log message containing changed fields."""
        old_data = model_to_dict(self.get_object())
        super().perform_update(serializer)
        obj = serializer.instance
        changed_fields = [
                fname for fname, value in serializer.validated_data.items()
                if fname in old_data and old_data[fname] != value
                ]
        change_message = construct_change_message(changed_fields)
        return create_logentry(self.request, obj, change_message, action=CHANGE)


# Versioning related mixins
class VersionedCreateModelMixin(LoggingCreateModelMixin):
    """A viewset that wraps `create` actions in a versioning block."""
    def perform_create(self, serializer):
        """Wrap the call to save() in a versioning block."""
        with create_revision():
            logentry = super().perform_create(serializer)
            set_user(self.request.user)
            set_comment(logentry.change_message)


class VersionedUpdateModelMixin(LoggingUpdateModelMixin):
    """A viewset that wraps `update` actions in a versioning block."""
    def perform_update(self, serializer):
        """Wrap the call to save() in a versioning block."""
        with create_revision():
            logentry = super().perform_update(serializer)
            set_user(self.request.user)
            set_comment(logentry.change_message)


# Combination mixins
class PFPModelMixin(PFPCreateModelMixin, PFPUpdateModelMixin):
    """Mixin that applies field permissions to `create` and `update` actions."""
    pass


class VersionedPFPModelMixin(VersionedCreateModelMixin,
                             VersionedUpdateModelMixin,
                             PFPModelMixin):
    """
    Mixin that applies versioning, field permissions, and creates a log entry when performing
    `create` and `update` actions.
    """
    pass


class VersionedPFPUpdateModelMixin(VersionedUpdateModelMixin, PFPUpdateModelMixin):
    """
    Mixin that applies versioning, field permissions, and creates a log entry when performing
    `update` actions.
    """
    pass
