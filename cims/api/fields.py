"""
CIMS API serializer fields.
"""
import bleach
from django.conf import settings
from rest_framework.serializers import CharField


class BleachField(CharField):
    """
    API textfield implementation that uses the `bleach` package to sanitise the input, preserving
    allowed html tags, attributes etc.
    """
    def __init__(self, allowed_tags=None, allowed_attributes=None,
                 allowed_styles=None, strip_tags=None, strip_comments=None,
                 *args, **kwargs):

        super(BleachField, self).__init__(*args, **kwargs)

        self.bleach_kwargs = {}
        if hasattr(settings, 'BLEACHFIELD_DEFAULTS'):
            self.bleach_kwargs.update(settings.BLEACHFIELD_DEFAULTS)

        if allowed_tags:
            self.bleach_kwargs['tags'] = allowed_tags
        if allowed_attributes:
            self.bleach_kwargs['attributes'] = allowed_attributes
        if allowed_styles:
            self.bleach_kwargs['styles'] = allowed_styles
        if strip_tags:
            self.bleach_kwargs['strip'] = strip_tags
        if strip_comments:
            self.bleach_kwargs['strip_comments'] = strip_comments

    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        data = bleach.clean(data, **self.bleach_kwargs)
        return data
