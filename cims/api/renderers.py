"""
Renderers for the CIMS API.
"""
from rest_framework.renderers import BrowsableAPIRenderer


class CIMSBrowsableAPIRenderer(BrowsableAPIRenderer):

    def get_context(self, data, accepted_media_type, renderer_context):
        context = super().get_context(data, accepted_media_type, renderer_context)
        context['display_edit_forms'] = False
        return context
