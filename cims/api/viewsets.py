"""
REST framework ViewSets for the CIMS application
"""
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from cims.api.mixins import VersionedPFPModelMixin


class LRViewset(mixins.ListModelMixin,
                mixins.RetrieveModelMixin,
                GenericViewSet):
    """
    Standard viewset providing `list`, 'retrieve' actions for models that need only be viewed.
    """
    pass


class CLRUViewset(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  GenericViewSet):
    """Standard viewset providing `create`, `list`, 'retrieve', `update` actions."""
    pass


class CIMSBaseCLRUViewSet(VersionedPFPModelMixin,
                          mixins.ListModelMixin,
                          mixins.RetrieveModelMixin,
                          GenericViewSet):
    """
    A viewset that provides `create`, `list`, `retrieve`, `update` actions. Creates a LogEntry when
    objects are created or changed. Modifies the serializer `fields` and `validated_data` by
    stripping out entries the user lacks access to during `create` or `update` actions.
    """
    pass
