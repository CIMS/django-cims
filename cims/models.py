"""
Models for the main data model of the CIMS application.
"""
import datetime
from decimal import Decimal

import reversion
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import F, Sum
from django.urls import reverse, reverse_lazy
from django.urls.exceptions import NoReverseMatch
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeManyToManyField
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from reversion.models import Version

from cims.fields import BleachField


def validate_start_end_dates(start_date, end_date, interval=None):
    """
    Validate that `end_date` is >= `start_date`. If a postive `interval` is specified, ensure
    `end_date` is at least `interval` greater than `start_date` (`end_date` - `start_date`) >=
    `interval`.
    """
    if not (isinstance(start_date, datetime.date) and isinstance(end_date, datetime.date)):
        raise ValidationError('Can\'t validate start/end dates if one of them is not a date.')

    if end_date < start_date:
        raise ValidationError('End date must be greater than or equal to the start date. end_date '
                              '>= start_date.')
    if interval is not None:
        if not isinstance(interval, datetime.timedelta) or interval.days < 0:
            raise ValidationError(
                    'Minimum interval, if specified, must be datetime.timedelta >= 1 day .'
                    )
        elif (start_date + interval) > end_date:
            raise ValidationError('End date must be greater than or equal to the start date plus '
                                  'the minimum interval of {}. end_date >= start_date + '
                                  'interval.'.format(interval))


class ValidatedModelMixin:
    """
    Mixin that provides some additional methods to assist with making sure the data model remains
    valid, especially where restrictions are applied to m2m relationships that can't be checked
    by signal receivers.
    """
    def validate_other(self):
        """Performs validation that can't be done in full_clean e.g. m2m relationships."""
        pass

    def validate(self, full_clean=True):
        """
        Performs validation of the whole instance. Requires the instance to exist in the database
        in case validation uses m2m fields.
        """
        assert self.pk is not None, ('Cannot call validate() on an instance that has not been '
                                     'saved.')
        if full_clean:
            self.full_clean()
        self.validate_other()


class ValidatedManagerMixin:
    """
    Manager that provides additional methods intended to create an interface for creating/updating
    instances that guarantees data model validity.
    """
    def validated_create(self, **kwargs):
        """
        Wrap `create` in a transaction, and perform additional validation. Rollback if
        validation fails.
        """
        # Extract m2m updates
        m2m_fields = {}
        opts = self.model._meta
        for f in opts.many_to_many:
            if f.name in kwargs:
                m2m_fields[f.name] = kwargs.pop(f.name)

        with transaction.atomic():
            instance = self.model(**kwargs)
            instance.full_clean()
            instance.save()
            for attr, value in m2m_fields.items():
                manager = getattr(instance, attr)
                manager.set(value)
            instance.validate(full_clean=False)

        return instance

    def validated_update(self, instance, **kwargs):
        """
        Wrap field updates in a transaction, rollback if the instance fails validation.

        Note that if the instance fails validation it may remain in an invalid state, though it
        won't have been saved to the database. It will need to be refreshed from the database if
        the update fails and you intend to keep using the instance.
        """
        opts = self.model._meta
        m2m_fields = [f.name for f in opts.many_to_many if f.name in kwargs]
        with transaction.atomic():
            for fname, value in [(fname, value) for fname, value in kwargs.items()
                                 if fname not in m2m_fields]:
                    setattr(instance, fname, value)
            instance.full_clean()
            instance.save()
            for fname in m2m_fields:
                value = kwargs[fname]
                manager = getattr(instance, fname)
                manager.set(value)
            instance.validate(full_clean=False)

        return instance


class ValidatedManager(ValidatedManagerMixin, models.Manager):
    """
    Base CIMS Manager.

    Attributes
    ----------
    select_related_fields : list
                            Additional fields/relationship to modify the default queryset with
                            using a select_related query. Useful for pulling in additional models
                            that are almost always used in the context of the model you're dealing
                            with.
    """
    select_related_fields = None

    def get_create_url(self):
        """
        If a UI based url exists for adding new instances return it, otherwise point at the
        appropriate API endpoint.
        """
        opts = self.model._meta
        try:
            create_url = reverse('{}:{}.create'.format(opts.app_label, opts.model_name))
        except NoReverseMatch:
            create_url = reverse('{}-list'.format(opts.model_name))
        return create_url

    def get_queryset(self):
        """Select related objects used in __str__ or regularly used in context"""
        qs = super().get_queryset()
        if self.select_related_fields is not None:
            qs = qs.select_related(*self.select_related_fields)
        return qs


class ValidatedTreeManager(ValidatedManagerMixin, TreeManager):
    """Manager to add validation to default MPTT manager."""
    pass


# Base/Abstract classes for CIMS application to ensure desired features are present in all models
class CIMSBaseModel(ValidatedModelMixin, models.Model):
    """
    Abstract base class that provides two self-updating fields: 'created' and
    'last_modified'.
    """
    created = models.DateTimeField(_('created'), auto_now_add=True)
    last_modified = models.DateTimeField(_('last modified'), auto_now=True)

    class Meta:
        abstract = True

    objects = ValidatedManager()

    def get_api_absolute_url(self):
        opts = self._meta
        return reverse_lazy('{}-detail'.format(opts.model_name), args=[self.pk])

    def get_ui_absolute_url(self):
        opts = self._meta
        return reverse_lazy('{}:{}.detail'.format(opts.app_label, opts.model_name), args=[self.pk])

    def get_ui_update_url(self):
        opts = self._meta
        return reverse_lazy('{}:{}.update'.format(opts.app_label, opts.model_name), args=[self.pk])

    def get_absolute_url(self):
        return self.get_api_absolute_url()

    get_update_url = get_api_absolute_url


# Model specific managers
# Mostly these are created so models which are practically always looked up in a join with a
# parent model do this by default. This massively reduces lookups for models which don't have
# e.g. a good __str__ representation without the context of related models.
class CourseManager(ValidatedManager):
    def get_course_for_object(self, obj):
        models_below_cv = (CourseInstance, LearningOutcome)
        models_below_ci = (CourseRelatedRole, Schedule, Task)
        if isinstance(obj, CourseVersion):
            return obj.course
        elif isinstance(obj, models_below_cv):
            return self.get(courseversion__pk=obj.courseversion_id)
        elif isinstance(obj, models_below_ci):
            return self.get(courseversion__courseinstance__pk=obj.courseinstance_id)
        return None


class CourseInstanceManager(ValidatedManager):
    select_related_fields = ['courseversion__course', 'teachingsession']

    def get_courseinstance_for_object(self, obj):
        models_below_ci = (CourseRelatedRole, Schedule, Task)
        if isinstance(obj, models_below_ci):
            return obj.courseinstance
        elif isinstance(obj, WeightedTaskToLO):
            return self.get(task__pk=obj.task_id)
        elif isinstance(obj, Annotation):
            if isinstance(obj.annotated_object, CourseInstance):
                return obj.annotated_object
            else:
                return None
        return None


class CourseRelatedRoleManager(ValidatedManager):
    # Would be good to do this with fewer joins...
    select_related_fields = ['user', 'courseinstance__courseversion__course', 'role_title']


class CourseVersionManager(ValidatedManager):
    select_related_fields = ['course']

    def get_courseversion_for_object(self, obj):
        models_below_cv = (CourseInstance, LearningOutcome)
        models_below_ci = (CourseRelatedRole, Schedule, Task)
        if isinstance(obj, models_below_cv):
            return obj.courseversion
        elif isinstance(obj, models_below_ci):
            return self.get(courseinstance__pk=obj.courseinstance_id)
        elif isinstance(obj, WeightedLOToSO):
            return self.get(learningoutcome__pk=obj.learningoutcome_id)
        elif isinstance(obj, Annotation):
            if isinstance(obj.annotated_object, CourseVersion):
                return obj.annotated_object
            else:
                return None
        return None


class ReviewStatusManager(ValidatedManager):
    def get_published_states(self):
        """
        Return a queryset of the ReviewStatus objects that denote an approved/published state.
        """
        qs = self.get_queryset()
        return qs.filter(name__startswith='Approved')


class ScheduleManager(ValidatedManager):
    # Would be good to find a better string representation...
    select_related_fields = ['courseinstance__courseversion__course']


class TaskManager(ValidatedManager):
    # Would be good to find a better string representation...
    select_related_fields = ['courseinstance__courseversion__course']


class WeightManager(ValidatedManager):
    select_related_fields = ['weightingscheme']


# Concrete models
# Competency/accreditation standards
@reversion.register()
class StandardsBody(CIMSBaseModel):
    """
    Organisation providing a set of standards against which courses are
    linked/assessed, e.g. IEEE.
    """
    name = models.CharField(max_length=120, unique=True)
    short_name = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return self.get_ui_absolute_url()


@reversion.register()
class StandardsFramework(CIMSBaseModel):
    """
    A collection of objectives or criteria associated with a standard or
    accrediting organisation.
    """
    standardsbody = models.ForeignKey(
            StandardsBody,
            on_delete=models.CASCADE,
            blank=False,
            null=False,
            help_text='Link this framework to a standards body.'
            )
    name = models.CharField(max_length=120)
    short_name = models.CharField(
            max_length=40,
            blank=True,
            help_text='Abbreviated name for this framework, suitable for use as e.g. a column '
                      'header in a table.'
            )
    description = BleachField(blank=True)

    def __str__(self):
        if self.short_name:
            return self.short_name
        elif len(self.name) <= 40:
            return self.name
        else:
            return '{}...'.format(self.name[:37])

    def get_absolute_url(self):
        return self.get_ui_absolute_url()

    def get_root_objectives(self):
        """Return a queryset of root objectives linked to this framework."""
        return StandardObjective.objects.root_nodes().filter(standardsframework=self)

    def get_objective_trees(self, ordering=None):
        """
        Return a list of TreeQuerySets for the StandardObjective trees linked to this framework.
        """
        root_objectives = self.get_root_objectives()
        if ordering:
            root_objectives = root_objectives.order_by(*ordering)
        return [root.get_descendants(include_self=True) for root in root_objectives]


@reversion.register()
class StandardObjective(CIMSBaseModel, MPTTModel):
    """
    A particular objective in a standards framework. StandardObjectives can be organised in a tree,
    in which case each node stores a link to its StandardsFramework to simplify filtering.
    """
    standardsframework = models.ForeignKey(
            StandardsFramework,
            on_delete=models.CASCADE,
            blank=True,
            null=True,
            help_text=('Link this objective to a standards framework. If both this and a parent '
                       'objective are set the framework of the parent will take precedence.'),
            )
    parent = TreeForeignKey(
            'self',
            on_delete=models.CASCADE,
            blank=True,
            null=True,
            related_name='children',
            help_text=('If the objectives in a framework are organised in a tree, use this field '
                       'to associate this objective with its parent.'),
            )
    name = models.CharField(
            max_length=120,
            help_text='Often this will just be a reference number e.g. "2.1". It\'s typically '
                      'displayed in context with its framework so there\'s no need to repeat those '
                      'details.'
            )
    short_name = models.CharField(
            max_length=40,
            blank=True,
            help_text='If the name is long enter a shortened version suitable for e.g. table '
                      'column headings here.'
            )
    description = BleachField(blank=True)

    # Set default manager
    objects = ValidatedTreeManager()

    class Meta:
        ordering = ('name',)

    def __str__(self):
        """Leaf nodes can have minimal names so construct something useful"""
        if self.short_name:
            short_name = self.short_name
        elif len(self.name) <= 40:
            short_name = self.name
        else:
            short_name = '{}...'.format(self.name[:37])
        return '{} - {}'.format(self.standardsframework, short_name)

    def clean(self):
        """Root nodes in an objective tree must link to a `StandardsFramework`."""
        if self.standardsframework is None and self.parent is None:
            raise ValidationError('A Standard Objective bust be linked to either a Standards '
                                  'Framework (making it the root of a tree), or a parent '
                                  'objective.')

    def get_absolute_url(self):
        return self.standardsframework.get_absolute_url()

    def save(self, **kwargs):
        # If we're a child objective, set the linked framework to that of the root of our tree.
        if self.parent is not None:
            root = self.parent.get_root()
            self.standardsframework = root.standardsframework
        return super().save(**kwargs)

    @property
    def children_are_leaves(self):
        """Returns if any of the children are leaf nodes."""
        if not self.is_leaf_node():
            for child in self.get_children():
                if child.is_leaf_node():
                    return True
        return False

    @property
    def leaf_count(self):
        """Return the number of leaf nodes in the tree under this one."""
        return self.get_leafnodes().count()


# Weighting schemes
@reversion.register()
class WeightingScheme(CIMSBaseModel):
    """A weighting scheme to be applied to some other relationship in the data model."""
    name = models.CharField(max_length=120)
    description = models.TextField(
            blank=True,
            help_text='What this scheme is for, how to use it etc.',
            )

    def __str__(self):
        return self.name


@reversion.register()
class Weight(CIMSBaseModel):
    """The values that can be applied when using a WeightingScheme."""
    weightingscheme = models.ForeignKey(WeightingScheme, on_delete=models.CASCADE)
    value = models.CharField(
            max_length=80,
            help_text='Values are entered as text. Interpretation, ordering and processing are up '
                      'to the scheme creator/user.',
            )

    objects = WeightManager()

    def __str__(self):
        return '{} - {}'.format(self.weightingscheme, self.value)


# Mappings
@reversion.register()
class WeightedLOToSO(CIMSBaseModel):
    """Associative class linking a LearningOutcome, StandardObjective with some optional Weights."""
    learningoutcome = models.ForeignKey(
            'LearningOutcome',
            on_delete=models.CASCADE,
            verbose_name='learning outcome'
            )
    standardobjective = TreeForeignKey(
            StandardObjective,
            on_delete=models.CASCADE,
            verbose_name='standard objective'
            )
    weights = models.ManyToManyField(Weight, blank=True)

    class Meta:
        unique_together = ('learningoutcome', 'standardobjective')
        verbose_name = 'learning outcome to competency mapping'

    def clean(self):
        """
        The linked StandardObjective must come from the set associated with the CourseVersion
        linked via the LearningOutcome.
        """
        courseversion = CourseVersion.objects.get(learningoutcome=self.learningoutcome_id)
        if not courseversion.objectives.filter(pk=self.standardobjective_id).exists():
            raise ValidationError('The StandardObjective "objective" must come from the set linked '
                                  'to "learningoutcome\'s" CourseVersion.')


@reversion.register()
class WeightedTaskToLO(CIMSBaseModel):
    """Associative class linking a Task, LearningOutcome and an optional set of Weights."""
    task = models.ForeignKey('Task', on_delete=models.CASCADE)
    learningoutcome = models.ForeignKey(
            'LearningOutcome',
            on_delete=models.CASCADE,
            verbose_name='learning outcome'
            )
    weights = models.ManyToManyField(Weight, blank=True)

    class Meta:
        unique_together = ('task', 'learningoutcome')
        verbose_name = 'task to learning outcome mapping'

    def clean(self):
        """
        The linked LearningOutcome must come from the set associated with the CourseVersion linked
        via the Task.
        """
        courseversion = CourseVersion.objects.get_courseversion_for_object(self.task)
        if not courseversion.learningoutcome_set.filter(pk=self.learningoutcome_id).exists():
            raise ValidationError('The linked LearningOutcome must come from the set associated '
                                  'with the parent CourseVersion of "task".')


# Course delivery models
@reversion.register()
class OrganisationalUnit(CIMSBaseModel, MPTTModel):
    """Some organisational body e.g. ANU, a College, a School, an external org."""
    name = models.CharField('full name of this body', max_length=120)
    short_name = models.CharField(
            'an abbreviation for the body',
            max_length=20,
            )
    description = models.TextField(
            blank=True,
            help_text='If not obvious, what this body is/does/why it\'s here.',
            )
    parent = TreeForeignKey(
            'self',
            on_delete=models.SET_NULL,
            blank=True,
            null=True,
            related_name='children',
            help_text='The parent of this body in the organisation structure.',
            )
    objects = ValidatedTreeManager()

    def __str__(self):
        return self.name


@reversion.register()
class TeachingSession(CIMSBaseModel):
    """An official ANU teaching session."""
    session_code = models.CharField(max_length=6, blank=True)
    name = models.CharField(max_length=64, unique=True)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return self.name

    def clean(self):
        # Validate start and end dates
        validate_start_end_dates(self.start_date, self.end_date)
        super().clean()


@reversion.register()
class AcademicCareer(CIMSBaseModel):
    """Academic path this CourseVersion sits in e.g. undergraduate/postgraduate."""
    career = models.CharField(
            max_length=80,
            help_text='The name of an academic career path e.g. undergraduate.')

    def __str__(self):
        return self.career


@reversion.register()
class DeliveryMode(CIMSBaseModel):
    """How a course is delivered e.g. In Person."""
    mode = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.mode


@reversion.register()
class Course(CIMSBaseModel):
    """Course code, top of courses model tree."""
    code = models.CharField(
            'course code',
            max_length=12,
            unique=True,
            help_text='course code of format AAAA####(F) e.g. COMP1100.',
            )
    objects = CourseManager()

    def __str__(self):
        return self.code

    def get_absolute_url(self):
        return self.get_ui_absolute_url()

    def get_current_courseversion(self, select_related=None):
        """
        Return the current approved/authoritative CourseVersion for this Course, or None if one
        doesn't exist.

        Multiple current version exceptions are allowed to propagate.

        Parameters
        ----------
        select_related : List, optional
                         List of related objects to fetch along with the CourseVersion
        """
        if select_related is None:
            select_related = []
        current_version = self.courseversion_set.select_related(*select_related).get(
                status__in=ReviewStatus.objects.get_published_states(),
                )
        return current_version


@reversion.register()
class CourseClassification(CIMSBaseModel):
    """A poorly defined field that isn't used much anymore?"""
    classification = models.CharField(max_length=80)

    def __str__(self):
        return self.classification


@reversion.register()
class CourseVersion(CIMSBaseModel):
    """An approved course that gets taught in multiple teaching sessions."""
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    version_descriptor = models.CharField(
            # 'short description of this version',
            max_length=200,
            help_text=('Short description of why this version was created. '
                       'Think changelog.'
                       )
            )
    status = models.ForeignKey('ReviewStatus', on_delete=models.PROTECT)
    status_freetext = models.CharField(
            max_length=80,
            blank=True,
            help_text=("Brief description of this course version's state in the workflow if it "
                       "doesn't match an official state."),
            )
    approval_date = models.DateField(blank=True, null=True)
    approval_description = models.CharField(
            max_length=120,
            blank=True,
            help_text='Where this version was approved, e.g. exec meeting.'
            )
    title = models.CharField(max_length=120, blank=True)
    description = BleachField(blank=True)
    delivery_mode = models.ForeignKey(
            DeliveryMode,
            on_delete=models.PROTECT,
            blank=True,
            null=True,
            verbose_name='delivery mode',
            )
    custodians = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)
    unit_value = models.CharField(
            max_length=20,
            blank=True,
            help_text='The unit value or weight of this course, typically 6 or 12.',
            )
    classification = models.ForeignKey(
            CourseClassification,
            on_delete=models.PROTECT,
            blank=True,
            null=True,
            )
    academiccareer = models.ForeignKey(
            AcademicCareer,
            on_delete=models.PROTECT,
            blank=True,
            null=True,
            verbose_name='academic career',
            help_text='The academic career path this course fits in e.g. undergraduate.',
            )
    standards_frameworks = models.ManyToManyField(
            StandardsFramework,
            blank=True,
            verbose_name='standards frameworks',
            help_text='Standards frameworks to associate with this course.'
            )
    objectives = TreeManyToManyField(
            StandardObjective,
            blank=True,
            verbose_name='standards',
            help_text='Standard objectives to associate with this course.'
            )
    weightingschemes = models.ManyToManyField(
            WeightingScheme,
            blank=True,
            verbose_name='weighting schemes',
            help_text='Optional weighting schemes to apply to competency/objective frameworks.',
            )
    course_group = models.ManyToManyField(
            'self',
            symmetrical=True,
            blank=True,
            help_text='Courses that must go through the approvals process together with this one. '
                      'In practice this means if one Course in the set is changed all will have '
                      'to be re-approved.',
            )
    assumed_knowledge = BleachField(blank=True)
    prerequisite_courses = BleachField(blank=True)
    corequisite_courses = BleachField(blank=True)
    incompatible_courses = BleachField(blank=True)
    research_led_teaching = BleachField(
            'research led teaching statement',
            blank=True,
            help_text='Describe the distinctive, research-led features of this course.'
            )
    required_resources = BleachField(
            blank=True,
            help_text='Resources the student will need access to which are not covered by tuition '
                      'fees.'
            )
    additional_costs = BleachField(
            'additional course costs',
            blank=True,
            help_text='Briefly describe additional costs students undertaking this course incur, '
                      'such as additional materials, excursions etc. subject to HESA provisions.'
            )
    participation = BleachField('participation statement', blank=True)
    field_trips = BleachField(
            blank=True,
            help_text='Outline details of field trips required as a component of this course.'
            )
    recommended_resources = BleachField(blank=True)
    other_information = BleachField(blank=True)

    objects = CourseVersionManager()

    def __str__(self):
        return '{0} {1} - {2}'.format(
                self.course.code,
                self.title,
                self.version_descriptor
                )

    def get_absolute_url(self):
        return self.get_ui_absolute_url()

    def get_update_url(self):
        return self.get_ui_update_url()

    def get_indicative_assessments(self, date=None):
        """
        Get a dict of assessed Tasks from this CourseVersion's most recent CourseInstances

        For each delivery mode for which there is a CourseInstance associated with this
        CourseVersion, get the CourseInstance which ran most recently relative to `date` (currently
        running or ran). Get the assessed Tasks for each CourseInstance aggreggated by Task title
        and return as a dict of strings, detailing assessment titles and contribution to assessment
        for each DeliveryMode.

        Parameters
        ----------
        date : datetime.Date, optional
               Determines which CourseInstances are "recent", defaults to today.
        """
        if date is None:
            date = now()
        if isinstance(date, datetime.datetime):
            date = datetime.date(date.year, date.month, date.day)

        assert isinstance(date, datetime.date), ('Date parameter must be datetime.date or '
                                                 'datetime.datetime.')

        # Get the most recent class for each deliver mode
        recent_classes = CourseInstance.objects.filter(
                courseversion_id=self.pk,
                teachingsession__start_date__lte=date,
                ).order_by('delivery_mode_id', '-teachingsession__start_date').distinct(
                        'delivery_mode_id').select_related('delivery_mode')
        indicative_assessments = dict()
        for recent_class in recent_classes:
            # If there is a class running outside a TeachingSession that starts after the one above
            # finishes then use it instead
            try:
                recent_class = CourseInstance.objects.filter(
                        teachingsession__isnull=True,
                        delivery_mode_id=recent_class.delivery_mode_id,
                        start_date__lte=date,
                        start_date__gt=recent_class.end_date,
                        ).order_by('-start_date')[0]
            except IndexError:
                pass

            tasks = Task.objects \
                .filter(courseinstance=recent_class, weighting__gt=0) \
                .values('title') \
                .order_by() \
                .annotate(contribution=Sum('weighting')) \
                .order_by('contribution')
            indicative_assessments[recent_class.delivery_mode.mode] = '; '.join(
                    ['{} {}%'.format(task['title'], task['contribution']) for task in tasks]
                    )

        return indicative_assessments

    def get_instance_for_year(self, year=None):
        """
        Get a related ``CourseInstance`` from a given year.

        Keyword args:
        year -- the calendar year to filter instances with
        """
        if year is None:
            year = now().year

        return self.courseinstance_set.filter(
            start_date__gte=datetime.date(year, 1, 1),
            start_date__lte=datetime.date(year, 12, 31)
            ).order_by('start_date').first()

    def get_allowed_standardobjectives(self):
        """
        Return all leaf StandardObjectives from StandardsFrameworks related to this
        CourseVersion.
        """
        return StandardObjective.objects.filter(
                standardsframework__courseversion__id=self.pk,
                lft=F('rght') - 1
                )


@reversion.register()
class CourseInstance(CIMSBaseModel):
    """An instance of a course taught during a teaching session."""
    courseversion = models.ForeignKey(CourseVersion, on_delete=models.CASCADE)
    instance_descriptor = models.CharField(
            'short description of this instance',
            max_length=120,
            help_text=('Short description of this instance. Details of '
                       'session/why run?.'),
            )
    start_date = models.DateField()
    end_date = models.DateField()
    taught_by = TreeManyToManyField(
            OrganisationalUnit,
            blank=False,
            help_text='Which organisational units actually teach this class/offering?',
            )
    conveners = models.ManyToManyField(
            settings.AUTH_USER_MODEL,
            blank=True,
            verbose_name='class conveners')
    class_number = models.CharField(
            'ANU class number',
            max_length=8,
            blank=True,
            )
    teachingsession = models.ForeignKey(
            TeachingSession,
            on_delete=models.PROTECT,
            blank=True,
            null=True,
            verbose_name='teaching session',
            help_text='The ANU teaching session this course instance will run in.',
            )
    delivery_mode = models.ForeignKey(
            DeliveryMode,
            on_delete=models.PROTECT,
            blank=True,
            null=True,
            verbose_name='delivery mode',
            )
    delivery_group = models.ManyToManyField(
            'self',
            symmetrical=True,
            blank=True,
            help_text='Classes/offerings that are delivered as a group, or have dependencies with '
                      'respect to delivery e.g. part of a class is actually delivered through a '
                      'related class. In practice this means things like scheduling changes in '
                      'one need to be checked for conflict with all the classes in this set.'
            )
    course_url = models.URLField(
            blank=True,
            help_text='URL of e.g. further information about this course, or the course homepage.'
            )
    inherent_requirements = BleachField(
            blank=True,
            help_text='Requirements essential to the completion of this course that aren\'t '
                      'contained in descriptions of assessable tasks. Consider e.g. mobility '
                      'requirements of a field trip or the need to lift heavy objects in a '
                      'workshop.'
                      )
    workload = BleachField(
            'workload statement',
            blank=True,
            help_text='A short description of the course workload as would appear on Programs and '
                      'Courses e.g. "13 lecturers, 6 tutorials, 6 labs, mid-semester test, exam." '
                      'For e.g. work experience courses this could be quite detailed.'
                      )
    contact_methods = BleachField(
            'preferred avenues of consultation',
            blank=True,
            help_text='Describe the ways you would prefer students go about contacting staff. '
                      'This could be any/a combination of e.g. email address(es), a phone number, '
                      'consultation hours, a directive to use forums or chat facilities or some '
                      'other method.'
            )
    prescribed_texts = BleachField(
            blank=True,
            help_text='Material/texts it is assumed a student will have access to during the '
                      'course.'
                      )
    preliminary_reading_list = BleachField(
            blank=True,
            help_text='Material a student is expected to have read before commencing the course.'
            )
    indicative_reading_list = BleachField(
            blank=True,
            help_text='Further material students may find it useful to consult during the course '
                      'but which are not required to complete the course.'
                      )
    exams = BleachField('description of exam(s)', blank=True)
    exam_material = BleachField(
            'allowed examination material or equipment',
            blank=True
            )
    online_submission = BleachField(
            'online submission statement',
            blank=True,
            help_text='A description of how online submissions will be handled.'
            )
    late_submission_policy = BleachField(
            blank=True,
            help_text='A description of under what conditions (if any) late submissions will be '
                      'accepted.'
                      )
    resubmission_policy = BleachField(
            blank=True,
            help_text='A description of under what conditions (if any) assignment resubmissions '
                      'will be accepted.'
            )
    assignment_return = BleachField(
            'assignment return statement',
            blank=True,
            help_text='Indicate how work will be returned to students e.g. through tutorials.'
            )
    reference_requirements = BleachField(
            'referencing requirements statement',
            blank=True,
            help_text='If references are required to be in a particular format, describe it here.'
            )
    hard_copy_submission = BleachField(
            'hard copy submission statement',
            blank=True,
            help_text='If hard copy submissions are permitted describe how this will be done, '
                      'e.g. cover letter and drop box.'
            )
    staff_feedback_statement = BleachField(
            blank=True,
            help_text='List the forms of feedback you will give to students e.g. written, verbal, '
                      'whole of class, project group etc.'
                      )
    other_information = BleachField(blank=True)

    objects = CourseInstanceManager()

    class Meta:
        verbose_name = 'class'
        verbose_name_plural = 'classes'

    def __str__(self):
        if self.teachingsession_id is not None:
            return '{} {} {}'.format(
                    self.courseversion.course.code,
                    self.class_number,
                    self.teachingsession
                    )
        else:
            return '{} {} {} - {}'.format(
                    self.courseversion.course.code,
                    self.class_number,
                    self.start_date,
                    self.end_date,
                    )

    def clean(self):
        # Validate start and end dates
        validate_start_end_dates(self.start_date, self.end_date)
        # Delivery_mode must match that of parent CourseVersion
        if self.delivery_mode != self.courseversion.delivery_mode:
            # raise ValidationError('CourseInstance delivery mode must match that of its parent '
            #                      'CourseVersion.')
            pass
        super().clean()

    def get_absolute_url(self):
        return self.get_ui_absolute_url()

    def get_update_url(self):
        return self.get_ui_update_url()

    def get_conveners(self):
        """
        Convenience method to get a list of people filling the role of convener
        for this course.
        """
        return self.courseversion.conveners


@reversion.register()
class LearningOutcome(CIMSBaseModel):
    """What was achieved by a course/task/assessment."""
    courseversion = models.ForeignKey(
            CourseVersion,
            on_delete=models.CASCADE,
            verbose_name=CourseVersion._meta.verbose_name,
            help_text='The course version this learning outcome is linked to.'
            )
    outcome = BleachField()
    outcome_short_form = models.CharField(
            max_length=40,
            blank=True,
            help_text='A short form of the outcome description suitable for display as e.g. a '
                      'heading in a table.'
            )
    order = models.IntegerField(
            blank=True,
            default=0,
            help_text='Order the display of the learning outcomes.'
            )
    objectives = models.ManyToManyField(
            StandardObjective,
            through=WeightedLOToSO,
            )

    class Meta:
        ordering = ('order', 'outcome')

    def __str__(self):
        if self.outcome_short_form:
            return self.outcome_short_form
        elif len(self.outcome) < 40:
            return self.outcome
        else:
            return '{}...'.format(self.outcome[:37])

    def get_absolute_url(self):
        return self.get_ui_absolute_url()

    def get_update_url(self):
        return self.get_ui_update_url()


@reversion.register()
class Schedule(CIMSBaseModel):
    """An item or module in a course schedule."""
    courseinstance = models.ForeignKey(
            CourseInstance,
            on_delete=models.CASCADE,
            verbose_name=CourseInstance._meta.verbose_name,
            help_text='The course offering this task is linked to.'
            )
    session_name = models.CharField(
            'module name',
            max_length=120,
            help_text='Duration of this module of the course e.g. Week 1 / Weeks 1-3 / 1st half '
                      'semester. Use whatever module "size" and grouping makes sense for your '
                      'course. E.g. an intensive course might use days or groups of days, or if '
                      'the first three weeks are lectures on a common topic you might group them '
                      'as "Weeks 1-3".'
            )
    session_order = models.IntegerField(
            'module order',
            default=0,
            help_text='Location of this module in the schedule order. This field takes '
                      'precedence when ordering schedule entries for display so use it to force a '
                      'display order. The ordering applied is ascending numerical order.'
            )
    session_number = models.CharField(
            'module number',
            max_length=40,
            blank=True,
            help_text='If there is some other code you use to distinguish modules of a similar '
                      'type e.g. lectures are labeled L1, L2 etc. use this field. If two entries '
                      'have the same module order then this field will be used to determine their '
                      'relative order. The ordering applied is alphabetical so e.g. "12" will '
                      'display before "2".'
            )
    summary = BleachField(
            'module summary',
            blank=True,
            help_text='What happens in the course during this module e.g. "Lecture, lab, '
                      'tutorial.", and optionally a brief mention of the topic(s) covered. Give '
                      'as much detail as you want e.g. "Lecture - basic circuit components, lab '
                      '- soldering and basic circuit construction."'
            )
    assessment = BleachField(
            'assessments during module',
            blank=True,
            help_text='Optional. Describe separately any assessment items that happen during this '
                      'module e.g. mid-semester test. There may be overlap between this field and '
                      'the activities field, use either/both to achieve desired clarity.'
            )

    objects = ScheduleManager()

    class Meta:
        ordering = ('session_order', 'session_number', 'pk')

    def __str__(self):
        return '{} ({})'.format(self.session_name, self.courseinstance)

    def get_absolute_url(self):
        return self.get_ui_absolute_url()

    def get_update_url(self):
        return self.get_ui_update_url()


@reversion.register()
class CourseRelatedRoleTitle(CIMSBaseModel):
    """Available roles within a course offering e.g. lecturer."""
    title = models.CharField(
            max_length=20,
            unique=True,
            help_text='Title of a role that helps deliver a course offering e.g. lecturer.'
            )

    def __str__(self):
        return self.title


@reversion.register()
class CourseRelatedRole(CIMSBaseModel):
    """Someone that helps deliver a course offering e.g. lecturer."""
    user = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete=models.CASCADE,
            related_name='courserelatedroles',
            help_text='The user that will occupy this role.',
            )
    courseinstance = models.ForeignKey(
            CourseInstance,
            on_delete=models.CASCADE,
            related_name='relatedroles',
            verbose_name=CourseInstance._meta.verbose_name,
            help_text='The course offering this role is associated with.'
            )
    role_title = models.ForeignKey(
            CourseRelatedRoleTitle,
            on_delete=models.CASCADE,
            related_name='courserelatedroles',
            help_text='The title or category of this role.'
            )

    objects = CourseRelatedRoleManager()

    def __str__(self):
        return '{}: {} - {}'.format(self.courseinstance, self.role_title, self.user)


@reversion.register()
class Task(CIMSBaseModel):
    """
    A generic task to be completed during a course. Could be e.g. an assessment item, a lab that
    just needs to be completed or some measure of participation.
    """
    courseinstance = models.ForeignKey(
            CourseInstance,
            on_delete=models.CASCADE,
            verbose_name=CourseInstance._meta.verbose_name,
            help_text='The course offering this task is linked to.'
            )
    learningoutcomes = models.ManyToManyField(
        LearningOutcome,
        through=WeightedTaskToLO,
        blank=True,
        help_text='Learning outcomes this task contributes towards.'
        )
    title = models.CharField(max_length=120)
    description = BleachField(
            blank=True,
            help_text='Give a brief overview of the task.'
            )
    order = models.IntegerField(
            blank=True,
            default=0,
            help_text='Order the display of the tasks.'
            )
    task_url = models.URLField(
            blank=True,
            help_text='The URL for this task or further information about it e.g. an online quiz '
                      'or detailed instructions for a lab.'
                      )
    is_exam = models.BooleanField(
            default=False,
            help_text='Flag this task as an exam.'
            )
    is_hurdle = models.BooleanField(
            default=False,
            help_text='Flag this task as a hurdle requirement to completing the course.'
            )
    weighting = models.DecimalField(
            max_digits=5,
            decimal_places=2,
            default=Decimal('0'),
            blank=True,
            help_text='Fraction of final mark this task comprises. There is no requirement for '
                      'task weights in a course to sum to 100.'
            )
    presentation_requirements = BleachField(
            blank=True,
            help_text='If this task involves a formal presentation e.g. in front of a tutorial '
                      'group, optionally describe requirements of the presentation format etc. '
                      'here.'
            )
    due_date = models.DateField(blank=True, null=True)
    due_time = models.TimeField(blank=True, null=True)
    return_date = models.CharField(
            'estimated feedback date',
            max_length=120,
            blank=True,
            help_text='The date by which the convener intends to have returned results/feedback '
                      'from this task. Doesn\'t have to be a date, could be a period of time '
                      'after the task due date e.g. "Two weeks after submission".'
            )
    hurdle_requirements = BleachField(
            blank=True,
            help_text='If this task has sub-elements that constitute a hurdle to its successful '
                      'completion, optionally describe them here.'
            )
    individual_in_group = BleachField(
            'individual assessment in a group task',
            blank=True,
            help_text='If this is a group task and people will be assessed as individuals within '
                      'the group, optionally describe here how this will be done.'
            )

    objects = TaskManager()

    class Meta:
        ordering = ('order',)

    def __str__(self):
        return '{} ({})'.format(self.title, self.courseinstance)

    def get_absolute_url(self):
        return self.get_ui_absolute_url()

    def get_update_url(self):
        return self.get_ui_update_url()


# Models associated with workflows, status, annotations.
@reversion.register()
class ReviewStatus(CIMSBaseModel):
    """
    The set of official states a `CourseVersion` or other model can be in as it travels
    through approvals processes etc.
    """
    name = models.CharField(
            max_length=80,
            unique=True,
            help_text='The name of this state in the workflow.'
            )
    description = models.TextField(
            blank=True,
            help_text='What this status means/requirements for it to be set.',
            )

    objects = ReviewStatusManager()

    def __str__(self):
        return self.name


class Annotation(CIMSBaseModel):
    """
    A note associated with a model instance.
    """
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(_('object ID'), max_length=255)
    annotated_object = GenericForeignKey('content_type', 'object_id')
    note = models.TextField(help_text='Free Text note to associate with this object.')
    version = models.ForeignKey(
            Version,
            on_delete=models.SET_NULL,
            blank=True,
            null=True,
            help_text='Version of the linked object to associate this note with.'
            )

    class Meta:
        verbose_name = 'note'

    def save(self, **kwargs):
        # Attempt to set the version to attach to if not already set
        if self.version is None:
            current_version = Version.objects.filter(
                    content_type=self.content_type,
                    object_id=self.object_id,
                ).order_by('revision__date_created').last()
            if current_version is not None:
                self.version = current_version
        super().save(**kwargs)
