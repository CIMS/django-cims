from functools import partial

from django.contrib import admin
from django.contrib import messages
from django.contrib.auth import get_permission_codename
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.db.models import F, TextField
from django.forms import Textarea
from django.utils.translation import ugettext_lazy as _
from mptt.admin import MPTTModelAdmin
from perfieldperms.admin import PFPInlineAdmin, PFPModelAdmin
from perfieldperms.forms import PFPModelForm
from reversion.admin import VersionAdmin
from reversion.models import Version

from cims import models
from cims.auth.forms import (CIMSUserChangeForm, CIMSUserCreationForm,
                             ConditionalPermissionsMapForm, ObjectStatusPermissionsMapForm)
from cims.auth.models import (ConditionalPermissionsMap, ObjectStatusPermissionsMap, Role,
                              User as CIMSUser)
from cims.utils.logging import construct_change_message


# Utility functions
def get_leaf_objectives(course_version_id):
    """Return all leaf StandardObjectives related to the given CourseVersion ID."""
    return models.StandardObjective.objects.filter(
            standardsframework__courseversion__id=course_version_id,
            lft=F('rght') - 1
            )


# class CopyableModelAdmin(admin.ModelAdmin):
#    """ModelAdmin with view extension for copy/duplicate functionality."""
#    clear_fields = []
#
#    def __init__(self, model, admin_site):
#        super().__init__(model, admin_site)
#
#    def get_clear_fields(self):
#        if self.clear_fields:
#            return self.clear_fields
#        # Do stuff to get fields to clear if not specified
#        return self.clear_fields
#
#    def copy_view(self, request):
#        """
#        Render a prepopulated form to enable copying of another object.
#
#        Replicates existing changeform_view() as much as possible. Uses GET
#        attribute "source" for source object id.
#        """
#        info = self.model._meta.app_label, self.model._meta.model_name
#        model = self.model
#        opts = model._meta
#        add = True
#        obj = None
#
#        if not self.has_add_permission(request):
#            raise PermissionDenied
#
#        if request.method == 'GET':
#            source_id = request.GET.get('source')
#            source_object = get_object_or_404(model, id=source_id)
#            ModelForm= self.get_form(request, obj)
#            prefill = dict()
#            # Only copy concrete fields from instance, excluding relations
#            # unless they are required i.e. field.blank=False
#            for field in source_object._meta.get_fields():
#                if field.concrete and (field.blank != field.is_relation):
#                    prefill[field.name] = getattr(source_object, field.name)
#            # Remove fields we want cleared from prefill, mostly relationship
#            # and required fields
#            for key in clear_fields:
#                del prefill[key]
#
#            form = ModelForm(initial=prefill)
#            formsets, inline_instances = self._create_formsets(request,
#                    form.instance, change=False)
#            adminForm = admin.helpers.AdminForm(
#                form,
#                list(self.get_fieldsets(request, obj)),
#                self.get_prepopulated_fields(request, obj),
#                self.get_readonly_fields(request, obj),
#                model_admin=self)
#            media = self.media + adminForm.media
#
#            inline_formsets = self.get_inline_formsets(request, formsets, inline_instances, obj)
#            for inline_formset in inline_formsets:
#                media = media + inline_formset.media
#
#            # Just prefilling so process form at standard "add" url
#            form_url = reverse('admin:{0[0]}_{0[1]}_add'.format(info))
#            context = dict(
#                    self.admin_site.each_context(request),
#                    title='Add {}'.format(force_text(opts.verbose_name)),
#                    adminform=adminForm,
#                    object_id=None,
#                    original=obj,
#                    is_popup=False,
#                    media=media,
#                    inline_admin_formsets=inline_formsets,
#                    errors=admin.helpers.AdminErrorList(form, formsets),
#                    preserved_filters=self.get_preserved_filters(request),
#                    show_save=True,
#                    show_save_and_continue=True,
#                    form=form,
#                    )
#            return self.render_change_form(request, context, add=add,
#                    form_url=form_url)
#
#        return HttpResponseRedirect(reverse(
#            'admin:{0[0]}_{0[1])_add'.format(info)))


# CIMS Base ModelAdmins
class CIMSBaseModelAdmin(PFPModelAdmin, VersionAdmin):
    """
    Default ModelAdmin for all models in the CIMS data model.

    Implements:
        - Version via reversion
        - Per-field permissions via PFPModelAdmin and CIMSAuthModelForm.
    """
    form = PFPModelForm

    def get_form(self, request, obj=None, **kwargs):
        """Modify field creation callback to include request object."""
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield, request=request, obj=obj)
        return super().get_form(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        """
        Extend to by default pop the obj instance added in self.get_form(), since many form fields
        don't expect additional kwargs.
        """
        kwargs.pop('obj', None)
        return super().formfield_for_dbfield(db_field, request, **kwargs)

    def has_change_permission(self, request, obj=None):
        """Modify for per-object permissions."""
        opts = self.opts
        codename = get_permission_codename('change', opts)
        # If obj None need to return True if permission or per-field permission is held on any
        # applicable object
        return request.user.has_perm('{}.{}'.format(opts.app_label, codename), obj=obj)

    def has_delete_permission(self, request, obj=None):
        """Modify for per-object permissions."""
        opts = self.opts
        codename = get_permission_codename('delete', opts)
        # If obj None need to return True if permission or per-field permission is held on any
        # applicable object
        return request.user.has_perm('{}.{}'.format(opts.app_label, codename), obj=obj)


class TabColInline(PFPInlineAdmin, admin.TabularInline):
    """
    Default TabularInline that sets some defaults.

    Implements:
        - Per-field permissions via PFPInlineAdmin and CIMSAuthModelForm.
    """
    extra = 0
    classes = ['collapse']
    form = PFPModelForm

    def get_formset(self, request, obj=None, **kwargs):
        """Modify field creation callback to include request object."""
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield, request=request, obj=obj)
        return super().get_formset(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        """
        Extend to by default pop the obj instance added in self.get_form(), since many form fields
        don't expect additional kwargs.
        """
        kwargs.pop('obj', None)
        return super().formfield_for_dbfield(db_field, request, **kwargs)


# Concrete ModelAdmins
# Inlines
class TaskInline(TabColInline):
    model = models.Task
    fields = ['title', 'weighting', 'is_exam', 'is_hurdle']
    show_change_link = True
    ordering = ['order']


class ScheduleInline(TabColInline):
    model = models.Schedule
    formfield_overrides = {
            TextField: {
                'widget': Textarea(attrs={
                    'rows': 2,
                    'style': 'vTextArea',
                    })}
            }


class LOInline(TabColInline):
    model = models.LearningOutcome
    show_change_link = True
    ordering = ['order']


class CourseRelatedRoleInline(TabColInline):
    model = models.CourseRelatedRole


class CIInline(TabColInline):
    model = models.CourseInstance
    fields = ['instance_descriptor', 'start_date', 'end_date', 'teachingsession', 'delivery_mode']
    readonly_fields = ['teachingsession', 'delivery_mode']
    can_delete = False
    show_change_link = True
    ordering = ['-start_date']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('teachingsession', 'delivery_mode')


class WeightInline(TabColInline):
    model = models.Weight
    ordering = ['value']


class WeightedLOToSOInline(TabColInline):
    """
    This shouldn't actually be used as we can't properly validate weights within a ModelAdmin
    """
    model = models.WeightedLOToSO
    verbose_name = 'Weighted link to Standard Objective'
    verbose_name_plural = 'Weighted links to Standard Objectives'
    ordering = ['learningoutcome_id']

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        obj = kwargs.pop('obj', None)  # type: models.LearningOutcome
        formfield = super().formfield_for_dbfield(db_field, request, **kwargs)
        # LearningOutcomes must come from those linked to the parent CourseVersion.
        if db_field.name == 'standardobjective':
            if obj is None:
                formfield.queryset = models.StandardObjective.objects.none()
            else:
                formfield.queryset = models.StandardObjective.objects.filter(
                        courseversion=obj.courseversion_id,
                        )
        # Weights must come from a WeightingScheme linked to the parent CourseVersion
        elif db_field.name == 'weights':
            if obj is None:
                formfield.queryset = models.Weight.objects.none()
            else:
                formfield.queryset = models.Weight.objects.filter(
                        weightingscheme__courseversion=obj.courseversion_id
                        )
        return formfield


class WeightedTaskToLOInline(TabColInline):
    """
    This shouldn't actually be used as we can't properly validate weights within a ModelAdmin
    """
    model = models.WeightedTaskToLO
    verbose_name = 'Weighted link to Learning Outcome'
    verbose_name_plural = 'Weighted links to Learning Outcomes'
    ordering = ['task_id']

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        obj = kwargs.pop('obj', None)  # type: models.Task
        formfield = super().formfield_for_dbfield(db_field, request, **kwargs)
        # LearningOutcomes must come from those linked to the parent CourseVersion.
        if db_field.name == 'learningoutcome':
            if obj is None:
                formfield.queryset = models.LearningOutcome.objects.none()
            else:
                courseinstance = obj.courseinstance
                formfield.queryset = models.LearningOutcome.objects.filter(
                        courseversion=courseinstance.courseversion_id,
                        )
        elif db_field.name == 'weight':
            if obj is None:
                formfield.queryset = models.Weight.objects.none()
            else:
                courseinstance = obj.courseinstance
                formfield.queryset = models.Weight.objects.filter(
                        weightingscheme__courseversion=courseinstance.courseversion_id
                        )
        return formfield


# Standalone admins
@admin.register(models.Course)
class CourseAdmin(CIMSBaseModelAdmin):
    pass


@admin.register(models.CourseVersion)
class CourseVersionAdmin(CIMSBaseModelAdmin):
    fieldsets = (
            (None, {
                'fields': ('course', 'version_descriptor',
                           ('status', 'status_freetext'),
                           ('approval_date', 'approval_description'),
                           'title', 'description', 'delivery_mode', 'custodians', 'unit_value',
                           ('classification', 'academiccareer'),
                           'standards_frameworks', 'objectives', 'weightingschemes',
                           'course_group', 'assumed_knowledge',
                           )
                }),
            ('Requisite Statements', {
                'fields': ('prerequisite_courses', 'corequisite_courses', 'incompatible_courses',),
                'classes': ('collapse',),
                }),
            ('Misc Statements', {
                'fields': ('research_led_teaching', 'required_resources', 'additional_costs',
                           'participation', 'field_trips', 'recommended_resources',
                           'other_information',),
                'classes': ('collapse',),
                }),
            )

    inlines = [LOInline]
    filter_horizontal = ['custodians', 'standards_frameworks', 'objectives', 'course_group']
    list_filter = ('course', 'courseinstance__taught_by')
    change_form_template = 'cims/admin/courseversion_changeform.html'
    # clear_fields = ['id', 'version_descriptor', 'approval_date', 'approval_desc',
    #            'objectives']

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """Add some context so we can provide an option to add annotations."""
        content_type = ContentType.objects.get_for_model(self.model)
        current_version = Version.objects.get_for_object_reference(self.model, object_id)
        if current_version:
            current_version = current_version.first()
        extra_context = {
                'content_type': content_type,
                'current_version': current_version,
                }
        return super().change_view(request, object_id, form_url, extra_context)

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        """
        StandardObjectives must come from StandardsFrameworks selected for this CourseVersion.
        """
        obj = kwargs.pop('obj', None)
        formfield = super().formfield_for_dbfield(db_field, request, **kwargs)
        if db_field.name == 'objectives':
            if obj is None:
                formfield.queryset = models.StandardObjective.objects.none()
            else:
                formfield.queryset = get_leaf_objectives(obj.id)
        return formfield

    # def get_urls(self):
    #    def wrap(view):
    #        def wrapper(*args, **kwargs):
    #            return self.admin_site.admin_view(view)(*args, **kwargs)
    #        wrapper.model_admin = self
    #        return update_wrapper(wrapper, view)
#
#        info = self.model._meta.app_label, self.model._meta.model_name
#
#        urls = [
#                url(r'^copy/$', wrap(self.copy_view),
#                    name='{0[0]}_{0[1]}_copy'.format(info)),
#                ]
#        return urls + super().get_urls()


@admin.register(models.CourseInstance)
class CourseInstanceAdmin(CIMSBaseModelAdmin):
    fieldsets = (
            (None, {
                'fields': ('courseversion', 'instance_descriptor', 'start_date', 'end_date',
                           'taught_by', 'conveners',
                           ('class_number', 'teachingsession'),
                           'delivery_mode', 'course_url', 'delivery_group', 'inherent_requirements',
                           'workload', 'prescribed_texts',
                           )
                }),
            ('Assignment Submission Statements', {
                'fields': ('online_submission', 'late_submission_policy', 'resubmission_policy',
                           'reference_requirements', 'assignment_return', 'hard_copy_submission',
                           ),
                'classes': ('collapse',),
                }),
            ('Miscellaneous Statements', {
                'fields': ('preliminary_reading_list', 'indicative_reading_list',
                           'staff_feedback_statement', 'exams', 'exam_material',
                           'other_information',
                           ),
                'classes': ('collapse',),
                }),
            )
    filter_horizontal = ['conveners', 'delivery_group']
    inlines = [ScheduleInline, TaskInline, CourseRelatedRoleInline]
    list_filter = ('courseversion__course', 'taught_by')


@admin.register(models.LearningOutcome)
class LearningOutcomeAdmin(CIMSBaseModelAdmin):
    pass


@admin.register(models.Task)
class TaskAdmin(CIMSBaseModelAdmin):
    pass


@admin.register(models.OrganisationalUnit)
class OrganisationalUnitAdmin(CIMSBaseModelAdmin, MPTTModelAdmin):
    pass


@admin.register(models.StandardObjective)
class StandardObjectiveAdmin(CIMSBaseModelAdmin, MPTTModelAdmin):
    pass


@admin.register(models.WeightingScheme)
class WeightingSchemeAdmin(CIMSBaseModelAdmin):
    inlines = [WeightInline]


# @admin.register(models.WeightedLOToSO)
# class WeightedLOToSOAdmin(CIMSBaseModelAdmin):
#
#     def get_form(self, request, obj=None, **kwargs):
#         form = super().get_form(request, obj, **kwargs)
#         weight = form.base_fields['weight']
#         weight.queryset = models.Weight.objects.none()
#         if obj is not None:
#             weight.queryset = models.Weight.objects.filter(
#                     weightingscheme__in=models.WeightingScheme.objects.filter(
#                         courseversion__learningoutcome=obj.learningoutcome_id,
#                         )
#                     )
#         return form


admin.site.register(models.Annotation, CIMSBaseModelAdmin)
admin.site.register(models.AcademicCareer, CIMSBaseModelAdmin)
admin.site.register(models.CourseClassification, CIMSBaseModelAdmin)
admin.site.register(models.CourseRelatedRole, CIMSBaseModelAdmin)
admin.site.register(models.CourseRelatedRoleTitle, CIMSBaseModelAdmin)
admin.site.register(models.DeliveryMode, CIMSBaseModelAdmin)
admin.site.register(models.ReviewStatus, CIMSBaseModelAdmin)
admin.site.register(models.Schedule, CIMSBaseModelAdmin)
admin.site.register(models.StandardsBody, CIMSBaseModelAdmin)
admin.site.register(models.StandardsFramework, CIMSBaseModelAdmin)
admin.site.register(models.TeachingSession, CIMSBaseModelAdmin)


# cims.auth ModelAdmins
@admin.register(ConditionalPermissionsMap)
class ConditionalPermissionsMapAdmin(CIMSBaseModelAdmin):
    form = ConditionalPermissionsMapForm
    filter_horizontal = ['conditional_permissions']


@admin.register(ObjectStatusPermissionsMap)
class ObjectStatusPermissionsMapAdmin(CIMSBaseModelAdmin):
    form = ObjectStatusPermissionsMapForm


@admin.register(CIMSUser)
class UserAdmin(BaseUserAdmin, CIMSBaseModelAdmin):
    fieldsets = (
            (None, {'fields': ('username', 'password')}),
            (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
            (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'roles',
                                'user_permissions')}),
            (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
            )
    form = CIMSUserChangeForm
    add_form = CIMSUserCreationForm
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'roles')
    filter_horizontal = ('roles', 'user_permissions',)
    actions = ['set_unusable_password']

    def set_unusable_password(self, request, queryset):
        """
        List action command to set an unusable password for the user, so force authentication
        against an external source e.g. LDAP.

        Parameters
        ----------
        request : HTTPRequest
                  Request for this action.
        queryset : Queryset
                   Queryset of users to apply action to.
        """
        change_perm = 'cims_auth.change_user__password'
        if request.user.has_perm(change_perm):
            for user in queryset:
                if request.user.has_perm(change_perm, user):
                    user.set_unusable_password()
                    user.save()
                    self.log_change(request, user, construct_change_message(['password']))
                else:
                    messages.add_message(
                            request,
                            messages.WARNING,
                            ('Password for user {} was not updated as you lack permission for that '
                             'user.'.format(user.username)),
                            )

        else:
            raise PermissionDenied('You do not have permission to perform this action.')

    set_unusable_password.short_description = 'Set user password unusable (require LDAP auth)'


@admin.register(Role)
class RoleAdmin(CIMSBaseModelAdmin):
    filter_horizontal = ('permissions', 'child_roles')
