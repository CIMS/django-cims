"""
Uses pandc_scraper.PandCScraper to compare CourseVersions against the PandC website.
"""
import bleach
import re
import time
from copy import copy
from datetime import date, timedelta
from difflib import HtmlDiff
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from html import unescape
from itertools import chain, zip_longest

from cims.models import CourseInstance, OrganisationalUnit
from .pandc_scraper import PandCScraper


_cleaner_attrs = {'*': [u'class', u'id'], 'td': [u'colspan', u'nowrap'], 'th': [u'colspan'],
                  'table': [u'rules', u'cellspacing', u'cellpadding']}
_cleaner_attrs.update(bleach.ALLOWED_ATTRIBUTES)


class PandCDiffer(object):
    """
    Class for comparing a CourseVersion with an entry of that course on the Programs and Courses
    website.

    Attributes
    ----------
    supported_attrs : Iterable
                      List of keys from output of PandCScraper.get_course_dict() that we support
                      comparison with
    Parameters
    ----------
    courseversion : CourseVersion
                    CourseVersion instance to compare against P&C website
    year : str, optional
           Four digit string representation of the year you want to compare against on
           programsandcourses.anu.edu.au
    """
    supported_attrs = ('code', 'title', 'introduction', 'learning_outcomes',
                       'indicative_assessments', 'workload', 'mode', 'prescribed_texts',
                       'offered_by', 'other_information', 'conveners', 'req_and_incompat',
                       'offerings')
    difftable_tablen = 4
    difftable_colwrap = 80
    difftable_from = 'CIMS'
    difftable_to = 'PandC'
    cleaner = bleach.sanitizer.Cleaner(
            tags=bleach.ALLOWED_TAGS + [u'br', u'h5', u'li', u'p', u'span', u'table', u'colgroup',
                                        u'thead', u'th', u'tbody', u'tr', u'td', u'ul'],
            attributes=_cleaner_attrs,
            )

    def __init__(self, courseversion, year=None):
        self.courseversion = courseversion
        self.scraper = PandCScraper(courseversion.course.code, year=year)
        self.pandc_dict = self.scraper.get_course_dict()
        self.course_dict = self._get_course_dict(self.courseversion)
        self.html_differ = HtmlDiff(
                tabsize=self.difftable_tablen,
                wrapcolumn=self.difftable_colwrap
                )

    def _get_course_dict(self, courseversion):
        """
        Construct a dict of information associated with the CourseVersion `courseversion` to
        compare to PandC.
        """
        User = get_user_model()

        # Can't guarantee delivery mode is set.
        if courseversion.delivery_mode_id is not None:
            delivery_mode = courseversion.delivery_mode.mode
        else:
            delivery_mode = ''

        # Offered by is a bit more complex now...get set of OrganisationalUnits linked to
        # CourseInstances related to our CourseVersion that are running this year.
        ous = OrganisationalUnit.objects.filter(
                courseinstance__courseversion=courseversion,
                courseinstance__start_date__year=self.scraper.year,
                ).distinct().values_list('name', flat=True)
        offered_by = 'and '.join(ous)

        return {
            'code': courseversion.course.code,
            'title': courseversion.title,
            'introduction': courseversion.description,
            'learning_outcomes': list(courseversion.learningoutcome_set.values_list(
                'outcome',
                flat=True
                )),
            'indicative_assessments': [
                '{} - {}'.format(mode, ias or 'None') for mode, ias in
                courseversion.get_indicative_assessments().items()
                ],
            'workload': list(set(
                courseversion.courseinstance_set.values_list('workload', flat=True)
                )),
            'mode': delivery_mode,
            'conveners': [
                format_html('{} {}', convener[0], convener[1]) for convener in
                User.objects.filter(
                    courseinstance__start_date__year=self.scraper.year,
                    courseinstance__courseversion=courseversion,
                    ).values_list('first_name', 'last_name')
                ],
            'prescribed_texts': list(set(
                courseversion.courseinstance_set.values_list('prescribed_texts', flat=True)
                )),
            'offered_by': offered_by,
            'other_information': courseversion.other_information,
            }

    def _compare_req_and_incompat(self, cims_attr, pandc_attr):
        """
        Return if all of the requisite statements stored in CIMS are substrings of the statement on
        PandC.
        """
        # For all the requisite statements on P&C, test for any remaining words after stripping the
        # statements out. Since the requisites fields are stored as BleachFields characters like
        # ampersands are escaped, so unescape the string to do comparisons
        requisites = [unescape(self.courseversion.prerequisite_courses),
                      unescape(self.courseversion.corequisite_courses),
                      unescape(self.courseversion.incompatible_courses)]
        all_in = True
        stripped_pandc = copy(pandc_attr)
        for r in requisites:
            if r in pandc_attr:
                stripped_pandc = stripped_pandc.replace(r, '', count=1)
            else:
                all_in = all_in and False

        if all_in and not re.search(r'\w+', stripped_pandc):
                return True
        return False

    def _compare_conveners(self, cims_attr, pandc_attr):
        """
        Return if all of the convener names can be found on PandC. Ignore honorifics etc. Compare
        with CourseInstance conveners from CIMS, rather than CourseVersion custodians.
        """
        # Return False if one set and not the other
        if bool(cims_attr['conveners']) != bool(pandc_attr['conveners']):
                return False
        # Test for substrings
        for convener in cims_attr['conveners']:
            if convener not in pandc_attr['conveners']:
                return False
        return True

    def compare_attribute(self, attr_label, cims_attr, pandc_attr):
        """
        Compare an attribute from the CIMS with information from PandC. Assume we'll get a
        list or a single value coercable to a string.
        """
        # If there's a custom comparison function use it.
        comparison_func = getattr(self, '_compare_{}'.format(attr_label), None)
        if comparison_func is not None:
            return comparison_func(cims_attr, pandc_attr)

        if isinstance(cims_attr, list):
            cims_attr.sort()
        if isinstance(pandc_attr, list):
            pandc_attr.sort()
        return cims_attr == pandc_attr

    def get_mismatched_attributes(self):
        """List the names of course attributes/sections from CIMS/PandC that don't match."""
        return [
                attr for attr in self.supported_attrs
                if not self._compare_attribute(attr, self.course_dict[attr], self.pandc_dict[attr])
                ]

    def _render_tuplediff(self, list1, list2, tuple_cols=None):
        """
        Generate a table indicating where two lists of equal size tuples differ.

        Parameters
        ----------
        list1 : List
                List of sortable tuples
        list2 : List
                List of sortable tuples
        tuple_cols : List, optional
                     Sorted list of headings for items in the tuples.
        """
        if list1 and list2:
            assert len(list1[0]) == len(list2[0]), ('render_tuplediff() requires tuples of the '
                                                    'same size.')
        list1.sort()
        list2.sort()
        tuple_len = 1
        if list1:
            tuple_len = len(list1[0])
        elif list2:
            tuple_len = len(list2[0])
        elif tuple_cols is not None:
            tuple_len = len(tuple_cols)

        if tuple_cols is not None:
            assert tuple_len == len(tuple_cols), ('Parameter tuple_cols must have the same length '
                                                  'as the list tuples.')

        output = ['<table class="diff">\n<thead>\n<tr>\n<th class="diff_next"></th>',
                  '<th class="diff_header" colspan="{}">{}</th>'.format(
                      tuple_len,
                      self.difftable_from
                      ),
                  '<th class="diff_next"></th>',
                  '<th class="diff_header" colspan="{}">{}</th>'.format(
                      tuple_len,
                      self.difftable_to
                      ),
                  '</tr>', ]

        if tuple_cols is not None:
            output.append('<tr>\n<th class="diff_next"></th>')
            for col in tuple_cols:
                output.append('<th class="diff_header">{}</th>'.format(col))
            output.append('<th class="diff_next"></th>')
            for col in tuple_cols:
                output.append('<th class="diff_header">{}</th>'.format(col))
            output.append('</tr>')
        output.append('</thead>\n<tbody>')

        filler = tuple(['' for i in range(tuple_len)])
        for tuple1, tuple2 in zip_longest(list1, list2, fillvalue=filler):
            output.append('<tr>\n<td class="diff_next"></td>')
            for col in tuple1:
                output.append(format_html('<td>{}</td>', col))
            output.append('<td class="diff_next"></td>')
            for col1, col2 in zip(tuple1, tuple2):
                if col1 == col2:
                    output.append(format_html('<td>{}</td>', col2))
                else:
                    output.append(format_html('<td><span class="diff_chg">{}</span></td>', col2))

            output.append('</tr>')

        output.append('</tbody>\n</table>')
        return mark_safe(self.cleaner.clean('\n'.join(output)))

    def _gen_diff_table(self, attr_label):
        assert attr_label in self.supported_attrs, ('Attempting to compare with an unsupported '
                                                    'section of the Programs and Courses website.')
        course_attr = self.course_dict[attr_label]
        pandc_attr = self.pandc_dict[attr_label]
        if not isinstance(course_attr, list):
            course_attr = [course_attr]
        if not isinstance(pandc_attr, list):
            if pandc_attr is None:
                pandc_attr = []
            else:
                pandc_attr = [pandc_attr]

        course_attr.sort()
        pandc_attr.sort()

        # We use unescaped input for the diff comparison, then run the resulting html through a
        # Bleach Cleaner that allows table related tags so we strip unsafe html
        output = self.html_differ.make_table(
                course_attr,
                pandc_attr,
                fromdesc=self.difftable_from,
                todesc=self.difftable_to,
                )
        return mark_safe(self.cleaner.clean(output))

    # HTML table diff output
    @property
    def code_difftable(self):
        if not hasattr(self, '_code_difftable'):
            self._code_difftable = self._gen_diff_table('code')
        return self._code_difftable

    @property
    def title_difftable(self):
        if not hasattr(self, '_title_difftable'):
            self._title_difftable = self._gen_diff_table('title')
        return self._title_difftable

    @property
    def introduction_difftable(self):
        if not hasattr(self, '_introduction_difftable'):
            self._introduction_difftable = self._gen_diff_table('introduction')
        return self._introduction_difftable

    @property
    def learning_outcomes_difftable(self):
        if not hasattr(self, '_learning_outcomes_difftable'):
            self._learning_outcomes_difftable = self._gen_diff_table('learning_outcomes')
        return self._learning_outcomes_difftable

    @property
    def indicative_assessments_difftable(self):
        if not hasattr(self, '_indicative_assessments_difftable'):
            self._indicative_assessments_difftable = self._gen_diff_table('indicative_assessments')
        return self._indicative_assessments_difftable

    @property
    def workload_difftable(self):
        if not hasattr(self, '_workload_difftable'):
            self._workload_difftable = self._gen_diff_table('workload')
        return self._workload_difftable

    @property
    def mode_difftable(self):
        if not hasattr(self, '_mode_difftable'):
            self._mode_difftable = self._gen_diff_table('mode')
        return self._mode_difftable

    @property
    def prescribed_texts_difftable(self):
        if not hasattr(self, '_prescribed_texts_difftable'):
            self._prescribed_texts_difftable = self._gen_diff_table('prescribed_texts')
        return self._prescribed_texts_difftable

    @property
    def offered_by_difftable(self):
        if not hasattr(self, '_offered_by_difftable'):
            self._offered_by_difftable = self._gen_diff_table('offered_by')
        return self._offered_by_difftable

    @property
    def other_information_difftable(self):
        if not hasattr(self, '_other_information_difftable'):
            self._other_information_difftable = self._gen_diff_table('other_information')
        return self._other_information_difftable

    @property
    def req_and_incompat_difftable(self):
        if not hasattr(self, '_req_and_incomp_difftable'):
            labels = ['Pre-requisites', 'Co-requisites', 'Incompatibles']
            statements = [unescape(self.courseversion.prerequisite_courses),
                          unescape(self.courseversion.corequisite_courses),
                          unescape(self.courseversion.incompatible_courses)]
            output = ['<h5 class="text-muted">Requisites statement on Programs and Courses '
                      'is:</h5><p>{}</p>'.format(self.pandc_dict['req_and_incompat']),
                      '<h5 class="text-muted">CIMS statements</h5>',
                      '<table class="table table-striped table-auto">\n',
                      '<thead class="thead-light">\n<tr>\n<th scope="col">Label</th>',
                      '<th scope="col">Content</th>\n<th scope="col">Found</th>',
                      '</tr>\n</thead>\n<tbody>']

            for label, content in zip(labels, statements):
                lineout = '<tr><td>{}</td><td>{}</td>'.format(label, content)
                if content in self.pandc_dict['req_and_incompat']:
                    lineout = lineout + '<td class="diff_add">Y</td></tr>'
                else:
                    lineout = lineout + '<td class="diff_sub">N</td></tr>'
                output.append(lineout)

            output.append('</tbody>\n</table>')
            self._req_and_incomp_difftable = mark_safe(self.cleaner.clean('\n'.join(output)))
        return self._req_and_incomp_difftable

    @property
    def conveners_difftable(self):
        """
        HTML fragment checking conveners listed on P&C versus class custodians for the given year
        listed in CIMS.
        """
        if hasattr(self, '_conveners_difftable'):
            return self._conveners_difftable

        output = [
                '<table class="table table-striped table-auto">',
                '<thead class="thead-light">\n<tr>',
                '<th scope="col">CIMS Convener</th>\n',
                '<th scope="col">Found</th>\n',
                '<th scope="col">P&C Convener</th>\n</tr>\n</thead>\n<tbody>'
                ]

        # "conveners" entry in pandc_dict could be None or a list
        if self.pandc_dict['conveners']:
            pandc_conveners = self.pandc_dict['conveners'].copy()
        else:
            pandc_conveners = []

        # Construct a dict of conveners from the database, and which convener entries from P&C they
        # match
        convener_matches = {
                cims_convener: [
                    pandc_convener_index
                    for pandc_convener_index, pandc_convener in enumerate(pandc_conveners)
                    if cims_convener in pandc_convener
                    ]
                for cims_convener in self.course_dict['conveners']
                }

        # Go through the match dict and generate output lines for each entry
        for convener, matches in convener_matches.items():
            if matches:
                for match in matches:
                    output.append('<tr><td>{}</td><td class="diff_add">Y</td><td>{}</td>'
                                  '</tr>\n'.format(convener, pandc_conveners[match]))
            else:
                output.append('<tr><td>{}</td><td class="diff_sub">N</td><td></td>'
                              '</tr>'.format(convener))

        # Generate output lines for any P&C entries that have no match in our database
        for convener in [
                pandc_convener
                for index, pandc_convener in enumerate(pandc_conveners)
                if index not in set(chain.from_iterable(convener_matches.values()))
                ]:
            output.append('<tr><td></td><td class="diff_sub">N</td><td>{}</td>'
                          '</tr>'.format(convener))

        output.append('</tbody>\n</table>')
        self._conveners_difftable = mark_safe(self.cleaner.clean('\n'.join(output)))
        return self._conveners_difftable

    @property
    def offerings_difftable(self):
        if not hasattr(self, '_offerings_difftable'):
            output = ['<table class="table table-striped table-auto">',
                      '<thead class="thead-light">\n<tr>',
                      '<th scope="col">Year</th>',
                      '<th scope="col">Session</th>',
                      '<th scope="col">Class number</th>',
                      '<th scope="col">Class start date</th>',
                      '<th scope="col">Class end date</th>',
                      '<th scope="col">Mode of delivery</th>',
                      '<th scope="col">Found</th>',
                      '<th scope="col">CourseInstance(s)</th>',
                      '</tr>\n</thead>\n<tbody>']

            # Generate offering rows
            for year, sessions in self.pandc_dict['offerings'].items():
                for session, offerings in sessions.items():
                    for offering in offerings:
                        lineout = ('<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td>'
                                   '<td>{}</td>'.format(
                                       year,
                                       session,
                                       offering['Class number'],
                                       offering['Class start date'],
                                       offering['Class end date'],
                                       offering['Mode Of Delivery'],
                                       )
                                   )
                        start_date = time.strptime(offering['Class start date'], '%d %b %Y')
                        start_date = date.fromtimestamp(time.mktime(start_date))
                        end_date = time.strptime(offering['Class end date'], '%d %b %Y')
                        end_date = date.fromtimestamp(time.mktime(end_date))

                        # Try to find a matching CourseInstance, if we can't then a list of
                        # candidate instances
                        try:
                            courseinstance = CourseInstance.objects.get(
                                courseversion_id=self.courseversion.pk,
                                class_number=offering['Class number'],
                                teachingsession__start_date=start_date,
                                teachingsession__end_date=end_date,
                                delivery_mode__mode=offering['Mode Of Delivery'],
                                )
                            lineout = lineout + '<td class="diff_add">Y</td>'
                            lineout = lineout + '<td><a href="{}">{}</a></td></tr>'.format(
                                    courseinstance.get_absolute_url(),
                                    str(courseinstance),
                                    )
                        except ObjectDoesNotExist:
                            lineout = lineout + ('<td class="diff_sub">N</td><td>Candidate '
                                                 'offerings:<ul>')
                            # Candidate classes are any linked to this CourseVersion with a start
                            # and end a month either side of our scraped values, or a linked
                            # teachingsession with start and end dates in the same range.
                            # Don't need to be precise with dates so add and substract 31 days to
                            # get "about a month"
                            rough_month = timedelta(days=31)
                            start_range = (start_date - rough_month, start_date + rough_month)
                            end_range = (end_date - rough_month, end_date + rough_month)
                            qs = CourseInstance.objects.filter(
                                    (
                                        (
                                            Q(start_date__range=start_range)
                                            | Q(end_date__range=end_range)
                                            )
                                        |
                                        (
                                            Q(teachingsession__start_date__range=start_range)
                                            | Q(teachingsession__end_date__range=end_range)
                                            )
                                        ),
                                    courseversion=self.courseversion,
                                    )
                            for courseinstance in qs:
                                lineout = lineout + '<li><a href="{}">{}</a></li>'.format(
                                    courseinstance.get_absolute_url(),
                                    str(courseinstance),
                                    )
                            lineout = lineout + '</ul></td></tr>'
                        output.append(lineout)

            output.append('</tbody>\n</table>')
            self._offerings_difftable = mark_safe(self.cleaner.clean('\n'.join(output)))
        return self._offerings_difftable
