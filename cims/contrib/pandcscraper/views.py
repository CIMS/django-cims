import re
from datetime import datetime
from difflib import _legend as difftable_legend
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ChoiceField, Form
from django.shortcuts import get_object_or_404, render
from requests.exceptions import ConnectionError, HTTPError, Timeout, TooManyRedirects

from cims.models import Course, CourseVersion
from cims.contrib.pandcscraper.pandcdiffer import PandCDiffer


def pandc_diff(request, course_code, courseversion_pk=None):
    """
    View for displaying diffs of a course version against the P&C website.

    Parameters
    ----------
    course_code : String
                Course Code of the Course to compare against P&C
    """
    course = get_object_or_404(Course, code__iexact=course_code)
    context = {
            'course': course,
            'diff_legend': difftable_legend,
            'title': 'Programs and Courses comparison for: {}'.format(course.code),
            }
    try:
        if courseversion_pk is None:
            courseversion = course.get_current_courseversion(select_related=['course'])
        else:
            courseversion = CourseVersion.objects.get(pk=courseversion_pk)
        context.update(courseversion=courseversion)
    except ObjectDoesNotExist:
        messages.add_message(
                request,
                messages.ERROR,
                'No appropriate CourseVersion found to compare to Programs and Courses. This '
                'implies none have been approved.',
                )
        courseversion = None

    # Compare against the P&C course page for the year passed as a GET query
    # argument
    if courseversion:
        try:
            if request.GET.get('year') and re.match('^[0-9]{4}$', request.GET['year']):
                year = request.GET.get('year')
                differ = PandCDiffer(courseversion, year=year)
            else:
                differ = PandCDiffer(courseversion)
            context.update(pandc_differ=differ)
            years = differ.scraper.get_available_years()
            initial_year = differ.scraper.get_academic_year()
        except (ConnectionError, HTTPError, Timeout, TooManyRedirects) as exc:
            messages.add_message(
                    request,
                    messages.ERROR,
                    'Trying to retrieve the page from Programs and Courses failed. The error '
                    'was: {}'.format(str(exc)),
                    )
            current_year = datetime.now().year
            years = [current_year + offset for offset in range(1, -4, -1)]
            if 'year' in locals():
                initial_year = year if (int(year) in years) else None
            else:
                initial_year = None

        yearchoices = [(y, y) for y in years]
        if initial_year is None:
            yearchoices.insert(0, (None, "----"))

        class YearForm(Form):
            year = ChoiceField(choices=yearchoices, label='Programs and Courses Academic Year')

        year_form = YearForm(initial={'year': initial_year})
        context.update(year_form=year_form)

    return render(request, 'pandcscraper/course_pandcdiff.html', context)
