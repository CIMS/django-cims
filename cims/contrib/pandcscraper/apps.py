from django.apps import AppConfig


class PandCScraperConfig(AppConfig):
    name = 'cims.contrib.pandcscraper'
    label = 'pandcscraper'
