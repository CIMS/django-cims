from django.conf.urls import url
from cims.contrib.pandcscraper import views


app_name = 'pandcscraper'

urlpatterns = [
    url(r'^pandcdiff/([A-Za-z]{4}[0-9]{4}[A-Za-z]?)/$',
        views.pandc_diff, name='pandcdiff'),
    url(r'^pandcdiff/([A-Za-z]{4}[0-9]{4}[A-Za-z]?)/version/([0-9]+)/$',
        views.pandc_diff, name='pandcdiff.version'),
]
