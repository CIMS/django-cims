"""
Models for the main data model of the CIMS application.
"""
# from django.db import models
# from django.core.validators import MaxValueValidator, MinValueValidator
#
# from cims.models import CIMSBaseModel, CourseVersion
#
#
# # Model to store results of doing a diff against PandC
# class PandCDiffResult(CIMSBaseModel):
#     courseversion = models.ForeignKey(
#             CourseVersion,
#             on_delete=models.CASCADE,
#             help_text='The CourseVersion that was compared to Programs and Courses.'
#             )
#     academic_year = models.IntegerField(
#             validators=[
#                 MinValueValidator(1946, message='No Academic Years prior to founding please!'),
#                 MaxValueValidator(2999, message='That\'s...optimistic.')
#                 ],
#             help_text='The Academic Year this PandC page belongs too.',
#             )
#     pandc_url = models.URLField(
#             help_text='The Programs and Courses URL the comparison was made against.'
#             )
#     page_html = models.TextField(
#             help_text=('The raw html from the Programs and Courses page the comparison was made '
#                        'against.')
#             )
#     supported_attrs = models.CharField(
#             max_length=200,
#             blank=True,
#             help_text=('Which sections of the PandC website were checked. Separate field names '
#                        'with blank spaces.'),
#             )
#     failed_attrs = models.CharField(
#             max_length=200,
#             blank=True,
#             help_text=('Which sections of the PandC website failed the comparison check. Separate '
#                        'field names with blank spaces.'),
#             )
#
#     def __str__(self):
#         return '{} compared against {} on {}'.format(
#                 self.courseversion,
#                 self.pandc_url,
#                 self.created,
#                 )
