"""
A scraper for extracting course details from the mess of semi-parseable HTML that is
the ANU's Programs and Courses website (http://programsandcourses.anu.edu.au)
"""
import re
import requests
from bs4 import BeautifulSoup
from requests.exceptions import HTTPError


class PandCScraper(object):
    """
    Attempts to extract various bits of information from the Programs and Courses page for a Course
    for a given year.

    Parameters
    ----------
    code : String
           Course code for the Course we want to scrape.
    year : String/Integer, optional
           Year to scrape details for if we don't want the current P&C default year.
    """
    pandc_default_url = 'http://programsandcourses.anu.edu.au/course/{}/'
    pandc_year_url = 'http://programsandcourses.anu.edu.au/{}/course/{}/'

    def __init__(self, code, year=None):
        self.code = code.upper()

        if year is None:
            self.pandc_url = self.pandc_default_url.format(code.upper())
        else:
            self.pandc_url = self.pandc_year_url.format(year, code.upper())

        response = requests.get(self.pandc_url, timeout=10)
        response.raise_for_status()

        # html5lib parser is slow and isn't built in, but seems to deal with some
        # poorly formatted P&C HTML better (e.g. <br> tags being immediately closed
        # instead of nesting the following content inside it)
        self.soup = BeautifulSoup(response.content, "html5lib")
        if self.soup.find('p', class_='error-page-message'):
            raise HTTPError('The page {} could not be found'.format(self.pandc_url))

        if year is None:
            year = self.get_academic_year()
        self.year = year

        # Get roots of various sections
        self.roots = {
                'introduction': self.soup.find("div", id="introduction"),
                'learning_outcomes': self.get_siblings_after("learning-outcomes"),
                'indicative_assessments': self.get_siblings_after("indicative-assessment"),
                'workload': self.get_siblings_after("workload"),
                'prescribed_texts': self.get_siblings_after("prescribed-texts"),
                'other_information': self.get_siblings_after("other-information"),
                'req_and_incomp': self.get_siblings_after("incompatibility"),
                }

    @staticmethod
    def _fix_whitespace(text):
        """Replace whitespace in a string with single spaces."""
        return ' '.join(text.split())

    def _join_strings(self, strings):
        """Replace whitespace in strings with single spaces, then join with newlines."""
        return "\n".join([self._fix_whitespace(string) for string in strings])

    def get_siblings_after(self, start_id, start_tag='h2', end_tag=None):
        if end_tag is None:
            end_tag = start_tag

        strt = self.soup.find(start_tag, id=start_id)

        if strt is None:
            return None

        out_root = BeautifulSoup("", "html.parser")
        # out_root = out_soup.document

        contents = []
        nxt = strt.next_sibling
        while nxt is not None and nxt.name != end_tag:
            contents.append(nxt)
            nxt = nxt.next_sibling

        for c in contents:
            out_root.append(c)

        return out_root

    def get_summary_detail(self, detail_name):
        """
        Extract the entry identified by `detail_name` from the course summary block. Return a
        list of strings in the detail block, or `None` if no block matching `detail_name` was
        found.
        """
        # Get the div containing the summary details
        block = self.soup.find('div', class_='degree-summary__codes')
        if not block:
            return None

        # Summary details are in a ul, with headings and contents in span elements. Work out which
        # of the li elements is the one we want by matching against the 'heading' span contained
        # within it.
        ul_elements = block.find_all('li', class_='degree-summary__code')
        detail_li = None
        # Take the first list element that matches our detail name
        try:
            detail_li = next((
                    li_element
                    for li_element in ul_elements
                    if detail_name in li_element.find(
                        'span',
                        class_='degree-summary__code-heading')
                    ))
        except StopIteration:
            return None

        return [
                span.get_text(strip=True)
                for span in detail_li.find_all('span', class_='degree-summary__code-text')
                if span.get_text(strip=True)
                ]

    def get_offerings_table(self):
        return self.soup  # soup.find("div", id="tabs-container").find("table")

    @staticmethod
    def get_offerings_headings(table):
        # table = get_offerings_table(soup)
        ths = table.find("thead").find("tr").find_all("th")
        return [a.get_text(strip=True) for a in ths]

    @staticmethod
    def get_offerings_contents(table):
        # table = get_offerings_table(soup)
        trs = table.find("tbody").find_all("tr")[1:]
        return [
                [td.get_text(strip=True) for td in tr.find_all("td")]
                for tr in trs
                ]

    def get_offerings_dict(self, soup):
        headings = self.get_offerings_headings(soup)
        contents = self.get_offerings_contents(soup)
        offerings = []
        for row in contents:
            if len(row) > 1:
                # offerings rows can (very occasionally) consist of only a text heading
                # (see ENGN4520). Exclude these rows.
                offerings.append(dict(zip(headings, row)))
        return offerings

    def get_offerings_detail(self, soup, detail_string):
        offerings = self.get_offerings_dict(soup)
        return [a[detail_string] for a in offerings]

    def get_offerings_all(self):
        root = self.soup.find("div", id="tabs-container")
        if root is None:
            return root
        years = {}
        for i in range(1, 4):
            id = "course-tab-{}".format(i)
            yearname_el = root.find("a", href="#"+id)
            if yearname_el is not None:
                    yearname = yearname_el.text
                    yeartab = root.find("div", id=id)
                    year_offerings = {}
                    for el_semname in yeartab.find_all("h3"):
                        semname = el_semname.text
                        tbl = el_semname.find_next_sibling("")
                        offerings = self.get_offerings_dict(tbl)
                        year_offerings[semname] = offerings
                    years[yearname] = year_offerings
        return years

    # extract specific parts of course details
    def get_academic_year(self):
        """Extract the acadmic year from the page we're processing."""
        return self.soup.find('a', class_='current-academic-year__toggle').get_text(strip=True)

    def get_available_years(self):
        """Extract the list of academix years available on P&C."""
        soup = self.soup.find('div', class_='intro__apply-to-study__current-academic-year')
        return list(soup.find('ul').stripped_strings)

    def get_title(self):
        return self.soup.find("h1", class_="intro__degree-title").get_text(strip=True)

    def get_introduction(self, unsafe=False):
        if unsafe:
            return self.roots['introduction'].decode_contents()  # return as HTML
        return self.roots['introduction'].get_text().strip()

    def get_learning_outcomes(self):
        if self.roots['learning_outcomes'] is None:
                return list()
        los_list = [
                self._fix_whitespace(text)
                for text in self.roots['learning_outcomes'].stripped_strings
                ]

        # Remove first line if it's introductory text
        if len(los_list) > 0 and (
                los_list[0].startswith("Upon completion")
                or los_list[0].startswith("Upon successful completion")
                or los_list[0].startswith("After completing")
                or los_list[0].startswith("In this course")):
            los_list.pop(0)

        # Remove last two lines if they're the link to the professional skills
        # mapping pages.
        if len(los_list) > 2 and los_list[-2].startswith("Profe"):
                los_list = los_list[:-2]

        # Remove list indices of the form "1. " at the start of outcomes
        los_list = [re.sub(r'^\d+\.\s+', '', lo) for lo in los_list]

        return los_list

    # def get_learning_outcomes_formatted(code):
    #     out_lines = get_learning_outcomes(code)
    #
    #     # Add numbering for formatting if not already present
    #     # NB: Assumes exactly one line of introductory text such as
    #     # "At the end of this course a student can:" or similar.
    #     for i, line in enumerate(out_lines):
    #         if not re.search('^\d', line) and i >= 1:
    #             line = str(i) + ". " + line
    #             out_lines[i] = line
    #
    #     return out_lines

    def get_indicative_assessments(self):
        if self.roots['indicative_assessments'] is None:
            return self.roots['indicative_assessments']

        # We're treating IAs as pretty simple text strings now.
        txt = list(self.roots['indicative_assessments'].stripped_strings)
        # Strip out everything from the Turnitin statement onward
        turnitin = 'The ANU uses Turnitin'
        txt = txt[:next(iter([i for i, line in enumerate(txt) if line.startswith(turnitin)]), -1)]
        return txt

    def get_workload(self):
        if self.roots['workload'] is None:
                return self.roots['workload']
        return self._join_strings(self.roots['workload'].stripped_strings)

    def get_prescribed_texts(self, unsafe=False):
        if self.roots['prescribed_texts'] is None:
            return self.roots['prescribed_texts']
        elif unsafe:
            return self.roots['prescribed_texts'].decode_contents()  # return as HTML
        return self._join_strings(self.roots['prescribed_texts'].stripped_strings)

    def get_other_information(self, unsafe=False):
        if self.roots['other_information'] is None:
            return self.roots['other_information']
        elif unsafe:
            return self.roots['other_information'].decode_contents()  # return as HTML
        return self._join_strings(self.roots['other_information'].stripped_strings)

    def get_mode(self):
        return self.get_summary_detail("Mode of delivery")

    def get_convener(self):
        return self.get_summary_detail("Course convener")

    def get_offered_by(self):
        return self.get_summary_detail("Offered by")

    def get_req_and_incomp(self):
        if self.roots['req_and_incomp'] is None:
            return self.roots['req_and_incomp']
        return self._fix_whitespace(self.roots['req_and_incomp'].get_text())

    def get_course_dict(self):
            return {
                "code": self.code,
                "title": self.get_title(),
                "introduction": self.get_introduction() or "",
                "learning_outcomes": self.get_learning_outcomes() or [],
                "indicative_assessments": self.get_indicative_assessments(),
                "workload": self.get_workload() or "",
                "mode": self.get_mode(),
                "conveners": self.get_convener(),
                "req_and_incompat": self.get_req_and_incomp() or "",
                "offerings": self.get_offerings_all() or {},
                "prescribed_texts": self.get_prescribed_texts() or "",
                "offered_by": self.get_offered_by(),
                "other_information": self.get_other_information() or "",
            }
