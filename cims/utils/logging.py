"""
Functiions for creating messages and cims.contrib.admin.models.LogEntry objects.
"""
import json
from django.contrib.admin.models import LogEntry, ADDITION
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_text


def json_dumps_list(message):
    """If `message` is a list json.dumps() it."""
    if isinstance(message, list):
        message = json.dumps(message)
    return message


def construct_change_message(changed_fields=None, create=False):
    """
    Contruct a change message for storing in a LogEntry instance.

    Parameters
    ----------
    changed_fields : list, optional
                     A list of model fields that have been modified by this action.
    create : bool, optional
             Whether this is a create or an update action.
    """
    message = []
    if create:
        message.append({'added': {}})
    else:
        message.append({'changed': {'fields': changed_fields}})
    return message


def create_logentry(request, obj, message, action=ADDITION):
    """
    Create a LogEntry for this action.

    Parameters
    ----------
    request : django.http.HttpRequest
              The request object for this request.
    obj : django.db.models.Model
          Model instance for being created/updated.
    message : str/list
              Message to be logged with this action.
    action : django.contrib.admin.models.(ADDITION/CHANGE/DELETE), optional
             Action.
    """
    message = json_dumps_list(message)
    return LogEntry.objects.log_action(
            user_id=request.user.pk,
            content_type_id=ContentType.objects.get_for_model(obj).pk,
            object_id=obj.pk,
            object_repr=force_text(obj),
            action_flag=action,
            change_message=message,
            )
