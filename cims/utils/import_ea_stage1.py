
def import_ea_stage1(filename, root_id=None, root_name="Stage One Competencies"):
    """Import Engineers Australia competencies from a csv file.
    
    filename: CSV file to extract data from.
    root_id: optional Taxonomy ID to attach the imported tree to.
    root_name: optional Taxonomy name to attach the imported tree to.

    csv format:
        column 1 - Competency Area
        column 2+ - Specific Competency
        
    Parser will attempt to extract an item number from the start of each
    column
    """
    import csv
    from . import models

    if root_id:
        root = models.Taxonomy.objects.get(id=root_id)
    else:
        root = models.Taxonomy.objects.get(name=root_name)

    with open(filename, 'r', newline='') as csvfile:
        eareader = csv.reader(csvfile)

        for line in eareader:
            # Check the first entry in the row is properly formatted
            if len(line) and len(line[0].split(maxsplit=1)) == 2:
                subcols = line[0].split(maxsplit=1)
                competency = models.Taxonomy(
                        short_name = subcols[0],
                        name = subcols[1],
                        parent=root,
                        )
                competency.save()
                print('Saved {} with parent {}.'.format(competency.name,
                    root.name))

                #Process remaining columns in row
                for column in line[1:]:
                    subcols = column.split(maxsplit=1)
                    # Check column is properly formatted
                    if len(subcols) == 2:
                        criteria = models.Taxonomy(
                                short_name = subcols[0],
                                name = subcols[0],
                                description=subcols[1],
                                parent=competency,
                                )
                        criteria.save()
                        print('Saved {} with parent {}.'.format(criteria.name,
                            competency.name))
                    else:
                        print('Skipped bad/blank column on line: {}'.format(
                            eareader.line_num))
            else:
                print('Skipped bad/blank line: {}'.format(eareader.line_num))

