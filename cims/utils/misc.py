"""
Miscellaneous utility functions for the CECS Course Information management
system that don't fit nicely anywhere else.
"""
from django.apps import apps
from django.db import connection, models
from django.db.models import Q
from django.db.models.functions import Lower
from itertools import chain


def get_fields_for_model(model):
    """
    Return a list of concrete, editable, not auto-created, non-GenericForeignKey fields for
    `model`.
    """
    return [
            f for f in model._meta.get_fields() if
            f.concrete
            and (
                (not f.auto_created and f.editable)
                or (f.is_relation and f.related_model is None)
                )
            ]


def instance_to_dict(instance, fields=None, exclude=None):
    """
    Returns a dict containing the data in ``instance`` suitable for passing as the argument dict to
    a CIMS manager's `validated_update` method. Pretty much identical to
    django.form.models.model_to_dict() except that we return instances instead of primary keys for
    ForeignKey fields.

    ``fields`` is an optional list of field names. If provided, only the named
    fields will be included in the returned dict.

    ``exclude`` is an optional list of field names. If provided, the named
    fields will be excluded from the returned dict, even if they are listed in
    the ``fields`` argument.
    """
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        if not getattr(f, 'editable', False):
            continue
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        # This is where we differ
        if isinstance(f, models.ForeignKey):
            data[f.name] = getattr(instance, f.name)
        else:
            data[f.name] = f.value_from_object(instance)
        # Evaluate ManyToManyField QuerySets to prevent subsequent model
        # alteration of that field from being reflected in the data.
        if isinstance(f, models.ManyToManyField):
            data[f.name] = list(data[f.name])
    return data


def nasty_search(search_string, search_vector):
    """
    A quick and dirty search for small tables until a better solution is implemented. Returns a
    Q object to be used a in an ORM `filter()` call.

    Uses `__icontains` for substring matches, and the django.contrib.postres modules
    `trigram_similar` filter to give the search some fuzziness.

    Tokenises the search string, then for each token creates an OR-ing Q object across the
    `search_vector` i.e. database fields, then AND's these objects together using each token from
    the search terms. Will scale horribly with table and field size.

    Parameters
    ----------
    search_string : str,
                    Whitespaced list of search tokens
    search_vector : list
                    List of django ORM field lookups
    """
    models.CharField.register_lookup(Lower, 'lower')
    models.TextField.register_lookup(Lower, 'lower')
    # Work out if we're using postgres and the trigram extension is available
    use_trgm = connection.vendor == 'postgresql' and apps.is_installed('django.contrib.postgres')
    if use_trgm:
        with connection.cursor() as cursor:
            cursor.execute(
                    'SELECT EXISTS(SELECT 1 FROM pg_extension WHERE extname=%s)',
                    ['pg_trgm']
                    )
            use_trgm = use_trgm and cursor.fetchone()[0]
    query_vector = search_string.split()
    q_filters = Q()
    for query_token in query_vector:
        token_filter = Q()
        for field in search_vector:
            if use_trgm:
                token_filter |= Q(
                        ('{}__lower__trigram_similar'.format(field), query_token)
                        )
            token_filter |= Q(('{}__icontains'.format(field), query_token))
        q_filters &= token_filter

    return q_filters
