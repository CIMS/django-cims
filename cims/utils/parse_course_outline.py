# Byron Vickers, Aug 2016

"""These functions are useful for extracting information from the
course outlines provided to Engineers Australia as part of the
College's 2015 accreditation process. Processing the .docx versions
of the course outlines with docx2txt will produce plain-text files
which can then be parsed using these provided functions."""


import re, os, sys, docx2txt


## utility functions

def get_between(tok1, tok2, text):
  m1 = re.search(tok1, text)
  if m1 is None:
    return None
  m2 = re.search(tok2, text[m1.end():])
  if m2 is None:
    return None
  return text[m1.end():m1.end()+m2.start()].strip()

def get_between_lines(line1, line2, lines):
    """
    Extract the lines between two specified lines.

    Extracts all lines after the first line match until the second match or the
    end of the list. 

    Positional argument:
    line1 -- start line match
    line2 -- end line(s) to match
    lines -- list of strings to search
    """
    m1 = m2 = None
    for i, line in enumerate(lines):
        if re.fullmatch(line1, line, re.I):
            m1 = i + 1
            for j, endline in enumerate(lines[i+1:]):
                if re.fullmatch(line2, endline, re.I):
                    m2 = m1 + j
                    break
            break
    if m1:
        return lines[m1:m2]
    else:
        return []

def get_lines(l1, l2, text):
  return '\n'.join( text.splitlines()[l1:l2] ).strip()

def get_line_after(tok1, lines, offset=1):
  for i, line in enumerate(lines):
    if re.search(tok1, line):
      return lines[i + offset]
  return None

def get_lines_while(tok1, whiletok, lines):
  linesout = []
  for i, line in enumerate(lines):
    if re.search(tok1, line):
      for line2 in lines[i+1:]:
        if re.search(whiletok, line2):
          linesout.append(line2)
        else:
          break
      return linesout
  return None

def _strip_inner_text(to_remove, text):
  # NOTE: This was developed for reading from the
  # PDF-derived text files. Retained in case we need
  # to go back to those.
  stripped_text = text
  m = re.search(to_remove, stripped_text)
  while m:
    stripped_text = stripped_text[:m.start()] + stripped_text[m.end():]
    m = re.search(to_remove, stripped_text)
  return stripped_text

def _remove_footers(text):
  # NOTE: This was developed for reading from the
  # PDF-derived text files. Retained in case we need
  # to go back to those.
  return strip_inner_text("\d+ | THE AUSTRALIAN NATIONAL UNIVERSITY", text)


## Get specific parts

#tested

class CourseOutlineParser():
    """
    Extract text from a MS Word formatted course outline and make it available
    for import into course info management system.

    Positional arguments:
    filename -- file to import
    """

    def __init__(self, filename):
        if not re.search(r'\.docx$', filename):
            raise RuntimeError('{} lacks docx extension, '
                    'aborting.'.format(filename))
        self.filename = filename
        text = docx2txt.process(filename)
        # get rid of extraneous white space and blank lines
        lines = [line.strip() for line in text.splitlines() if line.strip()]
        text = '\n'.join(lines)
        self.text = text
        self.lines = lines

    def get_code(self):
        return re.search("(COMP|ENGN)[^\n]*", self.text).group()
    
    def get_title(self):
        return get_line_after(self.get_code(), self.text, offset=2)
    
    def get_abstract(self):
        tok1 = self.get_title()
        tok2 = "Mode of Delivery"
        return get_between_lines(tok1, tok2, self.lines)
    
    def get_description(self):
        # NOTE: This was developed for reading from the
        # PDF-derived text files. Retained in case we need
        # to go back to those.
        tok1 = "Course Description (optional)"
        tok2 = "Learning Outcomes"
        # Check multiple possible starting tokens in order.
        # Can't check in single regex because we want the
        # shortest match (latest starting position).
        res = get_between(tok1, tok2, self.text)
        if res is None:
            tok1 = "Course Description"
            res = get_between(tok1, tok2, self.text)
        if res is None:
            tok1 = "COURSE OVERVIEW"
            res = get_between(tok1, tok2, self.text)
        return res
    
    def get_mode(self):
        return get_line_after("Mode of Delivery", self.text)
    
    def get_prerequisites(self):
        return get_line_after("Prerequisites", self.text)
    
    def get_incompatible(self):
        return get_line_after("Incompatible Courses", self.text)
    
    def get_convenor(text, use_email_fallback = True):
        convline = get_line_after("^Course Convener", self.text)
        if convline is None and use_email_fallback:
            email = get_email(text)
            if email is not None:
                return email.split("@")[0].replace("."," ").title()
        return convline
    
    def get_email(self):
        email_line = get_line_after("^Email:?", self.text)
        if email_line is not None:
            return email_line.split(",")[0].strip()
        return None
    
    def get_research_interests(self):
        tok = "^Research Interests:?$"
        interests = get_lines_while(tok, "^ ", self.text)
        if interests:
            return "\n".join(interests)
        return get_line_after(tok, self.text)
    
    def get_lecturers(self):
        return get_between("\nLecturer(\(s\))?.*\n","(Email|Phone|Tutor)", self.text) or get_line_after("Lecturer(\(s?\))?$", self.text)
    
    def get_required_resources(self):
        return get_between("Required Resources:?", "Field trips", self.text)
    
    def get_field_trips(self):
        return get_between("Field trips( \(optional\))?:?", "Additional course costs", self.text)
    
    def get_additional_course_costs(self):
        return get_line_after("Additional course costs", self.text)
    
    def get_examination_material_or_equipment(self):
        tok1 = "Examination material or equipment:?"
        tok2 = "Recommended Resources"
        return get_between(tok1, tok2, self.text)
    
    def get_recommended_resources(self):
        tok1 = "Recommended Resources:?"
        tok2 = "(COURSE SCHEDULE|\n\n|The ANU)"
        return get_between(tok1, tok2, self.text)
    
    def get_research_led_teaching(self):
        tok1 = "Research-Led Teaching"
        tok2 = "(Feedback|Learning activities)"
        return get_between(tok1, tok2, self.text)

    def get_returning_assignments(self):
        tok1 = 'returning assignments'
        tok2 = ('(resubmission of assignments'
        '|referencing requirements'
        '|support for students)')
        return get_between_lines(tok1, tok2, self.lines)

    def get_resubmission(self):
        tok1 = 'resubmission of assignments'
        tok2 = '(referencing requirements|support for students)'
        return get_between_lines(tok1, tok2, self.lines)

    def get_referencing_reqs(self):
        tok1 = 'referencing requirements'
        tok2 = 'support for students'
        return get_between_lines(tok1, tok2, self.lines)

    def get_online_submission(self):
        tok1 = 'Online Submission:?'
        tok2 = ('(Returning assignments'
                    '|Resubmission of assignments'
                    '|referencing requirements'
                    '|extensions and penalties)')
        lines = get_between_lines(tok1, tok2, self.lines)

        # Remove any Turnitin boilerplate
        for i, line in enumerate(lines):
            if re.search(r'turnitin', line, re.I):
                return lines[:i]
        return lines

    def get_hardcopy_submission(self):
        tok1 = 'Hard Copy Submission:?'
        tok2 = '(Extensions and penalties|Returning assignments)'
        return get_between_lines(tok1, tok2, self.lines)

    def get_late_submission(self):
        tok1 = 'Extensions and penalties'
        tok2 = ('(Returning assignments'
                '|Resubmission of assignments'
                '|support for students)')

        # Attempt to remove any "Students will...:" style preamble
        lines = get_between_lines(tok1, tok2, self.lines)
        if lines and re.search(r'Extensions and late submission', lines[0]):
            return lines[2:]
        return lines
    
    def get_learning_outcomes(self):
        tok1 = 'Learning outcomes'
        tok2 = '(Assessment Summary|Research-Led Teaching)'
        lines = get_between_lines(tok1, tok2, self.lines)

        # Remove any left over ordered list elements
        for i, line in enumerate(lines):
            lines[i] = re.sub(r'^[0-9]+\.?', '', line).strip()

        # Attempt to remove any "On completion...:" style preamble
        if lines and re.search(r':$', lines[0]):
            return lines[1:]
        return lines

    def get_staff_feedback(self):
        tok1 = 'Staff Feedback'
        tok2 = '(Student Feedback|Policies)'
        lines = get_between_lines(tok1, tok2, self.lines)

        # Attempt to remove any "Students will...:" style preamble
        if lines and re.search(r':$', lines[0]):
            return lines[1:]
        return lines


## testing code

if __name__ == "__main__":
    folder = sys.argv[1]
    subset_regex = '\.docx$'
    fnames = [os.path.join(folder, fname) for fname in os.listdir(folder) if re.search(subset_regex, fname)]
    for fname in fnames:
        parser = CourseOutlineParser(fname)
        print("------------", fname, "------------")
        print( '\n'.join(parser.get_learning_outcomes()) + '\n' )
