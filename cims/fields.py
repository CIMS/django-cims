"""
Fields for CIMS application.
"""
import bleach
from django.conf import settings
from django.db import models
from django.utils.safestring import mark_safe
from django.utils.timezone import now


class AutoCreatedField(models.DateTimeField):
    """DateTimeField that sets itself at object creation."""
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('editable', False)
        kwargs.setdefault('default', now)
        super().__init__(*args, **kwargs)


class AutoLastModifiedField(AutoCreatedField):
    """
    DateTimeField that sets itself at object creation, and on subsequent saves.
    """
    def pre_save(self, model_instance, add):
        value = now()
        setattr(model_instance, self.attname, value)
        return value


class BleachField(models.TextField):
    """
    Extension to `TextField` that uses the `bleach` module to strip/preserve html in the input.

    Upon retrieval the text is run through `mark_safe()` so it gets displayed properly when
    rendered.

    Parameters
    ----------
    allowed_tags : iterable optional
                   List of allowed tags.
    allowed_attributes : iterable optional`
                         Allowed list or tag attributes.
    allowed_styles : iterable optional
                     List of allowed styles
    strip_tags : bool optional
                 Whether to escape or strip tags
    strip_comments : bool optional
                     Whether to strip comments or not

    Notes
    -----
    For configuring `bleach` including defaults see [1]_.

    References
    ----------
    [1] http://bleach.readthedocs.io/en/latest/clean.html
    """
    description = 'TextField that sanitises html and then marks it safe.'

    def __init__(self, verbose_name=None, allowed_tags=None, allowed_attributes=None,
                 allowed_styles=None, strip_tags=None, strip_comments=None, *args, **kwargs):

        super(BleachField, self).__init__(verbose_name=verbose_name, *args, **kwargs)

        self.bleach_kwargs = {}
        if hasattr(settings, 'BLEACHFIELD_DEFAULTS'):
            self.bleach_kwargs.update(settings.BLEACHFIELD_DEFAULTS)

        if allowed_tags:
            self.bleach_kwargs['tags'] = allowed_tags
        if allowed_attributes:
            self.bleach_kwargs['attributes'] = allowed_attributes
        if allowed_styles:
            self.bleach_kwargs['styles'] = allowed_styles
        if strip_tags:
            self.bleach_kwargs['strip'] = strip_tags
        if strip_comments:
            self.bleach_kwargs['strip_comments'] = strip_comments

    def clean(self, value, model_instance):
        """Validate the _bleached_ input against the superclass, and return it"""
        value = bleach.clean(
            value,
            **self.bleach_kwargs
        )
        return super(BleachField, self).clean(value, model_instance)

    def from_db_value(self, value, *args):
        if value is None:
            return value
        return mark_safe(value)

    # def to_python(self, value):
    #    sval = super(BleachField, self).to_python(value)
    #    return mark_safe(bleach.linkify(sval, parse_email=True))
