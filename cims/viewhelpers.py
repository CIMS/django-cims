"""
Classes to help simplify view rendering.
"""
from django.contrib.auth import get_permission_codename
from django.db.models import Exists, OuterRef, Subquery
from django.forms import Form, ModelChoiceField, models as model_forms
from django.template.loader import get_template

from cims import models
from cims.crispy_helpers import BaseFormHelper


# Detail views related objects helper
class DetailViewTabHelper:
    """
    Collect additional context and render a template that is a block of content within an object
    detail view.

    Attributes
    ----------
    label : str
            Label to apply to the tab.
    template : str
              The path to the template to be used when rendering this set of objects.
    permission_required : list, optional
                          A list of permissions required to view the output of this helper.

    Parameters
    ----------
    parent_model : django.db.models.Model
                   The model from the view which is using this helper.
    instance : django.db.models.Model
               The model instance whose details we are viewing. Could be useful.
    """
    label = None
    permission_required = None
    template = None

    def __init__(self, parent_model, instance, request=None):
        if self.template is None:
            raise ValueError('{0} has no template specified.'.format(self.__class__.__name__))
        self.parent_model = parent_model
        self.instance = instance
        self.request = request
        self.permission_required = self.permission_required or []

    def get_context(self):
        """
        Generate any extra context required by the template.
        :return: dict
        """
        return {'tab_helper': self}

    def get_label(self):
        return self.label

    def has_permission(self, request):
        """
        Check if the request user has the permissions required to view this tab's contents.

        :param request: django.http.request.HttpRequest
        :return: bool
        """
        return request.user.has_perms(self.permission_required)

    def render(self, context):
        """
        Render this Tab's content.

        :param context: dict
        Context dict from the parent template
        :return: str
        """
        context.update(self.get_context())
        template = get_template(self.template)
        return template.render(context.flatten())


class RelatedModelTabHelper(DetailViewTabHelper):
    """
    Stores information about querysets of related objects so they can be dealt with in a somewhat
    more generic way in templates.

    Attributes
    ----------
    model : django.db.models.Model
            The model we're using objects from
    fk_name : str, optional
              The name of the ForeignKey field on our model that links it to the parent_model
    ordering : list, optional
               A list of field names to be used to order the queryset.
    base_queryset : The base queryset to use for this model.
    object_detail_template : str
                             A template for displaying the details of the related objects in a
                             collapsible div.
    linked_objects_template : str, optional
                              A template for displaying objects that are linked to the related
                              object currently being displayed e.g a small template to create a
                              list of StandardObjectives linked to the LearningOutcome that is
                              currently being rendered on the LearningOutcomes tab of a
                              CourseVersion detail page.
    linked_objects_title : str, optional
                           The title for the column displaying the linked objects.
    """
    template = 'cims/related_model_helper.html'
    model = None
    fk_name = None
    ordering = None
    base_queryset = None
    object_detail_template = None
    linked_objects_template = None
    linked_objects_title = None

    def __init__(self, parent_model, instance, request=None):
        if self.model is None:
            raise ValueError('{0} has no model class specified.'.format(self.__class__.__name__))
        if self.template is None:
            raise ValueError('{0} has no template specified.'.format(self.__class__.__name__))
        self.opts = self.model._meta
        self.label = self.model._meta.verbose_name_plural
        if not self.object_detail_template:
            self.object_detail_template = 'cims/{}_detail.html'.format(self.opts.model_name)
        super().__init__(parent_model, instance, request)

    def get_context(self):
        """Get any extra context this object needs to help render the related objects."""
        context = super().get_context()
        context['queryset'] = self.queryset
        codename = get_permission_codename('change', self.opts)
        context['update_permission'] = '{}.{}'.format(self.opts.app_label, codename)
        context['related_object_template'] = self.object_detail_template
        context['linked_objects_template'] = self.linked_objects_template
        context['linked_objects_title'] = self.linked_objects_title
        return context

    def get_queryset(self):
        """
        Attempt to determine the base queryset, then filter based in related field name and parent
        object. If wanting to do something funky like present information from the other end of a
        m2m-through-model relationship this will need to be overriden.
        """
        fk = model_forms._get_foreign_key(self.parent_model, self.model, fk_name=self.fk_name)
        queryset = self.base_queryset or self.model._default_manager.get_queryset()
        qs = queryset.filter(**{fk.name: self.instance})
        return qs

    @property
    def queryset(self):
        """
        Get the queryset of objects from this model, related to the parent model. Apply ordering if
        defined.
        """
        qs = self.get_queryset()
        if self.ordering:
            qs = qs.order_by(*self.ordering)
        return qs


class MappingTabHelper(DetailViewTabHelper):
    template = 'cims/mappings.html'
    label = 'Mappings'
    source_model = None
    source_order = None
    target_model = None
    target_order = None
    mapping_model = None

    def __init__(self, parent_model, instance, request=None):
        super().__init__(parent_model, instance, request)
        assert self.source_model and self.target_model and self.mapping_model, (
            '{} must have source, mapping and target models configured.'.format(
                    self.__class__.__name__
                    )
            )

    def create_mapping_table_row(self, source_object, mapping_values):
        """
        Create a tuple used to help render a row in a mapping table.

        Tuple format: (<row header>, <row header popover description>, row cell values)

        :param source_object: django.db.Model
        :param mapping_values: list
        :return: tuple
        """
        raise NotImplementedError(
                '{}.create_mapping_table_row() requires implementation.'.format(
                        self.__class__.__name__
                        )
                )

    def get_courseversion(self):
        """Return the CourseVersion needed to filter other querysets."""
        raise NotImplementedError(
                '{}.get_courseversion() requires implementation.'.format(
                        self.__class__.__name__
                        )
                )

    def get_source_model_filters(self):
        """Return a dict of queryset filters for our source model."""
        return {}

    def get_target_model_filters(self):
        """Return a dict of queryset filters for our target model."""
        return {}

    def get_target_model_tuples(self, targets):
        """
        Return a list of tuples for displaying the target objects in the mapping table header
        row. Tuple format: (<header cell entry>, <header cell popover text>).
        """
        raise NotImplementedError(
                '{}.get_target_model_tuples() requires implementation.'.format(
                        self.__class__.__name__
                        )
                )

    def get_context(self):
        context = super().get_context()
        source_opts = self.source_model._meta
        target_opts = self.target_model._meta
        mapping_opts = self.mapping_model._meta
        courseversion = self.get_courseversion()  # models.CourseVersion

        sources = self.source_model.objects.filter(**self.get_source_model_filters())
        if self.source_order:
            sources = sources.order_by(*self.source_order)

        targets = self.target_model.objects.filter(**self.get_target_model_filters())
        if self.target_order:
            targets = targets.order_by(*self.target_order)

        weightingschemes = courseversion.weightingschemes.order_by('name')
        context['source_model_opts'] = source_opts
        context['target_model_opts'] = target_opts
        context['target_objects'] = self.get_target_model_tuples(targets)
        context['weightingschemes'] = weightingschemes

        # Generate the lists that will be used to render the mapping tables. For each weighting
        # scheme, and two additional tables dealing with unweighted/unmapped entries, generate a
        # row for each source object.
        #
        # This could do with a little bit more optimisation to reduce the number of DB queries,
        # currently (len(weightingschemes) + 2) * len(sources) per request. This could potentially
        # be reduced to three if we can generate the tables in one go instead of by row. The last
        # unlinked table can be generated from the previous tables rather than directly from the
        # DB
        mappings = []
        for scheme in weightingschemes:
            scheme_mapping = []
            for source in sources:
                # Use a subquery to annotate the target learning outcomes with the value of
                # linked weights for the weighting scheme we're concerned with.
                weights = models.Weight.objects.filter(
                        weightingscheme=scheme,
                        **{
                            '{}__{}'.format(mapping_opts.model_name, source_opts.model_name):
                                source,
                            '{}__{}'.format(mapping_opts.model_name, target_opts.model_name):
                                OuterRef('pk')
                            }
                        )
                # If somehow we have multiple weights from the same scheme (invalid data) then
                # limit the number of returned results to the first. SQLite does this
                # automatically, other DBMS raise an exception.
                annotated_targets = targets.annotate(
                        weight=Subquery(weights.values('value')[:1])
                        )
                scheme_mapping.append(
                        self.create_mapping_table_row(
                                source,
                                annotated_targets.values_list('weight', flat=True)
                                )
                        )
            mappings.append((scheme, scheme_mapping))

        # Generate the lists for unweighted and unmapped mappings
        extra_mappings = []
        unweighted_mapping = []
        for source in sources:
            # Annotate with whether a WeightedTaskToLO exists for the this Task/LearningOutcome
            # combo that has no weights associated
            weights = self.mapping_model.objects.filter(
                    weights__isnull=True,
                    **{source_opts.model_name: source, target_opts.model_name: OuterRef('pk')}
                    )
            annotated_targets = targets.annotate(weight=Exists(weights))
            unweighted_mapping.append(
                    self.create_mapping_table_row(
                            source,
                            annotated_targets.values_list('weight', flat=True)
                            )
                    )
        extra_mappings.append(
                (
                    'unweighted',
                    'Mappings that exist but have no weight applied to them.',
                    unweighted_mapping
                    )
                )

        # Generate a table that displays whether or not the source object has any mapping links
        # in each WeightingScheme associated with the parent CourseVersion.
        unmapped_mapping = []
        for index, source in enumerate(sources):
            table_row = []
            # Use the mapping results generated above for each WeightingScheme to determine
            # whether any links exist using that WeightingScheme or not.
            for scheme, mapping in mappings:
                source_obj, description, row_values = mapping[index]
                table_row.append(not any(row_values))
            unmapped_mapping.append(
                    self.create_mapping_table_row(
                            source,
                            table_row
                            )
                    )
        extra_mappings.append(
                (
                    'unmapped',
                    '{} without any link in the indicated weighting scheme.'.format(
                            source_opts.verbose_name_plural
                            ),
                    unmapped_mapping
                    )
                )

        context['mappings'] = mappings
        context['extra_mappings'] = extra_mappings
        return context


# Concrete helpers
class CourseRelatedCVHelper(RelatedModelTabHelper):
    model = models.CourseVersion
    ordering = ['-last_modified']
    template = 'cims/course_cv_manager.html'


class CourseInstanceMappingHelper(MappingTabHelper):
    template = 'cims/mappings.html'
    label = 'Mappings'
    source_model = models.Task
    target_model = models.LearningOutcome
    mapping_model = models.WeightedTaskToLO

    def create_mapping_table_row(self, source_object, mapping_values):
        return source_object.title, source_object.description, mapping_values

    def get_courseversion(self):
        return self.instance.courseversion

    def get_source_model_filters(self):
        return {'courseinstance': self.instance}

    def get_target_model_filters(self):
        return {'courseversion': self.get_courseversion()}

    def get_target_model_tuples(self, targets):
        return [(str(target), target.outcome) for target in targets]


class CourseVersionMappingHelper(MappingTabHelper):
    template = 'cims/mappings.html'
    label = 'Mappings'
    source_model = models.LearningOutcome
    target_model = models.StandardObjective
    mapping_model = models.WeightedLOToSO

    def create_mapping_table_row(self, source_object, mapping_values):
        return str(source_object), source_object.outcome, mapping_values

    def get_courseversion(self):
        return self.instance

    def get_frameworks_form(self):
        """
        Create a simple form so the user can choose which StandardsFramework to view the
        mappings for.
        """
        class FrameworkForm(Form):
            framework = ModelChoiceField(
                    queryset=self.instance.standards_frameworks.all(),
                    required=False,
                    label='Select framework'
                    )

            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.helper = BaseFormHelper()
        return FrameworkForm

    def get_source_model_filters(self):
        return {'courseversion': self.instance}

    def get_target_model_filters(self):
        # If there are multiple standards frameworks associated with this CourseVersion either
        # select the first, or whichever the user has selected from the selection form
        filters = {'courseversion': self.get_courseversion()}
        if self.instance.standards_frameworks.count() < 2:
            return filters

        if 'framework' in self.request.GET:
            form = self.get_frameworks_form()(self.request.GET)
            if form.is_bound and form.is_valid():
                filters['standardsframework'] = form.cleaned_data['framework']
                return filters

        filters['standardsframework'] = self.instance.standards_frameworks.first()
        return filters

    def get_target_model_tuples(self, targets):
        return [(str(target), target.description) for target in targets]

    def get_context(self):
        context = super().get_context()
        # If there are multiple standards frameworks available add a flag and a form to the context
        if self.instance.standards_frameworks.count() > 1:
            context['has_multiple_frameworks'] = True
            if 'framework' in self.request.GET:
                frameworks_form = self.get_frameworks_form()(self.request.GET)
            else:
                initial = {'framework': self.instance.standards_frameworks.first().pk}
                frameworks_form = self.get_frameworks_form()(initial=initial)
            context['frameworks_form'] = frameworks_form

            if frameworks_form.is_bound and frameworks_form.is_valid():
                context['framework'] = frameworks_form.cleaned_data['framework']
            else:
                context['framework'] = self.instance.standards_frameworks.first()

        return context


class CourseInstanceRelatedScheduleHelper(RelatedModelTabHelper):
    model = models.Schedule


class CourseInstanceRelatedTaskHelper(RelatedModelTabHelper):
    model = models.Task
    linked_objects_template = 'cims/task_linked_learningoutcomes.html'
    linked_objects_title = 'Linked learning outcomes'


class CourseVersionRelatedLOHelper(RelatedModelTabHelper):
    model = models.LearningOutcome
    linked_objects_template = 'cims/learningoutcome_linked_objectives.html'
    linked_objects_title = 'Linked objectives'


class CourseVersionRelatedCIHelper(RelatedModelTabHelper):
    model = models.CourseInstance
    ordering = ['-start_date']
    template = 'cims/cv_ci_manager.html'


class LORelatedObjectiveHelper(RelatedModelTabHelper):
    model = models.StandardObjective

    def get_queryset(self):
        return self.instance.objectives.all()


class LearningOutcomeMappingHelper(MappingTabHelper):
    """Helper for rendering the mapping table for LearningOutcome -> StandardObjective mappings"""
    source_model = models.LearningOutcome
    target_model = models.StandardObjective
    mapping_model = models.WeightedLOToSO

    def create_mapping_table_row(self, source_object, mapping_values):
        return str(source_object), source_object.outcome, mapping_values

    def get_courseversion(self):
        return self.instance.courseversion

    def get_source_model_filters(self):
        return {'pk': self.instance.pk}

    def get_target_model_filters(self):
        return {'courseversion': self.instance.courseversion}

    def get_target_model_tuples(self, targets):
        return [(str(target), target.description) for target in targets]


class TaskRelatedLOHelper(RelatedModelTabHelper):
    model = models.LearningOutcome
    linked_objects_template = 'cims/learningoutcome_linked_objectives.html'
    linked_objects_title = 'Linked objectives'

    def get_queryset(self):
        return self.instance.learningoutcomes.all()


class TaskMappingHelper(MappingTabHelper):
    """Helper for rendering the mapping table for Task - > LearningOutcome mappings"""
    source_model = models.Task
    target_model = models.LearningOutcome
    mapping_model = models.WeightedTaskToLO

    def create_mapping_table_row(self, source_object, mapping_values):
        return source_object.title, source_object.description, mapping_values

    def get_courseversion(self):
        return models.CourseVersion.objects.get(pk=self.instance.courseinstance.courseversion_id)

    def get_source_model_filters(self):
        return {'pk': self.instance.pk}

    def get_target_model_filters(self):
        return {'courseversion': self.instance.courseinstance.courseversion_id}

    def get_target_model_tuples(self, targets):
        return [(str(target), target.outcome) for target in targets]
