// JS to enable widgets etc.

// Select2 for select and multiple select inputs
function applySelect2(el, style) {
  style = (typeof style !== 'undefined') ? style : 'style';
  // Avoid template forms
  var inputs = el.find('select').not("[name*='__prefix__']");
  inputs.select2({width: style});
}

// HTML5 date picker fallback to js
var hasDatePicker = (function () {
  var test = document.createElement('input');
  try {
    test.type = 'date';
  }
  catch(error){}
  return test.type !== 'text';
})();

function fallbackDatePick(el) {
  if (!hasDatePicker) {
    // Avoid template forms
    var inputs = el.find('input.dateinput').not("[name*='__prefix__']");
    inputs.datepicker({format: 'yyyy-mm-dd'});
  }
}

// HTML5 time picker fallback to js
var hasTimePicker = (function () {
  var test = document.createElement('input');
  try {
  test.type = 'time';
  }
  catch(error) {}
  return test.type !== 'text';
})();

function fallbackTimePick(el) {
  if (!hasTimePicker) {
    // Avoid template forms
    var inputs = el.find('input.timeinput').not("[name*='__prefix__']");
    inputs.timepicker({timeFormat: 'G:i'});
  }
}

$(function () {
  $('a.fa-toggle-collapse').on('click', function () {
    $(this)
      .find('[data-fa-i2svg]')
      .toggleClass('fa-minus')
      .toggleClass('fa-plus');
  });
});

// Store which tab was selected to avoid some navigation on a page refresh
$('nav#object-tabs > a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var id = $(e.target).attr('href');
  localStorage.setItem('selectedTab', id);
});
var selectedTab = localStorage.getItem('selectedTab');
if (selectedTab != null) {
  $('a[data-toggle="tab"][href="' + selectedTab + '"]').tab('show');
}
