// JS to enable widgets on form heavy pages etc.
function formsetAddRow(row) {
  applySelect2(row);
  fallbackDatePick(row);
  fallbackTimePick(row);
}

// Apply widgets and other styling changes.
$(document).ready(function() {
  var doc = $(':root');
  applySelect2(doc);
  fallbackDatePick(doc);
  fallbackTimePick(doc);

  // Resize some inputs...
  //$('div.form-group input[type=number], div.form-group input[type=date], div.form-group input[type=time]').parent().removeClass().addClass('col-2');
  
  // Reposition check boxes in tables...
  $('td[class=form-check]').children('input[type=checkbox]').removeClass('form-check-input');
  $('input[type=checkbox]').parent('td[class=form-check]').removeClass('form-check');
  
  // Remove 'required' attribute from empty template form fields so we don't break browser pre-validation
  // Formsets no longer set 'required' attribute on fields, should be able to remove this shortly
  // $('.formset-empty-form :input[required]').removeAttr('required');
});
