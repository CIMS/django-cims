"""
Templates for the CIMS application.
"""
import pdfkit
from django.template.response import TemplateResponse


class PDFTemplateResponse(TemplateResponse):
    """`TemplateResponse` that renders the response as a pdf."""
    content_type = 'application/pdf'

    def __init__(self, request, template, context=None, content_type=None, status=None,
                 charset=None, using=None, filename=None, pdfkit_opts=None):
        self.pdfkit_opts = pdfkit_opts
        if filename is not None:
                self['Content-Disposition'] = 'attachment;filename="{}"'.format(filename)
        if content_type is None:
            content_type = self.content_type
        super().__init__(request, template, context, content_type, status, charset, using)

    def render_to_pdf(self, content):
        """Take the current content and render it as a pdf."""
        return pdfkit.from_string(content, False, options=self.pdfkit_opts)

    @property
    def rendered_content(self):
        content = super().rendered_content
        return self.render_to_pdf(content)
