# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-04 02:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cims', '0026_auto_20180503_1341'),
    ]

    operations = [
        migrations.AddField(
            model_name='learningoutcome',
            name='outcome_short_form',
            field=models.CharField(blank=True, help_text='A short form of the outcome desription suitable for display as e.g. a heading in a table.', max_length=60),
        ),
    ]
