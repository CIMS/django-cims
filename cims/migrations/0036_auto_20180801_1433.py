# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-01 04:33
from __future__ import unicode_literals

from django.db import migrations


def forward_ho(apps, schema_editor):
    CourseInstance = apps.get_model('cims', 'CourseInstance')
    imports = CourseInstance.objects.filter(instance_descriptor__contains=' - Imported from')
    for ci in imports:
        ci.instance_descriptor = ci.instance_descriptor[:ci.instance_descriptor.find(
                ' - Imported from'
                )]
        ci.save()


def backwards_go(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('cims', '0035_auto_20180801_1321'),
        ]

    operations = [
        migrations.RunPython(forward_ho, backwards_go, elidable=True),
        ]
