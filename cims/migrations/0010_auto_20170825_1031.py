# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-25 00:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cims', '0009_auto_20170825_1026'),
    ]

    operations = [
        migrations.AddField(
            model_name='courseinstance',
            name='delivery_group',
            field=models.ManyToManyField(blank=True, help_text='Classes/offerings that are delivered as a group, or have dependencies with respect to delivery e.g. part of a class is actually delivered through a related class. In practice this means things like scheduling changes in one need to be checked for conflict with all the classes in this set.', related_name='_courseinstance_delivery_group_+', to='cims.CourseInstance'),
        ),
        migrations.AddField(
            model_name='courseversion',
            name='course_group',
            field=models.ManyToManyField(blank=True, help_text='Courses that must go through the approvals process together with this one. In practice this means if one Course in the set is changed all will have to be re-approved.', related_name='_courseversion_course_group_+', to='cims.CourseVersion'),
        ),
        migrations.AlterField(
            model_name='courseinstance',
            name='delivery_mode',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='cims.DeliveryMode', verbose_name='delivery mode'),
        ),
        migrations.AlterField(
            model_name='courseversion',
            name='academiccareer',
            field=models.ForeignKey(blank=True, help_text='The academic career path this course fits in e.g. undergraduate.', null=True, on_delete=django.db.models.deletion.PROTECT, to='cims.AcademicCareer', verbose_name='academic career'),
        ),
    ]
