# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-24 05:20
from __future__ import unicode_literals

import cims.fields
import cims.models
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('cims', '0007_auto_20170811_1108'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcademicCareer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', cims.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, null=True, verbose_name='created')),
                ('last_modified', cims.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='last modified')),
                ('career', models.CharField(help_text='The name of an academic career path e.g. undergraduate.', max_length=80)),
            ],
            options={
                'abstract': False,
            },
            bases=(cims.models.ValidatedModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='CourseClassification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', cims.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, null=True, verbose_name='created')),
                ('last_modified', cims.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='last modified')),
                ('classification', models.CharField(max_length=80)),
            ],
            options={
                'abstract': False,
            },
            bases=(cims.models.ValidatedModelMixin, models.Model),
        ),
        migrations.AddField(
            model_name='courseversion',
            name='unit_value',
            field=models.CharField(blank=True, help_text='The unit value or weight of this course, typically 6 or 12.', max_length=20),
        ),
        migrations.AddField(
            model_name='courseversion',
            name='academic_career',
            field=models.ForeignKey(blank=True, help_text='The academic career path this course fits in e.g. undergraduate.', null=True, on_delete=django.db.models.deletion.PROTECT, to='cims.AcademicCareer'),
        ),
        migrations.AddField(
            model_name='courseversion',
            name='classification',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='cims.CourseClassification'),
        ),
    ]
