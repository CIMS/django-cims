# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-03-14 01:17
from __future__ import unicode_literals

import cims.fields
from django.db import migrations


def forward_ho(apps, schema_editor):
    # Update CourseInstances and set workload/prescribed_texts fields equal to that
    # parent CourseVersion. Can't do this in an update() because F statements...
    CourseInstance = apps.get_model('cims', 'CourseInstance')
    for ci in CourseInstance.objects.all().select_related('courseversion'):
        ci.workload = ci.courseversion.workload
        ci.prescribed_texts = ci.courseversion.prescribed_texts
        ci.save()


def backwards_go(apps, schema_editor):
    # Go through CourseVersions, set workload and prescribed_texts to the concatenation of the
    # field values in their related CourseInstances
    CourseVersion = apps.get_model('cims', 'CourseVersion')
    for cv in CourseVersion.objects.all():
        workload = list(set(cv.courseinstance_set.values_list('workload', flat=True)))
        workload.sort()
        cv.workload = ' '.join(workload)
        prescribed_texts = list(
                set(cv.courseinstance_set.values_list('prescribed_texts', flat=True))
                )
        prescribed_texts.sort()
        cv.prescribed_texts = '; '.join(prescribed_texts)
        cv.save()


class Migration(migrations.Migration):

    dependencies = [
        ('cims', '0021_auto_20171207_1138'),
    ]

    operations = [
        migrations.AddField(
            model_name='courseinstance',
            name='indicative_reading_list',
            field=cims.fields.BleachField(blank=True),
        ),
        migrations.AddField(
            model_name='courseinstance',
            name='preliminary_reading_list',
            field=cims.fields.BleachField(blank=True),
        ),
        migrations.AddField(
            model_name='courseinstance',
            name='prescribed_texts',
            field=cims.fields.BleachField(blank=True),
        ),
        migrations.AddField(
            model_name='courseinstance',
            name='workload',
            field=cims.fields.BleachField(blank=True, verbose_name='workload statement'),
        ),
        migrations.AddField(
            model_name='courseversion',
            name='assumed_knowledge',
            field=cims.fields.BleachField(blank=True),
        ),
        migrations.RunPython(forward_ho, backwards_go, elidable=True),
        migrations.RemoveField(
            model_name='courseversion',
            name='prescribed_texts',
        ),
        migrations.RemoveField(
            model_name='courseversion',
            name='workload',
        ),
    ]
