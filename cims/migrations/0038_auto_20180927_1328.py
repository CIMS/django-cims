# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-09-27 03:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cims', '0037_auto_20180816_1336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='annotation',
            name='version',
            field=models.ForeignKey(blank=True, help_text='Version of the linked object to associate this note with.', null=True, on_delete=django.db.models.deletion.SET_NULL, to='reversion.Version'),
        ),
    ]
