#!/usr/bin/env python

import argparse
import os
import sys

import django
from django.apps import apps
from django.conf import settings
from django.test.runner import default_test_processes
from django.test.selenium import SeleniumTestCaseBase
from django.test.utils import get_runner


def get_installed():
    return [app_config.name for app_config in apps.get_app_configs()]


def setup():
    settings.INSTALLED_APPS = (
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            'django.contrib.postgres',
            'django_auth_ldap',
            'reversion',
            'perfieldperms',
            'rest_framework',
            'drf_yasg',
            'cims.auth.apps.CIMSAuthConfig',
            'cims.apps.CimsConfig',
            'django.contrib.admin',
            'cims.contrib.pandcscraper.apps.PandCScraperConfig',
            'crispy_forms',
            'tests.apps.TestsConfig',
            'tests.api.apps.TestsApiConfig',
            'tests.auth.apps.TestsAuthConfig',
            )
    settings.MIDDLEWARE = [
            'django.middleware.security.SecurityMiddleware',
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.common.CommonMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
            'django.middleware.clickjacking.XFrameOptionsMiddleware',
            ]
    settings.REST_FRAMEWORK = {
            'DEFAULT_PERMISSION_CLASSES': (
                'cims.api.permissions.DjangoObjectPermissionsOrReadOnly',
                ),
            }
    settings.ROOT_URLCONF = 'tests.urls'
    settings.TEMPLATES = [
            {
                'BACKEND': 'django.template.backends.django.DjangoTemplates',
                'DIRS': [],
                'APP_DIRS': True,
                'OPTIONS': {
                    'context_processors': [
                        'django.template.context_processors.debug',
                        'django.template.context_processors.request',
                        'django.contrib.auth.context_processors.auth',
                        'django.contrib.messages.context_processors.messages',
                        ],
                    },
                },
            ]
    settings.AUTH_USER_MODEL = 'cims_auth.User'
    settings.STATIC_URL = '/static/'

    django.setup()


class ActionSelenium(argparse.Action):
    """
    Validate the comma-separated list of requested browsers.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        browsers = values.split(',')
        for browser in browsers:
            try:
                SeleniumTestCaseBase.import_webdriver(browser)
            except ImportError:
                raise argparse.ArgumentError(self, "Selenium browser specification '%s' is not "
                                             "valid." % browser)
        setattr(namespace, self.dest, browsers)


def django_tests(verbosity, interactive, failfast, keepdb, reverse,
                 test_labels, debug_sql, parallel, tags, exclude_tags):
    # state = setup(verbosity, test_labels, parallel)
    extra_tests = []

    # Run the test suite, including the extra validation tests.
    if not hasattr(settings, 'TEST_RUNNER'):
        settings.TEST_RUNNER = 'django.test.runner.DiscoverRunner'
    TestRunner = get_runner(settings)

    test_runner = TestRunner(
        verbosity=verbosity,
        interactive=interactive,
        failfast=failfast,
        keepdb=keepdb,
        reverse=reverse,
        debug_sql=debug_sql,
        # parallel=actual_test_processes(parallel),
        tags=tags,
        exclude_tags=exclude_tags,
            )
    failures = test_runner.run_tests(
            test_labels or get_installed(),
            extra_tests=extra_tests,
            )
    # teardown(state)
    return failures


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Run the CIMS test suite.")
    parser.add_argument(
        'modules', nargs='*', metavar='module',
        help='Optional path(s) to test modules; e.g. "i18n" or '
             '"i18n.tests.TranslationTests.test_lazy_objects".',
            )
    parser.add_argument(
        '-v', '--verbosity', default=1, type=int, choices=[0, 1, 2, 3],
        help='Verbosity level; 0=minimal output, 1=normal output, 2=all output',
            )
    parser.add_argument(
        '--noinput', action='store_false', dest='interactive',
        help='Tells Django to NOT prompt the user for input of any kind.'

    )
    parser.add_argument(
        '--failfast', action='store_true', dest='failfast',
        help='Tells Django to stop running the test suite after first failed test.',
            )
    parser.add_argument(
        '-k', '--keepdb', action='store_true', dest='keepdb',
        help='Tells Django to preserve the test database between runs.',
            )
    parser.add_argument(
        '--settings',
        help='Python path to settings module, e.g. "myproject.settings". If '
             'this isn\'t provided, either the DJANGO_SETTINGS_MODULE '
             'environment variable or "test_sqlite" will be used.',
            )
    parser.add_argument(
        '--bisect',
        help='Bisect the test suite to discover a test that causes a test '
             'failure when combined with the named test.',
            )
    parser.add_argument(
        '--pair',
        help='Run the test suite in pairs with the named test to find problem pairs.',
            )
    parser.add_argument(
        '--reverse', action='store_true',
        help='Sort test suites and test cases in opposite order to debug '
             'test side effects not apparent with normal execution lineup.',
            )
    parser.add_argument(
        '--selenium', dest='selenium', action=ActionSelenium, metavar='BROWSERS',
        help='A comma-separated list of browsers to run the Selenium tests against.',
            )
    parser.add_argument(
        '--debug-sql', action='store_true', dest='debug_sql',
        help='Turn on the SQL query logger within tests.',
        )
    parser.add_argument(
        '--parallel', dest='parallel', nargs='?', default=0, type=int,
        const=default_test_processes(), metavar='N',
        help='Run tests using up to N parallel processes.',
            )
    parser.add_argument(
        '--tag', dest='tags', action='append',
        help='Run only tests with the specified tags. Can be used multiple times.',
            )
    parser.add_argument(
        '--exclude-tag', dest='exclude_tags', action='append',
        help='Do not run tests with the specified tag. Can be used multiple times.',
            )

    options = parser.parse_args()

    # Allow including a trailing slash on app_labels for tab completion convenience
    options.modules = [os.path.normpath(labels) for labels in options.modules]

    if options.settings:
        os.environ['DJANGO_SETTINGS_MODULE'] = options.settings
    else:
        if "DJANGO_SETTINGS_MODULE" not in os.environ:
            os.environ['DJANGO_SETTINGS_MODULE'] = 'tests.sqlite'
        options.settings = os.environ['DJANGO_SETTINGS_MODULE']

    if options.selenium:
        if not options.tags:
            options.tags = ['selenium']
        elif 'selenium' not in options.tags:
            options.tags.append('selenium')
        SeleniumTestCaseBase.browsers = options.selenium

    setup()

    if options.bisect:
        pass
    elif options.pair:
        pass
    else:
        failures = django_tests(
            options.verbosity, options.interactive, options.failfast,
            options.keepdb, options.reverse, options.modules,
            options.debug_sql, options.parallel, options.tags,
            options.exclude_tags,
                )
        if failures:
            sys.exit(1)
