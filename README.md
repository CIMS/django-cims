Implements the CECS Course Information Management System.

Created as a Django app so more or less drop it into your django installation.

## Requirements
* Python 3.4+
* Django 1.11 
* Postgresql

## Required Libraries
* Beautiful Soup 4 (beautifulsoup4, bs4)
* bleach
* django-auth-ldap
* django-cors-headers
* django-crispy-forms
* django-filter
* django-mptt
* django-perfieldperms
* django-reversion
* django-spaghetti-and-meatballs
* djangorestframework
* psycopg2

See requirements.txt for a list of known working package versions.

## Gitlab repository
[https://gitlab.anu.edu.au/CIMS/django-cims]

## Really quick'n'dirty setup
It's recommended that you setup a python virtualenv for this project, here's [a
link](http://docs.python-guide.org/en/latest/dev/virtualenvs/) to some info on
setting up a virtualenv.

Create a Django project, then clone the `django-cims` repository to somewhere.
Create a link to the cims python module directory inside the root of your new
Django project. CIMS isn't on PyPI so this is one way to set it up e.g.:
```
user@machine:~/git$ git clone https://gitlab.anu.edu.au/CIMS/django-cims.git
user@machine:~/git$ cd cims-test 
user@machine:~/git/cims-test$ ln -s ../django-cims/cims
```

The minimum required changes to the project `settings.py` are as follows:
```python
from django.contrib.messages import constants as message_constants
from django_auth_ldap.config import LDAPSearch, ActiveDirectoryGroupType

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',
    'corsheaders',
    'mptt',
    'reversion',
    'perfieldperms.apps.PerfieldpermsConfig',
    'cims.auth.apps.CIMSAuthConfig',
    'cims.apps.CimsConfig',
    'cims.contrib.pandcscraper.apps.PandCScraperConfig',
    'crispy-forms',
    'rest_framework',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTHENTICATION_BACKENDS = [
    'cims.auth.backends.ObjectStatusPermissionsMapBackend',
    'cims.auth.backends.CIMSBackend',
    'cims.auth.backends.ConditionalPermissionsMapBackend',
    'django_auth_ldap.backend.LDAPBackend',
]

AUTH_USER_MODEL = 'cims_auth.User'

# REST Framework
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        ),
    'DEFAULT_PERMISSION_CLASSES': (
        'cims.api.permissions.DjangoObjectPermissionsOrReadOnly',
        ),
    'DEFAULT_RENDERER_CLASSES': (
            'rest_framework.renderers.JSONRenderer',
            'cims.api.renderers.CIMSBrowsableAPIRenderer',
            )
    }


CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_CLASS_CONVERTERS = {'checkboxinput': 'form-check-input'}

MESSAGE_TAGS = {
        message_constants.DEBUG: 'alert alert-primary',
        message_constants.INFO: 'alert alert-info',
        message_constants.SUCCESS: 'alert alert-success',
        message_constants.WARNING: 'alert alert-warning',
        message_constants.ERROR: 'alert alert-danger',
        }

CORS_ORIGIN_ALLOW_ALL = True
CORS_URLS_REGEX = r'^/api/.*$'
```

A simple `urls.py` might look like:
```python
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^api/', include('cims.api.urls')),
    url(r'^pandcscraper/', include('cims.contrib.pandcscraper.urls')),
    url(r'^', include('cims.reg_urls')),
    url(r'^', include('cims.urls')),
    url(r'^admin/', admin.site.urls),
]

admin.site.site_title = 'CIMS'
admin.site.site_header = 'Course Information Management System'
```

CIMS uses its own user model so `settings.py` needs to be configured before
running the initial database migrations.
